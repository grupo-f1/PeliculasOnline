**VENTA DE PELÍCULAS DIGITALES** 

**Programación II** 

*Alumnos:* 

Eric Muñoz, Diego López, Mikel Pablos García 

**ÍNDICE:** 

1. IDEA DE PROYECTO Y ÁREA DE NEGOCIO
1. MODELO DE DATOS
1. MODELO-ENTIDAD-RELACIÓN (MER)
1. FUNCIONES DE LA APLICACIÓN





**IDEA DE PROYECTO Y ÁREA DE NEGOCIO**

*Descripción* 

Se quiere realizar una plataforma, parecida a los comienzos que tuvo Netflix. El hecho de montar un negocio que sea una página web de venta de películas digitales y que para gestionar toda la información que surge de un negocio, se necesita un gestor de contenidos, de ahí la posibilidad de crear un proyecto montando una base de datos y uniéndose a la programación para la facilitación del entorno de trabajo en un videoclub. 

Operaciones sencillas como gestionar socios de la página web junto con sus tarifas, la cantidad de películas que tiene, sus empleados, etc. 

Dado que estamos todos con pocas oportunidades de salir al exterior hemos pensado como empresa que sería una buena idea apuntar a este sector de personas que están confinadas en casa para ofrecerles una forma de entretenimiento que no involucra salir a fuera. 

**MODELO DE DATOS**

*venta online de películas digitales*

la página web tiene socios, los cuales queremos saber su identificador, nombre de usuario, contraseña, nombre, apellidos, fecha de nacimiento, fecha de tarifa (cuando se haya dado de alta como socio), email, dni y teléfono de cada socio/a, estado (dato relevante a la hora de  dar  de  baja  al socio). Cada socio tendrá asignada una tarifa, por ello guardaremos también el identificador de la tarifa. 

Cuando el socio tenga problemas o dudas al respecto del funcionamiento del programa podrá acudir a un correo el cual estará detrás uno de los encargados de recibir las consultas (el empleado tipo técnico). De las consultas queremos saber fecha, incidencia (descripción de  la  incidencia)  y  su  resolución  (descripción  de  la  resolución  de  la  incidencia).  Un empleado puede responder a muchas consultas. 

La página web tendrá diferentes tarifas, de cada tarifa queremos saber su identidad, su tipo de tarifa (básica, avanzada o premium) y el precio anual. Un usuario puede tener un solo tipo de tarifa a la vez. 

El tipo de tarifa depende de: 

- Básica: Resolución de peliculas no mayor que 1080p 
- Avanzada: Resolución máxima 
- Premium: Desbloqueo de películas premium. 

La empresa tiene empleados, de ellos queremos guardar su id, su nombre de usuario, su contraseña, id de qué tipo de empleado es, nombre, apellidos, fecha de nacimiento, DNI, email, teléfono, fecha de dado de alta.  

El tipo de empleado va a contener un identificador, un nombre( que va a depender de si es gestor de recursos, administrador, técnico) y una descripción de su puesto de trabajo.  

Aquí se definen los diferentes tipos de roles de la aplicación: 

- El gestor gestiona las compras de películas.  
- El administrador es el que maneja la base de datos. 
- El técnico es el que se encarga de las consultas. 

Un  socio  puede  comprar  varias  películas.  De  las  películas  queremos  saber  el  id,  el proveedor que pone a disposición esa película, el empleado asignado a esa película y que es el encargado de comprar esa película, id del tipo de película, clasificación, duración, idioma, subtítulos, resolución de la pelicula, título de ella, la portada y una breve descripción de la película. 

De los tipos de película tendremos a nuestra disposición un identificador, un nombre para el tipo o género de película y una descripción de esta. 

Cada película tiene un proveedor que es de quien se compra la licencia de la película que se  vende,  dando  así  derecho  a  venderla.  De  cada  proveedor  queremos  conocer  su identificador, nombre, email y teléfono para poder contactar con el. 

**MODELO-ENTIDAD-RELACIÓN (MER)** 

![alt text](Modelo%20entidad-relacion.PNG)

**FUNCIONES DE LA APLICACIÓN**

- Login diferenciado de los socios y de los empleados 
- Los socios harán las siguientes funciones: 
  - Registrarse 
  - Elegir la tarifa (la tarifa Premium solo está disponible para mayores de 18 años.) 
  - Modificar su perfil 
  - elegir una película si tiene una tarifa elegida. 
  - Consultar dudas, abriendo una consulta de soporte 
  - Darse de baja 
  - Si la recomendación de la película es mayor a la edad de dicho usuario, no podrá descargarse la película hasta tener la edad recomendada. 
  - El socio puede pagar su tarifa anual. 
- Los empleados harán las siguientes funciones: 
  - Administradores 
- Puede dar de alta y eliminar a otros administradores y otros usuarios. 
  - Puede consultar información de socios (excepto contraseñas que estarán encriptadas), empleados, consultas de soporte, tickets, proveedores, películas y tarifas. 
  - Puede eliminar, modificar, insertar y mostrar todas las tarifas del videoclub. 
  - Cambiar cualquier dato pertinente a la base de datos. 
  - Mantener la base de datos segura y actualizada. 
- Técnico 
  - Marca las consultas que llegan desde las consultas de soporte dando una resolución, el técnico puede ver los datos del socio para ponerse en contacto con el socio y responder a la consulta. 
- Gestor 
  - Encargado de la compra de películas al proveedor. 
  - Puede listar, añadir, borrar o modificar un proveedor. 
  - Se encarga de hacer estadísticas con las películas compradas. 
