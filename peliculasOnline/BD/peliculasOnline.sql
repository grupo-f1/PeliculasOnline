-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema PeliculasOnline
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `PeliculasOnline` ;

-- -----------------------------------------------------
-- Schema PeliculasOnline
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `PeliculasOnline` DEFAULT CHARACTER SET utf8 ;
USE `PeliculasOnline` ;

-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Tarifa`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Tarifa` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Tarifa` (
  `idTarifa` INT NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(100) NOT NULL,
  `precio` INT NOT NULL,
  PRIMARY KEY (`idTarifa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Socio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Socio` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Socio` (
  `idSocio` INT NOT NULL AUTO_INCREMENT,
  `tarifa_FK` INT NOT NULL,
  `user_name` VARCHAR(16) NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `apellidos` VARCHAR(100) NOT NULL,
  `fecha_nacimiento` DATE NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `telefono` VARCHAR(9) NULL,
  `dni` VARCHAR(9) NOT NULL,
  `estado` TINYINT NOT NULL,
  `fecha_tarifa` DATE NOT NULL,
  `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idSocio`),
  INDEX `fk_Socio_Tarifa1_idx` (`tarifa_FK` ASC) VISIBLE,
  UNIQUE INDEX `Telefono_UNIQUE` (`telefono` ASC) VISIBLE,
  UNIQUE INDEX `User_name_UNIQUE` (`user_name` ASC) VISIBLE,
  CONSTRAINT `fk_Socio_Tarifa1`
    FOREIGN KEY (`tarifa_FK`)
    REFERENCES `PeliculasOnline`.`Tarifa` (`idTarifa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Tipo_empleado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Tipo_empleado` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Tipo_empleado` (
  `idTipo_empleado` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `descripcion` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`idTipo_empleado`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Empleado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Empleado` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Empleado` (
  `idEmpleado` INT NOT NULL AUTO_INCREMENT,
  `tipo_empleado_FK` INT NOT NULL,
  `user_name` VARCHAR(16) NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `apellidos` VARCHAR(200) NOT NULL,
  `fecha_nacimiento` DATE NOT NULL,
  `dni` VARCHAR(9) NOT NULL,
  `telefono` VARCHAR(9) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idEmpleado`),
  UNIQUE INDEX `DNI_UNIQUE` (`dni` ASC) VISIBLE,
  UNIQUE INDEX `Telefono_UNIQUE` (`telefono` ASC) VISIBLE,
  UNIQUE INDEX `Username_UNIQUE` (`user_name` ASC) VISIBLE,
  INDEX `fk_Empleado_tipo_empleado1_idx` (`tipo_empleado_FK` ASC) VISIBLE,
  CONSTRAINT `fk_Empleado_tipo_empleado1`
    FOREIGN KEY (`tipo_empleado_FK`)
    REFERENCES `PeliculasOnline`.`Tipo_empleado` (`idTipo_empleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Proveedor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Proveedor` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Proveedor` (
  `idProveedor` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `telefono` VARCHAR(9) NOT NULL,
  PRIMARY KEY (`idProveedor`),
  UNIQUE INDEX `Nombre_UNIQUE` (`nombre` ASC) VISIBLE,
  UNIQUE INDEX `Email_UNIQUE` (`email` ASC) VISIBLE,
  UNIQUE INDEX `Telefono_UNIQUE` (`telefono` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Pelicula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Pelicula` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Pelicula` (
  `idPelicula` INT NOT NULL AUTO_INCREMENT,
  `empleado_FK` INT ,
  `proveedor_FK` INT NOT NULL,
  `clasificacion` VARCHAR(3) NOT NULL,
  `duracion` INT NOT NULL,
  `idiomas` VARCHAR(200) NOT NULL,
  `subtitulos` TINYINT NOT NULL,
  `resolucion` VARCHAR(100) NOT NULL,
  `titulo` VARCHAR(100) NOT NULL,
  `portada` VARCHAR(200),
  `descripcion` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`idPelicula`),
  INDEX `fk_Pelicula_Proveedor1_idx` (`proveedor_FK` ASC) VISIBLE,
  INDEX `fk_Pelicula_Empleado1_idx` (`empleado_FK` ASC) VISIBLE,
  CONSTRAINT `fk_Pelicula_Proveedor1`
    FOREIGN KEY (`proveedor_FK`)
    REFERENCES `PeliculasOnline`.`Proveedor` (`idProveedor`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Pelicula_Empleado1`
    FOREIGN KEY (`empleado_FK`)
    REFERENCES `PeliculasOnline`.`Empleado` (`idEmpleado`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Consulta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Consulta` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Consulta` (
  `socio_PK` INT NOT NULL,
  `empleado_PK` INT NOT NULL,
  `fecha` DATE NOT NULL,
  `incidencia` VARCHAR(1000) NOT NULL,
  `resolucion` TINYINT NOT NULL,
  PRIMARY KEY (`socio_PK`, `empleado_PK`, `fecha`),
  INDEX `fk_Socio_has_Empleado_Empleado1_idx` (`empleado_PK` ASC) VISIBLE,
  INDEX `fk_Consulta_Socio1_idx` (`socio_PK` ASC) VISIBLE,
  CONSTRAINT `fk_Socio_has_Empleado_Empleado1`
    FOREIGN KEY (`empleado_PK`)
    REFERENCES `PeliculasOnline`.`Empleado` (`idEmpleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Consulta_Socio1`
    FOREIGN KEY (`socio_PK`)
    REFERENCES `PeliculasOnline`.`Socio` (`idSocio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Tipo_pelicula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Tipo_pelicula` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Tipo_pelicula` (
  `idTipo_pelicula` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `descripcion` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`idTipo_pelicula`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Pelicula_tipos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Pelicula_tipos` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Pelicula_tipos` (
  `pelicula_FK` INT NOT NULL,
  `tipo_pelicula_FK` INT NOT NULL,
  PRIMARY KEY (`pelicula_FK`, `tipo_pelicula_FK`),
  INDEX `fk_Pelicula_has_Tipo_pelicula_Tipo_pelicula1_idx` (`tipo_pelicula_FK` ASC) VISIBLE,
  INDEX `fk_Pelicula_has_Tipo_pelicula_Pelicula1_idx` (`pelicula_FK` ASC) VISIBLE,
  CONSTRAINT `fk_Pelicula_has_Tipo_pelicula_Pelicula1`
    FOREIGN KEY (`pelicula_FK`)
    REFERENCES `PeliculasOnline`.`Pelicula` (`idPelicula`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Pelicula_has_Tipo_pelicula_Tipo_pelicula1`
    FOREIGN KEY (`tipo_pelicula_FK`)
    REFERENCES `PeliculasOnline`.`Tipo_pelicula` (`idTipo_pelicula`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Ticket`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Ticket` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Ticket` (
  `socio_FK` INT NOT NULL,
  `pelicula_FK` INT NOT NULL,
  `fecha_compra` DATE NOT NULL,
  PRIMARY KEY (`socio_FK`, `pelicula_FK`, `fecha_compra`),
  INDEX `fk_Socio_has_Pelicula_Pelicula1_idx` (`pelicula_FK` ASC) VISIBLE,
  INDEX `fk_Socio_has_Pelicula_Socio1_idx` (`socio_FK` ASC) VISIBLE,
  CONSTRAINT `fk_Socio_has_Pelicula_Socio1`
    FOREIGN KEY (`socio_FK`)
    REFERENCES `PeliculasOnline`.`Socio` (`idSocio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Socio_has_Pelicula_Pelicula1`
    FOREIGN KEY (`pelicula_FK`)
    REFERENCES `PeliculasOnline`.`Pelicula` (`idPelicula`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `PeliculasOnline`.`Evento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PeliculasOnline`.`Oscar` ;

CREATE TABLE IF NOT EXISTS `PeliculasOnline`.`Oscar` (
  `idOscar` INT NOT NULL AUTO_INCREMENT,
  `pelicula` VARCHAR(100) NOT NULL,
  `descripcion` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`idOscar`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
