/*TARIFAS*/
INSERT INTO `peliculasonline`.`tarifa`
(`tipo`,`precio`) VALUES
("basica",50),
("avanzada",75),
("premium",100);

/*SOCIOS*/
INSERT INTO `peliculasonline`.`socio`
(`tarifa_FK`,`user_name`,`password`,`nombre`,`apellidos`,`fecha_nacimiento`,`email`,`telefono`,`dni`,`estado`,`fecha_tarifa`,`create_time`) VALUES
(1,"socio1","pass1","nombre1","apellido1","2000-12-12","email1@gmail.com","987654321","12345678Q",true,"2000-12-12",CURRENT_TIMESTAMP),
(2,"socio2","pass2","nombre2","apellido2","2000-12-12","email2@gmail.com","987652321","12545678Q",true,"2000-12-12",CURRENT_TIMESTAMP),
(3,"socio3","pass3","nombre3","apellido2","2000-12-12","email2@gmail.com","987655321","12342678Q",true,"2000-12-12",CURRENT_TIMESTAMP);

/*TIPO EMPLEADOS*/
INSERT INTO `peliculasonline`.`tipo_empleado`
(`nombre`,`descripcion`) VALUES
("Administrador","Encargado de gestionar la base de datos, actualizarla y mantenerla segura"),
("Gestor","Encargado de comprar peliculas a los proveedores"),
("Tecnico","Encargado de gestionar las consultas de los socios");

/*EMPLEADOS*/
INSERT INTO `peliculasonline`.`empleado`
(`tipo_empleado_FK`,`user_name`,`password`,`nombre`,`apellidos`,`fecha_nacimiento`,`dni`,`telefono`,`email`,`create_time`) VALUES
(1,"empleado1","pass1","nombre1","apellido1","2000-12-12","12142678Q","967655321","emailEmpleado1@gmail.com",CURRENT_TIMESTAMP),
(2,"empleado2","pass2","nombre2","apellido2","2000-12-12","12142668Q","967355321","emailEmpleado2@gmail.com",CURRENT_TIMESTAMP),
(3,"empleado3","pass3","nombre3","apellido3","2000-12-12","12142638Q","967455321","emailEmpleado3@gmail.com",CURRENT_TIMESTAMP);

/*PROVEEDORES*/
INSERT INTO `peliculasonline`.`proveedor`
(`nombre`,`email`,`telefono`) VALUES
("proveedor1","emailProveedro1@gmail.es","865748594"),
("proveedor2","emailProveedro2@gmail.es","863748594"),
("proveedor3","emailProveedro3@gmail.es","864748594"),
("proveedor4","emailProveedro4@gmail.es","867748594"),
("proveedor5","emailProveedro5@gmail.es","868748594");

/*TIPO PELICULAS*/
INSERT INTO `peliculasonline`.`tipo_pelicula`
(`nombre`,`descripcion`) VALUES
("accion", "peleas, golpes y mucho movimiento"),
("aventura", "descubrimiento de lo nuevo"),
("drama", "relaciones sentimentales"),
("terror", "miedo fuerte"),
("ciencia ficcion", "tecnologia futurista"),
("animacion", "dibujos");

/*PELICULAS*/
INSERT INTO `peliculasonline`.`pelicula`
(`empleado_FK`,`proveedor_FK`,`clasificacion`,`duracion`,`idiomas`,`subtitulos`,`resolucion`,`titulo`,`portada`,`descripcion`) VALUES
(2,1,"+18",123,"ingles,aleman,polaco,hungaro,español",true,"4k","He Man",null,"descipcion1"),
(2,2,"+7",453,"ingles,aleman,polaco,hungaro,español",false,"2k","He Man",null,"descipcion2"),
(2,3,"+16",123,"ingles,aleman,polaco,hungaro,español",true,"4k","Avatar",null,"descipcion3"),
(2,4,"+3",125,"ingles,aleman,polaco,hungaro,español",false,"1080","Alien vs Predator",null,"descipcion4"),
(2,5,"+12",223,"ingles,aleman,polaco,hungaro,español",true,"4k","Vengadores",null,"descipcion5"),
(2,1,"+7",300,"ingles,aleman,polaco,hungaro,español",false,"1080","Superman",null,"descipcion6"),
(2,3,"+3",287,"ingles,aleman,polaco,hungaro,español",true,"4k","Batman",null,"descipcion7");

/*PELICULA TIPOS*/
INSERT INTO `peliculasonline`.`pelicula_tipos`
(`pelicula_FK`,`tipo_pelicula_FK`) VALUES
(1,1),
(1,2),
(2,6),
(2,1),
(3,4),
(4,2),
(4,3),
(5,2),
(5,3),
(6,4),
(6,3),
(7,4),
(7,1),
(7,5);

/*CONSULTAS*/
INSERT INTO `peliculasonline`.`consulta`
(`socio_PK`,`empleado_PK`,`fecha`,`incidencia`,`resolucion`) VALUES
(1,3,"2000-12-20","incidencia1",true),
(2,3,"2000-12-21","incidencia2",true),
(3,3,"2000-10-12","incidencia3",true),
(2,3,"2000-11-12","incidencia4",false),
(1,3,"2000-09-12","incidencia5",true),
(1,3,"2000-08-12","incidencia6",false);

/*TICKETS*/
INSERT INTO `peliculasonline`.`ticket`(`socio_FK`,`pelicula_FK`,`fecha_compra`) VALUES
(1,2,"2012-12-12"),
(3,1,"2012-11-12"),
(3,4,"2012-10-12"),
(2,6,"2012-09-12"),
(2,3,"2012-08-12"),
(1,5,"2012-07-12");