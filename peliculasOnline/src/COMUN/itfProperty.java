package COMUN;

/**
 * Interfaz que se usara como plantilla para los metodos relaiconados con el
 * envio de datos a la logica de presentacion
 * 
 * @author Eric
 *
 */
public interface itfProperty {
	/**
	 * Funcion que sirve para sacar cualquier tipo de dato de las clases en las que
	 * tenga una interfaz y de la propipedad expecificada
	 * 
	 * @param propiedad atributo del objeto que buscamos
	 * @return el valor del atributo a buscar
	 */
	public Object getProperty(String propiedad);
}
