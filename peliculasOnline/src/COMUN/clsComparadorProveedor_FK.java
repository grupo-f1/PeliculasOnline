package COMUN;

import java.util.Comparator;

import LN.clsPelicula;

/**
 * clase para implementar comparador de proveedores por id en pelicula
 * 
 * @author Eric
 *
 */
public class clsComparadorProveedor_FK implements Comparator<clsPelicula> {

	@Override
	public int compare(clsPelicula o1, clsPelicula o2) {

		return ((Integer) o1.getProveedor_FK()).compareTo(o2.getProveedor_FK());

	}
}
