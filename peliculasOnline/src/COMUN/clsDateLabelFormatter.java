package COMUN;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JFormattedTextField.AbstractFormatter;

/**
 * clase abstracta que encargada de formatear la fecha para el date picker
 * 
 * @author Eric
 *
 */
public class clsDateLabelFormatter extends AbstractFormatter {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5714328064393420359L;
	/**
	 * patron de fecha a usar
	 */
	private String datePattern = "yyyy-MM-dd";
	/**
	 * tipo de formato a usar
	 */
	private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

	@Override
	public Object stringToValue(String text) throws ParseException {
		return dateFormatter.parseObject(text);
	}

	@Override
	public String valueToString(Object value) throws ParseException {
		if (value != null) {
			Calendar cal = (Calendar) value;
			return dateFormatter.format(cal.getTime());
		}

		return "";
	}

}