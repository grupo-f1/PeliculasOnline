package COMUN;

/**
 * Clase donde guardaremos todas las constantes referenteS a las sentencias de
 * la base de datos y la posicion de esa columna en la sentencia
 * 
 * @author Eric y Mikel
 *
 */
public class clsConstantesDB {
	/**
	 * sentencia insert de todos los apartados de tarifa
	 */
	public static final String SQL_INSERT_TARIFA = "INSERT INTO `peliculasonline`.`tarifa`(`tipo`,`precio`)VALUES(?,?);";
	/**
	 * sentencia insert de todos los apartados de socio
	 */
	public static final String SQL_INSERT_SOCIO = "INSERT INTO `peliculasonline`.`socio`(`tarifa_FK`,`user_name`,`password`,`nombre`,`apellidos`,`fecha_nacimiento`,`email`,`telefono`,`dni`,`estado`,`fecha_tarifa`,`create_time`)VALUES(?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP);";
	/**
	 * sentencia insert de todos los apartados de tipo empleado
	 */
	public static final String SQL_INSERT_TIPO_EMPLEADO = "INSERT INTO `peliculasonline`.`tipo_empleado`(`nombre`,`descripcion`)VALUES(?,?);";
	/**
	 * sentencia insert de todos los apartados de empleado
	 */
	public static final String SQL_INSERT_EMPLEADO = "INSERT INTO `peliculasonline`.`empleado`(`tipo_empleado_FK`,`user_name`,`password`,`nombre`,`apellidos`,`fecha_nacimiento`,`dni`,`telefono`,`email`,`create_time`)VALUES(?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP);";
	/**
	 * sentencia insert de todos los apartados de proveedor
	 */
	public static final String SQL_INSERT_PROVEEDOR = "INSERT INTO `peliculasonline`.`proveedor`(`nombre`,`email`,`telefono`)VALUES(?,?,?);";
	/**
	 * sentencia insert de todos los apartados de tipo pelicula
	 */
	public static final String SQL_INSERT_TIPO_PELICULA = "INSERT INTO `peliculasonline`.`tipo_pelicula`(`nombre`,`descripcion`)VALUES(?,?);";
	/**
	 * sentencia insert de todos los apartados de pelicula
	 */
	public static final String SQL_INSERT_PELICULA = "INSERT INTO `peliculasonline`.`pelicula`(`empleado_FK`,`proveedor_FK`,`clasificacion`,`duracion`,`idiomas`,`subtitulos`,`resolucion`,`titulo`,`portada`,`descripcion`)VALUES(?,?,?,?,?,?,?,?,?,?);";
	/**
	 * sentencia insert de todos los apartados de peliucula tipos
	 */
	public static final String SQL_INSERT_PELICULA_TIPOS = "INSERT INTO `peliculasonline`.`pelicula_tipos`(`pelicula_FK`,`tipo_pelicula_FK`)VALUES(?,?);";
	/**
	 * sentencia insert de todos los apartados de consulta
	 */
	public static final String SQL_INSERT_CONSULTA = "INSERT INTO `peliculasonline`.`consulta`(`socio_PK`,`empleado_PK`,`fecha`,`incidencia`,`resolucion`)VALUES(?,?,?,?,?);";
	/**
	 * sentencia insert de todos los apartados de ticket
	 */
	public static final String SQL_INSERT_TICKET = "INSERT INTO `peliculasonline`.`ticket`(`socio_FK`,`pelicula_FK`,`fecha_compra`)VALUES(?,?,?);";
	/**
	 * sentencia insert de todos los apartados de oscar para el hilo
	 */
	public static final String SQL_INSERT_OSCAR = "INSERT INTO `peliculasonline`.`oscar`(`pelicula`,`descripcion`) VALUES(?,?);";

	/**
	 * sentencia select de todas las tuplas de tarifas
	 */
	public static final String SQL_SELECT_TARIFAS = "SELECT`tarifa`.`tipo`,`tarifa`.`precio`, `tarifa`.`idTarifa`FROM `peliculasonline`.`tarifa`;";
	/**
	 * sentencia select de todas las tuplas de socios
	 */
	public static final String SQL_SELECT_SOCIOS = "SELECT `socio`.`tarifa_FK`,`socio`.`user_name`,`socio`.`password`,`socio`.`nombre`,`socio`.`apellidos`,`socio`.`fecha_nacimiento`,`socio`.`email`,`socio`.`telefono`,`socio`.`dni`,`socio`.`estado`,`socio`.`fecha_tarifa`,`socio`.`idSocio`FROM `peliculasonline`.`socio`;";
	/**
	 * sentencia select de todas las tuplas de tipo empleados
	 */
	public static final String SQL_SELECT_TIPO_EMPLEADOS = "SELECT `tipo_empleado`.`nombre`,`tipo_empleado`.`descripcion`,`tipo_empleado`.`idTipo_empleado`FROM `peliculasonline`.`tipo_empleado`;";
	/**
	 * sentencia select de todas las tuplas de empleados
	 */
	public static final String SQL_SELECT_EMPLEADOS = "SELECT `empleado`.`tipo_empleado_FK`,`empleado`.`user_name`,`empleado`.`password`,`empleado`.`nombre`,`empleado`.`apellidos`,`empleado`.`fecha_nacimiento`,`empleado`.`dni`,`empleado`.`telefono`,`empleado`.`email`,`empleado`.`idEmpleado`FROM `peliculasonline`.`empleado`;";
	/**
	 * sentencia select de todas las tuplas de provedores
	 */
	public static final String SQL_SELECT_PROVEEDORES = "SELECT `proveedor`.`nombre`,`proveedor`.`email`,`proveedor`.`telefono`,`proveedor`.`idProveedor`FROM `peliculasonline`.`proveedor`;";
	/**
	 * sentencia select de las tuplas de provedores en base a su email
	 */
	public static final String SQL_SELECT_PROVEEDOR_EMAIL = "SELECT `proveedor`.`nombre`,`proveedor`.`email`,`proveedor`.`telefono`,`proveedor`.`idProveedor`FROM `peliculasonline`.`proveedor` WHERE `proveedor`.`email`=?;";
	/**
	 * sentencia select de las tuplas de provedores en base a su nombre
	 */
	public static final String SQL_SELECT_PROVEEDOR_NOMBRE = "SELECT `proveedor`.`nombre`,`proveedor`.`email`,`proveedor`.`telefono`,`proveedor`.`idProveedor`FROM `peliculasonline`.`proveedor` WHERE `proveedor`.`nombre`=?;";
	/**
	 * sentencia select de las tuplas de provedores en base a su id
	 */
	public static final String SQL_SELECT_PROVEEDOR_ID = "SELECT `proveedor`.`nombre`,`proveedor`.`email`,`proveedor`.`telefono`,`proveedor`.`idProveedor`FROM `peliculasonline`.`proveedor` WHERE `proveedor`.`idProveedor`=?;";
	/**
	 * sentencia select de la tupla de proveedor en base a su nombre y titulo de la
	 * pelicula a buscar
	 */
	public static final String SQL_SELECT_PROVEEDOR_NOMBRE_PELICULA_TITULO = "SELECT `proveedor`.`nombre`,`proveedor`.`email`,`proveedor`.`telefono`,`proveedor`.`idProveedor` FROM `peliculasonline`.`proveedor`,`peliculasonline`.`pelicula` where `proveedor`.`idProveedor`=`pelicula`.`proveedor_FK` and `proveedor`.`nombre`=? and `pelicula`.`titulo`=?;";
	/**
	 * sentencia select de todas las tuplas de tipo peliculas
	 */
	public static final String SQL_SELECT_TIPO_PELICULAS = "SELECT `tipo_pelicula`.`nombre`,`tipo_pelicula`.`descripcion`,`tipo_pelicula`.`idTipo_pelicula`FROM `peliculasonline`.`tipo_pelicula`;";
	/**
	 * sentencia select de todas las tuplas de peliculas
	 */
	public static final String SQL_SELECT_PELICULAS = "SELECT `pelicula`.`empleado_FK`,`pelicula`.`proveedor_FK`,`pelicula`.`clasificacion`,`pelicula`.`duracion`,`pelicula`.`idiomas`,`pelicula`.`subtitulos`,`pelicula`.`resolucion`,`pelicula`.`titulo`,`pelicula`.`portada`,`pelicula`.`descripcion`,`pelicula`.`idPelicula`FROM `peliculasonline`.`pelicula`;";
	/**
	 * sentencia select la pelicula con id
	 */
	public static final String SQL_SELECT_PELICULA_ID = "SELECT `pelicula`.`empleado_FK`,`pelicula`.`proveedor_FK`,`pelicula`.`clasificacion`,`pelicula`.`duracion`,`pelicula`.`idiomas`,`pelicula`.`subtitulos`,`pelicula`.`resolucion`,`pelicula`.`titulo`,`pelicula`.`portada`,`pelicula`.`descripcion`,`pelicula`.`idPelicula` FROM `peliculasonline`.`pelicula` WHERE `pelicula`.`idPelicula`=?;";
	/**
	 * sentencia select de todas las tuplas de peliculas tipos
	 */
	public static final String SQL_SELECT_PELICULAS_TIPOS = "SELECT `pelicula_tipos`.`pelicula_FK`,`pelicula_tipos`.`tipo_pelicula_FK`FROM `peliculasonline`.`pelicula_tipos`;";
	/**
	 * sentencia select de todas las tuplas de consultas
	 */
	public static final String SQL_SELECT_CONSULTAS = "SELECT `consulta`.`socio_PK`,`consulta`.`empleado_PK`,`consulta`.`fecha`,`consulta`.`incidencia`,`consulta`.`resolucion`FROM `peliculasonline`.`consulta`;";
	/**
	 * sentencia select de todas las tuplas de tickets
	 */
	public static final String SQL_SELECT_TICKETS = "SELECT `ticket`.`socio_FK`,`ticket`.`pelicula_FK`,`ticket`.`fecha_compra`FROM `peliculasonline`.`ticket`;";
	/**
	 * sentencia select de todos los socio que tiene un ticket
	 */
	public static final String SQL_SELECT_SOCIOS_CON_TICKET = "SELECT distinct `socio`.`tarifa_FK`,`socio`.`user_name`,`socio`.`password`,`socio`.`nombre`,`socio`.`apellidos`,`socio`.`fecha_nacimiento`,`socio`.`email`,`socio`.`telefono`,`socio`.`dni`,`socio`.`estado`,`socio`.`fecha_tarifa`,`socio`.`idSocio` FROM `peliculasonline`.`socio`,`peliculasonline`.`ticket` WHERE `socio`.`idSocio`=`ticket`.`socio_FK`;";
	/**
	 * sentencia select de todas las peliculas de un socio con ticket
	 */
	public static final String SQL_SELECT_PELICULAS_CON_TICKET_DE_SOCIO = "SELECT distinct `pelicula`.`empleado_FK`,`pelicula`.`proveedor_FK`,`pelicula`.`clasificacion`,`pelicula`.`duracion`,`pelicula`.`idiomas`,`pelicula`.`subtitulos`,`pelicula`.`resolucion`,`pelicula`.`titulo`,`pelicula`.`portada`,`pelicula`.`descripcion`,`pelicula`.`idPelicula`FROM `peliculasonline`.`pelicula`,`peliculasonline`.`socio`,`peliculasonline`.`ticket` where `pelicula`.`idPelicula`= `ticket`.`pelicula_FK` and  `ticket`.`socio_FK`= `socio`.`idSocio` and `socio`.`idSocio`=?;";
	/*
	 * sentencia select de los ticket de un socio de esa pelicula
	 */
	public static final String SQL_SELECT_TICKET_SOCIO_PELICULA = "SELECT `ticket`.`socio_FK`,`ticket`.`pelicula_FK`,`ticket`.`fecha_compra` FROM `peliculasonline`.`pelicula`,`peliculasonline`.`socio`,`peliculasonline`.`ticket` where `pelicula`.`idPelicula`= `ticket`.`pelicula_FK` and  `ticket`.`socio_FK`= `socio`.`idSocio` and `socio`.`idSocio`=? and `pelicula`.`idPelicula`=?;";
	/**
	 * sentencia select de todos los oscar
	 */
	public static final String SQL_SELECT_OSCARS = "SELECT `oscar`.`pelicula`,`oscar`.`descripcion`, `oscar`.`idOscar` FROM `peliculasonline`.`oscar` order by idOscar desc;";

	/**
	 * sentencia update de modificacionde todos los apartados de ticket que
	 * coincidan con su primary key
	 */
	public static final String SQL_UPDATE_TARIFA = "UPDATE `peliculasonline`.`tarifa`SET`tipo` = ?,`precio` = ?,`idTarifa` = ? WHERE `idTarifa` = ?;";
	/**
	 * sentencia update de modificacionde todos los apartados de socio que coincidan
	 * con su primary key
	 */
	public static final String SQL_UPDATE_SOCIO = "UPDATE peliculasonline.socio SET tarifa_FK = ?, user_name = ?, password = ?, nombre = ?, apellidos = ?, fecha_nacimiento = ?, email = ?, telefono = ?, dni = ?, estado = ?, fecha_tarifa = ? WHERE idSocio = ? ;";
	/**
	 * sentencia update de modificacionde todos los apartados de socio que coincidan
	 * con su primary key
	 */
	public static final String SQL_UPDATE_SOCIO_ESTADO = "UPDATE peliculasonline.socio SET estado = ? WHERE idSocio = ?;";
	/**
	 * sentencia update de modificacionde todos los apartados de tipo empleado que
	 * coincidan con su primary key
	 */
	public static final String SQL_UPDATE_TIPO_EMPLEADO = "UPDATE `peliculasonline`.`tipo_empleado`SET`nombre` = ?,`descripcion` = ?,`idTipo_empleado` = ? WHERE `idTipo_empleado` = ?;";
	/**
	 * sentencia update de modificacionde todos los apartados de empleado que
	 * coincidan con su primary key
	 */
	public static final String SQL_UPDATE_EMPLEADO = "UPDATE `peliculasonline`.`empleado`SET`tipo_empleado_FK` = ?,`user_name` = ?,`password` = ?,`nombre` = ?,`apellidos` = ?,`fecha_nacimiento` = ?,`dni` = ?,`telefono` = ?,`email` = ?,`idEmpleado` = ? WHERE `idEmpleado` = ?;";
	/**
	 * sentencia update de modificacionde todos los apartados de proveedor que
	 * coincidan con su primary key
	 */
	public static final String SQL_UPDATE_PROVEEDOR = "UPDATE `peliculasonline`.`proveedor`SET`nombre` = ?,`email` = ?,`telefono` = ?,`idProveedor` = ? WHERE `idProveedor` = ?;";
	/**
	 * sentencia update de modificacionde todos los apartados de tipo pelicula que
	 * coincidan con su primary key
	 */
	public static final String SQL_UPDATE_TIPO_PELICULA = "UPDATE `peliculasonline`.`tipo_pelicula`SET`nombre` = ?,`descripcion` = ?,`idTipo_pelicula` = ? WHERE `idTipo_pelicula` = ?;";
	/**
	 * sentencia update de modificacionde todos los apartados de peliculas que
	 * coincidan con su primary key
	 */
	public static final String SQL_UPDATE_PELICULA = "UPDATE `peliculasonline`.`pelicula`SET`empleado_FK` = ?,`proveedor_FK` = ?,`clasificacion` = ?,`duracion` =?,`idiomas` = ?,`subtitulos` = ?,`resolucion` =?,`titulo` = ?,`portada` = ?,`descripcion` = ?,`idPelicula` = ? WHERE `idPelicula` = ?;";
	/**
	 * sentencia update de modificacionde todos los apartados de peliculas tipos que
	 * coincidan con su primary key
	 */
	public static final String SQL_UPDATE_PELICULAS_TIPOS = "UPDATE `peliculasonline`.`pelicula_tipos`SET`pelicula_FK` = ?,`tipo_pelicula_FK` = ? WHERE `pelicula_FK` = ? AND `tipo_pelicula_FK` = ?;";
	/**
	 * sentencia update de modificacionde todos los apartados de consulta que
	 * coincidan con su primary key
	 */
	public static final String SQL_UPDATE_CONSULTA = "UPDATE `peliculasonline`.`consulta`SET`socio_PK` = ?,`empleado_PK` = ?,`fecha` = ?,`incidencia` = ?,`resolucion` = ? WHERE `socio_PK` = ? AND `empleado_PK` = ? AND `fecha` = ?;";
	/**
	 * sentencia update de modificacionde todos los apartados de ticket que
	 * coincidan con su primary key
	 */
	public static final String SQL_UPDATE_TICKET = "UPDATE `peliculasonline`.`ticket`SET`socio_FK` =?,`pelicula_FK` = ?,`fecha_compra` = ? WHERE `socio_FK` = ? AND `pelicula_FK` = ? AND `fecha_compra` = ?;";

	/**
	 * sentencia delete donde se elimina la tupla consulta que coincidan con su
	 * primary key
	 */
	public static final String SQL_DELETE_CONSULTA_EMPLEADO_SOCIO_FECHA = "DELETE FROM `peliculasonline`.`consulta` WHERE `socio_PK`=? and `empleado_PK`=? and `fecha`=?;";

	/**
	 * sentencia delete donde se elimina la tupla ticket que coincidan con su
	 * primary key
	 */
	public static final String SQL_DELETE_TICKET_SOCIO_PELICULA_FECHA = "DELETE FROM `peliculasonline`.`ticket` WHERE `socio_FK`=? and `pelicula_FK`=? and `fecha_compra`=?;";

	/**
	 * sentencia delete donde se elimina la tupla de ticket que coincida con el id
	 * de pelicula
	 */
	public static final String SQL_DELETE_TICKET_PELICULA_ID = "DELETE FROM `peliculasonline`.`ticket` WHERE `ticket`.pelicula_FK=?;";

	/**
	 * sentencia delete donde se elimina la tupla socio que coincida con su primary
	 * key
	 */
	public static final String SQL_DELETE_SOCIO = "DELETE FROM `peliculasonline`.`socio` WHERE `idSocio`=?;";
	/**
	 * sentencia delete donde se elimina la tupla pelicula_tipos que coincida con id
	 * de pelicula
	 */
	public static final String SQL_DELETE_PELICULA_TIPOS_PELICULA_ID = "DELETE FROM `peliculasonline`.`pelicula_tipos` WHERE `pelicula_tipos`.pelicula_FK=?;";
	/**
	 * sentencia delete donde se elimina la tupla pelicula que coincida con su
	 * primary key
	 */
	public static final String SQL_DELETE_PELICULA = "DELETE FROM `peliculasonline`.`pelicula` WHERE `pelicula`.idPelicula=?;";
	/**
	 * sentencia delete donde se elimina la tupla proveedor que coincida con su
	 * primary key
	 */
	public static final String SQL_DELETE_PROVEEDOR_ID_PROVEEDOR = "DELETE FROM `peliculasonline`.`proveedor` WHERE `proveedor`.idProveedor=?;";
	/**
	 * sentencia delete donde se elimina la tupla empleado que coincida con su
	 * primary key
	 */
	public static final String SQL_DELETE_EMPLEADO_ID_EMPLEADO = "DELETE FROM `peliculasonline`.`empleado` WHERE `empleado`.`idEmpleado`=?;";

	/**
	 * posicion de tipo en la sentencia de tarifa
	 */
	public static int TARIFA_TIPO = 1;
	/**
	 * posicion de precio en la sentencia de tarifa
	 */
	public static int TARIFA_PRECIO = 2;
	/**
	 * posicion de id tarifa en la sentencia de tarifa
	 */
	public static int TARIFA_ID = 3;
	/**
	 * posicion de id tarifa en la sentencia de tarifa update
	 */
	public static int TARIFA_UPDATE_ID = 4;

	/**
	 * posicion del id de la tarifa en la sentencia de socio
	 */
	public static int SOCIO_TARIFA_FK = 1;
	/**
	 * posicion del nombre de usuario en la sentencia de socio
	 */
	public static int SOCIO_USER_NAME = 2;
	/**
	 * posicion del password en la sentencia de socio
	 */
	public static int SOCIO_PASSWORD = 3;
	/**
	 * posicion del nombre en la sentencia de socio
	 */
	public static int SOCIO_NOMBRE = 4;
	/**
	 * posicion del apellido en la sentencia de socio
	 */
	public static int SOCIO_APELLIDOS = 5;
	/**
	 * posicion de la fecha de nacimiento en la sentencia de socio
	 */
	public static int SOCIO_FECHA_NACIMIENTO = 6;
	/**
	 * posicion del email en la sentencia de socio
	 */
	public static int SOCIO_EMAIL = 7;
	/**
	 * posicion del telefono en la sentencia de socio
	 */
	public static int SOCIO_TELEFONO = 8;
	/**
	 * posicion del dni en la sentencia de socio
	 */
	public static int SOCIO_DNI = 9;
	/**
	 * posicion del estado en la sentencia de socio
	 */
	public static int SOCIO_ESTADO = 10;
	/**
	 * posicion de la fecha de la tarifa en la sentencia de socio
	 */
	public static int SOCIO_FECHA_TARIFA = 11;
	/**
	 * posicion del id del socio en la sentencia de socio
	 */
	public static int SOCIO_ID = 12;
	/**
	 * posicion del id del socio en la sentencia de socio update
	 */
	public static int SOCIO_UPDATE_ID = 13;

	/**
	 * posicion del nombre en la sentencia de tipo empleado
	 */
	public static int TIPO_EMPLEADO_NOMBRE = 1;
	/**
	 * posicion de la descripcion en la sentencia de tipo empleado
	 */
	public static int TIPO_EMPLEADO_DESCRIPCION = 2;
	/**
	 * posicion del id de tipo empleado en la sentencia de tipo empleado
	 */
	public static int TIPO_EMPLEADO_ID = 3;
	/**
	 * posicion del id de tipo empleado en la sentencia de tipo empleado update
	 */
	public static int TIPO_EMPLEADO_UPDATE_ID = 4;

	/**
	 * posicion del id del tipo empleado en la sentencia de empleado
	 */
	public static int EMPLEADO_TIPO_EMPLEADO_FK = 1;
	/**
	 * posicion del nombre de usuario en la sentencia de empleado
	 */
	public static int EMPLEADO_USER_NAME = 2;
	/**
	 * posicion del password en la sentencia de empleado
	 */
	public static int EMPLEADO_PASSWORD = 3;
	/**
	 * posicion del nombre en la sentencia de empleado
	 */
	public static int EMPLEADO_NOMBRE = 4;
	/**
	 * posicion del apellido en la sentencia de empleado
	 */
	public static int EMPLEADO_APELLIDOS = 5;
	/**
	 * posicion de la fecha de nacimiento en la sentencia de empleado
	 */
	public static int EMPLEADO_FECHA_NACIMIENTO = 6;
	/**
	 * posicion del dni en la sentencia de empleado
	 */
	public static int EMPLEADO_DNI = 7;
	/**
	 * posicion del telefono en la sentencia de empleado
	 */
	public static int EMPLEADO_TELEFONO = 8;
	/**
	 * posicion del email en la sentencia de empleado
	 */
	public static int EMPLEADO_EMAIL = 9;
	/**
	 * posicion del id del empleado en la sentencia de empleado
	 */
	public static int EMPLEADO_ID = 10;
	/**
	 * posicion del id del empleado en la sentencia de empleado update
	 */
	public static int EMPLEADO_UPDATE_ID = 11;

	/**
	 * posicion del nombre en la sentencia de proveedor
	 */
	public static int PROVEEDOR_NOMBRE = 1;
	/**
	 * posicion del email en la sentencia de proveedor
	 */
	public static int PROVEEDOR_EMAIL = 2;
	/**
	 * posicion del telefono en la sentencia de proveedor
	 */
	public static int PROVEEDOR_TELEFONO = 3;
	/**
	 * posicion del id del proveedor en la sentencia de proveedor
	 */
	public static int PROVEEDOR_ID = 4;
	/**
	 * posicion del id del proveedor en la sentencia de proveedor update
	 */
	public static int PROVEEDOR_UPDATE_ID = 5;

	/**
	 * posicion del nombre de tipo pelicula en la sentencia de tipo pelicula
	 */
	public static int TIPO_PELICULA_NOMBRE = 1;
	/**
	 * posicion de la descripcion en la sentencia de tipo pelicula
	 */
	public static int TIPO_PELICULA_DESCRIPCION = 2;
	/**
	 * posicion del id de tipo pelicula en la sentencia de tipo pelicula
	 */
	public static int TIPO_PELICULA_ID = 3;
	/**
	 * posicion del id de tipo pelicula en la sentencia de tipo pelicula update
	 */
	public static int TIPO_PELICULA_UPDATE_ID = 4;

	/**
	 * posicion del id del empleado en la sentencia de pelicula
	 */
	public static int PELICULA_EMPLEADO_FK = 1;
	/**
	 * posicion del id del proveedor en la sentencia de pelicula
	 */
	public static int PELICULA_PROVEEDOR_FK = 2;
	/**
	 * posicion de la clasificacion en la sentencia de pelicula
	 */
	public static int PELICULA_CLASIFICACION = 3;
	/**
	 * posicion dela dureacion en la sentencia de pelicula
	 */
	public static int PELICULA_DURACION = 4;
	/**
	 * posicion de los idiomas en la sentencia de pelicula
	 */
	public static int PELICULA_IDIOMAS = 5;
	/**
	 * posicion de los subtitulos en la sentencia de pelicula
	 */
	public static int PELICULA_SUBTITULOS = 6;
	/**
	 * posicion de la resolucion en la sentencia de pelicula
	 */
	public static int PELICULA_RESOLUCION = 7;
	/**
	 * posicion del titulo en la sentencia de pelicula
	 */
	public static int PELICULA_TITULO = 8;
	/**
	 * posicion de la portada en la sentencia de pelicula
	 */
	public static int PELICULA_PROTADA = 9;
	/**
	 * posicion de la descripcion en la sentencia de pelicula
	 */
	public static int PELICULA_DESCRIPCION = 10;
	/**
	 * posicion del id de la pelicula en la sentencia de pelicula
	 */
	public static int PELICULA_ID = 11;
	/**
	 * posicion del id de la pelicula en la sentencia de pelicula update
	 */
	public static int PELICULA_UPDATE_ID = 12;

	/**
	 * posicion del id de pelicula en la sentencia de pelicula tipos
	 */
	public static int PELICULA_TIPOS_PELICULA_FK = 1;
	/**
	 * posicion del id de tipo en la sentencia de pelicula tipos
	 */
	public static int PELICULA_TIPOS_TIPO_PELICULA_FK = 2;
	/**
	 * posicion del id de pelicula en la sentencia de pelicula tipos update
	 */
	public static int PELICULA_TIPOS_UPDATE_PELICULA_FK = 3;
	/**
	 * posicion del id de tipo en la sentencia de pelicula tipos update
	 */
	public static int PELICULA_TIPOS_UPDATE_TIPO_PELICULA_FK = 4;

	/**
	 * posicion del id del socio en la sentencia de consulta
	 */
	public static int CONSULTA_SOCIO_PK = 1;
	/**
	 * posicion del id del empleado en la sentencia de consulta
	 */
	public static int CONSULTA_EMPLEADO_PK = 2;
	/**
	 * posicion de la fecha en la sentencia de consulta
	 */
	public static int CONSULTA_FECHA = 3;
	/**
	 * posicion de la incidencia en la sentencia de consulta
	 */
	public static int CONSULTA_INCIDENCIA = 4;
	/**
	 * posicion de la resolucion en la sentencia de consulta
	 */
	public static int CONSULTA_RESOLUCION = 5;
	/**
	 * posicion del idl de socio en la sentencia de consulta update
	 */
	public static int CONSULTA_UPDATE_SOCIO_PK = 6;
	/**
	 * posicion del idl de empleado en la sentencia de consulta update
	 */
	public static int CONSULTA_UPDATE_EMPLEADO_PK = 7;
	/**
	 * posicion de la fecha en la sentencia de consulta update
	 */
	public static int CONSULTA_UPDATE_FECHA = 8;

	/**
	 * posicion del id del socio en la sentencia de ticket
	 */
	public static int TICKET_SOCIO_FK = 1;
	/**
	 * posicion del id de la pelicula en la sentencia de ticket
	 */
	public static int TICKET_PELICULA_FK = 2;
	/**
	 * posicion de la fecha de compra en la sentencia de ticket
	 */
	public static int TICKET_FECHA_COMPRA = 3;
	/**
	 * posicion del id del socio en la sentencia de ticket update
	 */
	public static int TICKET_UPDATE_SOCIO_FK = 4;
	/**
	 * posicion del id de la pelicula en la sentencia de ticket update
	 */
	public static int TICKET_UPDATE_PELICULA_FK = 5;
	/**
	 * posicion de la fecha de compra en la sentencia de ticket update
	 */
	public static int TICKET_UPDATE_FECHA_COMPRA = 6;

	/**
	 * posicion de la pelicula en la sentencia oscar select
	 */
	public static int OSCAR_PELICULA = 1;
	/**
	 * posicion de la descripcion de la pelicula en la sentencia oscar select
	 */
	public static int OSCAR_DESCRIPCION = 2;
	/**
	 * posicion del id del oscar en la sentencia oscar select
	 */
	public static int OSCAR_ID = 3;

}
