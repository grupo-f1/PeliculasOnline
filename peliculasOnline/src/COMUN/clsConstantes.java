package COMUN;

/**
 * constantes generales para la aplicacion
 * 
 * @author eric
 *
 */
public class clsConstantes {
	/**
	 * constante para ir al menu anterior
	 */
	public static final String ATRAS = "atras";
	/**
	 * constante para salir del menu
	 */
	public static final String EXIT = "exit";
	/**
	 * constante opcion true
	 */
	public static final String TRUE = "true";
	/**
	 * constante patron de la fecha
	 */
	public static final String PATRON_FECHA = "yyyy-MM-dd";

	/**
	 * constante para salir del menu
	 */
	public static final String SALIR = "SALIR";
	/**
	 * constante para el login
	 */
	public static final String LOGIN = "LOGIN";
	/**
	 * constante para el registro
	 */
	public static final String REGISTRO = "REGISTRO";
	/**
	 * constante para insertar
	 */
	public static final String INSERTAR = "INSERTAR";
	/**
	 * constante para editar
	 */
	public static final String EDITAR = "EDITAR";
	/**
	 * constante para ver mas datos
	 */
	public static final String VER = "VER";
	/**
	 * constante para ver listas
	 */
	public static final String LISTA = "LISTA";
	/**
	 * constante para eliminar
	 */
	public static final String ELIMINAR = "ELIMINAR";
	/**
	 * constante para la ruta de las portadas
	 */
	public static final String PORTADA = "imagenes\\Portadas";
	/**
	 * constante para las imagenes de fondo
	 */
	public static final String FONDO = "imagenes\\Fondos";
	/**
	 * constante para la imagen de fondo del gestor
	 */
	public static final String FONDO_GESTOR = "fondoGestor.jpg";

	// -----------------VENTANA ADMINISTRADOR------------------//
	/**
	 * constante para menu administrador en ventana de menu principal para listar
	 * socios
	 */
	public static final String LISTAR_SOCIOS = "listar Socios";
	/**
	 * constante para menu administrador en ventana de menu principal para buscar
	 * socio
	 */
	public static final String BUSCAR_SOCIO = "buscar Socio";
	/**
	 * constante para menu administrador en ventana de menu principal para crear
	 * socio
	 */
	public static final String CREAR_SOCIO = "crear Socio";
	/**
	 * constante para menu administrador en ventana de menu principal para editar
	 * socio
	 */
	public static final String EDITAR_SOCIO = "editar Socio";
	/**
	 * constante para menu administrador en ventana de menu principal para borrar
	 * socio
	 */
	public static final String BORRAR_SOCIO = "borrar Socio";
	/**
	 * constante para menu administrador en ventana de menu principal para listar
	 * empleados
	 */
	public static final String LISTAR_EMPLEADOS = "listar Empleados";
	/**
	 * constante para menu administrador en ventana de menu principal para buscar
	 * empleado
	 */
	public static final String BUSCAR_EMPLEADO = "buscar Empleado";
	/**
	 * constante para menu administrador en ventana de menu principal para crear
	 * empleado
	 */
	public static final String CREAR_EMPLEADO = "crear Empleado";
	/**
	 * constante para menu administrador en ventana de menu principal para crear
	 * empleado
	 */
	public static final String EDITAR_EMPLEADO = "editar Empleado";
	/**
	 * constante para menu administrador en ventana de menu principal para borrar
	 * empleado
	 */
	public static final String BORRAR_EMPLEADO = "borrar Empleado";
	/**
	 * constante para menu administrador en ventana de menu principal para listar
	 * consultas
	 */
	public static final String LISTAR_CONSULTAS = "listar Consutas";
	/**
	 * constante para menu administrador en ventana de menu principal para buscar
	 * consulta
	 */
	public static final String BUSCAR_CONSULTA = "buscar Consulta";
	/**
	 * constante para menu administrador en ventana de menu principal para crear
	 * consulta
	 */
	public static final String CREAR_CONSULTA = "crear Consulta";
	/**
	 * constante para menu administrador en ventana de menu principal para crear
	 * consulta
	 */
	public static final String EDITAR_CONSULTA = "editar Consulta";
	/**
	 * constante para menu administrador en ventana de menu principal para borrar
	 * consulta
	 */
	public static final String BORRAR_CONSULTA = "borrar Consulta";
	/**
	 * constante para menu administrador en ventana de menu principal para listar
	 * tickets
	 */
	public static final String LISTAR_TICKETS = "listar Tickets";
	/**
	 * constante para menu administrador en ventana de menu principal para buscar
	 * ticket
	 */
	public static final String BUSCAR_TICKET = "buscar Ticket";
	/**
	 * constante para menu administrador en ventana de menu principal para crear
	 * ticket
	 */
	public static final String CREAR_TICKET = "crear Ticket";
	/**
	 * constante para menu administrador en ventana de menu principal para crear
	 * ticket
	 */
	public static final String EDITAR_TICKET = "editar Ticket";
	/**
	 * constante para menu administrador en ventana de menu principal para borrar
	 * ticket
	 */
	public static final String BORRAR_TICKET = "borrar Ticket";
	/**
	 * constante para menu administrador en ventana de menu principal para listar
	 * tarifas
	 */
	public static final String LISTAR_TARIFAS = "listar Tarifas";
	/**
	 * constante para menu administrador en ventana de menu principal para buscar
	 * tarifa
	 */
	public static final String BUSCAR_TARIFA = "buscar Tarifa";
	/**
	 * constante para menu administrador en ventana de menu principal para crear
	 * tarifa
	 */
	public static final String CREAR_TARIFA = "crear Tarifa";
	/**
	 * constante para menu administrador en ventana de menu principal para crear
	 * tarifa
	 */
	public static final String EDITAR_TARIFA = "editar Tarifa";
	/**
	 * constante para menu administrador en ventana de menu principal para borrar
	 * tarifa
	 */
	public static final String BORRAR_TARIFA = "borrar Tarifa";

	// -------------------VENTANA GESTOR-----------------//
	/**
	 * constante para menu gestor en ventana de menu principal para listar peliculas
	 * por titulo
	 */
	public static final String LISTAR_PELICULAS = "listar Peliculas";
	/**
	 * constante para menu gestor en ventana de menu principal para listar
	 * proveedores
	 */
	public static final String LISTAR_PROVEEDORES = "listar Proveedores";
	/**
	 * constante para menu gestor en ventana de menu principal para estadisticas
	 */
	public static final String ESTADISTICAS = "estadisticas";
	/**
	 * constante para menu gestor en ventana de menu principal para mostrar la lista
	 * de estadisitcas
	 */
	public static final String INTERNAL_ESTADISTICAS = "LP.frmInEstadisticas";
	/**
	 * constante para menu gestor en ventana de menu principal para mostrar la lista
	 * de oscars
	 */
	public static final String INTERNAL_OSCAR = "LP.frmInListarOscars";
	/**
	 * constante para menu gestor en ventana de menu principal para los oscars
	 */
	public static final String OSCARS = "oscars";

	// ---------------------INTERNAL PROVEEDORES---------------------------//

	/**
	 * constante para menu gestor en ventana de menu principal para mostrar la lista
	 * de proveedores
	 */
	public static final String INTERNAL_PROVEEDOR = "LP.frmInListarProveedores";
	/**
	 * constante para la ventana internal de listar proveedores
	 */
	public static final String INTERNAL_INSERT_PROVEEDOR = "LP.frmInInsertProveedor";
	/**
	 * constante para menu gestor en ventana de menu principal para mostrar la lista
	 * de pelicuas al cerrar un internal de update pelicula
	 */
	public static final String INTERNAL_UPDATE_PROVEEDOR = "LP.frmInUpdateProveedor";
	/**
	 * constante para menu gestor en ventana de menu principal para buscar Proveedor
	 */
	public static final String BUSCAR_PROVEEDOR = "buscar Proveedor";
	/**
	 * constante para menu gestor en ventana de menu principal para crear Proveedor
	 */
	public static final String CREAR_PROVEEDOR = "crear Proveedor";
	/**
	 * constante para menu gestor en ventana de menu principal para borrar Proveedor
	 */
	public static final String BORRAR_PROVEEDOR = "borrar Proveedor";
	/**
	 * constante para menu gestor en ventana de menu principal para editar Proveedor
	 */
	public static final String EDITAR_PROVEEDOR = "editar Proveedor";

	// ---------------------INTERNAL PELICULA--------------------------//

	/**
	 * constante para menu gestor en ventana de menu principal para mostrar la lista
	 * de peliculas
	 */
	public static final String INTERNAL_PELICULA = "LP.frmInListarPeliculas";
	/**
	 * constante para menu gestor en ventana de menu principal para mostrar la lista
	 * de pelicuas al cerrar un internal de insert pelicula
	 */
	public static final String INTERNAL_INSERT_PELICULA = "LP.frmInInsertPelicula";
	/**
	 * constante para menu gestor en ventana de menu principal para mostrar la lista
	 * de pelicuas al cerrar un internal de update pelicula
	 */
	public static final String INTERNAL_UPDATE_PELICULA = "LP.frmInUpdatePelicula";
	/**
	 * constante para menu gestor en ventana de menu principal para mostrar la lista
	 * de pelicuas al cerrar un internal de ver mas datos de una pelicula
	 */
	public static final String INTERNAL_VER_PELICULA = "LP.frmInVerPelicula";
	/**
	 * constante para menu gestor en ventana de menu principal para crear pelicula
	 */
	public static final String CREAR_PELICULA = "crear Pelicula";
	/**
	 * constante para menu gestor en ventana de menu principal para borrar pelicula
	 */
	public static final String BORRAR_PELICULA = "borrar Pelicula";
	/**
	 * constante para menu gestor en ventana de menu principal para editar pelicula
	 */
	public static final String EDITAR_PELICULA = "editar Pelicula";
	/**
	 * constante para menu gestor en ventana de menu principal para ver mas sobre
	 * una pelicula
	 */
	public static final String VER_PELICULA = "ver Pelicula";

	// ----------------------INTERNAL INSERT PELICULA-------------------------//
	/**
	 * constante para menu gestor en ventana interna de crear pelicula para eliminar
	 * un tipo pelicula de la lista
	 */
	public static final String BORRAR_TIPO_PELICULA_LISTA = "borrar tipo pelicula de lista";
	/**
	 * constante para menu gestor en ventana interna de crear pelicula para eliminar
	 * un idioma de la lista
	 */
	public static final String BORRAR_IDIOMA_LISTA = "borrar idioma de lista";

	/**
	 * constante para menu gestor en ventana interna de crear pelicula para cambios
	 * en la comboBox de tipo Pelicula
	 */
	public static final String CAMBIO_COMBO_TIPO_PELICULA = "cambio comboBox tipo pelicula";
	/**
	 * constante para menu gestor en ventana interna de crear pelicula para cambios
	 * en la comboBox de idiomas
	 */
	public static final String CAMBIO_COMBO_IDIOMA = "cambio comboBox idioma";
	/**
	 * constante para menu gestor en ventana interna de crear pelicula para los
	 * idiomas de las peliculas
	 */
	public static final String[] IDIOMAS = { "Ingles", "Espa�ol", "Aleman", "Hungaro", "Turco", "Frances", "Chino",
			"Japones", "Coreano" };
	/**
	 * constante para menu gestor en ventana interna de crear pelicula para las
	 * clasificaciones de las peliculas
	 * 
	 */
	public static final String[] CLASIFICACIONES = { "+3", "+7", "+12", "+16", "+18", "TP" };
	/**
	 * constante para menu gestor en ventana interna de crear pelicula para las
	 * resoluciones de las peliculas
	 * 
	 */
	public static final String[] RESOLUCIONES = { "4k", "2k", "1080", "720", "460", "240", "144" };

	// ---------------------VENTANA SOCIO--------------------//
	/**
	 * constante para menu socio en la ventana de menu principal para elegir tarifa
	 */
	public static final String ELEGIR_TARIFA = "elegir Tarifa";
	/**
	 * constante para menu socio en la ventana de menu principal para elegir
	 * pelicula y aceptar el proceso de elegir una tarifa
	 */
	public static final String BOTON_ACEPTAR_TARIFA = "botón aceptar tarifa";
	/**
	 * constante para menu socio en la ventana de menu principal para elegir
	 * pelicula
	 */
	public static final String ELEGIR_PELICULA = "elegir Pelicula";
	/**
	 * constante para menu socio en la ventana de menu principal para elegir
	 * pelicula y aceptar la eleccion de la pel�cula
	 */
	public static final String BOTON_ACEPTAR_PELICULA = "botón aceptar pelicula";

	/**
	 * constante para menu socio en la ventana de menu principal ver las peliculas
	 * del socio
	 */
	public static final String MIS_PELICULAS = "mis Peliculas";
	/**
	 * constante para menu socio en la ventana de menu principal para hacer una
	 * consulta al soporte
	 */
	public static final String CONSULTA_SOPORTE = "consulta Soporte";
	/**
	 * constante para menu socio en la ventana de menu principal para hacer una
	 * consulta al soporte y bot�on aceptar para ejecutar la acci�n
	 */
	public static final String BOTON_ACEPTAR_CONSULTA_SOPORTE = "bot�n aceptar consulta Soporte";
	/**
	 * constante para menu socio en la ventana de menu principal editar el perfil
	 */
	public static final String EDITAR_PERFIL = "editar Perfil";
	/**
	 * constante para menu socio en la ventana de menu principal editar el perfil y accion para guardar todos los datos.
	 */
	public static final String BOTON_ACEPTAR_EDITAR_PERFIL = "BOT�N ACEPTAR editar Perfil";
	/**
	 * constante para menu socio en la ventana de menu principal darse de baja
	 */
	public static final String DARSE_BAJA = "dar Baja";
	/**
	 * constante para menu socio en la ventana de menu principal darse de baja y bot�n de aceptar de completar esa funcionalidad
	 */
	public static final String BOTON_ACEPTAR_DARSE_BAJA = "bot�n dar Baja";

	// --------------USUARIOS----------------//
	/**
	 * primer tipo de usuario
	 */
	public static final String USUARIO_EMPLEADO = "empleado";
	/**
	 * segundo tipo de usuario
	 */
	public static final String USUARIO_SOCIO = "socio";
	/**
	 * columna user_name de usuario
	 */
	public static final String USUARIO_COL_USER_NAME = "user name";
	/**
	 * columna password de usuario
	 */
	public static final String USUARIO_COL_PASSWORD = "password";
	/**
	 * columna nombre de usuario
	 */
	public static final String USUARIO_COL_NOMBRE = "nombre";
	/**
	 * columna apellidos de usuario
	 */
	public static final String USUARIO_COL_APELLIDOS = "apellidos";
	/**
	 * columna fecha_nacimiento de usuario
	 */
	public static final String USUARIO_COL_FECHA_NACIMIENTO = "fecha nacimiento";
	/**
	 * columna dni de usuario
	 */
	public static final String USUARIO_COL_DNI = "dni";
	/**
	 * columna telefono de usuario
	 */
	public static final String USUARIO_COL_TELEFONO = "telefono";
	/**
	 * columna email de usuario
	 */
	public static final String USUARIO_COL_EMAIL = "email";

	// -------------------SOCIO-------------------------//

	/**
	 * columna estado del socio
	 */
	public static final String SOCIO_COL_ESTADO = "estado";
	/**
	 * columna id del socio
	 */
	public static final String SOCIO_COL_ID = "id Socio";
	/**
	 * columna tarifa_FK del socio
	 */
	public static final String SOCIO_COL_TARIFA_FK = "tarifa";
	/**
	 * columna fecha_tarifa del socio
	 */
	public static final String SOCIO_COL_FECHA_TARIFA = "fecha tarifa";

	// -------------------EMPLEADO----------------------//
	/**
	 * columna idEmpleado de empleado
	 */
	public static final String EMPLEADO_COL_ID = "id Empleado";
	/**
	 * columna tipoEmpleado_FK de empleado
	 */
	public static final String EMPLEADO_COL_TIPO_EMPLEADO_FK = "tipo Empleado";

	// -----------------TIPO EMPLEADO-----------------//

	/**
	 * primer tipo empleado
	 */
	public static final String TIPO_EMPLEADO_ADMIN = "Administrador";
	/**
	 * segundo tipo empleado
	 */
	public static final String TIPO_EMPLEADO_GESTOR = "Gestor";
	/**
	 * tercer tipo empleado
	 */
	public static final String TIPO_EMPLEADO_TECNICO = "Tecnico";

	/**
	 * columna nombre del tipo empleado
	 */
	public static final String TIPO_EMPLEADO_COL_NOMBRE = "nombre";
	/**
	 * columna id del tipo empleado
	 */
	public static final String TIPO_EMPLEADO_COL_ID = "id Tipo Empleado";

	// -------------TARIFAS-------------------//

	/**
	 * primera tarifa
	 */
	public static final String TARIFA_BASICA = "basica";
	/**
	 * segunda tarifa
	 */
	public static final String TARIFA_AVANZADA = "avanzada";
	/**
	 * tercera tarifa
	 */
	public static final String TARIFA_PREMIUM = "premium";
	/**
	 * columna tipo de tarifa
	 */
	public static final String TARIFA_COL_TIPO = "tipo";
	/**
	 * columna precio de tarifa
	 */
	public static final String TARIFA_COL_PRECIO = "precio";
	/**
	 * columna id de tarifa
	 */
	public static final String TARIFA_COL_ID = "id Tarifa";

	// ----------------TICKET-----------------------//

	/**
	 * columna socio_FK de ticket
	 */
	public static final String TICKET_COL_SOCIO_FK = "socio";
	/**
	 * columna pelicula_FK de ticket
	 */
	public static final String TICKET_COL_PELICULA_FK = "pelicula";
	/**
	 * columna fecha_compra de ticket
	 */
	public static final String TICKET_COL_FECHA_COMPRA = "fecha compra";

	// ----------------PELICULA--------------------//

	/**
	 * columna titulo de pelicula
	 */
	public static final String PELICULA_COL_TITULO = "titulo";
	/**
	 * columna idPelicula de pelicula
	 */
	public static final String PELICULA_COL_ID = "id Pelicula";
	/**
	 * columna proveedor_FK de pelicula
	 */
	public static final String PELICULA_COL_PROVEEDOR_FK = "proveedor";
	/**
	 * columna descripcion de pelicula
	 */
	public static final String PELICULA_COL_DESCRIPCION = "descripcion";
	/**
	 * columna portada de pelicula
	 */
	public static final String PELICULA_COL_PORTADA = "portada";
	/**
	 * columna empleado_FK de pelicula
	 */
	public static final String PELICULA_COL_EMPLEADO_FK = "empleado";
	/**
	 * columna resolucion de pelicula
	 */
	public static final String PELICULA_COL_RESOLUCION = "resolucion";
	/**
	 * columna idiomas de pelicula
	 */
	public static final String PELICULA_COL_IDIOMAS = "idiomas";
	/**
	 * columna subtitulos de pelicula
	 */
	public static final String PELICULA_COL_SUBTITULOS = "subtitulos";
	/**
	 * columna duracion de pelicula
	 */
	public static final String PELICULA_COL_DURACION = "duracion";
	/**
	 * columna clasificacion de pelicula
	 */
	public static final String PELICULA_COL_CLASIFICACION = "clasificacion";

	// -------------------TIPO PELICULA-----------------------//
	/**
	 * columna nombre del tipo pelicula
	 */
	public static final String TIPO_PELICULA_COL_NOMBRE = "nombre";
	/**
	 * columna idTipoPelicula del tipo pelicula
	 */
	public static final String TIPO_PELICULA_COL_ID = "id Tipo Pelicula";

	// -----------------PELICULA TIPOS----------------------//
	/**
	 * columna tipo_pelicula_FK de la relacion entre tipo pelicula y pelicula
	 */
	public static final String PELICULA_TIPOS_COL_TIPO_PELICULA_FK = "tipo pelicula";
	/**
	 * columna pelicula_FK de la relacion entre tipo pelicula y pelicula
	 */
	public static final String PELICULA_TIPOS_COL_PELICULA_FK = "pelicula";

	// --------------PROVEEDOR-------------------//

	/**
	 * columna idProveedor de proveedor
	 */
	public static final String PROVEEDOR_COL_ID = "id Proveedor";
	/**
	 * columna nombre de proveedor
	 */
	public static final String PROVEEDOR_COL_NOMBRE = "nombre";
	/**
	 * columna email de proveedor
	 */
	public static final String PROVEEDOR_COL_EMAIL = "email";
	/**
	 * columna telefono de proveedor
	 */
	public static final String PROVEEDOR_COL_TELEFONO = "telefono";

	// ----------------------------CONSULTA------------------------//
	/**
	 * columna empleado_FK de consulta
	 */
	public static final String CONSULTA_COL_EMPLEADO_FK = "empleado";
	/**
	 * columna socio_FK de consulta
	 */
	public static final String CONSULTA_COL_SOCIO_FK = "socio";
	/**
	 * columna fecha de consulta
	 */
	public static final String CONSULTA_COL_FECHA = "fecha";

	// ------------------------OSCAR--------------------//

	/**
	 * columna pelicula de oscar
	 */
	public static final String OSCAR_COL_PELICULA = "pelicula";
	/**
	 * columna descripcion de oscar
	 */
	public static final String OSCAR_COL_DESCRIPCION = "descripcion";
	/**
	 * columna id de oscar
	 */
	public static final String OSCAR_COL_ID = "id Oscar";

	// ----------------------TABLAS-----------------------//
	/**
	 * nombre de la tabla proveedor
	 */
	public static final String PROVEEDOR = "PROVEEDOR";
	/**
	 * nombre de la tabla pelicula
	 */
	public static final String PELICULA = "PELICULA";
}
