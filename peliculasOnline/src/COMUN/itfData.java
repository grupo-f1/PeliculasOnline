package COMUN;

/**
 * Interfaz que implementan aquellos objetos que representen una fila de un base
 * de datos. La te�rica fila estar�a compuesta de columnas y valores para esas
 * respectivas columnas.
 * 
 * @author Eric
 *
 */
public interface itfData {
	Object getData(Integer columna);

}
