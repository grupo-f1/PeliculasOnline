package LP;

import static COMUN.clsConstantes.*;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import LN.clsGestorLN;
import javax.swing.JLabel;

/**
 * ventana internal para listar los proveedores y hacer acciones con ellos
 * 
 * @author Eric
 *
 */
public class frmInEstadisticas extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5791123111936426235L;
	/**
	 * objeto gestor de la logica de negocio
	 */
	private clsGestorLN objGestor;

	/**
	 * dimensiones de el escritorio padre
	 */
	private Rectangle desktopPadreDim;
	/**
	 * array list de peliculas
	 */
	private ArrayList<itfProperty> peliculas;

	/**
	 * constructor de la internal frame de estadisticas
	 * 
	 * @param objGestor       objeto gestor de la logica de negocio
	 * @param desktopPadreDim dimensiones de la ventana padre
	 * @param titulo          titulo de la internal frame
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmInEstadisticas(clsGestorLN objGestor, Rectangle desktopPadreDim, String titulo, boolean redimensionable,
			boolean cerrable, boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.objGestor = objGestor;
		this.desktopPadreDim = desktopPadreDim;
		this.CrearPanel();

	}

	/**
	 * Create the frame.
	 */
	private void CrearPanel() {
		setBounds(desktopPadreDim);

		JDesktopPane desktop = new JDesktopPane();
		desktop.setBounds(desktopPadreDim);
		getContentPane().add(desktop);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(44, 46, 230, 302);
		desktop.add(scrollPane);

		JTable tablaCantPeliculas = new JTable();
		scrollPane.setViewportView(tablaCantPeliculas);

		DefaultTableModel modeloTablaCantPeliculas = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 5302908543210052730L;

			/**
			 * funcion que evita editar columnas en la tabla
			 */
			@Override
			public boolean isCellEditable(int filas, int columnas) {
				if (columnas == 3) {
					return true;
				} else {
					return false;
				}

			}
		};

		modeloTablaCantPeliculas.addColumn(PELICULA_COL_TITULO);
		modeloTablaCantPeliculas.addColumn("Veces vendido");

		try {
			peliculas = objGestor.listarPeliculaProveedor();
			ArrayList<itfProperty> tickets = objGestor.listarTicket();
			ArrayList<Integer> cantidadPeli = new ArrayList<Integer>();
			int cantPeliculas = 0;
			int totalPeliculas = 0;
			for (itfProperty pelicula : peliculas) {
				for (itfProperty ticket : tickets) {
					if (pelicula.getProperty(PELICULA_COL_ID) == ticket.getProperty(TICKET_COL_PELICULA_FK)) {
						cantPeliculas++;
					}
				}
				totalPeliculas = totalPeliculas + cantPeliculas;
				cantidadPeli.add(cantPeliculas);
				cantPeliculas = 0;
			}
			int i = 0;
			for (itfProperty pelicula : peliculas) {
				modeloTablaCantPeliculas
						.addRow(new Object[] { pelicula.getProperty(PELICULA_COL_TITULO), cantidadPeli.get(i) });
				i++;
			}
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		tablaCantPeliculas.setModel(modeloTablaCantPeliculas);
		tablaCantPeliculas.setRowHeight(30);
		tablaCantPeliculas.setAutoCreateRowSorter(true);
		tablaCantPeliculas.setPreferredScrollableViewportSize(new Dimension(500, 300));
		tablaCantPeliculas.setFillsViewportHeight(true);
		// Se llama al metodo que ajusta las columnas
		redimensionarAnchoColumna(tablaCantPeliculas);
		// Se desactiva el Auto Resize de la tabla
		// Es importante que vaya despues de el metodo que ajusta el ancho de la columna
		tablaCantPeliculas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(407, 46, 214, 302);
		desktop.add(scrollPane_1);

		JTable tablaPorcentajeProd = new JTable();
		scrollPane_1.setViewportView(tablaPorcentajeProd);

		DefaultTableModel modeloTablaPorcentajeProd = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4668036295124533090L;

			/**
			 * funcion que evita editar columnas en la tabla
			 */
			@Override
			public boolean isCellEditable(int filas, int columnas) {
				if (columnas == 3) {
					return true;
				} else {
					return false;
				}

			}
		};
		modeloTablaPorcentajeProd.addColumn(PROVEEDOR_COL_NOMBRE);
		modeloTablaPorcentajeProd.addColumn("Porcentaje");
		try {
			ArrayList<itfProperty> proveedores = objGestor.listarProveedor();
			ArrayList<Integer> cantidadPeli = new ArrayList<Integer>();
			int cantPeliculas = 0;
			int totalPeliculas = 0;
			for (itfProperty proveedor : proveedores) {
				for (itfProperty pelicula : peliculas) {
					if (pelicula.getProperty(PELICULA_COL_PROVEEDOR_FK) == proveedor.getProperty(PROVEEDOR_COL_ID)) {
						cantPeliculas++;
					}
				}
				totalPeliculas = totalPeliculas + cantPeliculas;
				cantidadPeli.add(cantPeliculas);
				cantPeliculas = 0;
			}
			int i = 0;
			for (itfProperty proveedor : proveedores) {
				modeloTablaPorcentajeProd.addRow(new Object[] { proveedor.getProperty(PROVEEDOR_COL_NOMBRE),
						((cantidadPeli.get(i) * 100) / totalPeliculas) + "%" });
				i++;
			}
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}

		tablaPorcentajeProd.setModel(modeloTablaPorcentajeProd);
		tablaPorcentajeProd.setRowHeight(30);
		tablaPorcentajeProd.setAutoCreateRowSorter(true);
		tablaPorcentajeProd.setPreferredScrollableViewportSize(new Dimension(500, 300));
		tablaPorcentajeProd.setFillsViewportHeight(true);
		// Se llama al metodo que ajusta las columnas
		redimensionarAnchoColumna(tablaPorcentajeProd);
		// Se desactiva el Auto Resize de la tabla
		// Es importante que vaya despues de el metodo que ajusta el ancho de la columna
		tablaPorcentajeProd.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		JLabel lblCantPeliculas = new JLabel("Cantidad de veces vendiada una pelicula", SwingConstants.CENTER);
		lblCantPeliculas.setBounds(32, 21, 251, 14);
		desktop.add(lblCantPeliculas);

		JLabel lblPorcentajeProv = new JLabel("Porcentaje de las peliculas por proveedor en la aplicacion",
				SwingConstants.CENTER);
		lblPorcentajeProv.setBounds(335, 21, 373, 14);
		desktop.add(lblPorcentajeProv);

	}

	/**
	 * Metodo que ajusta el ancho de la columna de una tabla segun su contenido
	 * 
	 * @param tabla tabla a editar
	 */
	private void redimensionarAnchoColumna(JTable tabla) {
		// Se obtiene el modelo de la columna
		TableColumnModel columnModel = tabla.getColumnModel();
		// Se obtiene el total de las columnas
		for (int column = 0; column < tabla.getColumnCount(); column++) {
			// Establecemos un valor minimo para el ancho de la columna
			int width = 100; // Min Width
			// Obtenemos el numero de filas de la tabla
			for (int row = 0; row < tabla.getRowCount(); row++) {
				// Obtenemos el renderizador de la tabla
				TableCellRenderer renderer = tabla.getCellRenderer(row, column);
				// Creamos un objeto para preparar el renderer
				Component comp = tabla.prepareRenderer(renderer, row, column);
				// Establecemos el width segun el valor maximo del ancho de la columna
				width = Math.max(comp.getPreferredSize().width + 1, width);

			}
			// Se establece una condicion para no sobrepasar el valor de 300
			// Esto es Opcional
			if (width > 300) {
				width = 300;
			}
			// Se establece el ancho de la columna
			columnModel.getColumn(column).setPreferredWidth(width);
		}
	}
}
