package LP;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;

import static COMUN.clsConstantes.*;
import LN.clsGestorLN;

/**
 * Ventana interna para cuando hay que hacer una insert de proveedor
 * 
 * @author Eric
 *
 */
public class frmInInsertProveedor extends JInternalFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8680496596054409384L;
	/**
	 * panel donde ira todo el contenido de la ventana interna
	 */
	JPanel jpContent;
	/**
	 * usuario actual conectado
	 */
	private clsGestorLN objGestor;
	/**
	 * texto deonde se guarda el nombre
	 */
	private JTextField txtNombre;
	/**
	 * texto donde se guarda el email
	 */
	private JTextField txtEmail;
	/**
	 * texto donde se guarda el telefono
	 */
	private JTextField txtTelefono;
	/**
	 * id del porveedor insertado
	 */
	private int idProveedor;

	/**
	 * constructor de la internal frame
	 * 
	 * @param objGestor       objeto gestor de la logica de negocio
	 * @param titulo          titulo de la internal frame
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmInInsertProveedor(clsGestorLN objGestor, String titulo, boolean redimensionable, boolean cerrable,
			boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.objGestor = objGestor;
		this.CrearPanel();

	}

	/**
	 * funcion que rellean el panel con los componentes pertinentes
	 */
	private void CrearPanel() {
		setBounds(100, 100, 450, 300);
		jpContent = new JPanel();
		jpContent.setBackground(Color.WHITE);
		jpContent.repaint();

		this.setContentPane(jpContent);
		jpContent.setLayout(null);
		JLabel lblTitulo = new JLabel("Insertar Proveedor", SwingConstants.CENTER);
		lblTitulo.setBounds(152, 11, 110, 14);
		jpContent.add(lblTitulo);

		JLabel lblNombre = new JLabel("Nombre", SwingConstants.CENTER);
		lblNombre.setBounds(159, 44, 96, 14);
		jpContent.add(lblNombre);

		JLabel lblEmail = new JLabel("Email", SwingConstants.CENTER);
		lblEmail.setBounds(159, 100, 96, 14);
		jpContent.add(lblEmail);

		JLabel lblTelefono = new JLabel("Telefono", SwingConstants.CENTER);
		lblTelefono.setBounds(159, 156, 96, 14);
		jpContent.add(lblTelefono);

		txtNombre = new JTextField();
		txtNombre.setBounds(159, 69, 96, 20);
		jpContent.add(txtNombre);
		txtNombre.setColumns(10);

		txtEmail = new JTextField();
		txtEmail.setBounds(159, 125, 96, 20);
		jpContent.add(txtEmail);
		txtEmail.setColumns(10);

		txtTelefono = new JTextField();
		txtTelefono.setBounds(159, 181, 96, 20);
		jpContent.add(txtTelefono);
		txtTelefono.setColumns(10);

		JButton btnInsertar = new JButton("Insertar");
		btnInsertar.setBounds(159, 220, 96, 23);
		btnInsertar.addActionListener(this);
		btnInsertar.setActionCommand(INSERTAR);
		jpContent.add(btnInsertar);
	}

	/**
	 * funcion para insertar un nuevo proveedor en la base de datos
	 * 
	 * @return id del proveedor insertado
	 */
	private int insertarProveedor() {
		idProveedor = -1;
		do {

			String nombre = txtNombre.getText();
			String email = txtEmail.getText();
			String telefono = txtTelefono.getText();
			try {
				idProveedor = objGestor.insertarProveedor(nombre, email, telefono);
				if (idProveedor != -1) {
					System.out.println("proveedor insertado");
				}

			} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConsulta
					| clsErrorSQLConexion e) {
				System.out.println(e.getMessage());
			}
		} while (idProveedor == -1);
		JOptionPane.showMessageDialog(this, "Proveedor insertado correctamente");
		return idProveedor;
	}

	/**
	 * funcion que devuleve el proveedor insertado
	 * 
	 * @return proveedor insertado
	 */
	public itfProperty devolverProveedor() {
		itfProperty proveedor = null;
		if (idProveedor != -1) {
			try {
				proveedor = objGestor.devolverProveedor(idProveedor);
			} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
				JOptionPane.showMessageDialog(this, e.getMessage());
			}
		}
		return proveedor;
	}

	/**
	 * escuchador de eventos
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case INSERTAR: {
			insertarProveedor();
			this.dispose();
			break;
		}

		}
	}
}
