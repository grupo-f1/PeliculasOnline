package LP;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;
import LN.clsGestorLN;
import LN.clsPelicula;
import LN.clsTarifa;
import peliculasOnline.clsMain;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;

import static COMUN.clsConstantes.*;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * JFrame del menu socio donde se mostrara las diferentes acciones que puede
 * hacer un socio
 * 
 * @author Eric
 *
 */
public class frmMenuSocio extends JFrame implements ActionListener, InternalFrameListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3857951287312700775L;
	/**
	 * objeto de la clase clsGestorLN
	 */
	private clsGestorLN objGestor;
	/**
	 * objeto de la clase clsGestorLN
	 */
	private itfProperty usuarioActual;

	/**
	 * obj de escritorio para poner el jInternalFrame
	 */
	JDesktopPane desktop;
	/**
	 * obj escuchador de la ventana
	 */
	JInternalFrame listenedToWindow;

	/**
	 * anchura de la pantalla
	 */
	static int widthP;
	/**
	 * altura de la pantalla
	 */
	static int heightP;

	/**
	 * panel donde se muestra las listas
	 */
	private JPanel panel;

	/**
	 * Constructor del jFrame del menu principal
	 * 
	 * @param usuarioActual usuario conectado
	 * @param objGestor     objeto gestor de la logica de negocio
	 */
	public frmMenuSocio(itfProperty usuarioActual, clsGestorLN objGestor) {
		this.usuarioActual = usuarioActual;
		this.objGestor = objGestor;

		// posicion pantalla
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();// ver tama�o de pantalla
		heightP = pantalla.height - 100;
		widthP = pantalla.width;
		// Se crea un desktopPane para poder usar internalFrames. Este caso lo que hace
		// es establecer el
		// panel de contenidos (contenpane) del formulario, por un contentpane.
		desktop = new JDesktopPane();
		desktop.setPreferredSize(new Dimension((widthP), (heightP)));
		setContentPane(desktop);
		desktop.setLayout(null);

		JMenuBar menuBarSocio = new JMenuBar();
		menuBarSocio.setBounds(0, 0, widthP, 23);
		desktop.add(menuBarSocio);

		JMenu menuElegirTarifa = new JMenu("Elegir Tarifa");
		menuBarSocio.add(menuElegirTarifa);

		JMenuItem menuItemElegirTarifa = new JMenuItem("ElegirTarifa");
		menuItemElegirTarifa.addActionListener(this);
		menuItemElegirTarifa.setActionCommand(ELEGIR_TARIFA);
		menuElegirTarifa.add(menuItemElegirTarifa);

		JMenu menuElegirPelicula = new JMenu("Elegir Pelicula");
		menuBarSocio.add(menuElegirPelicula);

		JMenuItem menuItemElegirPelicula = new JMenuItem("Elegir Pelicula");
		menuItemElegirPelicula.addActionListener(this);
		menuItemElegirPelicula.setActionCommand(ELEGIR_PELICULA);
		menuElegirPelicula.add(menuItemElegirPelicula);

		JMenu menuVisualizarPeliculasCompradas = new JMenu("Mis Peliculas");
		menuBarSocio.add(menuVisualizarPeliculasCompradas);

		JMenuItem menuItemVisualizarPeliculasCompradas = new JMenuItem("Mis Peliculas");
		menuItemVisualizarPeliculasCompradas.addActionListener(this);
		menuItemVisualizarPeliculasCompradas.setActionCommand(MIS_PELICULAS);
		menuVisualizarPeliculasCompradas.add(menuItemVisualizarPeliculasCompradas);

		JMenu menuConsultarDudas = new JMenu("Consultar Soporte");
		menuBarSocio.add(menuConsultarDudas);

		JMenuItem menuItemConsultarDudas = new JMenuItem("Consultar Soporte");
		menuItemConsultarDudas.addActionListener(this);
		menuItemConsultarDudas.setActionCommand(CONSULTA_SOPORTE);
		menuConsultarDudas.add(menuItemConsultarDudas);

		JMenu menuEditarPerfil = new JMenu("Editar Perfil");
		menuBarSocio.add(menuEditarPerfil);

		JMenuItem menuItemEditarPerfil = new JMenuItem("Editar Perfil");
		menuItemEditarPerfil.addActionListener(this);
		menuItemEditarPerfil.setActionCommand(EDITAR_PERFIL);
		menuEditarPerfil.add(menuItemEditarPerfil);

		JMenu menuBaja = new JMenu("Darse de baja");
		menuBarSocio.add(menuBaja);

		JMenuItem menuItemBaja = new JMenuItem("Darse de baja");
		menuItemBaja.addActionListener(this);
		menuItemBaja.setActionCommand(DARSE_BAJA);
		menuBaja.add(menuItemBaja);

	}

	/**
	 * funcion que muestra la ventana del menu principal
	 * 
	 */
	public void createAndShowGUI() {
		// Si se cierra el formulario se acaba la aplicacion.
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Display the window.
		this.pack();
		// centrar
		this.setLocationRelativeTo(null);
		// impedir hacer mas grande
		this.setResizable(false);

		this.setVisible(true);
	}

	// Elegir Tarifa
	JLabel explicacionElegirTarifas;
	JComboBox verTodasTarifas;
	ArrayList<String> todasTarifas = new ArrayList<String>();
	JButton botonAceptarElegirTarifas;

	// Elegir Pelicula
	JLabel explicacionElegirPelicula;
	JComboBox verTodasPeliculas;
	ArrayList<String> todasPeliculas = new ArrayList<String>();
	JButton botonAceptarElegirPeliculas;

	// Mostrar Pel�culas compradas
	JLabel explicacionPeliculasCompradas;
	JButton botonAceptarMostrarPeliculas;
	JTextArea textAreaPeliculasCompradas;
	JScrollPane scrollPeliculasCompradas;

	// Darse de baja
	JLabel explicacionDarseBaja;
	JButton botonDarseBaja;

	// Consultar soporte
	JLabel explicacionConsultaSoporte;
	JTextArea textAreaConsultaSoporte;
	JScrollPane scrollConsultaSoporte;
	JButton botonAceptarConsultaSoporte;

	// Modificar perfil
	JTextField perfilNombreUsuario;
	JTextField perfilPassword;
	JTextField perfilNombre;
	JTextField perfilApellidos;
	JTextField perfilFechaNacimiento;
	JTextField perfilEmail;
	JTextField perfilTelefono;
	JTextField perfilDni;
	JTextField perfilIdSocio;
	JTextField perfilEstado;
	JTextField perfilFechatarifa;
	JLabel explicacionNombreUsuario;
	JLabel explicacionPassword;
	JLabel explicacionNombre;
	JLabel explicacionApellidos;
	JLabel explicacionFechaNacimiento;
	JLabel explicacionEmail;
	JLabel explicacionTelefono;
	JLabel explicacionDni;
	JLabel explicacionIdSocio;
	JLabel explicacionEstado;
	JLabel explicacionFechaTarifa;
	JButton botonAceptarEditarPerfil;
	JLabel explicacionEditarPerfil;

	/**
	 * metodo para escuchar los distintos eventos que ocurren en la ventana
	 * drmMenuPrincipal
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// si hay un panel con algo lo quita de la ventana para reemplazar con lo que
		// vendra
		if (panel != null) {
			desktop.remove(panel);
		}
		if (e.getActionCommand() == ELEGIR_TARIFA || e.getActionCommand() == ELEGIR_PELICULA
				|| e.getActionCommand() == MIS_PELICULAS || e.getActionCommand() == CONSULTA_SOPORTE
				|| e.getActionCommand() == EDITAR_PERFIL || e.getActionCommand() == DARSE_BAJA) {
			if (explicacionElegirTarifas != null) {
				desktop.remove(explicacionElegirTarifas);
				desktop.remove(verTodasTarifas);
				desktop.remove(botonAceptarElegirTarifas);
				explicacionElegirTarifas.setVisible(false);
				verTodasTarifas.setVisible(false);
				botonAceptarElegirTarifas.setVisible(false);
				explicacionElegirTarifas = null;
			}
			if (explicacionElegirPelicula != null) {
				desktop.remove(explicacionElegirPelicula);
				desktop.remove(verTodasPeliculas);
				desktop.remove(botonAceptarElegirPeliculas);
				explicacionElegirPelicula.setVisible(false);
				verTodasPeliculas.setVisible(false);
				botonAceptarElegirPeliculas.setVisible(false);
				explicacionElegirPelicula = null;
			}
			if (explicacionPeliculasCompradas != null) {
				desktop.remove(explicacionPeliculasCompradas);
				desktop.remove(textAreaPeliculasCompradas);
				desktop.remove(scrollPeliculasCompradas);
				explicacionPeliculasCompradas.setVisible(false);
				textAreaPeliculasCompradas.setVisible(false);
				scrollPeliculasCompradas.setVisible(false);
				explicacionPeliculasCompradas = null;
			}

			if (explicacionDarseBaja != null) {
				desktop.remove(explicacionDarseBaja);
				desktop.remove(botonDarseBaja);
				explicacionDarseBaja.setVisible(false);
				botonDarseBaja.setVisible(false);
				explicacionDarseBaja = null;
			}

			if (explicacionConsultaSoporte != null) {
				desktop.remove(explicacionConsultaSoporte);
				desktop.remove(textAreaConsultaSoporte);
				desktop.remove(scrollConsultaSoporte);
				explicacionConsultaSoporte.setVisible(false);
				textAreaConsultaSoporte.setVisible(false);
				scrollConsultaSoporte.setVisible(false);
				botonAceptarConsultaSoporte.setVisible(false);
				explicacionConsultaSoporte = null;
			}

			if (perfilNombreUsuario != null) {
				desktop.remove(perfilNombreUsuario);
				desktop.remove(perfilPassword);
				desktop.remove(perfilNombre);
				desktop.remove(perfilApellidos);
				desktop.remove(perfilFechaNacimiento);
				desktop.remove(perfilEmail);
				desktop.remove(perfilTelefono);
				desktop.remove(perfilDni);
				desktop.remove(perfilIdSocio);
				desktop.remove(perfilEstado);
				desktop.remove(perfilFechatarifa);
				desktop.remove(explicacionNombreUsuario);
				desktop.remove(explicacionPassword);
				desktop.remove(explicacionNombre);
				desktop.remove(explicacionApellidos);
				desktop.remove(explicacionFechaNacimiento);
				desktop.remove(explicacionEmail);
				desktop.remove(explicacionTelefono);
				desktop.remove(explicacionDni);
				desktop.remove(explicacionIdSocio);
				desktop.remove(explicacionEstado);
				desktop.remove(explicacionFechaTarifa);
				desktop.remove(botonAceptarEditarPerfil);
				desktop.remove(explicacionEditarPerfil);
				explicacionNombreUsuario.setVisible(false);
				explicacionPassword.setVisible(false);
				explicacionNombre.setVisible(false);
				explicacionApellidos.setVisible(false);
				explicacionFechaNacimiento.setVisible(false);
				explicacionEmail.setVisible(false);
				explicacionTelefono.setVisible(false);
				explicacionDni.setVisible(false);
				explicacionIdSocio.setVisible(false);
				explicacionEstado.setVisible(false);
				explicacionFechaTarifa.setVisible(false);
				botonAceptarEditarPerfil.setVisible(false);
				explicacionEditarPerfil.setVisible(false);

				perfilNombreUsuario = null;
			}

		}
		switch (e.getActionCommand()) {
		/*
		 * case LOGIN: { frmInLogin objLogin = new frmInLogin(objGestor, usuarioActual,
		 * this, "Login", false, // resizable true, // closable false, // maximizable
		 * false); // iconifiable); desktop.add(objLogin);
		 * objLogin.addInternalFrameListener(this); // Set its size and location. We'd
		 * use pack() to set the size // if the window contained anything. Dimension
		 * desktopSize = desktop.getSize(); Dimension FrameSize = objLogin.getSize();
		 * objLogin.setLocation((desktopSize.width - FrameSize.width) / 2,
		 * (desktopSize.height - FrameSize.height) / 2); objLogin.show(); break; }
		 */
		case ELEGIR_TARIFA: {
			// Para que el socio pueda elegir la tarifa a pagar
			explicacionElegirTarifas = new JLabel("Estas son las diferentes tarifas, escoge una:");
			// Coordenadas: (x,y,anchura,altura)
			explicacionElegirTarifas.setBounds(500, 100, 300, 23);
			explicacionElegirTarifas.setVerticalAlignment(SwingConstants.CENTER);
			explicacionElegirTarifas.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionElegirTarifas);

			verTodasTarifas = new JComboBox<clsTarifa>();
			verTodasTarifas.setBounds(400, 150, 550, 23);

			ArrayList<itfProperty> tarifasElegir = new ArrayList<itfProperty>();
			try {
				tarifasElegir = objGestor.listarTarifa();
			} catch (clsErrorSQLConsulta | clsErrorSQLConexion exception) {

				System.out.println(exception.getMessage());
			}
			for (itfProperty tarifa : tarifasElegir) {
				verTodasTarifas.addItem(tarifa.toString());
				todasTarifas.add(tarifa.getProperty("tipo").toString());
			}
			desktop.add(verTodasTarifas);

			botonAceptarElegirTarifas = new JButton("Aceptar");
			botonAceptarElegirTarifas.addActionListener(this);
			botonAceptarElegirTarifas.setActionCommand(BOTON_ACEPTAR_TARIFA);
			botonAceptarElegirTarifas.setBounds(550, 200, 200, 23);
			desktop.add(botonAceptarElegirTarifas);
			explicacionElegirTarifas.setVisible(true);
			verTodasTarifas.setVisible(true);
			botonAceptarElegirTarifas.setVisible(true);

			break;
		}
		case BOTON_ACEPTAR_TARIFA: {
			int comprobacion = -1;
			int op = verTodasTarifas.getSelectedIndex() + 1;

			try {
				comprobacion = objGestor.editarSocioTarifa(op, usuarioActual);
			} catch (clsErrorSQLParametros | clsErrorSQLConsulta | clsErrorSQLInsertDeleteUpdate
					| clsErrorSQLConexion error) {
				System.out.println(error.getMessage());
			}
			if (comprobacion == 1) {
				JOptionPane.showInternalMessageDialog(null,
						"Felicidades has escogido la tarifa: " + todasTarifas.get(verTodasTarifas.getSelectedIndex()));
			} else {
				JOptionPane.showInternalMessageDialog(null, "Error Int�ntalo de nuevo!");
			}

			break;
		}
		case ELEGIR_PELICULA: {
			// Mostrarmos todas las peliculas
			explicacionElegirPelicula = new JLabel("Escoge la pelicula que quieras comprar:");
			explicacionElegirPelicula.setBounds(500, 100, 300, 23);
			explicacionElegirPelicula.setVerticalAlignment(SwingConstants.CENTER);
			explicacionElegirPelicula.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionElegirPelicula);
			verTodasPeliculas = new JComboBox<clsPelicula>();
			verTodasPeliculas.setBounds(400, 150, 550, 23);
			ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
			try {
				listaPeliculas = objGestor.listarPeliculaTitulo();
			} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {
				System.out.println(error.getMessage());
			}

			for (itfProperty pelicula : listaPeliculas) {
				String item = "titulo: " + pelicula.getProperty(PELICULA_COL_TITULO) + ", proveedor: "
						+ pelicula.getProperty(PELICULA_COL_PROVEEDOR_FK);
				verTodasPeliculas.addItem(item);
				todasPeliculas.add(pelicula.getProperty(PELICULA_COL_TITULO).toString());
			}
			desktop.add(verTodasPeliculas);

			botonAceptarElegirPeliculas = new JButton("Aceptar");
			botonAceptarElegirPeliculas.addActionListener(this);
			botonAceptarElegirPeliculas.setActionCommand(BOTON_ACEPTAR_PELICULA);
			botonAceptarElegirPeliculas.setBounds(550, 200, 200, 23);
			desktop.add(botonAceptarElegirPeliculas);
			explicacionElegirPelicula.setVisible(true);
			verTodasPeliculas.setVisible(true);
			botonAceptarElegirPeliculas.setVisible(true);

			break;
		}
		case BOTON_ACEPTAR_PELICULA: {
			int op = verTodasPeliculas.getSelectedIndex() + 1;
			int comprobacion = -1;

			try {
				comprobacion = objGestor.insertarTicket((int) usuarioActual.getProperty(SOCIO_COL_ID), op);
			} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion error) {
				System.out.println(error.getMessage());
			}
			if (comprobacion == 1) {
				JOptionPane.showMessageDialog(null, "Felicidades! ya has comprado la pelicula: "
						+ todasPeliculas.get(verTodasPeliculas.getSelectedIndex()));
			} else {
				JOptionPane.showMessageDialog(null,
						"Error no se ha podido comprar la pel�cula, ya que has comprado la misma pel�cula anteriormente en el mismo d�a, pulsa en 'Mis pel�culas' para verla");
			}

			break;
		}
		case MIS_PELICULAS: {
			String texto = "";
			int idSocioActual = (int) usuarioActual.getProperty(SOCIO_COL_ID);
			ArrayList<itfProperty> listaTickets = new ArrayList<itfProperty>();
			ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
			try {
				listaTickets = objGestor.listarTicket();
				listaPeliculas = objGestor.listarPeliculaTitulo();
			} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {
				System.out.println(error.getMessage());
			}
			boolean vacio = true;
			for (itfProperty ticket : listaTickets) {
				int idSocioTicket = (int) ticket.getProperty(TICKET_COL_SOCIO_FK);
				if (idSocioActual == idSocioTicket) {
					for (itfProperty pelicula : listaPeliculas) {
						if (pelicula.getProperty(PELICULA_COL_ID) == ticket.getProperty(TICKET_COL_PELICULA_FK)) {
							texto += "Has comprado la pelicula " + pelicula.getProperty(PELICULA_COL_TITULO)
									+ ", con fecha: " + ticket.getProperty(TICKET_COL_FECHA_COMPRA) + "\n";
							vacio = false;
							break;
						}
					}
				}

			}

			if (vacio) {
				texto = "No has comprado ninguna pelicula.";
			}

			explicacionPeliculasCompradas = new JLabel("Estas son las pel�culas que has comprado:");
			explicacionPeliculasCompradas.setBounds(500, 100, 300, 23);
			explicacionPeliculasCompradas.setVerticalAlignment(SwingConstants.CENTER);
			explicacionPeliculasCompradas.setHorizontalAlignment(SwingConstants.CENTER);
			textAreaPeliculasCompradas = new JTextArea(texto);
			textAreaPeliculasCompradas.setBounds(500, 150, 550, 223);
			textAreaPeliculasCompradas.setAlignmentX(SwingConstants.CENTER);
			textAreaPeliculasCompradas.setAlignmentY(SwingConstants.CENTER);
			textAreaPeliculasCompradas.setEditable(false);
			textAreaPeliculasCompradas.setWrapStyleWord(true);
			textAreaPeliculasCompradas.setLineWrap(true);
			// A�adir scroll al textarea
			scrollPeliculasCompradas = new JScrollPane(textAreaPeliculasCompradas); // Objeto
			scrollPeliculasCompradas.setBounds(500, 150, 550, 223); // Misma coordenadas y tama�o que el objeto

			desktop.add(explicacionPeliculasCompradas);
			desktop.add(textAreaPeliculasCompradas);
			desktop.add(scrollPeliculasCompradas);
			explicacionPeliculasCompradas.setVisible(true);
			textAreaPeliculasCompradas.setVisible(true);
			scrollPeliculasCompradas.setVisible(true);

			break;
		}
		case CONSULTA_SOPORTE: {
			// Para que el socio pueda abrir un ticket de consulta en soporte
			explicacionConsultaSoporte = new JLabel("Que problema tiene?. Indica su incidencia: ");
			explicacionConsultaSoporte.setBounds(500, 100, 300, 23);
			explicacionConsultaSoporte.setVerticalAlignment(SwingConstants.CENTER);
			explicacionConsultaSoporte.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionConsultaSoporte);

			textAreaConsultaSoporte = new JTextArea("");
			textAreaConsultaSoporte.setBounds(375, 150, 550, 135);
			textAreaConsultaSoporte.setAlignmentX(SwingConstants.CENTER);
			textAreaConsultaSoporte.setAlignmentY(SwingConstants.CENTER);
			textAreaConsultaSoporte.setEditable(true);
			textAreaConsultaSoporte.setBackground(Color.white);
			Border border = BorderFactory.createLineBorder(Color.BLACK);
			textAreaConsultaSoporte.setBorder(
					BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10)));
			textAreaConsultaSoporte.setWrapStyleWord(true);
			textAreaConsultaSoporte.setLineWrap(true);

			// A�adir scroll al textarea
			scrollConsultaSoporte = new JScrollPane(textAreaConsultaSoporte); // Objeto
			scrollConsultaSoporte.setBounds(375, 150, 550, 135); // Misma coordenadas y tama�o que el objeto

			botonAceptarConsultaSoporte = new JButton("Enviar");
			botonAceptarConsultaSoporte.addActionListener(this);
			botonAceptarConsultaSoporte.setActionCommand(BOTON_ACEPTAR_CONSULTA_SOPORTE);
			botonAceptarConsultaSoporte.setBounds(550, 350, 200, 23);

			desktop.add(textAreaConsultaSoporte);
			desktop.add(botonAceptarConsultaSoporte);
			desktop.add(scrollConsultaSoporte);
			textAreaConsultaSoporte.setVisible(true);
			scrollConsultaSoporte.setVisible(true);
			botonAceptarConsultaSoporte.setVisible(true);

			break;
		}
		case BOTON_ACEPTAR_CONSULTA_SOPORTE: {

			String incidencia = textAreaConsultaSoporte.getText();
			int idSocio = (int) usuarioActual.getProperty(SOCIO_COL_ID);
			LocalDate fechaHoy = LocalDate.now();

			// lo inserto en un arraylist //decidir que hacer con el 3
			int retorno = -1;
			try {
				retorno = objGestor.insertarConsulta(3, idSocio, fechaHoy.toString(), incidencia, 0);
			} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate
					| clsErrorSQLConexion error) {
				System.out.println(error.getMessage());
			}
			if (retorno == 1) {
				String texto = "";
				texto += "Consulta insertada correctamente" + ".\n";
				texto += "Fecha incidencia: " + fechaHoy.toString() + ".\n";
				texto += "Incidencia: " + incidencia + ".\n";
				texto += "Socio que ha creado la incidencia: " + usuarioActual.getProperty("nombre") + ".\n";
				texto += "Le enviar�n la respuesta a su correo electr�nico. Gracias.";
				JOptionPane.showMessageDialog(null, texto);
			} else {
				JOptionPane.showMessageDialog(null, "Consulta no insertada correctamente");
			}
			break;
		}
		case EDITAR_PERFIL: {
			// recogemos los datos
			String nombreUsuario = (String) usuarioActual.getProperty(USUARIO_COL_USER_NAME);
			String password = (String) usuarioActual.getProperty(USUARIO_COL_PASSWORD);
			String nombre = (String) usuarioActual.getProperty(USUARIO_COL_NOMBRE);
			String apellidos = (String) usuarioActual.getProperty(USUARIO_COL_APELLIDOS);
			String fechaNacimiento = (String) usuarioActual.getProperty(USUARIO_COL_FECHA_NACIMIENTO).toString();
			String email = (String) usuarioActual.getProperty(USUARIO_COL_EMAIL);
			String telefono = (String) usuarioActual.getProperty(USUARIO_COL_TELEFONO);
			String dni = (String) usuarioActual.getProperty(USUARIO_COL_DNI);
			int idSocio = (int) usuarioActual.getProperty(SOCIO_COL_ID);
			int tarifaFK = (int) usuarioActual.getProperty(SOCIO_COL_TARIFA_FK);
			System.out.println(usuarioActual.getProperty(SOCIO_COL_ESTADO));
			boolean estado = (boolean) usuarioActual.getProperty(SOCIO_COL_ESTADO);
			String fechaTarifa = (String) usuarioActual.getProperty(SOCIO_COL_FECHA_TARIFA).toString();
			String estadoString = "0 - No disponible";

			if (estado) {
				estadoString = "1 - Disponible";
			}

			explicacionNombreUsuario = new JLabel("Nombre de usuario:");
			explicacionNombreUsuario.setBounds(375, 100, 200, 30);
			explicacionNombreUsuario.setVerticalAlignment(SwingConstants.CENTER);
			explicacionNombreUsuario.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionNombreUsuario);
			explicacionNombreUsuario.setVisible(true);
			perfilNombreUsuario = new JTextField(nombreUsuario);
			perfilNombreUsuario.setBounds(575, 100, 200, 30);
			desktop.add(perfilNombreUsuario);

			explicacionPassword = new JLabel("Contrase�a:");
			explicacionPassword.setBounds(375, 130, 200, 30);
			explicacionPassword.setVerticalAlignment(SwingConstants.CENTER);
			explicacionPassword.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionPassword);
			explicacionPassword.setVisible(true);
			perfilPassword = new JTextField(password);
			perfilPassword.setBounds(575, 130, 200, 30);
			desktop.add(perfilPassword);

			explicacionNombre = new JLabel("Nombre:");
			explicacionNombre.setBounds(375, 160, 200, 30);
			explicacionNombre.setVerticalAlignment(SwingConstants.CENTER);
			explicacionNombre.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionNombre);
			explicacionNombre.setVisible(true);
			perfilNombre = new JTextField(nombre);
			perfilNombre.setBounds(575, 160, 200, 30);
			desktop.add(perfilNombre);

			explicacionApellidos = new JLabel("Apellidos:");
			explicacionApellidos.setBounds(375, 190, 200, 30);
			explicacionApellidos.setVerticalAlignment(SwingConstants.CENTER);
			explicacionApellidos.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionApellidos);
			explicacionApellidos.setVisible(true);
			perfilApellidos = new JTextField(apellidos);
			perfilApellidos.setBounds(575, 190, 200, 30);
			desktop.add(perfilApellidos);

			explicacionFechaNacimiento = new JLabel("Fecha de Nacimiento:");
			explicacionFechaNacimiento.setBounds(375, 220, 200, 30);
			explicacionFechaNacimiento.setVerticalAlignment(SwingConstants.CENTER);
			explicacionFechaNacimiento.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionFechaNacimiento);
			explicacionFechaNacimiento.setVisible(true);
			perfilFechaNacimiento = new JTextField(fechaNacimiento);
			perfilFechaNacimiento.setBounds(575, 220, 200, 30);
			desktop.add(perfilFechaNacimiento);

			explicacionEmail = new JLabel("Email:");
			explicacionEmail.setBounds(375, 250, 200, 30);
			explicacionEmail.setVerticalAlignment(SwingConstants.CENTER);
			explicacionEmail.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionEmail);
			explicacionEmail.setVisible(true);
			perfilEmail = new JTextField(email);
			perfilEmail.setBounds(575, 250, 200, 30);
			desktop.add(perfilEmail);

			explicacionTelefono = new JLabel("Tel�fono:");
			explicacionTelefono.setBounds(375, 280, 200, 30);
			explicacionTelefono.setVerticalAlignment(SwingConstants.CENTER);
			explicacionTelefono.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionTelefono);
			explicacionTelefono.setVisible(true);
			perfilTelefono = new JTextField(telefono);
			perfilTelefono.setBounds(575, 280, 200, 30);
			desktop.add(perfilTelefono);

			explicacionDni = new JLabel("DNI: ");
			explicacionDni.setBounds(375, 310, 200, 30);
			explicacionDni.setVerticalAlignment(SwingConstants.CENTER);
			explicacionDni.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionDni);
			explicacionDni.setVisible(true);
			perfilDni = new JTextField(dni);
			perfilDni.setBounds(575, 310, 200, 30);
			desktop.add(perfilDni);

			explicacionIdSocio = new JLabel("N�mero de abonado:");
			explicacionIdSocio.setBounds(375, 340, 200, 30);
			explicacionIdSocio.setVerticalAlignment(SwingConstants.CENTER);
			explicacionIdSocio.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionIdSocio);
			explicacionIdSocio.setVisible(true);
			perfilIdSocio = new JTextField(idSocio);
			perfilIdSocio.setBounds(575, 340, 200, 30);
			perfilIdSocio.setEditable(false);
			desktop.add(perfilIdSocio);

			explicacionEstado = new JLabel("Estado de abonado:");
			explicacionEstado.setBounds(375, 370, 200, 30);
			explicacionEstado.setVerticalAlignment(SwingConstants.CENTER);
			explicacionEstado.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionEstado);
			explicacionEstado.setVisible(true);
			perfilEstado = new JTextField(estadoString);
			perfilEstado.setBounds(575, 370, 200, 30);
			perfilEstado.setEditable(false);
			desktop.add(perfilEstado);

			explicacionFechaTarifa = new JLabel("Fecha de tarifa:");
			explicacionFechaTarifa.setBounds(375, 400, 200, 30);
			explicacionFechaTarifa.setVerticalAlignment(SwingConstants.CENTER);
			explicacionFechaTarifa.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionFechaTarifa);
			explicacionFechaTarifa.setVisible(true);
			perfilFechatarifa = new JTextField(fechaTarifa);
			perfilFechatarifa.setBounds(575, 400, 200, 30);
			perfilFechatarifa.setEditable(false);
			desktop.add(perfilFechatarifa);

			botonAceptarEditarPerfil = new JButton("Guardar");
			botonAceptarEditarPerfil.addActionListener(this);
			botonAceptarEditarPerfil.setActionCommand(BOTON_ACEPTAR_EDITAR_PERFIL);
			botonAceptarEditarPerfil.setBounds(500, 475, 200, 23);
			desktop.add(botonAceptarEditarPerfil);
			botonAceptarEditarPerfil.setVisible(true);

			explicacionEditarPerfil = new JLabel("Modifica los datos de tu perfil");
			explicacionEditarPerfil.setBounds(350, 50, 500, 30);
			explicacionEditarPerfil.setVerticalAlignment(SwingConstants.CENTER);
			explicacionEditarPerfil.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionEditarPerfil);
			explicacionEditarPerfil.setVisible(true);

			break;
		}
		case BOTON_ACEPTAR_EDITAR_PERFIL: {

			int tarifaFK = (int) usuarioActual.getProperty(SOCIO_COL_TARIFA_FK);
			boolean estado = (boolean) usuarioActual.getProperty(SOCIO_COL_ESTADO);
			int idSocio = (int) usuarioActual.getProperty(SOCIO_COL_ID);


			int retorno = -1;
			try {
				retorno = objGestor.editarSocio(perfilNombreUsuario.getText(), perfilPassword.getText(),
						perfilNombre.getText(), perfilApellidos.getText(), perfilFechaNacimiento.getText(),
						perfilEmail.getText(), perfilTelefono.getText(), perfilDni.getText(), idSocio,
						tarifaFK, estado, perfilFechatarifa.getText());
			} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate
					| clsErrorSQLConexion error) {

				System.out.println(error.getMessage());
			}
			if (retorno == 1) {
				JOptionPane.showMessageDialog(null, "Socio editado correctamente.");
			} else {
				JOptionPane.showMessageDialog(null, "Error editando socio.");

			}

			break;
		}
		case DARSE_BAJA: {
			explicacionDarseBaja = new JLabel("�Est�s seguro/a que deseas darte de baja?");
			explicacionDarseBaja.setBounds(500, 100, 300, 23);
			explicacionDarseBaja.setVerticalAlignment(SwingConstants.CENTER);
			explicacionDarseBaja.setHorizontalAlignment(SwingConstants.CENTER);
			desktop.add(explicacionDarseBaja);
			explicacionDarseBaja.setVisible(true);

			botonDarseBaja = new JButton("S�, estoy seguro/a");
			botonDarseBaja.addActionListener(this);
			botonDarseBaja.setActionCommand(BOTON_ACEPTAR_DARSE_BAJA);
			botonDarseBaja.setBounds(550, 150, 200, 23);
			desktop.add(botonDarseBaja);
			botonDarseBaja.setVisible(true);
			break;
		}
		case BOTON_ACEPTAR_DARSE_BAJA: {
			int idSocioActual = (int) usuarioActual.getProperty(SOCIO_COL_ID);
			boolean estadoActual = (boolean) usuarioActual.getProperty(SOCIO_COL_ESTADO);
			try {
				if (objGestor.cambiarEstadoSocio(idSocioActual, estadoActual)) {
					JOptionPane.showMessageDialog(null, "Estado cambiado. \n Vuelve a iniciar sesi�n");
					desktop.setVisible(false);
					this.dispose();
					clsMain.main(null);
				} else {
					JOptionPane.showMessageDialog(null, "Problemas cambiando el estado del socio int�ntelo de nuevo.");
				}
			} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate
					| clsErrorSQLConexion error) {
				System.out.println(error.getMessage());
			}
			break;
		}
		}
	}

	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {
		// TODO Auto-generated method stub
		System.out.println(e);

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}
}
