package LP;

import static COMUN.clsConstantes.*;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;
import LN.clsGestorLN;

/**
 * ventana internal para la muestra de la lista de peliculas de la base de datos
 * 
 * @author Eric
 *
 */
public class frmInListarPeliculas extends JInternalFrame implements ActionListener, InternalFrameListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6360330611055663487L;
	/**
	 * objeto gestor de la logica de negocio
	 */
	private clsGestorLN objGestor;
	/**
	 * usuario actual conectado
	 */
	private itfProperty usuarioActual;
	/**
	 * escritorio para las internalFrame
	 */
	private JDesktopPane desktop;
	/**
	 * tabla de la lista de las peliculas
	 */
	private JTable tablaPeliculas;
	/**
	 * modelo de la tabla de la lista de peliculas
	 */
	private DefaultTableModel modeloTablaPeliculas;
	/**
	 * lista de las peliculas de la BBDD
	 */
	private ArrayList<itfProperty> listaPeliculas;
	/**
	 * dimensiones de la ventana padre
	 */
	private Rectangle desktopPadreDim;
	/**
	 * objeto internalFrame para ver mas datos de la pelicula
	 */
	private frmInVerPelicula objVerPelicula;
	/**
	 * objeto internalFrame para editar la pelicula
	 */
	private frmInUpdatePelicula objUpdatePelicula;
	/**
	 * objeto internalFrame para insertar la pelicula
	 */
	private frmInInsertPelicula objInsertPelicula;

	/**
	 * constructor de la internal frame insertar Pelicula
	 * 
	 * @param objGestor       objeto gestor de la logica de negocio
	 * @param usuarioActual   usuario actual conectado
	 * @param desktopPadreDim dimensiones de la ventana padre
	 * @param titulo          titulo de la internal frame
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmInListarPeliculas(clsGestorLN objGestor, itfProperty usuarioActual, Rectangle desktopPadreDim,
			String titulo, boolean redimensionable, boolean cerrable, boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.objGestor = objGestor;
		this.usuarioActual = usuarioActual;
		this.desktopPadreDim = desktopPadreDim;
		this.CrearPanel();

	}

	/**
	 * Create the frame.
	 */
	private void CrearPanel() {
		setBounds(desktopPadreDim);

		desktop = new JDesktopPane();
		desktop.setBounds(desktopPadreDim);
		getContentPane().add(desktop);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 30, 603, 399);
		desktop.add(scrollPane);

		tablaPeliculas = new JTable();
		scrollPane.setViewportView(tablaPeliculas);

		modeloTablaPeliculas = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -8347897486103084838L;

			/**
			 * funcion que evita editar columnas en la tabla
			 */
			@Override
			public boolean isCellEditable(int filas, int columnas) {
				if (columnas == 8) {
					return true;
				} else {
					return false;
				}

			}
		};

		modeloTablaPeliculas.addColumn(PELICULA_COL_TITULO);
		modeloTablaPeliculas.addColumn(PELICULA_COL_DESCRIPCION);
		modeloTablaPeliculas.addColumn(PELICULA_COL_PROVEEDOR_FK);
		modeloTablaPeliculas.addColumn(PELICULA_COL_DURACION);

		listaPeliculas = new ArrayList<itfProperty>();
		try {
			listaPeliculas = objGestor.listarPeliculaTitulo();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {

			JOptionPane.showMessageDialog(this, error.getMessage());

		}
		for (itfProperty aux : listaPeliculas) {
			try {
				itfProperty proveedor = objGestor.devolverProveedor((int) aux.getProperty(PELICULA_COL_PROVEEDOR_FK));

				modeloTablaPeliculas.addRow(
						new Object[] { aux.getProperty(PELICULA_COL_TITULO), aux.getProperty(PELICULA_COL_DESCRIPCION),
								proveedor.getProperty(PROVEEDOR_COL_NOMBRE), aux.getProperty(PELICULA_COL_DURACION) });
			} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
				JOptionPane.showMessageDialog(this, e.getMessage());
			}
		}
		tablaPeliculas.setModel(modeloTablaPeliculas);
		tablaPeliculas.setRowHeight(30);
		tablaPeliculas.setAutoCreateRowSorter(true);
		tablaPeliculas.setPreferredScrollableViewportSize(new Dimension(500, 300));
		tablaPeliculas.setFillsViewportHeight(true);
		// Se llama al metodo que ajusta las columnas
		redimensionarAnchoColumna(tablaPeliculas);
		// Se desactiva el Auto Resize de la tabla
		// Es importante que vaya despues de el metodo que ajusta el ancho de la columna
		tablaPeliculas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		JButton btnActualizarPelicula = new JButton("Actualizar");
		btnActualizarPelicula.addActionListener(this);
		btnActualizarPelicula.setActionCommand(EDITAR_PELICULA);
		btnActualizarPelicula.setBounds(681, 103, 110, 23);
		desktop.add(btnActualizarPelicula);

		JButton btnInsertarPelicula = new JButton("Insertar");
		btnInsertarPelicula.addActionListener(this);
		btnInsertarPelicula.setActionCommand(CREAR_PELICULA);
		btnInsertarPelicula.setBounds(681, 152, 110, 23);
		desktop.add(btnInsertarPelicula);
		JButton btnEliminarPelicula = new JButton("Eliminar");
		btnEliminarPelicula.addActionListener(this);
		btnEliminarPelicula.setActionCommand(BORRAR_PELICULA);
		btnEliminarPelicula.setBounds(681, 192, 110, 23);
		desktop.add(btnEliminarPelicula);

		JButton btnVerPelicula = new JButton("Ver mas");
		btnVerPelicula.addActionListener(this);
		btnVerPelicula.setActionCommand(VER_PELICULA);
		btnVerPelicula.setBounds(681, 236, 110, 23);
		desktop.add(btnVerPelicula);

	}

	/**
	 * Metodo que ajusta el ancho de la columna de una tabla segun su contenido
	 * 
	 * @param tabla tabla a editar
	 */
	private void redimensionarAnchoColumna(JTable tabla) {
		// Se obtiene el modelo de la columna
		TableColumnModel columnModel = tabla.getColumnModel();
		// Se obtiene el total de las columnas
		for (int column = 0; column < tabla.getColumnCount(); column++) {
			// Establecemos un valor minimo para el ancho de la columna
			int width = 100; // Min Width
			// Obtenemos el numero de filas de la tabla
			for (int row = 0; row < tabla.getRowCount(); row++) {
				// Obtenemos el renderizador de la tabla
				TableCellRenderer renderer = tabla.getCellRenderer(row, column);
				// Creamos un objeto para preparar el renderer
				Component comp = tabla.prepareRenderer(renderer, row, column);
				// Establecemos el width segun el valor maximo del ancho de la columna
				width = Math.max(comp.getPreferredSize().width + 1, width);

			}
			// Se establece una condicion para no sobrepasar el valor de 300
			// Esto es Opcional
			if (width > 300) {
				width = 300;
			}
			// Se establece el ancho de la columna
			columnModel.getColumn(column).setPreferredWidth(width);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case BORRAR_PELICULA: {
			if (tablaPeliculas.getSelectedRow() == -1) {
				JOptionPane.showMessageDialog(this, "Elige una pelicula");
			} else {
				String titulo = (String) modeloTablaPeliculas.getValueAt(tablaPeliculas.getSelectedRow(), 0);
				String nombreProveedor = (String) modeloTablaPeliculas.getValueAt(tablaPeliculas.getSelectedRow(), 2);
				try {
					itfProperty proveedor = objGestor.devolverProveedorDePelicula(titulo, nombreProveedor);
					borrarPelicula(titulo, (int) proveedor.getProperty(PROVEEDOR_COL_ID));
					modeloTablaPeliculas.removeRow(tablaPeliculas.getSelectedRow());
				} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {
					JOptionPane.showMessageDialog(this, error.getMessage());
				}
			}
			break;
		}
		case CREAR_PELICULA: {
			crearPelicula();
			break;
		}
		case EDITAR_PELICULA: {
			if (tablaPeliculas.getSelectedRow() == -1) {
				JOptionPane.showMessageDialog(this, "Elige una pelicula");
			} else {
				editarPelicula();
			}
			break;
		}
		case VER_PELICULA: {
			if (tablaPeliculas.getSelectedRow() == -1) {
				JOptionPane.showMessageDialog(this, "Elige una pelicula");
			} else {
				verPelicula();
			}
			break;
		}
		}
	}

	/**
	 * funcion que crea un InternalFrame para crear una pelicula
	 */
	private void crearPelicula() {
		if (objInsertPelicula != null) {
			objInsertPelicula.dispose();
		}
		objInsertPelicula = new frmInInsertPelicula(objGestor, usuarioActual, INSERTAR + " " + PELICULA, false, true,
				false, false);
		desktop.add(objInsertPelicula);
		objInsertPelicula.addInternalFrameListener(this);
		Dimension desktopSize = desktop.getSize();
		Dimension FrameSize = objInsertPelicula.getSize();
		objInsertPelicula.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objInsertPelicula.show();
	}

	/**
	 * funcion que crea un InternalFrame para edita una pelicula
	 */
	private void editarPelicula() {
		String titulo = (String) modeloTablaPeliculas.getValueAt(tablaPeliculas.getSelectedRow(), 0);
		String proveedorNombre = (String) modeloTablaPeliculas.getValueAt(tablaPeliculas.getSelectedRow(), 2);
		itfProperty proveedor;
		itfProperty pelicula = null;
		try {
			proveedor = objGestor.devolverProveedorNombre(proveedorNombre);
			pelicula = objGestor.devolverPeliculaTituloProveedor(titulo, (int) proveedor.getProperty(PROVEEDOR_COL_ID));
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		if (objUpdatePelicula != null) {
			objUpdatePelicula.dispose();
		}
		objUpdatePelicula = new frmInUpdatePelicula(objGestor, pelicula, EDITAR + " " + PELICULA, false, true, false,
				false);
		desktop.add(objUpdatePelicula);
		objUpdatePelicula.addInternalFrameListener(this);
		Dimension desktopSize = desktop.getSize();
		Dimension FrameSize = objUpdatePelicula.getSize();
		objUpdatePelicula.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objUpdatePelicula.show();
	}

	/**
	 * funcion que crea un InternalFrame para ver mas datos de una pelicula
	 */
	private void verPelicula() {
		String titulo = (String) modeloTablaPeliculas.getValueAt(tablaPeliculas.getSelectedRow(), 0);
		String proveedorNombre = (String) modeloTablaPeliculas.getValueAt(tablaPeliculas.getSelectedRow(), 2);
		itfProperty proveedor;
		itfProperty pelicula = null;
		try {
			proveedor = objGestor.devolverProveedorNombre(proveedorNombre);
			pelicula = objGestor.devolverPeliculaTituloProveedor(titulo, (int) proveedor.getProperty(PROVEEDOR_COL_ID));
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		if (objVerPelicula != null) {
			objVerPelicula.dispose();
		}
		objVerPelicula = new frmInVerPelicula(objGestor, pelicula, VER + " " + PELICULA, false, true, false, false);
		desktop.add(objVerPelicula);
		objVerPelicula.addInternalFrameListener(this);
		Dimension desktopSize = desktop.getSize();
		Dimension FrameSize = objVerPelicula.getSize();
		objVerPelicula.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objVerPelicula.show();
	}

	/**
	 * funcion que borra una pelicula
	 * 
	 * @param titulo    titulo de la pelicula
	 * @param proveedor id del proveedor
	 */
	private void borrarPelicula(String titulo, int proveedor) {
		itfProperty peliculaBuscar = null;
		try {
			peliculaBuscar = objGestor.devolverPeliculaTituloProveedor(titulo, proveedor);
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage());
		}

		if (peliculaBuscar.getProperty(PELICULA_COL_PORTADA) != null) {
			File portada = new File(PORTADA + "\\" + peliculaBuscar.getProperty(PELICULA_COL_PORTADA));
			portada.delete();
		}
		try {
			objGestor.borrarPelicula((int) peliculaBuscar.getProperty(PELICULA_COL_ID));
		} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion | clsErrorSQLConsulta e) {
			JOptionPane.showMessageDialog(this, e.getMessage());

		}
		JOptionPane.showMessageDialog(this, "pelicula borrada");
	}

	/**
	 * funcion que escucha las acciones de esta ventana cuando se abre un
	 * JInternalFrame
	 */
	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		switch (e.getSource().getClass().getName()) {
		case INTERNAL_INSERT_PELICULA: {
			if (objUpdatePelicula != null) {
				objUpdatePelicula.dispose();
			}
			if (objVerPelicula != null) {
				objVerPelicula.dispose();
			}
			break;
		}
		case INTERNAL_UPDATE_PELICULA: {
			if (objInsertPelicula != null) {
				objInsertPelicula.dispose();
			}
			if (objVerPelicula != null) {
				objVerPelicula.dispose();
			}
			break;
		}
		case INTERNAL_VER_PELICULA: {
			if (objInsertPelicula != null) {
				objInsertPelicula.dispose();
			}
			if (objUpdatePelicula != null) {
				objUpdatePelicula.dispose();
			}
			break;
		}
		}
	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		switch (e.getSource().getClass().getName()) {
		case INTERNAL_INSERT_PELICULA: {
			itfProperty pelicula = objInsertPelicula.devolverPelicula();
			if (pelicula != null) {
				itfProperty proveedor = null;
				try {
					proveedor = objGestor.devolverProveedor((int) pelicula.getProperty(PELICULA_COL_PROVEEDOR_FK));
				} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {
					JOptionPane.showMessageDialog(this, error.getMessage());
				}
				modeloTablaPeliculas.addRow(new Object[] { pelicula.getProperty(PELICULA_COL_TITULO),
						pelicula.getProperty(PELICULA_COL_DESCRIPCION), proveedor.getProperty(PROVEEDOR_COL_NOMBRE),
						pelicula.getProperty(PELICULA_COL_DURACION) });
			}
			break;
		}
		case INTERNAL_UPDATE_PELICULA: {
			listaPeliculas = new ArrayList<itfProperty>();
			try {
				listaPeliculas = objGestor.listarPeliculaTitulo();
			} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {

				JOptionPane.showMessageDialog(this, error.getMessage());

			}
			int filas = modeloTablaPeliculas.getRowCount();
			for (int i = 0; i < filas; i++) {
				modeloTablaPeliculas.removeRow(0);
			}
			for (itfProperty aux : listaPeliculas) {
				try {
					itfProperty proveedor = objGestor
							.devolverProveedor((int) aux.getProperty(PELICULA_COL_PROVEEDOR_FK));

					modeloTablaPeliculas.addRow(new Object[] { aux.getProperty(PELICULA_COL_TITULO),
							aux.getProperty(PELICULA_COL_DESCRIPCION), proveedor.getProperty(PROVEEDOR_COL_NOMBRE),
							aux.getProperty(PELICULA_COL_DURACION) });
				} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {
					JOptionPane.showMessageDialog(this, error.getMessage());
				}
			}
			break;
		}
		}
	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}
}
