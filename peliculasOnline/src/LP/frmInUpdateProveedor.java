package LP;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;

import static COMUN.clsConstantes.*;
import LN.clsGestorLN;

/**
 * Ventana interna para cuando hay que hacer una update de proveedor
 * 
 * @author Eric
 *
 */
public class frmInUpdateProveedor extends JInternalFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5076220575436347664L;
	/**
	 * panel donde ira todo el contenido de la ventana interna
	 */
	JPanel jpContent;
	/**
	 * usuario actual conectado
	 */
	private clsGestorLN objGestor;
	/**
	 * texto donde se guarda el nombre
	 */
	private JTextField txtNombre;
	/**
	 * texto donde se guarda el email
	 */
	private JTextField txtEmail;
	/**
	 * texto donde se guarda el telefono
	 */
	private JTextField txtTelefono;
	/**
	 * datos del proveedor a editar
	 */
	private itfProperty proveedor;

	/**
	 * constructor de la internal frame
	 * 
	 * @param objGestor       objeto gestor de la logica de negocio
	 * @param titulo          titulo de la internal frame
	 * @param proveedor       itfProperty del proveedor a editar
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmInUpdateProveedor(clsGestorLN objGestor, String titulo, itfProperty proveedor, boolean redimensionable,
			boolean cerrable, boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.proveedor = proveedor;
		this.objGestor = objGestor;
		this.CrearPanel();

	}

	/**
	 * funcion que rellean el panel con los componentes pertinentes
	 */
	private void CrearPanel() {
		setBounds(100, 100, 450, 300);
		jpContent = new JPanel();
		jpContent.setBackground(Color.WHITE);
		jpContent.repaint();

		this.setContentPane(jpContent);
		jpContent.setLayout(null);
		JLabel lblTitulo = new JLabel("Editar Proveedor", SwingConstants.CENTER);
		lblTitulo.setBounds(152, 11, 110, 14);
		jpContent.add(lblTitulo);

		JLabel lblNombre = new JLabel("Nombre", SwingConstants.CENTER);
		lblNombre.setBounds(159, 44, 96, 14);
		jpContent.add(lblNombre);

		JLabel lblEmail = new JLabel("Email", SwingConstants.CENTER);
		lblEmail.setBounds(159, 100, 96, 14);
		jpContent.add(lblEmail);

		JLabel lblTelefono = new JLabel("Telefono", SwingConstants.CENTER);
		lblTelefono.setBounds(159, 156, 96, 14);
		jpContent.add(lblTelefono);

		txtNombre = new JTextField();
		txtNombre.setBounds(159, 69, 96, 20);
		txtNombre.setText((String) proveedor.getProperty(PROVEEDOR_COL_NOMBRE));
		jpContent.add(txtNombre);
		txtNombre.setColumns(10);

		txtEmail = new JTextField();
		txtEmail.setBounds(159, 125, 96, 20);
		txtEmail.setText((String) proveedor.getProperty(PROVEEDOR_COL_EMAIL));
		jpContent.add(txtEmail);
		txtEmail.setColumns(10);

		txtTelefono = new JTextField();
		txtTelefono.setBounds(159, 181, 96, 20);
		txtTelefono.setText((String) proveedor.getProperty(PROVEEDOR_COL_TELEFONO));
		jpContent.add(txtTelefono);
		txtTelefono.setColumns(10);

		JButton btnEditar = new JButton("Editar");
		btnEditar.setBounds(159, 220, 96, 23);
		btnEditar.addActionListener(this);
		btnEditar.setActionCommand(EDITAR);
		jpContent.add(btnEditar);
	}

	/**
	 * editar proveedor
	 */
	private void editarProveedor() {
		try {
			String nombre = txtNombre.getText();
			String email = txtEmail.getText();
			String telefono = txtTelefono.getText();
			int retorno = objGestor.editarProveedor(nombre, email, telefono,
					(int) proveedor.getProperty(PROVEEDOR_COL_ID));
			if (retorno == 1) {
				JOptionPane.showMessageDialog(this, "Proveedor editado correctamente");
			}
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * escuchador de eventos
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case EDITAR: {
			editarProveedor();
			this.dispose();
			break;
		}

		}
	}
}
