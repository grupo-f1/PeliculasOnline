package LP;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * JInternaFrame que busca una imagen para la portada de la pelicula
 * 
 * @author Eric
 *
 */
public class frmArchivoPortada extends JInternalFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8502254586751170935L;

	/**
	 * salto de linea
	 */
	private String newline = "\n";
	/**
	 * area de texto de las acciones del usuario
	 */
	private JTextArea log;
	/**
	 * escogedor de archivos
	 */
	private JFileChooser fc;
	/**
	 * imagen elegida
	 */
	private File file;
	/**
	 * label de la portada donde ira el nombre de la imagen
	 */
	private JLabel lblPortada;

	/**
	 * constructor de la internal frame de el selector de archivos para la portada
	 * 
	 * @param lblPortada      label donde ira el nombre de la imagen de portada
	 * @param titulo          titulo de la internal frame
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmArchivoPortada(JLabel lblPortada, String titulo, boolean redimensionable, boolean cerrable,
			boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.lblPortada = lblPortada;
		this.crearPanel();
	}

	/**
	 * creacion del panel
	 */
	private void crearPanel() {

		// Create the log first, because the action listener
		// needs to refer to it.
		log = new JTextArea(5, 20);
		log.setMargin(new Insets(5, 5, 5, 5));
		log.setEditable(false);
		JScrollPane logScrollPane = new JScrollPane(log);

		JButton sendButton = new JButton("Insertar...");
		sendButton.addActionListener(this);

		add(sendButton, BorderLayout.PAGE_START);
		add(logScrollPane, BorderLayout.CENTER);
	}

	/**
	 * escuchador de eventos de la ventana
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// Set up the file chooser.
		if (fc == null) {
			fc = new JFileChooser();

			// Add a custom file filter and disable the default
			// (Accept All) file filter.
			fc.addChoosableFileFilter(new ImageFilter());
			fc.setAcceptAllFileFilterUsed(false);

			// Add custom icons for file types.
			fc.setFileView(new ImageFileView());

			// Add the preview pane.
			fc.setAccessory(new ImagePreview(fc));
		}

		// Show it.
		int returnVal = fc.showDialog(frmArchivoPortada.this, "Portada");

		// Process the results.
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
			log.append("Eligiendo portada: " + file.getName() + "." + newline);
			updateLabel(file.getName());

		} else {
			log.append("Eleccion de portada cancelado por usuario." + newline);
		}
		log.setCaretPosition(log.getDocument().getLength());

		// Reset the file chooser for the next time it's shown.
		fc.setSelectedFile(null);
	}

	/**
	 * actualiza el nombre de la imagen a mostrar
	 * 
	 * @param name nombre de la imagen a cambiar
	 */
	private void updateLabel(String name) {
		ImageIcon icon = createImageIcon("Portada/" + name + ".gif");
		if (icon != null) {
			lblPortada.setText(null);
			lblPortada.setToolTipText(file.getName());
			ImageIcon thumbnail = null;
			if (icon.getIconWidth() > 90) {
				thumbnail = new ImageIcon(icon.getImage().getScaledInstance(90, -1, Image.SCALE_DEFAULT));
			} else if (icon.getIconHeight() > 50) {
				thumbnail = new ImageIcon(icon.getImage().getScaledInstance(-1, 50, Image.SCALE_DEFAULT));
			} else { // no need to miniaturize
				thumbnail = icon;
			}
			lblPortada.setIcon(thumbnail);
		} else {
			lblPortada.setText("Portada no encontrada");
		}
	}

	/**
	 * devuelve la imagen o un mensaje de no encontrado
	 * 
	 * @param path ruta a la imagen
	 * @return imagen a mostrar
	 */
	private ImageIcon createImageIcon(String path) {
		String imgURL = file.getPath();
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("No es ha encontrado el archivo: " + path);
			return null;
		}
	}

	/**
	 * funcion que devuelve el archvio
	 * 
	 * @return archivo a devolver
	 */
	public File cogerFile() {
		return file;

	}

}