package LP;

import static COMUN.clsConstantes.*;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import LN.clsGestorLN;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JRadioButton;
import javax.swing.JDesktopPane;

/**
 * ventana interna para vista de los datos de pelicula
 * 
 * @author Eric
 *
 */
public class frmInVerPelicula extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8887959241602632300L;

	/**
	 * objeto gestor de la logica de negocio
	 */
	private clsGestorLN objGestor;

	/**
	 * pelicula a editar
	 */
	private itfProperty peliculaEditar;
	/**
	 * lbl donde va la imagen de la portada
	 */
	private JLabel lblPortadaContenido;

	/**
	 * constructor de la internal frame ver Pelicula
	 * 
	 * @param objGestor       objeto gestor de la logica de negocio
	 * @param pelicula        pelicula a mostrar
	 * @param titulo          titulo de la internal frame
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmInVerPelicula(clsGestorLN objGestor, itfProperty pelicula, String titulo, boolean redimensionable,
			boolean cerrable, boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.objGestor = objGestor;
		this.peliculaEditar = pelicula;
		this.CrearPanel();

	}

	/**
	 * funcion que rellean el panel con los componentes pertinentes
	 */
	private void CrearPanel() {
		setBounds(100, 100, 605, 423);

		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 595, 394);
		getContentPane().add(desktopPane);

		JLabel lblListaTiposPelicula = new JLabel("lista tipos pelicula", SwingConstants.CENTER);
		lblListaTiposPelicula.setBounds(450, 209, 118, 14);
		desktopPane.add(lblListaTiposPelicula);

		JLabel lblListaIdiomas = new JLabel("lista idiomas", SwingConstants.CENTER);
		lblListaIdiomas.setBounds(328, 209, 112, 14);
		desktopPane.add(lblListaIdiomas);

		JLabel lblProveedor = new JLabel("Proveedor", SwingConstants.CENTER);
		lblProveedor.setBounds(35, 95, 108, 14);
		desktopPane.add(lblProveedor);

		JLabel lblEmpleado = new JLabel("Empleado", SwingConstants.CENTER);
		lblEmpleado.setBounds(35, 50, 108, 14);
		desktopPane.add(lblEmpleado);

		JLabel lblTitulo = new JLabel("Titulo", SwingConstants.CENTER);
		lblTitulo.setBounds(35, 11, 108, 14);
		desktopPane.add(lblTitulo);

		JLabel lblDuracion = new JLabel("Duracion", SwingConstants.CENTER);
		lblDuracion.setBounds(35, 142, 108, 14);
		desktopPane.add(lblDuracion);

		JLabel lblDescripcion = new JLabel("Descripcion", SwingConstants.CENTER);
		lblDescripcion.setBounds(278, 11, 280, 14);
		desktopPane.add(lblDescripcion);

		JLabel lblClasificacion = new JLabel("Clasificacion", SwingConstants.CENTER);
		lblClasificacion.setBounds(35, 191, 108, 14);
		desktopPane.add(lblClasificacion);

		JLabel lblSubtitulos = new JLabel("Subtitulos", SwingConstants.CENTER);
		lblSubtitulos.setBounds(35, 242, 108, 14);
		desktopPane.add(lblSubtitulos);

		JLabel lblResolucion = new JLabel("Resolucion", SwingConstants.CENTER);
		lblResolucion.setBounds(35, 293, 108, 14);
		desktopPane.add(lblResolucion);

		JLabel lblPortada = new JLabel("Portada", SwingConstants.CENTER);
		lblPortada.setBounds(207, 151, 49, 14);
		desktopPane.add(lblPortada);

		lblPortadaContenido = new JLabel();
		lblPortadaContenido.setFont(lblPortadaContenido.getFont().deriveFont(Font.ITALIC));
		lblPortadaContenido.setHorizontalAlignment(JLabel.CENTER);
		if (peliculaEditar.getProperty(PELICULA_COL_PORTADA) != null) {
			String[] portadaSplit = peliculaEditar.getProperty(PELICULA_COL_PORTADA).toString().split("[.]");
			updateLabel(portadaSplit[0]);
		} else {
			lblPortadaContenido.setText("Sin portada");
		}
		lblPortadaContenido.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));

		// The preferred size is hard-coded to be the width of the
		// widest image and the height of the tallest image + the border.
		// A real program would compute this.
		lblPortadaContenido.setPreferredSize(new Dimension(177, 122 + 10));
		lblPortadaContenido.setBounds(163, 176, 143, 155);
		desktopPane.add(lblPortadaContenido);

		JComboBox<Object> comboProveedor = new JComboBox<Object>();
		ArrayList<itfProperty> listaProveedores = rellenarComboBoxProveedor();
		for (itfProperty aux : listaProveedores) {
			comboProveedor.addItem((String) aux.getProperty(PROVEEDOR_COL_NOMBRE));
			if (aux.getProperty(PROVEEDOR_COL_ID) == peliculaEditar.getProperty(PELICULA_COL_PROVEEDOR_FK)) {
				comboProveedor.setSelectedItem((String) aux.getProperty(PROVEEDOR_COL_NOMBRE));
			}
		}
		comboProveedor.setBounds(35, 111, 108, 20);
		comboProveedor.setEnabled(false);
		desktopPane.add(comboProveedor);

		JComboBox<Object> comboEmpleado = new JComboBox<Object>();
		ArrayList<itfProperty> listaEmpleados = rellenarComboBoxEmpleado();
		for (itfProperty aux : listaEmpleados) {
			comboEmpleado.addItem((String) aux.getProperty(USUARIO_COL_USER_NAME));
			if (aux.getProperty(EMPLEADO_COL_ID) == peliculaEditar.getProperty(PELICULA_COL_EMPLEADO_FK)) {
				comboEmpleado.setSelectedItem((String) aux.getProperty(USUARIO_COL_USER_NAME));
			}
		}
		comboEmpleado.setBounds(35, 67, 108, 20);
		comboEmpleado.setEnabled(false);

		desktopPane.add(comboEmpleado);

		DefaultListModel modelTiposPelicula = new DefaultListModel<>();
		JScrollPane scrollPanelListaTiposPelicula = new JScrollPane();
		scrollPanelListaTiposPelicula.setBounds(450, 234, 118, 97);
		desktopPane.add(scrollPanelListaTiposPelicula);

		JList<Object> tipoPeliculasElegidas = new JList<Object>();
		ArrayList<itfProperty> listaPeliculaTipos = new ArrayList<itfProperty>();
		try {
			listaPeliculaTipos = objGestor.devolverPeliculaTipos((int) peliculaEditar.getProperty(PELICULA_COL_ID));
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		ArrayList<itfProperty> listaTiposPelicula = new ArrayList<itfProperty>();
		try {
			listaTiposPelicula = objGestor.listarTipoPelicula();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		for (itfProperty aux : listaPeliculaTipos) {
			for (itfProperty tipo : listaTiposPelicula) {
				if (tipo.getProperty(TIPO_PELICULA_COL_ID) == aux.getProperty(PELICULA_TIPOS_COL_TIPO_PELICULA_FK)) {
					modelTiposPelicula.addElement((String) tipo.getProperty(TIPO_PELICULA_COL_NOMBRE));
				}
			}
		}
		tipoPeliculasElegidas.setModel(modelTiposPelicula);
		tipoPeliculasElegidas.setEnabled(false);
		scrollPanelListaTiposPelicula.setViewportView(tipoPeliculasElegidas);

		DefaultListModel modelIdiomas = new DefaultListModel<>();

		JScrollPane scrollPanelIdiomas = new JScrollPane();
		scrollPanelIdiomas.setBounds(328, 234, 112, 97);
		desktopPane.add(scrollPanelIdiomas);

		JList<Object> idiomasLista = new JList<Object>();
		String[] idiomas = peliculaEditar.getProperty(PELICULA_COL_IDIOMAS).toString().split(",");
		for (int i = 0; i < idiomas.length; i++) {
			modelIdiomas.addElement(idiomas[i].trim());
		}
		idiomasLista.setModel(modelIdiomas);
		idiomasLista.setEnabled(false);
		scrollPanelIdiomas.setViewportView(idiomasLista);

		JTextField txtTitulo = new JTextField();
		txtTitulo.setBounds(35, 25, 108, 20);
		txtTitulo.setText((String) peliculaEditar.getProperty(PELICULA_COL_TITULO));
		txtTitulo.setEditable(false);
		desktopPane.add(txtTitulo);
		txtTitulo.setColumns(10);

		JScrollPane scrollPanelDescripcion = new JScrollPane();
		scrollPanelDescripcion.setBounds(278, 36, 280, 128);
		desktopPane.add(scrollPanelDescripcion);

		JTextArea txtAreaDescripcion = new JTextArea();
		txtAreaDescripcion.setText((String) peliculaEditar.getProperty(PELICULA_COL_DESCRIPCION));
		txtAreaDescripcion.setEditable(false);
		scrollPanelDescripcion.setViewportView(txtAreaDescripcion);

		JTextField txtDuracion = new JTextField();
		txtDuracion.setBounds(35, 160, 108, 20);
		txtDuracion.setText(peliculaEditar.getProperty(PELICULA_COL_DURACION).toString());
		txtDuracion.setEditable(false);
		desktopPane.add(txtDuracion);
		txtDuracion.setColumns(10);

		JComboBox<Object> comboClasificacion = new JComboBox<Object>(CLASIFICACIONES);
		comboClasificacion.setBounds(35, 209, 108, 20);
		for (int i = 0; i < CLASIFICACIONES.length; i++) {
			if (CLASIFICACIONES[i].equals(peliculaEditar.getProperty(PELICULA_COL_CLASIFICACION))) {
				comboClasificacion.setSelectedItem(CLASIFICACIONES[i]);
			}
		}
		comboClasificacion.setEnabled(false);
		desktopPane.add(comboClasificacion);

		ButtonGroup rBtnGroupSubtitulos = new ButtonGroup();

		JRadioButton rdbtnSubtitulosSi = new JRadioButton("Si", true);
		rdbtnSubtitulosSi.setBounds(35, 263, 52, 23);

		rBtnGroupSubtitulos.add(rdbtnSubtitulosSi);
		desktopPane.add(rdbtnSubtitulosSi);

		JRadioButton rdbtnSubtitulosNo = new JRadioButton("No", false);
		rdbtnSubtitulosNo.setBounds(86, 263, 57, 23);
		rBtnGroupSubtitulos.add(rdbtnSubtitulosNo);
		desktopPane.add(rdbtnSubtitulosNo);
		if ((boolean) peliculaEditar.getProperty(PELICULA_COL_SUBTITULOS)) {
			rdbtnSubtitulosSi.setSelected(true);
		} else {
			rdbtnSubtitulosNo.setSelected(true);
		}
		rdbtnSubtitulosSi.setEnabled(false);
		rdbtnSubtitulosNo.setEnabled(false);

		JComboBox<Object> comboResolucion = new JComboBox<Object>(RESOLUCIONES);
		comboResolucion.setBounds(35, 317, 108, 20);
		desktopPane.setLayout(null);
		for (int i = 0; i < RESOLUCIONES.length; i++) {
			if (RESOLUCIONES[i].equals(peliculaEditar.getProperty(PELICULA_COL_RESOLUCION))) {
				comboResolucion.setSelectedItem(RESOLUCIONES[i]);
			}
		}
		comboResolucion.setEnabled(false);
		desktopPane.add(comboResolucion);
	}

	/**
	 * actualiza el nombre de la imagen a mostrar
	 * 
	 * @param name nombre de la imagen a cambiar
	 */
	private void updateLabel(String name) {
		ImageIcon icon = createImageIcon("Portada/" + name + ".gif");
		if (icon != null) {
			lblPortadaContenido.setText(null);
			lblPortadaContenido.setToolTipText("Una portada de " + name.toLowerCase());
			ImageIcon thumbnail = null;
			if (icon.getIconWidth() > 90) {
				thumbnail = new ImageIcon(icon.getImage().getScaledInstance(90, -1, Image.SCALE_DEFAULT));
			} else if (icon.getIconHeight() > 50) {
				thumbnail = new ImageIcon(icon.getImage().getScaledInstance(-1, 50, Image.SCALE_DEFAULT));
			} else { // no need to miniaturize
				thumbnail = icon;
			}
			lblPortadaContenido.setIcon(thumbnail);
		} else {
			lblPortadaContenido.setText("Portada no encontrada");
		}
	}

	/**
	 * devuelve la imagen o un mensaje de no encontrado
	 * 
	 * @param path ruta a la imagen
	 * @return imagen a mostrar
	 */
	private ImageIcon createImageIcon(String path) {
		File archivoPortada = new File(PORTADA + "\\" + peliculaEditar.getProperty(PELICULA_COL_PORTADA));
		String imgURL = archivoPortada.getPath();
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("No es ha encontrado el archivo: " + path);
			return null;
		}
	}

	/**
	 * coger todos los proveedores de BBDD para el comboBox de proveedores
	 * 
	 * @return lista de los proveedores
	 */
	private ArrayList<itfProperty> rellenarComboBoxProveedor() {
		ArrayList<itfProperty> listaProveedor = null;
		try {
			listaProveedor = objGestor.listarProveedor();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		return listaProveedor;
	}

	/**
	 * coger todos los empleados de BBDD para el comboBox de empleados
	 * 
	 * @return lista de los proveedores
	 */
	private ArrayList<itfProperty> rellenarComboBoxEmpleado() {
		ArrayList<itfProperty> listaEmpleados = null;
		try {
			listaEmpleados = objGestor.listarUsuario(USUARIO_EMPLEADO);
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		return listaEmpleados;
	}
}
