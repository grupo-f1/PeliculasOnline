package LP;

import java.time.LocalDate;
import java.util.ArrayList;

import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;
import LN.clsGestorLN;
import peliculasOnline.clsMain;
import static COMUN.clsConstantes.*;

/**
 * 
 * Objeto encargado de la gestion del inicio.
 * 
 * @author Eric y Mikel
 *
 */
public class clsMenu {
	/**
	 * declaracion del objeto
	 */
	private clsGestorLN objGestor;
	private itfProperty usuarioActual;

	/**
	 * Constructor del objeto menu
	 */
	public clsMenu() {
		objGestor = new clsGestorLN();
	}

	/**
	 * Menu donde se inicia la aplicacion
	 */
	public void menuPrincipal() {
		int op;
		op = 99;

		do {
			usuarioActual = null;
			System.out.println("1.- Iniciar Sesion \n2.-Resgistrarse \n3.-Salir");
			op = Utilidades.leerEntero();
			switch (op) {
			case 1: {
				login();
				break;
			}
			case 2: {
				registro();
				break;
			}
			case 3: {
				System.out.println("Saliendo del programa");
				System.exit(0);
				break;
			}
			default: {
				System.out.println("opcion incorrecta");
			}
			}
		} while (op != 3);
	}

	/**
	 * Menu donde el usuario hace login
	 */
	private void login() {
		String usuario = "";
		String password = "";
		do {
			do {
				System.out.println("Inserta 'atras' para volver al menu principal:");
				System.out.println("Inserta usuario:");
				usuario = Utilidades.leerCadena();
				if (usuario.equalsIgnoreCase(ATRAS)) {
					menuPrincipal();
				}
				System.out.println("Inserta password:");
				password = Utilidades.leerCadena();
			} while (usuario.equalsIgnoreCase("") || password.equalsIgnoreCase(""));
			try {
				usuarioActual = objGestor.devolverUsuario(usuario);
				if (usuarioActual == null) {
					System.out.println("nombre de usuario o password erroneos");
				} else if (!usuarioActual.getProperty(USUARIO_COL_PASSWORD).equals(password)) {
					usuarioActual = null;
					System.out.println("nombre de usuario o password erroneos");
				}
			} catch (clsErrorSQLConexion | clsErrorSQLConsulta e) {
				System.out.println(e.getMessage());
			}
		} while (usuarioActual == null);
		elegirMenu();
	}

	/**
	 * funcion que determina que menu mostrar
	 */
	private void elegirMenu() {
		try {
			boolean tipoUsuario = objGestor.tipoUsuario(usuarioActual);
			if (tipoUsuario) {
				menuEmpleado();
			} else {
				menuSocio();
			}
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			System.out.println(e.getMessage());
		}
	}

	// -------------REGISTROS-------------------//

	/**
	 * Menu donde se elige el tipo de usuario que se va a registrar
	 */
	private void registro() {
		int op;
		op = 99;

		do {
			System.out.println("1.- Empleado \n2.-Socio \n3.-Atras");
			op = Utilidades.leerEntero();
			switch (op) {
			case 1: {
				registroEmpleado();
				break;
			}
			case 2: {
				registroSocio();
				break;
			}
			case 3: {
				menuPrincipal();
			}
			default: {
				System.out.println("opcion incorrecta");
			}
			}
		} while (op != 3);
	}

	/**
	 * Menu de registro del empleado
	 */
	private void registroEmpleado() {
		// se piden todos los datos del usuario que se necesite

		System.out.println("Introduzca su nombre de usuario");
		String usuario = Utilidades.leerCadena();
		System.out.println("Introduzca su password");
		String password = Utilidades.leerCadena();
		System.out.println("Introduzca su nombre");
		String nombre = Utilidades.leerCadena();
		System.out.println("Introduzca su apellido");
		String apellido = Utilidades.leerCadena();
		System.out.println("Introduzca su fecha de nacimiento " + PATRON_FECHA);
		String fechaNacimiento = Utilidades.leerCadena();
		System.out.println("Introduzca su dni");
		String dni = Utilidades.leerCadena();
		System.out.println("Introduzca su telefono");
		String telf = Utilidades.leerCadena();
		System.out.println("Introduzca su email");
		String email = Utilidades.leerCadena();
		System.out.println("Seleccione tipo de empleado");

		ArrayList<itfProperty> listaEmpleadoTipo = new ArrayList<itfProperty>();
		try {
			listaEmpleadoTipo = objGestor.listarTipoEmpleado();
			for (itfProperty tipoE : listaEmpleadoTipo) {
				System.out.print(tipoE.getProperty(TIPO_EMPLEADO_COL_NOMBRE) + "\n");
			}
			String tipo = Utilidades.leerCadena();
			int tipoEmpleado = -1;
			do {
				switch (tipo) {
				case TIPO_EMPLEADO_ADMIN:
					for (itfProperty aux : listaEmpleadoTipo) {
						if (aux.getProperty(TIPO_EMPLEADO_COL_NOMBRE).equals(TIPO_EMPLEADO_ADMIN)) {
							tipoEmpleado = (int) aux.getProperty(TIPO_EMPLEADO_COL_ID);
						}
					}
					break;
				case TIPO_EMPLEADO_GESTOR:
					for (itfProperty aux : listaEmpleadoTipo) {
						if (aux.getProperty(TIPO_EMPLEADO_COL_NOMBRE).equals(TIPO_EMPLEADO_GESTOR)) {
							tipoEmpleado = (int) aux.getProperty(TIPO_EMPLEADO_COL_ID);
						}
					}
					break;
				case TIPO_EMPLEADO_TECNICO:
					for (itfProperty aux : listaEmpleadoTipo) {
						if (aux.getProperty(TIPO_EMPLEADO_COL_NOMBRE).equals(TIPO_EMPLEADO_TECNICO)) {
							tipoEmpleado = (int) aux.getProperty(TIPO_EMPLEADO_COL_ID);
						}
					}
					break;
				default:
					System.out.println("No existe ese tipo de empleado");
					break;

				}
			} while (tipoEmpleado != -1);
			int resultado = -1;
			resultado = objGestor.insertarEmpleado(tipoEmpleado, usuario, password, nombre, apellido, fechaNacimiento,
					dni, telf, email);
			if (resultado != 1) {
				System.out.println("Empleado creado correctamente");
			} else {
				System.out.println("El empleado no se ha creado");
			}
		} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConsulta | clsErrorSQLConexion e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * Menu de registro del socio
	 */
	private void registroSocio() {
		// se piden todos los datos del usuario que se necesite
		System.out.println("Introduzca su nombre de usuario");
		String usuario = Utilidades.leerCadena();
		System.out.println("Introduzca su password");
		String password = Utilidades.leerCadena();
		System.out.println("Introduzca su nombre");
		String nombre = Utilidades.leerCadena();
		System.out.println("Introduzca sus apellidos");
		String apellidos = Utilidades.leerCadena();
		System.out.println("Introduzca su fecha de nacimiento " + PATRON_FECHA);
		String fechaNacimiento = Utilidades.leerCadena();
		System.out.println("Introduzca su email");
		String email = Utilidades.leerCadena();
		System.out.println("Introduzca su telefono");
		String telefono = Utilidades.leerCadena();
		System.out.println("Introduzca su dni");
		String dni = Utilidades.leerCadena();

		ArrayList<itfProperty> listaTarifas = new ArrayList<itfProperty>();
		int retorno = -1;

		try {
			listaTarifas = objGestor.listarTarifa();

			for (itfProperty tarifa : listaTarifas) {
				System.out.println("nombre tarifa: " + tarifa.getProperty(TARIFA_COL_TIPO) + " de "
						+ tarifa.getProperty(TARIFA_COL_PRECIO) + "�");

			}
			String tarifa = Utilidades.leerCadena();
			int tarifaId = -1;
			do {
				switch (tarifa) {
				case TARIFA_BASICA: {
					for (itfProperty aux : listaTarifas) {
						if (aux.getProperty(TARIFA_COL_TIPO).equals(TARIFA_BASICA)) {
							tarifaId = (int) aux.getProperty(TARIFA_COL_ID);
						}
					}
				}
				case TARIFA_AVANZADA: {
					for (itfProperty aux : listaTarifas) {
						if (aux.getProperty(TARIFA_COL_TIPO).equals(TARIFA_AVANZADA)) {
							tarifaId = (int) aux.getProperty(TARIFA_COL_ID);
						}
					}
				}
				case TARIFA_PREMIUM: {
					for (itfProperty aux : listaTarifas) {
						if (aux.getProperty(TARIFA_COL_TIPO).equals(TARIFA_PREMIUM)) {
							tarifaId = (int) aux.getProperty(TARIFA_COL_ID);
						}
					}
				}
				default: {
					System.out.println("tarifa no existente");
				}
				}
				// lo inserto en un arraylist
			} while (tarifaId != -1);
			retorno = objGestor.insertarSocio(tarifaId, usuario, password, nombre, apellidos, fechaNacimiento, email,
					telefono, dni);
		} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion | clsErrorSQLConsulta e) {

			System.out.println(e.getMessage());
		}
		if (retorno == 1) {
			System.out.println("socio insertado correctamente");
		}
	}

	// -----------------MENUS------------------//

	/**
	 * Menu del empleado, va a dirigir a tres tipos de empleados diferentes
	 */
	private void menuEmpleado() {
		int idTipoEmpleado = (int) usuarioActual.getProperty("tipo_empleado_FK");
		itfProperty tipoEmpleado = null;
		try {
			tipoEmpleado = objGestor.devolverTipoEmpleado(idTipoEmpleado);
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		switch ((String) tipoEmpleado.getProperty(TIPO_EMPLEADO_COL_NOMBRE)) {
		case TIPO_EMPLEADO_ADMIN: {
			menuAdministrador();
			break;
		}
		case TIPO_EMPLEADO_GESTOR: {
			menuGestor();
			break;
		}
		case TIPO_EMPLEADO_TECNICO: {

			break;
		}
		}

	}

	/**
	 * Menu del socio al hacer login
	 * 
	 */
	private void menuSocio() {
		int op = 0;

		if ((boolean) usuarioActual.getProperty(SOCIO_COL_ESTADO) == true) {
			do {
				String menu = "Menu socio \n" + "1.- Elegir tarifa \n" + "2.- Mofidicar su perfil \n"
						+ "3.- Elegir una pelicula \n" + "4.- Listado de peliculas compradas \n"
						+ "5.- Consultar dudas en soporte \n" + "6.- Darse de baja \n" + "7.- Atras \n";
				System.out.println(menu);
				op = Utilidades.leerEntero();
				switch (op) {
				case 1:
					menuSocioElegirTarifa();
					break;
				case 2:
					menuSocioModificarPerfil(usuarioActual);
					break;
				case 3:
					menuSocioElegirPelicula();
					break;
				case 4:
					menuSocioVisualizarPeliculasCompradas();
					break;
				case 5:
					menuSocioConsultarDudasSoporte();
					break;
				case 6:
					int idSocioActual = (int) usuarioActual.getProperty(SOCIO_COL_ID);
					boolean estadoActual = (boolean) usuarioActual.getProperty(SOCIO_COL_ESTADO);
					menuSocioDarseBaja(idSocioActual, estadoActual);
					break;
				case 7:
					login();
					break;
				default:
					System.out.println("opcion no valida");
				}
			} while (op != 7);
		} else {
			System.out.println("Tu cuenta esta suspendida, si deseas activarla, pulsa 1.");
			op = Utilidades.leerEntero();
			switch (op) {
			case 1:
				int idSocioActual = (int) usuarioActual.getProperty(SOCIO_COL_ID);
				boolean estadoActual = (boolean) usuarioActual.getProperty(SOCIO_COL_ESTADO);
				menuSocioDarseBaja(idSocioActual, estadoActual);
				break;
			default:
				System.out.println("No hay mas opciones hasta tener la cuenta activada.");
				menuSocio();
			}
		}

	}

	/**
	 * Menu del empleado gestor
	 */
	private void menuGestor() {
		int opGestor = 99;
		do {
			System.out.println("Menu Gestor: ");
			System.out.println("1.-menu pelicula \n2.-menu proveedor \n3.-estadisticas peliculas \n4.-Menu Principal");
			opGestor = Utilidades.leerEntero();
			switch (opGestor) {
			case 1: {
				menuPelicula();
				break;
			}
			case 2: {
				menuProveedor();
				break;
			}
			case 3: {
				menuEstadisticasPeliculas();
				break;
			}
			case 4: {
				menuPrincipal();
				break;
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (opGestor != 4);
	}

	/**
	 * Menu del tecnico
	 */
	private void menuTecnico() {
		String menu = "";

		System.out.println(menu);
	}

	/**
	 * Menu que vera el administrador al hacer login
	 */
	private void menuAdministrador() {
		int opAdmin = 99;
		do {
			System.out.println("Menu Administrador: ");
			System.out.println("1.-administrar socios " + "\n2.-administrar empleados " + "\n3.-administar consultas"
					+ "\n4.-administar tickets" + "\n5.-administrar tarifas" + "\n6.-administrar proveedores"
					+ "\n7.-administar peliculas" + "\n8.-administar tipo de pelicula"
					+ "\n9.-administrar relaciones de tipos de peliculas en las peliculas" + "\n10.-Inicio");
			opAdmin = Utilidades.leerEntero();
			switch (opAdmin) {
			case 1: {
				menuAdminSocio();
				break;
			}
			case 2: {
				menuAdminEmpleado();
				break;
			}
			case 3: {
				menuAdminConsulta();
				break;
			}
			case 4: {
				menuAdminTicket();
				break;
			}
			case 5: {
				menuAdminTarifa();
				break;
			}
			case 6: {
				menuProveedor();
				break;
			}
			case 7: {
				menuPelicula();
				break;
			}
			case 8: {
				break;
			}
			case 9: {
				menuAdminPeliculaTipos();
				break;
			}
			case 10: {
				menuPrincipal();
				;
				break;
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (opAdmin != 10);
	}

	/**
	 * Menu de administrador donde podra ver modificar crear o borrar socios
	 */
	private void menuAdminSocio() {
		int opAdminSocio = 99;
		do {
			System.out.println("Administracion de socios:");
			System.out.println("1.- Lista de socios" + "\n2.-Buscar un socio" + "\n3.-Alta de socio"
					+ "\n4.-Baja de socio" + "\n5.-Modificacion de socio" + "\n6.-Atras");
			opAdminSocio = Utilidades.leerEntero();
			switch (opAdminSocio) {
			case 1: {
				listarSocios();
				break;
			}
			case 2: {
				itfProperty socio = elegirSocio();
				System.out.println(socio.toString());
			}
			case 3: {
				registroSocio();
				break;
			}
			case 4: {
				itfProperty socio = elegirSocio();
				menuSocioDarseBaja((int) socio.getProperty(SOCIO_COL_ID),
						(boolean) socio.getProperty(SOCIO_COL_ESTADO));
				break;
			}
			case 5: {
				itfProperty socio = elegirSocio();
				menuSocioModificarPerfil(socio);
				break;
			}
			case 6: {
				menuEmpleado();
				break;
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (opAdminSocio != 5);
	}

	/**
	 * Menu donde el administrador puede ver editar y borrar los empleados
	 */
	private void menuAdminEmpleado() {
		int opAdminEmpleado = 99;
		do {
			System.out.println("Administracion de empleados:");
			System.out.println("1.- Lista de empleados" + "\n2.-Nuevo empleado" + "\n3.-Borrar empleado"
					+ "\n4.-Modificacion de empleado" + "\n5.-Atras");
			opAdminEmpleado = Utilidades.leerEntero();
			switch (opAdminEmpleado) {
			case 1: {
				listarEmpleados();
				break;
			}
			case 2: {
				registroEmpleado();
				break;
			}
			case 3: {
				itfProperty empleado = elegirEmpleado();
				borrarEmpleado(empleado);
				break;
			}
			case 4: {
				itfProperty empleado = elegirEmpleado();
				modificarEmpleado(empleado);
				break;
			}
			case 5: {
				menuEmpleado();
				break;
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (opAdminEmpleado != 5);
	}

	/**
	 * Menu donde el administrador puede ver editar y borrar las consultas
	 */
	private void menuAdminConsulta() {
		int opAdminConsulta = 99;
		do {
			System.out.println("Administracion de consultas:");
			System.out.println("1.- Lista de consultas" + "\n2.-Nueva consulta" + "\n3.-Borrar consulta"
					+ "\n4.-Modificacion de consulta" + "\n5.-buscar consulta" + "\n6.-Atras");
			opAdminConsulta = Utilidades.leerEntero();
			switch (opAdminConsulta) {
			case 1: {
				listarTodasConsultas();
				break;
			}
			case 2: {
				crearConsulta();
				break;
			}
			case 3: {
				borrarConsulta();
				break;
			}
			case 4: {
				modificarConsulta();
				break;
			}
			case 5: {
				buscarConsulta();
				break;
			}
			case 6: {
				menuEmpleado();
				break;
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (opAdminConsulta != 6);
	}

	/**
	 * Menu donde el empleado puede ver editar y borrar las peliculas
	 */
	private void menuPelicula() {
		int opAdminPelicula = 99;
		do {
			System.out.println("Administracion de peliculas:");
			System.out.println("1.- Lista de peliculas por titulo" + "\n2.- Lista de peliculas por proveedor"
					+ "\n3.-Nueva pelicula" + "\n4.-Borrar pelicula"
					+ "\n5.-Modificacion de pelicula \n6.-buscar pelicula \n7.-Atras");
			opAdminPelicula = Utilidades.leerEntero();
			switch (opAdminPelicula) {
			case 1: {
				listarPeliculasTitulo();
				break;
			}
			case 2: {
				listarPeliculasProveedor();
				break;
			}
			case 3: {
				annadirPelicula();
				break;
			}
			case 4: {
				borrarPelicula();
				break;
			}
			case 5: {
				modificarPelicula();
				break;
			}
			case 6: {
				ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
				try {
					listaPeliculas = objGestor.listarPeliculaTitulo();
				} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
					System.out.println(e.getMessage());
				}
				elegirPeliculaTituloProveedor(listaPeliculas);
				break;
			}
			case 7: {
				menuEmpleado();
				break;
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (opAdminPelicula != 6);
	}

	/**
	 * Menu donde el empleado puede ver editar y borrar los proveedores
	 */
	private void menuProveedor() {
		int opAdminProveedor = 99;
		do {
			System.out.println("Administracion de proveedores:");
			System.out.println("1.- Lista de proveedores" + "\n2.-Nueva proveedor" + "\n3.-Borrar proveedor"
					+ "\n4.-Modificacion de proveedor" + "\n5.-Atras");
			opAdminProveedor = Utilidades.leerEntero();
			switch (opAdminProveedor) {
			case 1: {
				listarProveedores();
				break;
			}
			case 2: {
				annadirProveedor();
				break;
			}
			case 3: {
				borrarProveedor();
				break;
			}
			case 4: {
				modificarProveedor();
				break;
			}
			case 5: {
				menuEmpleado();
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (opAdminProveedor != 5);
	}

	/**
	 * Menu donde el administrador puede ver editar y borrar las tarifas
	 */
	private void menuAdminTicket() {
		int opAdminTicket = 99;
		do {
			System.out.println("Administracion de tickets:");
			System.out.println("1.- Lista de tickets" + "\n2.-BuscarTicket" + "\n3.-Nuevo ticket" + "\n4.-Borrar ticket"
					+ "\n5.-Modificacion de ticket" + "\n6.-Atras");
			opAdminTicket = Utilidades.leerEntero();
			switch (opAdminTicket) {
			case 1: {
				listarTickets();
				break;
			}
			case 2: {
				itfProperty ticket = buscarTicket();
				System.out.println(ticket.toString());
				break;
			}
			case 3: {
				adminNuevoTicket();
				break;
			}
			case 4: {
				borrarTicket();
				break;
			}
			case 5: {
				modificarTicket();
				break;
			}
			case 6: {
				menuEmpleado();
				break;
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (opAdminTicket != 5);
	}

	/**
	 * Menu donde el administrador puede ver editar y borrar las tarifas
	 */
	private void menuAdminTarifa() {
		int opAdminTarifa = 99;
		do {
			System.out.println("Administracion de tarifas:");
			System.out.println("1.- Lista de tarifas" + "\n2.-Nueva tarifa" + "\n3.-Borrar tarifa"
					+ "\n4.-Modificacion de tarifa" + "\n5.-Atras");
			opAdminTarifa = Utilidades.leerEntero();
			switch (opAdminTarifa) {
			case 1: {
				break;
			}
			case 2: {

				break;
			}
			case 3: {

				break;
			}
			case 4: {
				break;
			}
			case 5: {
				menuEmpleado();
				break;
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (opAdminTarifa != 5);
	}

	/**
	 * Menu donde el administrador puede ver editar y borrar las relaciones de las
	 * peliculas con los tipos de peliculas
	 */
	private void menuAdminPeliculaTipos() {
		int opAdminPeliculaTipos = 99;
		do {
			System.out.println("Administracion de la relacion entre peliculas y tipo de peliculas:");
			System.out.println("1.- Lista de todas las relaciones" + "\n2.-peliculas con tipo de pelicula especifico"
					+ "\n3.-Tipo de peliculas de una pelicula" + "\n4.-Modificar tipos peliculas de una pelicula"
					+ "\n5.-Borrar tipo de pelicula de una pelicula" + "\n6.-Atras");
			opAdminPeliculaTipos = Utilidades.leerEntero();
			switch (opAdminPeliculaTipos) {
			case 1: {

				break;
			}
			case 2: {

				break;
			}
			case 3: {

				break;
			}
			case 4: {

				break;
			}
			case 5: {

				break;
			}
			case 6: {
				menuEmpleado();
				break;
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (opAdminPeliculaTipos != 6);
	}

	/**
	 * menu de las estadisticas de las peliculas
	 */
	private void menuEstadisticasPeliculas() {
		int op = 99;
		do {
			System.out.println(
					"1.-Porcentaje de peliculas de cada proveedor \n2.-Cantidad de veces conprada cada pelicula \n3.-Atras");
			op = Utilidades.leerEntero();
			switch (op) {
			case 1: {
				porcentajeProveedor();
				break;
			}
			case 2: {
				cantidadPeliculas();
				break;
			}
			case 3: {
				menuEmpleado();
				break;
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (op != 3);
	}

	// ---------------------EMPLEADO--------------------//
	/**
	 * mostar todos los empleados
	 */
	private void listarEmpleados() {
		ArrayList<itfProperty> listaEmpleados = new ArrayList<itfProperty>();
		try {
			listaEmpleados = objGestor.listarUsuario(USUARIO_EMPLEADO);
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty aux : listaEmpleados) {
			System.out.println(aux.toString());
		}
	}

	/**
	 * Funcion que modifica los datos del empleaod
	 * 
	 * @param usuario empleado a editar
	 */
	private void modificarEmpleado(itfProperty usuario) {
		// recogemos los datos
		String nombreUsuario = (String) usuario.getProperty(USUARIO_COL_USER_NAME);
		String password = (String) usuario.getProperty(USUARIO_COL_PASSWORD);
		String nombre = (String) usuario.getProperty(USUARIO_COL_NOMBRE);
		String apellidos = (String) usuario.getProperty(USUARIO_COL_APELLIDOS);
		String fechaNacimiento = (String) usuario.getProperty(USUARIO_COL_FECHA_NACIMIENTO).toString();
		String dni = (String) usuario.getProperty(USUARIO_COL_DNI);
		String telefono = (String) usuario.getProperty(USUARIO_COL_TELEFONO);
		String email = (String) usuario.getProperty(USUARIO_COL_EMAIL);
		int idEmpleado = (int) usuario.getProperty(EMPLEADO_COL_ID);
		int tipoEmpleado_FK = (int) usuario.getProperty(EMPLEADO_COL_TIPO_EMPLEADO_FK);

		boolean seguirModificando = true;
		do {

			System.out.println(usuarioActual.toString());

			System.out.println("Que desea modificar? \n" + "1.- Nombre de usuario. \n" + "2.- password. \n"
					+ "3.- Nombre \n" + "4.- Apellidos \n" + "5.- Fecha de nacimineto \n" + "6.- Email \n"
					+ "7.- Telefono \n" + "8.- DNI \n9.- Tipo empleado");

			int op = Utilidades.leerEntero();
			switch (op) {
			case 1:
				System.out.println("Inserta el nuevo nombre de usuario: ");
				nombreUsuario = Utilidades.leerCadena();

				break;
			case 2:
				System.out.println("Inserta la nueva password");
				password = Utilidades.leerCadena();
				break;
			case 3:
				System.out.println("Inserta el nuevo nombre");
				nombre = Utilidades.leerCadena();
				break;
			case 4:
				System.out.println("Inserta los nuevos apellidos");
				apellidos = Utilidades.leerCadena();
				break;
			case 5:
				System.out.println("Inserta la nueva fecha de nacimiento " + PATRON_FECHA);
				fechaNacimiento = Utilidades.leerCadena();
				break;
			case 6:
				System.out.println("Inserta el nuevo email");
				email = Utilidades.leerCadena();
				break;
			case 7:
				System.out.println("Inserta el nuevo telefono");
				telefono = Utilidades.leerCadena();
				break;
			case 8:
				System.out.println("Inserta el nuevo dni");
				dni = Utilidades.leerCadena();
				break;
			case 9:
				do {
					ArrayList<itfProperty> listaTipoEmpleados = new ArrayList<itfProperty>();
					try {
						listaTipoEmpleados = objGestor.listarTipoEmpleado();
					} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
						System.out.println(e.getMessage());
					}
					for (itfProperty tipoEmpleado : listaTipoEmpleados) {
						System.out.println("tipo Empleado: " + tipoEmpleado.getProperty(TIPO_EMPLEADO_COL_NOMBRE));
					}
					System.out.println("Inserta el nuevo tipo Empleado: ");
					String tipoEmpleadoString = Utilidades.leerCadena();
					for (itfProperty tipoEmpleado : listaTipoEmpleados) {
						if (tipoEmpleadoString
								.equalsIgnoreCase((String) tipoEmpleado.getProperty(TIPO_EMPLEADO_COL_NOMBRE))) {
							tipoEmpleado_FK = (int) tipoEmpleado.getProperty(TIPO_EMPLEADO_COL_ID);
						}
					}
				} while (tipoEmpleado_FK == (int) usuario.getProperty(EMPLEADO_COL_TIPO_EMPLEADO_FK));
				break;
			default:
				System.out.println("No es una opcion valida");
			}

			System.out.println("Quieres seguir modificando?: si/no");
			String seguirModificandoString = Utilidades.leerCadena();
			if (seguirModificandoString.equalsIgnoreCase("no")) {
				seguirModificando = false;
			}
		} while (seguirModificando);

		int retorno = -1;
		try {
			retorno = objGestor.editarEmpleado(nombreUsuario, password, nombre, apellidos, fechaNacimiento, dni,
					telefono, email, idEmpleado, tipoEmpleado_FK);
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		if (retorno == 1) {
			System.out.println("Empleado editado correctamente.");
		} else {
			System.out.println("Error editando empleado.");
		}
	}

	/**
	 * Funcion que elige un empleado de la lista de usuarios
	 * 
	 * @return empleado elegido
	 */
	private itfProperty elegirEmpleado() {
		ArrayList<itfProperty> listaEmpleados = new ArrayList<itfProperty>();
		try {
			listaEmpleados = objGestor.listarUsuario(USUARIO_EMPLEADO);

		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty empleado : listaEmpleados) {
			System.out.println("Nombre de usuario= " + empleado.getProperty(USUARIO_COL_USER_NAME));
		}
		itfProperty empleado = null;
		do {
			System.out.println("Elige nombre de usuario del socio");
			String userName = Utilidades.leerCadena();
			for (itfProperty aux : listaEmpleados) {
				if (userName.equalsIgnoreCase((String) empleado.getProperty(USUARIO_COL_USER_NAME))) {
					empleado = (itfProperty) aux;
				}
			}
			if (empleado == null) {
				System.out.println("socio no encontrado");
			}
		} while (empleado != null);
		return empleado;
	}

	/**
	 * funcion que borra un empleado
	 * 
	 * @param empleado empleado a borrar
	 */
	private void borrarEmpleado(itfProperty empleado) {
		try {
			objGestor.borrarEmpleado((int) empleado.getProperty(EMPLEADO_COL_ID));
		} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConsulta | clsErrorSQLConexion e) {
			System.out.println(e.getMessage());
		}
	}

	// --------------------SOCIO-----------------------//

	/**
	 * mostrar todos los socios
	 */
	private void listarSocios() {
		ArrayList<itfProperty> listaSocios = new ArrayList<itfProperty>();
		try {
			listaSocios = objGestor.listarUsuario(USUARIO_SOCIO);
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty aux : listaSocios) {
			System.out.println(aux.toString());
		}
	}

	/**
	 * Cambia el estado del socio y con ello bloquea sus funciones o las activa
	 * 
	 * @param idSocio id del socio al dar de baja
	 * @param estado  estado del socio al dar de baja
	 */
	private void menuSocioDarseBaja(int idSocio, boolean estado) {
		// Para que el socio se de de baja
		try {
			if (objGestor.cambiarEstadoSocio(idSocio, estado)) {
				System.out.println("Estado cambiado. \n Vuelve a iniciar sesion");
				clsMain.main(null);

			} else {
				System.out.println("Problemas cambiando el estado del socio intantelo de nuevo.");
				menuSocio();
			}
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
	}

	private void menuSocioConsultarDudasSoporte() {
		// Para que el socio pueda abrir un ticket de consulta en soporte
		System.out.println("Que problema tiene?. Indica su incidencia: ");
		String incidencia = Utilidades.leerCadena();
		int idSocio = (int) usuarioActual.getProperty("idSocio");
		LocalDate fechaHoy = LocalDate.now();

		// lo inserto en un arraylist //decidir que hacer con el 3
		int retorno = -1;
		try {
			retorno = objGestor.insertarConsulta(3, idSocio, fechaHoy.toString(), incidencia, 0);
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {
			System.out.println(e.getMessage());
		}
		if (retorno == 1) {
			System.out.println("Consulta insertada correctamente");
			System.out.println("Fecha incidencia: " + fechaHoy.toString());
			System.out.println("Incidencia: " + incidencia);
			System.out.println("Socio que ha creado la incidencia: " + usuarioActual.getProperty("nombre"));

		} else {
			System.out.println("Consulta no insertada correctamente");
		}

	}

	/**
	 * Sirve para ver los tickets de las peliculas compradas
	 */
	private void menuSocioVisualizarPeliculasCompradas() {
		// System.out.println(usuarioActual.toString());
		// Para que el socio pueda ver sus peliculas compradas
		int idSocioActual = (int) usuarioActual.getProperty(SOCIO_COL_ID);
		ArrayList<itfProperty> listaTickets = new ArrayList<itfProperty>();
		ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
		try {
			listaTickets = objGestor.listarTicket();
			listaPeliculas = objGestor.listarPeliculaTitulo();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		boolean vacio = true;
		for (itfProperty ticket : listaTickets) {
			int idSocioTicket = (int) ticket.getProperty(TICKET_COL_SOCIO_FK);
			if (idSocioActual == idSocioTicket) {
				for (itfProperty pelicula : listaPeliculas) {
					if (pelicula.getProperty(PELICULA_COL_ID) == ticket.getProperty(TICKET_COL_PELICULA_FK)) {
						String texto = "Has comprado la pelicula " + pelicula.getProperty(PELICULA_COL_TITULO)
								+ ", con fecha: " + ticket.getProperty(TICKET_COL_FECHA_COMPRA);
						System.out.println(texto);
						vacio = false;
						break;
					}
				}
			}

		}
		if (vacio) {
			System.out.println("No has comprado ninguna pelicula.");
		}
		System.out.println("--------------");
	}

	/**
	 * Funcion que sirve para comprar Peliculas
	 */
	private void menuSocioElegirPelicula() {
		boolean encontrado = false;
		do {
			// Mostrarmos todas las peliculas
			System.out.println("Escoge la pelicula que quieras comprar");
			ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
			try {
				listaPeliculas = objGestor.listarPeliculaTitulo();
			} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

				System.out.println(e.getMessage());
			}
			int op = elegirPeliculaTituloProveedor(listaPeliculas);
			String nombrepelicula = "";
			for (itfProperty pelicula : listaPeliculas) {
				if (op == (int) pelicula.getProperty(PELICULA_COL_ID)) {
					nombrepelicula = (String) pelicula.getProperty(PELICULA_COL_TITULO);
					encontrado = true;
					break;
				}
			}
			if (encontrado) {
				insertarTicket(op, nombrepelicula);
			} else {
				System.out.println("Prueba a volver a comprar");
			}
		} while (!encontrado);

	}

	/**
	 * Funcion que modifica los datos del socio
	 * 
	 * @param usuario socio a editar
	 */
	private void menuSocioModificarPerfil(itfProperty usuario) {
		// recogemos los datos
		String nombreUsuario = (String) usuario.getProperty(USUARIO_COL_USER_NAME);
		String password = (String) usuario.getProperty(USUARIO_COL_PASSWORD);
		String nombre = (String) usuario.getProperty(USUARIO_COL_NOMBRE);
		String apellidos = (String) usuario.getProperty(USUARIO_COL_APELLIDOS);
		String fechaNacimiento = (String) usuario.getProperty(USUARIO_COL_FECHA_NACIMIENTO).toString();
		String email = (String) usuario.getProperty(USUARIO_COL_EMAIL);
		String telefono = (String) usuario.getProperty(USUARIO_COL_TELEFONO);
		String dni = (String) usuario.getProperty(USUARIO_COL_DNI);
		int idSocio = (int) usuario.getProperty(SOCIO_COL_ID);
		int tarifaFK = (int) usuario.getProperty(SOCIO_COL_TARIFA_FK);
		System.out.println(usuario.getProperty(SOCIO_COL_ESTADO));
		boolean estado = (boolean) usuario.getProperty(SOCIO_COL_ESTADO);
		String fechaTarifa = (String) usuario.getProperty(SOCIO_COL_FECHA_TARIFA).toString();

		boolean seguirModificando = true;
		do {

			System.out.println(usuarioActual.toString());

			System.out.println("Que desea modificar? \n" + "1.- Nombre de usuario. \n" + "2.- password. \n"
					+ "3.- Nombre \n" + "4.- Apellidos \n" + "5.- Fecha de nacimineto \n" + "6.- Email \n"
					+ "7.- Telefono \n" + "8.- DNI \n");

			int op = Utilidades.leerEntero();
			switch (op) {
			case 1:
				System.out.println("Inserta el nuevo nombre de usuario: ");
				nombreUsuario = Utilidades.leerCadena();

				break;
			case 2:
				System.out.println("Inserta la nueva password");
				password = Utilidades.leerCadena();
				break;
			case 3:
				System.out.println("Inserta el nuevo nombre");
				nombre = Utilidades.leerCadena();
				break;
			case 4:
				System.out.println("Inserta los nuevos apellidos");
				apellidos = Utilidades.leerCadena();
				break;
			case 5:
				System.out.println("Inserta la nueva fecha de nacimiento " + PATRON_FECHA);
				fechaNacimiento = Utilidades.leerCadena();
				break;
			case 6:
				System.out.println("Inserta el nuevo email");
				email = Utilidades.leerCadena();
				break;
			case 7:
				System.out.println("Inserta el nuevo telefono");
				telefono = Utilidades.leerCadena();
				break;
			case 8:
				System.out.println("Inserta el nuevo dni");
				dni = Utilidades.leerCadena();
				break;
			default:
				System.out.println("No es una opcion valida");
			}

			System.out.println("Quieres seguir modificando?: si/no");
			String seguirModificandoString = Utilidades.leerCadena();
			if (seguirModificandoString.equalsIgnoreCase("no")) {
				seguirModificando = false;
			}
		} while (seguirModificando);

		int retorno = -1;
		try {
			retorno = objGestor.editarSocio(nombreUsuario, password, nombre, apellidos, fechaNacimiento, email,
					telefono, dni, idSocio, tarifaFK, estado, fechaTarifa);
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		if (retorno == 1) {
			System.out.println("Socio editado correctamente.");
		} else {
			System.out.println("Error editando socio.");
		}
	}

	/**
	 * Escoger tarifa del socio
	 */
	private void menuSocioElegirTarifa() {
		// Para que el socio pueda elegir la tarifa a pagar
		ArrayList<itfProperty> tarifasElegir = new ArrayList<itfProperty>();
		try {
			tarifasElegir = objGestor.listarTarifa();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty tarifa : tarifasElegir) {
			System.out.println(tarifa.toString());
		}
		int comprobacion = -1;
		do {
			System.out.println("Que tarifa desea?");
			String op = Utilidades.leerCadena();

			try {
				comprobacion = objGestor.buscarIdTarifa(op, usuarioActual);
			} catch (clsErrorSQLParametros | clsErrorSQLConsulta | clsErrorSQLInsertDeleteUpdate
					| clsErrorSQLConexion e) {
				System.out.println(e.getMessage());
			}
			if (comprobacion == 1) {
				System.out.println("Socio editado");
			} else {
				System.out.println("error en la edicion");
			}
		} while (comprobacion == -1);
	}

	/**
	 * Funcion que elige un socio de la lista de usuarios
	 * 
	 * @return socio elegido
	 */
	private itfProperty elegirSocio() {
		ArrayList<itfProperty> listaSocios = new ArrayList<itfProperty>();
		try {
			listaSocios = objGestor.listarUsuario(USUARIO_SOCIO);

		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty socio : listaSocios) {
			System.out.println("Nombre de usuario= " + socio.getProperty(USUARIO_COL_USER_NAME));
		}
		itfProperty socio = null;
		do {
			System.out.println("Elige nombre de usuario del socio");
			String userName = Utilidades.leerCadena();
			for (itfProperty aux : listaSocios) {
				if (userName.equalsIgnoreCase((String) aux.getProperty(USUARIO_COL_USER_NAME))) {
					socio = (itfProperty) aux;
				}
			}
			if (socio == null) {
				System.out.println("socio no encontrado");
			}
		} while (socio == null);
		return socio;
	}

	// ---------------PELICULAS----------------//

	/**
	 * lista de todas las peliculas por titulo
	 */
	private void listarPeliculasTitulo() {
		ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
		try {
			listaPeliculas = objGestor.listarPeliculaTitulo();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty aux : listaPeliculas) {
			System.out.println(aux.toString());
		}
	}

	/**
	 * lista de todas las peliculas por proveedor
	 */
	private void listarPeliculasProveedor() {
		ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
		try {
			listaPeliculas = objGestor.listarPeliculaProveedor();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty aux : listaPeliculas) {
			System.out.println(aux.toString());
		}
	}

	/**
	 * funcion que devuelve una pelicula buscando por titulo y proveedor
	 * @param listaPeliculas lista de peliculas a mostrar y elegir
	 * 
	 * @return id de la pelicula
	 */
	private int elegirPeliculaTituloProveedor(ArrayList<itfProperty> listaPeliculas) {
		int peliculaId = -1;
		do {
			System.out.println("Peliculas: ");
			for (itfProperty pelicula : listaPeliculas) {
				System.out.println("titulo: " + pelicula.getProperty(PELICULA_COL_TITULO) + ", proveedor: "
						+ pelicula.getProperty(PELICULA_COL_PROVEEDOR_FK));
			}
			System.out.print("titulo a buscar: ");
			String titulo = Utilidades.leerCadena();
			System.out.print("proveedor a buscar: ");
			int proveedor = Utilidades.leerEntero();

			for (itfProperty pelicula : listaPeliculas) {
				if (pelicula.getProperty(PELICULA_COL_TITULO).equals(titulo)
						&& (int) pelicula.getProperty(PELICULA_COL_PROVEEDOR_FK) == proveedor) {
					peliculaId = (int) pelicula.getProperty(PELICULA_COL_ID);
				}
			}
			if (peliculaId == -1) {
				System.out.println("pelicula no encontrada");
			}
		} while (peliculaId == -1);
		return peliculaId;
	}

	/**
	 * insertar una nueva pelicula y sus tipos de pelicula
	 */
	private void annadirPelicula() {
		ArrayList<itfProperty> listaProveedores = new ArrayList<itfProperty>();
		ArrayList<itfProperty> listaTiposPelicula = new ArrayList<itfProperty>();

		try {
			listaProveedores = objGestor.listarProveedor();
			listaTiposPelicula = objGestor.listarTipoPelicula();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		int proveedor_FK = -1;
		int empleado_FK = (int) usuarioActual.getProperty(EMPLEADO_COL_ID);
		System.out.println("Elige proveedor de la pelicula: ");
		for (itfProperty proveedor : listaProveedores) {
			System.out.println(proveedor.toString());
		}
		do {
			String proveedorElegido = Utilidades.leerCadena();
			for (itfProperty proveedor : listaProveedores) {
				if (proveedorElegido.equalsIgnoreCase((String) proveedor.getProperty(PROVEEDOR_COL_NOMBRE))) {
					proveedor_FK = (int) proveedor.getProperty(PROVEEDOR_COL_ID);
				}
			}
			if (proveedor_FK == -1) {
				System.out.println("proveedor no encontrado");
			}
		} while (proveedor_FK == -1);
		System.out.println("introduzca titulo de la pelicula: ");
		String titulo = Utilidades.leerCadena();
		System.out.println("introduzca descripcion de la pelicula: ");
		String descripcion = Utilidades.leerCadena();
		System.out.println("introduzca duracion de la pelicula en minutos: ");
		int duracion = Utilidades.leerEntero();
		System.out.println("introduzca clasificacion de la pelicula: ");
		String clasificacion = Utilidades.leerCadena();
		System.out.println("introduzca lista de idiomas de la pelicula: ");
		String idiomas = Utilidades.leerCadena();
		System.out.println("contiene subtitulos la pelicula (si/no): ");
		String subtitulosString = Utilidades.leerCadena();
		boolean subtitulos;
		if (subtitulosString.equalsIgnoreCase("si")) {
			subtitulos = true;
		} else {
			subtitulos = false;
		}
		System.out.println("introduzca resolucion de video de la pelicula: ");
		String resolucion = Utilidades.leerCadena();
		System.out.println("introduzca direccion de la portada de la pelicula: ");
		String portada = Utilidades.leerCadena();
		int resultado = -1;
		try {
			resultado = objGestor.insertarPelicula(empleado_FK, proveedor_FK, titulo, descripcion, duracion,
					clasificacion, idiomas, subtitulos, resolucion, portada);
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		if (resultado == 1) {
			System.out.println("pelicula insertada");
		}
		ArrayList<Integer> tiposPelicula = insertarTiposPelicula(listaTiposPelicula);
		// buscar id de la pelicula recien creada
		itfProperty pelicula = null;
		try {
			pelicula = objGestor.devolverPeliculaTituloProveedor(titulo, proveedor_FK);
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (Integer aux : tiposPelicula) {
			try {
				objGestor.insertarPeliculaTipos((int) aux, (int) pelicula.getProperty(PELICULA_COL_ID));
			} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate
					| clsErrorSQLConexion e) {

				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * funcion que crea arraylist de los tipos de pelicula de esa pelicula
	 * 
	 * @param listaTiposPelicula lista de todos los tipos de peliculas
	 * @return lista de los tipos de peliculas elegidas
	 */
	private ArrayList<Integer> insertarTiposPelicula(ArrayList<itfProperty> listaTiposPelicula) {
		ArrayList<Integer> tiposPelicula = new ArrayList<Integer>();
		String tipoPeliculaString;
		do {
			System.out.println("Elige cada vez un genero de la pelicula o escribe exit:");
			for (itfProperty tipoPelicula : listaTiposPelicula) {
				System.out.println(tipoPelicula.getProperty(TIPO_PELICULA_COL_NOMBRE));
			}
			tipoPeliculaString = Utilidades.leerCadena();
			if (!tipoPeliculaString.equalsIgnoreCase(EXIT)) {
				for (itfProperty tipoPelicula : listaTiposPelicula) {
					if (tipoPeliculaString
							.equalsIgnoreCase((String) tipoPelicula.getProperty(TIPO_PELICULA_COL_NOMBRE))) {
						tiposPelicula.add((int) tipoPelicula.getProperty(TIPO_PELICULA_COL_ID));
					}
				}
			}
		} while (!tipoPeliculaString.equalsIgnoreCase(EXIT));
		return tiposPelicula;
	}

	/**
	 * modificar una pelicula o sus tipos de peliculas
	 */
	private void modificarPelicula() {
		ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
		try {
			listaPeliculas = objGestor.listarPeliculaTitulo();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty pelicula : listaPeliculas) {
			System.out.println("titulo: " + pelicula.getProperty(PELICULA_COL_TITULO) + ", proveedor: "
					+ pelicula.getProperty(PELICULA_COL_PROVEEDOR_FK));
		}
		itfProperty peliculaBuscar = null;
		String titulo;
		do {
			System.out.println("Escriba 'atras' para salir");
			System.out.println("Titulo de la pelicula:");
			titulo = Utilidades.leerCadena();
			if (!titulo.equalsIgnoreCase(ATRAS)) {
				do {
					System.out.println("Proveedor de la pelicula:");
					int proveedor = Utilidades.leerEntero();
					for (itfProperty pelicula : listaPeliculas) {
						if (titulo.equalsIgnoreCase((String) pelicula.getProperty(PELICULA_COL_TITULO))
								&& proveedor == (int) pelicula.getProperty(PELICULA_COL_PROVEEDOR_FK)) {
							peliculaBuscar = pelicula;
						}
					}
					if (peliculaBuscar == null) {
						System.out.println("Pelicula no encontrada");
					} else {
						int op = 99;
						do {
							System.out.println(
									"Quieres cambiar: 1.-Los tipos de pelicula \n2.-Otros datos de la pelicula");
							op = Utilidades.leerEntero();
							switch (op) {
							case 1: {
								editarTiposPeliculaDePelicula(peliculaBuscar);
								break;
							}
							case 2: {
								editarDatosPelicula(peliculaBuscar);

								break;
							}
							default:
								System.out.println("opcion no valida");
							}
						} while (op != 1 || op != 2);
					}

				} while (peliculaBuscar == null);
			}
		} while (!titulo.equalsIgnoreCase(ATRAS));

	}

	/**
	 * menu para elegir entre editar los tipos de peliculas existentes o introducir
	 * mas tipos de pelicula a la pelicula
	 * 
	 * @param peliculaEditar pelicula a editar
	 */
	private void editarTiposPeliculaDePelicula(itfProperty peliculaEditar) {
		ArrayList<itfProperty> listaTiposPeliculaTodos = new ArrayList<itfProperty>();
		ArrayList<itfProperty> listaTiposEstaPelicula = new ArrayList<itfProperty>();
		ArrayList<itfProperty> listaTipos = new ArrayList<itfProperty>();
		try {
			listaTiposPeliculaTodos = objGestor.listarTipoPelicula();
			listaTiposEstaPelicula = objGestor.devolverPeliculaTipos((int) peliculaEditar.getProperty(PELICULA_COL_ID));
			listaTipos = objGestor.devolverTipoPelicula(listaTiposEstaPelicula);
		} catch (clsErrorSQLConsulta e) {

			System.out.println(e.getMessage());
		} catch (clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		int op2 = 99;
		do {
			System.out.println("1.-editar tipos de pelicula existentes \n2.-introducir nuevo tipo pelicula \n3.-Atras");
			op2 = Utilidades.leerEntero();
			switch (op2) {
			case 1: {
				editarGenerosExistentesDePelicula(peliculaEditar, listaTiposPeliculaTodos, listaTiposEstaPelicula,
						listaTipos);
				break;
			}
			case 2: {
				System.out.println("Los tipos de pelicula de esta pelicula son:");
				for (itfProperty aux : listaTipos) {
					System.out.println(aux.getProperty(TIPO_PELICULA_COL_NOMBRE));
				}
				System.out.println("\n");
				ArrayList<Integer> tiposPelicula = insertarTiposPelicula(listaTiposPeliculaTodos);
				for (Integer aux : tiposPelicula) {
					try {
						objGestor.insertarPeliculaTipos((int) aux, (int) peliculaEditar.getProperty(PELICULA_COL_ID));
					} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate
							| clsErrorSQLConexion e) {

						System.out.println(e.getMessage());
					}
				}
				break;
			}
			case 3: {
				modificarPelicula();
			}
			default: {
				System.out.println("opcion no valida");
				break;
			}
			}
		} while (op2 != 3);

	}

	/**
	 * funcion que edita los tipos de pelicula que existen en esta palicula
	 * 
	 * @param peliculaEditar          pelicula a editar
	 * @param listaTiposPeliculaTodos lista de tipos de esta pelicula
	 * @param listaTiposEstaPelicula  lista de todos los tipos de pelicula
	 * @param listaTipos              lista de los nombre de los tipos de peliculas
	 *                                de esta pelicula
	 */
	private void editarGenerosExistentesDePelicula(itfProperty peliculaEditar,
			ArrayList<itfProperty> listaTiposPeliculaTodos, ArrayList<itfProperty> listaTiposEstaPelicula,
			ArrayList<itfProperty> listaTipos) {

		ArrayList<Integer> tiposPelicula = new ArrayList<Integer>();
		String tipoPeliculaString;
		System.out.println("Los tipos de pelicula de esta pelicula son:");
		for (itfProperty aux : listaTipos) {
			System.out.println(aux.getProperty(TIPO_PELICULA_COL_NOMBRE));
		}

		System.out.println("\nElegir entre estas opciones: ");
		for (itfProperty tipoPelicula : listaTiposPeliculaTodos) {
			System.out.println(tipoPelicula.getProperty(TIPO_PELICULA_COL_NOMBRE));
		}
		for (itfProperty aux : listaTipos) {
			System.out.print("opcion: ");
			tipoPeliculaString = Utilidades.leerCadena();
			for (itfProperty tipoPelicula : listaTiposPeliculaTodos) {
				if (tipoPeliculaString.equalsIgnoreCase((String) tipoPelicula.getProperty(TIPO_PELICULA_COL_NOMBRE))) {
					tiposPelicula.add((int) tipoPelicula.getProperty(TIPO_PELICULA_COL_ID));
				}
			}
		}
		try {
			objGestor.editarPeliculaTipos(tiposPelicula, listaTiposEstaPelicula);
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
	}

	/**
	 * funcion que edita los datos de una pelicula
	 * 
	 * @param peliculaEditar pelicula a editar
	 */
	private void editarDatosPelicula(itfProperty peliculaEditar) {
		System.out.println("Elige que apartados editar y los demas copia la informacion:");
		System.out.print("id empleado: " + peliculaEditar.getProperty(PELICULA_COL_EMPLEADO_FK) + " nuevo: ");
		int empleado_FK = Utilidades.leerEntero();
		System.out.print("id proveedor: " + peliculaEditar.getProperty(PELICULA_COL_PROVEEDOR_FK) + " nuevo: ");
		int proveedor_FK = Utilidades.leerEntero();
		System.out.print("titulo: " + peliculaEditar.getProperty(PELICULA_COL_TITULO) + " nuevo: ");
		String titulo = Utilidades.leerCadena();
		System.out.print("descipcion: " + peliculaEditar.getProperty(PELICULA_COL_DESCRIPCION) + " nuevo: ");
		String descripcion = Utilidades.leerCadena();
		System.out.print("portada: " + peliculaEditar.getProperty(PELICULA_COL_PORTADA) + " nuevo: ");
		String portada = Utilidades.leerCadena();
		System.out.print("resolucion: " + peliculaEditar.getProperty(PELICULA_COL_RESOLUCION) + " nuevo: ");
		String resolucion = Utilidades.leerCadena();
		System.out.print("idiomas: " + peliculaEditar.getProperty(PELICULA_COL_IDIOMAS) + " nuevo: ");
		String idiomas = Utilidades.leerCadena();
		System.out.print("subtitulos: " + peliculaEditar.getProperty(PELICULA_COL_SUBTITULOS) + " nuevo: ");
		String subtitulosString = Utilidades.leerCadena();
		boolean subtitulos;
		if (subtitulosString.equalsIgnoreCase(TRUE)) {
			subtitulos = true;
		} else {
			subtitulos = false;
		}
		System.out.print("duracion: " + peliculaEditar.getProperty(PELICULA_COL_DURACION) + " nuevo: ");
		int duracion = Utilidades.leerEntero();
		System.out.print("clasificacion: " + peliculaEditar.getProperty(PELICULA_COL_CLASIFICACION) + " nuevo: ");
		String clasificacion = Utilidades.leerCadena();
		int retorno = -1;
		try {
			retorno = objGestor.editarPelicula(empleado_FK, proveedor_FK, titulo, descripcion, portada, resolucion,
					idiomas, subtitulos, duracion, clasificacion, (int) peliculaEditar.getProperty(PELICULA_COL_ID));
		} catch (clsErrorSQLParametros | clsErrorSQLConsulta | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		if (retorno == 1) {
			System.out.println("pelicula editada");
		}
	}

	/**
	 * borrar una pelicula
	 */
	private void borrarPelicula() {
		ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
		try {
			listaPeliculas = objGestor.listarPeliculaTitulo();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty pelicula : listaPeliculas) {
			System.out.println("titulo: " + pelicula.getProperty(PELICULA_COL_TITULO) + ", proveedor: "
					+ pelicula.getProperty(PELICULA_COL_PROVEEDOR_FK));
		}
		itfProperty peliculaBuscar = null;
		do {
			System.out.println("Titulo de la pelicula:");
			String titulo = Utilidades.leerCadena();
			System.out.println("Proveedor de la pelicula:");
			int proveedor = Utilidades.leerEntero();
			for (itfProperty pelicula : listaPeliculas) {
				if (titulo.equalsIgnoreCase((String) pelicula.getProperty(PELICULA_COL_TITULO))
						&& proveedor == (int) pelicula.getProperty(PELICULA_COL_PROVEEDOR_FK)) {
					peliculaBuscar = pelicula;
				}
			}
			if (peliculaBuscar == null) {
				System.out.println("Pelicula no encontrada");
			}
		} while (peliculaBuscar == null);
		try {
			objGestor.borrarPelicula((int) peliculaBuscar.getProperty(PELICULA_COL_ID));
		} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion | clsErrorSQLConsulta e) {

			System.out.println(e.getMessage());
		}
		System.out.println("pelicula borrada");
	}

	// ---------------PROVEEDOR--------------//

	/**
	 * listar proveedores
	 */
	private void listarProveedores() {
		ArrayList<itfProperty> listaProveedores = new ArrayList<itfProperty>();
		try {
			listaProveedores = objGestor.listarProveedor();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty aux : listaProveedores) {
			System.out.println(aux.toString());
		}
	}

	/**
	 * nuevo proveedor
	 */
	private void annadirProveedor() {
		int resultado = -1;
		do {
			System.out.println("introducir proveedor nuevo: ");
			System.out.print("nombre: ");
			String nombre = Utilidades.leerCadena();
			System.out.print("email: ");
			String email = Utilidades.leerCadena();
			System.out.print("telefono: ");
			String telefono = Utilidades.leerCadena();
			try {
				resultado = objGestor.insertarProveedor(nombre, email, telefono);
				if (resultado != -1) {
					System.out.println("proveedor insertado");
				}

			} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConsulta
					| clsErrorSQLConexion e) {
				System.out.println(e.getMessage());
			}
		} while (resultado != -1);
	}

	/**
	 * borrar proveedor
	 */
	private void borrarProveedor() {
		ArrayList<itfProperty> listaProveedores = new ArrayList<itfProperty>();
		try {
			listaProveedores = objGestor.listarProveedor();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty aux : listaProveedores) {
			System.out.println("nombre " + aux.getProperty(PROVEEDOR_COL_NOMBRE) + ", email: "
					+ aux.getProperty(PROVEEDOR_COL_EMAIL));
		}

		itfProperty proveedorBuscar = null;
		do {
			System.out.println("Elige el email de el proveedor a eliminar: ");
			String email = Utilidades.leerCadena();
			for (itfProperty aux : listaProveedores) {
				if (email.equalsIgnoreCase((String) aux.getProperty(PROVEEDOR_COL_EMAIL))) {
					proveedorBuscar = aux;
				}
			}
			if (proveedorBuscar == null) {
				System.out.println("proveedor no encontrado");
			}
		} while (proveedorBuscar == null);
		int resultado = -1;
		try {
			resultado = objGestor.borrarProveedor((int) proveedorBuscar.getProperty(PROVEEDOR_COL_ID));
		} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion | clsErrorSQLConsulta e) {

			System.out.println(e.getMessage());
		}
		if (resultado == 1) {
			System.out.println("proveedor eliminado");
		}
	}

	/**
	 * modificar proveedores
	 */
	private void modificarProveedor() {
		try {
			ArrayList<itfProperty> listaProveedores = objGestor.listarProveedor();
			for (itfProperty proveedor : listaProveedores) {
				System.out.println("nombre: " + proveedor.getProperty(PROVEEDOR_COL_NOMBRE) + "\nemail: "
						+ proveedor.getProperty(PROVEEDOR_COL_EMAIL));
			}
			System.out.println("elige email de proveedor");
			String email = Utilidades.leerCadena();
			itfProperty proveedorBuscar = objGestor.devolverProveedor(email);
			System.out.println("Introduce los datos a editar o copia lo antiguo");
			System.out.print("nombre: " + proveedorBuscar.getProperty(PROVEEDOR_COL_NOMBRE) + ", nuevo nombre: ");
			String nombre = Utilidades.leerCadena();
			System.out.print("email: " + proveedorBuscar.getProperty(PROVEEDOR_COL_EMAIL) + ", nuevo email: ");
			email = Utilidades.leerCadena();
			System.out.print("telefono: " + proveedorBuscar.getProperty(PROVEEDOR_COL_TELEFONO) + ", nuevo telefono: ");
			String telefono = Utilidades.leerCadena();

			int retorno = objGestor.editarProveedor(nombre, email, telefono,
					(int) proveedorBuscar.getProperty(PROVEEDOR_COL_ID));
			if (retorno == 1) {
				System.out.println("proveedor editado correctamente");
			}
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate e) {
			System.out.println(e.getMessage());
		}

	}

	// ---------------CONSULTA----------------//

	/**
	 * Funcion que busca una consulta en la base de datos
	 * 
	 * @return itfProperty de la consulta
	 */
	private itfProperty buscarConsulta() {
		System.out.println("Parametro a buscar: ");
		System.out.println("Empleado: ");
		int empleado = Utilidades.leerEntero();
		System.out.println("Socio: ");
		int socio = Utilidades.leerEntero();
		System.out.println("Fecha formato fecha " + PATRON_FECHA);
		String fecha = Utilidades.leerCadena();
		itfProperty consulta = null;
		try {
			consulta = objGestor.devolverConsulta(empleado, socio, fecha);
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		System.out.println(consulta.toString());
		return consulta;
	}

	/**
	 * Funcion que modifica una consulta
	 */
	private void modificarConsulta() {
		itfProperty consulta = buscarConsulta();
		System.out.println("cambia los datos que quieras y copia los demas: ");
		System.out.println("Empleado: ");
		int empleado = Utilidades.leerEntero();
		System.out.println("Socio: ");
		int socio = Utilidades.leerEntero();
		System.out.println("Fecha formato fecha " + PATRON_FECHA);
		String fecha = Utilidades.leerCadena();
		System.out.println("Incidencia: ");
		String incidencia = Utilidades.leerCadena();
		System.out.println("Resultado (1/0): ");
		int resultado = Utilidades.leerEntero();
		int retorno = -1;
		try {
			retorno = objGestor.editarConsulta(empleado, socio, fecha, incidencia, resultado,
					(int) consulta.getProperty(CONSULTA_COL_EMPLEADO_FK),
					(int) consulta.getProperty(CONSULTA_COL_SOCIO_FK),
					(LocalDate) consulta.getProperty(CONSULTA_COL_FECHA));
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		if (retorno == 1) {
			System.out.println("Consulta editada correctamente");
		}
	}

	/**
	 * Funcion que borra una consulta de la base de datos
	 */
	private void borrarConsulta() {
		System.out.println(
				"Elige la consulta a eliminar en base al empleado, socio y fecha si quieres eliminar solo una consulta");
		listarTodasConsultas();
		System.out.println("Empleado: ");
		int empleado = Utilidades.leerEntero();
		System.out.println("Socio: ");
		int socio = Utilidades.leerEntero();
		System.out.println("Fecha formato fecha " + PATRON_FECHA);
		String fecha = Utilidades.leerCadena();
		boolean retorno = false;
		try {
			retorno = objGestor.borrarConsulta(empleado, socio, fecha);
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		if (!retorno) {
			System.out.println("elemento no borrado");
		}
	}

	/**
	 * Funcion que crea una nueva consulta en la base de datos
	 */
	private void crearConsulta() {
		System.out.println("Empleado: ");
		int empleado = Utilidades.leerEntero();
		System.out.println("Socio: ");
		int socio = Utilidades.leerEntero();
		System.out.println("Fecha formato fecha " + PATRON_FECHA);
		String fecha = Utilidades.leerCadena();
		System.out.println("Incidencia: ");
		String incidencia = Utilidades.leerCadena();
		System.out.println("Resultado (1/0): ");
		int resultado = Utilidades.leerEntero();
		int retorno = -1;
		try {
			retorno = objGestor.insertarConsulta(empleado, socio, fecha, incidencia, resultado);
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		if (retorno == 1) {
			System.out.println("Consulta insertada correctamente");
		}
	}

	/**
	 * Funcion que muestra en pantalla todas las consultas que hay
	 */
	private void listarTodasConsultas() {
		ArrayList<itfProperty> listaConsultas = new ArrayList<itfProperty>();
		try {
			listaConsultas = objGestor.listarConsulta();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		for (itfProperty aux : listaConsultas) {
			System.out.println(aux.toString());
		}

	}

	// --------------------TICKET--------------------//

	/**
	 * Funcion que un admin genera un nuevo ticket
	 */
	private void adminNuevoTicket() {
		ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
		try {
			listaPeliculas = objGestor.listarPeliculaTitulo();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			System.out.println(e.getMessage());
		}
		int retorno = -1;
		do {
			int id_pelicula = elegirPeliculaTituloProveedor(listaPeliculas);
			itfProperty socio = elegirSocio();
			int id_socio = (int) socio.getProperty(SOCIO_COL_ID);
			try {
				retorno = objGestor.insertarTicket(id_socio, id_pelicula);
			} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

				System.out.println(e.getMessage());
			}
			if (retorno == 1) {
				System.out.println("pelicula insertada");
			} else {
				System.out.println("Error insertando un ticket.");
			}
		} while (retorno == -1);
	}

	/**
	 * funcion que lista todos los tickets
	 * 
	 */
	private void listarTickets() {
		ArrayList<itfProperty> listaTickets = new ArrayList<itfProperty>();
		try {
			listaTickets = objGestor.listarTicket();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}

		for (itfProperty ticket : listaTickets) {
			System.out.println("Los ticket son:");
			System.out.println(ticket.toString());
		}
	}

	/**
	 * funcion que borra un ticket
	 */
	private void borrarTicket() {
		listarTickets();
		System.out.println("Elige id del socio del ticket a borrar");
		int socio = Utilidades.leerEntero();
		System.out.println("Elige id de la pelicula del ticket a borrar");
		int pelicula = Utilidades.leerEntero();
		System.out.println("Elige la fecha del ticket a borrar (" + PATRON_FECHA + ")");
		String fecha = Utilidades.leerCadena();
		boolean retorno = false;
		try {
			retorno = objGestor.borrarTicket(socio, pelicula, fecha);
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		if (retorno) {
			System.out.println("ticket eliminado");
		} else {
			System.out.println("error al borrar ticket");
		}
	}

	/**
	 * funcion que busca un ticket en base a su socio, pelicula y fecha
	 * 
	 * @return ticket elegido
	 */
	private itfProperty buscarTicket() {
		itfProperty ticketElegido = null;
		try {
			int socioId = -1;
			ArrayList<itfProperty> listaSocios = objGestor.listarSociosConTicket();
			do {
				System.out.println("Socios que tienen tickets: ");
				for (itfProperty socio : listaSocios) {
					System.out.println("socio: " + socio.getProperty(USUARIO_COL_USER_NAME));
				}
				System.out.println("Socio buscar: ");
				String socioString = Utilidades.leerCadena();
				for (itfProperty socio : listaSocios) {
					if (socio.getProperty(USUARIO_COL_USER_NAME).equals(socioString)) {
						socioId = (int) socio.getProperty(SOCIO_COL_ID);
					}
				}
				if (socioId == -1) {
					System.out.println("socio no encontrado");
				}
			} while (socioId == -1);
			ArrayList<itfProperty> listaPeliculas = objGestor.listarPeliculasConTicketSocio(socioId);

			int peliculaId = elegirPeliculaTituloProveedor(listaPeliculas);

			ArrayList<itfProperty> listaTickets = objGestor.listarTicketSocioPelicula(socioId, peliculaId);
			do {
				System.out.println("fecha del ticket de ese socio y esa pelicula: ");
				for (itfProperty ticket : listaTickets) {
					System.out.println("fecha: " + ticket.getProperty(TICKET_COL_FECHA_COMPRA));
				}
				System.out.print("fecha a buscar: ");
				String fecha = Utilidades.leerCadena();

				for (itfProperty ticket : listaTickets) {
					if (fecha.equals(ticket.getProperty(TICKET_COL_FECHA_COMPRA).toString())) {
						ticketElegido = ticket;
					}
				}
				if (peliculaId == -1) {
					System.out.println("pelicula no encontrada");
				}
			} while (ticketElegido == null);

		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			System.out.println(e.getMessage());
		}
		return ticketElegido;
	}

	/**
	 * funcion que modifica un ticket
	 */
	private void modificarTicket() {
		int retorno = -1;

		itfProperty ticket = buscarTicket();
		System.out.println("cambia los datos que quieras y copia los demas: ");
		int socioId = -1;
		ArrayList<itfProperty> listaSocios;
		try {
			listaSocios = objGestor.listarUsuario(USUARIO_SOCIO);
			do {
				System.out.println("Socios: ");
				for (itfProperty socio : listaSocios) {
					System.out.println("socio: " + socio.getProperty(USUARIO_COL_USER_NAME));
				}
				System.out.println("Socio nuevo: ");
				String socioString = Utilidades.leerCadena();
				for (itfProperty socio : listaSocios) {
					if (socio.getProperty(USUARIO_COL_USER_NAME).equals(socioString)) {
						socioId = (int) socio.getProperty(SOCIO_COL_ID);
					}
				}
				if (socioId == -1) {
					System.out.println("socio no encontrado");
				}
			} while (socioId == -1);
			ArrayList<itfProperty> listaPeliculas = new ArrayList<itfProperty>();
			listaPeliculas = objGestor.listarPeliculaTitulo();
			int pelicula = elegirPeliculaTituloProveedor(listaPeliculas);
			System.out.println("Fecha nueva en formato fecha " + PATRON_FECHA);
			String fecha = Utilidades.leerCadena();
			retorno = objGestor.editarTicket((int) ticket.getProperty(TICKET_COL_SOCIO_FK),
					(int) ticket.getProperty(TICKET_COL_PELICULA_FK),
					ticket.getProperty(TICKET_COL_FECHA_COMPRA).toString(), socioId, pelicula, fecha);
		} catch (clsErrorSQLConsulta | clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {
			System.out.println(e.getMessage());
		}
		if (retorno == 1) {
			System.out.println("Ticket editado correctamente");
		}
	}

	/**
	 * Funcion que genera tickets
	 * 
	 * @param op             id de la pelicula elegida
	 * @param nombrePelicula nombre de la pelicula elegida
	 * 
	 */
	private void insertarTicket(int op, String nombrePelicula) {
		// para insertar un ticket de compra
		// lo inserto en un arraylist

		int retorno = -1;
		do {
			try {
				retorno = objGestor.insertarTicket((int) usuarioActual.getProperty(SOCIO_COL_ID), op);
			} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e) {

				System.out.println(e.getMessage());
			}
			if (retorno == 1) {
				System.out.println("Felicidades! ya has comprado la pelicula: " + nombrePelicula);
				menuSocio();
			} else {
				System.out.println("Error insertando un ticket.");
				menuSocioElegirPelicula();
			}
		} while (retorno == -1);

	}

	// -----------ESTADISTICAS-------------//

	/**
	 * cantidad de veces comprada cada pelicula
	 */
	private void cantidadPeliculas() {
		try {
			ArrayList<itfProperty> peliculas = objGestor.listarPeliculaProveedor();
			ArrayList<itfProperty> tickets = objGestor.listarTicket();
			ArrayList<Integer> cantidadPeli = new ArrayList<Integer>();
			int cantPeliculas = 0;
			int totalPeliculas = 0;
			for (itfProperty pelicula : peliculas) {
				for (itfProperty ticket : tickets) {
					if (pelicula.getProperty(PELICULA_COL_ID) == ticket.getProperty(TICKET_COL_PELICULA_FK)) {
						cantPeliculas++;
					}
				}
				totalPeliculas = totalPeliculas + cantPeliculas;
				cantidadPeli.add(cantPeliculas);
				cantPeliculas = 0;
			}
			int i = 0;
			for (itfProperty pelicula : peliculas) {
				System.out.println("la pelicula: " + pelicula.getProperty(PELICULA_COL_TITULO) + " ha sido comprada "
						+ cantidadPeli.get(i) + " veces.");
				i++;
			}
			System.out.println("");
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * mostrar el porcentaje de las peliculas de cada proveedor
	 */
	private void porcentajeProveedor() {
		try {
			ArrayList<itfProperty> peliculas = objGestor.listarPeliculaProveedor();
			ArrayList<itfProperty> proveedores = objGestor.listarProveedor();
			ArrayList<Integer> cantidadPeli = new ArrayList<Integer>();
			int cantPeliculas = 0;
			int totalPeliculas = 0;
			for (itfProperty proveedor : proveedores) {
				for (itfProperty pelicula : peliculas) {
					if (pelicula.getProperty(PELICULA_COL_PROVEEDOR_FK) == proveedor.getProperty(PROVEEDOR_COL_ID)) {
						cantPeliculas++;
					}
				}
				totalPeliculas = totalPeliculas + cantPeliculas;
				cantidadPeli.add(cantPeliculas);
				cantPeliculas = 0;
			}
			int i = 0;
			for (itfProperty proveedor : proveedores) {
				System.out.println("Proveedor: " + proveedor.getProperty(PROVEEDOR_COL_NOMBRE)
						+ " porcentaje peliculas: " + ((cantidadPeli.get(i) * 100) / totalPeliculas) + "%");
				i++;
			}
			System.out.println("");
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			System.out.println(e.getMessage());
		}
	}
}