package LP;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import COMUN.itfProperty;
import LN.clsGestorLN;
import javax.swing.JButton;
import javax.swing.JDesktopPane;

import static COMUN.clsConstantes.*;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

/**
 * JFrame del menu Admin donde se mostrara las diferentes acciones que puede
 * hacer un admin
 * 
 * @author Eric
 *
 */
public class frmMenuAdmin extends JFrame implements ActionListener, InternalFrameListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3857951287312700775L;
	/**
	 * objeto de la clase clsGestorLN
	 */
	private clsGestorLN objGestor;
	/**
	 * objeto de la clase clsGestorLN
	 */
	private itfProperty usuarioActual;

	/**
	 * obj de escritorio para poner el jInternalFrame
	 */
	JDesktopPane desktop;
	/**
	 * obj escuchador de la ventana
	 */
	JInternalFrame listenedToWindow;

	/**
	 * anchura de la pantalla
	 */
	static int widthP;
	/**
	 * altura de la pantalla
	 */
	static int heightP;

	/**
	 * panel donde se muestra las listas
	 */
	private JPanel panel;

	/**
	 * Constructor del jFrame del menu principal
	 * 
	 * @param usuarioActual usuario conectado
	 * @param objGestor     objeto gestor de la logica de negocio
	 */
	public frmMenuAdmin(itfProperty usuarioActual, clsGestorLN objGestor) {
		this.usuarioActual = usuarioActual;
		this.objGestor = objGestor;

		// posicion pantalla
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();// ver tama�o de pantalla
		heightP = pantalla.height;
		widthP = pantalla.width;
		// Se crea un desktopPane para poder usar internalFrames. Este caso lo que hace
		// es establecer el
		// panel de contenidos (contenpane) del formulario, por un contentpane.
		desktop = new JDesktopPane();
		desktop.setPreferredSize(new Dimension((widthP), (heightP)));
		setContentPane(desktop);
		desktop.setLayout(null);

		// boton login de pruebas para jInternalFrame
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(this);
		btnLogin.setActionCommand(LOGIN);
		btnLogin.setBounds(317, 44, 89, 23);
		desktop.add(btnLogin);

		JMenuBar menuBarEmpleadoAdmin = new JMenuBar();
		menuBarEmpleadoAdmin.setBounds(0, 0, widthP, 23);
		desktop.add(menuBarEmpleadoAdmin);

		JMenu menuSocios = new JMenu("Socios");
		menuBarEmpleadoAdmin.add(menuSocios);

		JMenuItem menuItemListarSocios = new JMenuItem("Listar Socios");
		menuItemListarSocios.addActionListener(this);
		menuItemListarSocios.setActionCommand(LISTAR_SOCIOS);
		menuSocios.add(menuItemListarSocios);

		JMenuItem menuItemBuscarSocio = new JMenuItem("Buscar Socio");
		menuItemBuscarSocio.addActionListener(this);
		menuItemBuscarSocio.setActionCommand(BUSCAR_SOCIO);
		menuSocios.add(menuItemBuscarSocio);

		JMenuItem menuItemCrearSocio = new JMenuItem("Crear Socio");
		menuItemCrearSocio.addActionListener(this);
		menuItemCrearSocio.setActionCommand(CREAR_SOCIO);
		menuSocios.add(menuItemCrearSocio);

		JMenuItem menuItemEditarSocio = new JMenuItem("Editar Socio");
		menuItemEditarSocio.addActionListener(this);
		menuItemEditarSocio.setActionCommand(EDITAR_SOCIO);
		menuSocios.add(menuItemEditarSocio);

		JMenuItem menuItemBorrarSocio = new JMenuItem("Borrar Socio");
		menuItemBorrarSocio.addActionListener(this);
		menuItemBorrarSocio.setActionCommand(BORRAR_SOCIO);
		menuSocios.add(menuItemBorrarSocio);

		JMenu menuEmpleados = new JMenu("Empleados");
		menuBarEmpleadoAdmin.add(menuEmpleados);

		JMenuItem menuItemListarEmpleados = new JMenuItem("Listar Empleados");
		menuItemListarEmpleados.addActionListener(this);
		menuItemListarEmpleados.setActionCommand(LISTAR_EMPLEADOS);
		menuEmpleados.add(menuItemListarEmpleados);

		JMenuItem menuItemBuscarEmpleado = new JMenuItem("Buscar Empleado");
		menuItemBuscarEmpleado.addActionListener(this);
		menuItemBuscarEmpleado.setActionCommand(BUSCAR_EMPLEADO);
		menuEmpleados.add(menuItemBuscarEmpleado);

		JMenuItem menuItemCrearEmpleado = new JMenuItem("Crear Empleado");
		menuItemCrearEmpleado.addActionListener(this);
		menuItemCrearEmpleado.setActionCommand(CREAR_EMPLEADO);
		menuEmpleados.add(menuItemCrearEmpleado);

		JMenuItem menuItemEditarEmpleado = new JMenuItem("Editar Empleado");
		menuItemEditarEmpleado.addActionListener(this);
		menuItemEditarEmpleado.setActionCommand(EDITAR_EMPLEADO);
		menuEmpleados.add(menuItemEditarEmpleado);

		JMenuItem menuItemBorrarEmpleado = new JMenuItem("Borrar Empleado");
		menuItemBorrarEmpleado.addActionListener(this);
		menuItemBorrarEmpleado.setActionCommand(BORRAR_EMPLEADO);
		menuEmpleados.add(menuItemBorrarEmpleado);

		JMenu menuConsultas = new JMenu("Consultas");
		menuBarEmpleadoAdmin.add(menuConsultas);

		JMenuItem menuItemListarConsultas = new JMenuItem("Listar Consultas");
		menuItemListarConsultas.addActionListener(this);
		menuItemListarConsultas.setActionCommand(LISTAR_CONSULTAS);
		menuConsultas.add(menuItemListarConsultas);

		JMenuItem menuItemBuscarConsulta = new JMenuItem("Buscar Consulta");
		menuItemBuscarConsulta.addActionListener(this);
		menuItemBuscarConsulta.setActionCommand(BUSCAR_CONSULTA);
		menuConsultas.add(menuItemBuscarConsulta);

		JMenuItem menuItemCrearConsulta = new JMenuItem("Crear Consulta");
		menuItemCrearConsulta.addActionListener(this);
		menuItemCrearConsulta.setActionCommand(CREAR_CONSULTA);
		menuConsultas.add(menuItemCrearConsulta);

		JMenuItem menuItemEditarConsulta = new JMenuItem("Editar Consulta");
		menuItemEditarConsulta.addActionListener(this);
		menuItemEditarConsulta.setActionCommand(EDITAR_CONSULTA);
		menuConsultas.add(menuItemEditarConsulta);

		JMenuItem menuItemBorrarConsulta = new JMenuItem("Borrar Consulta");
		menuItemBorrarConsulta.addActionListener(this);
		menuItemBorrarConsulta.setActionCommand(BORRAR_CONSULTA);
		menuConsultas.add(menuItemBorrarConsulta);

		JMenu menuTickets = new JMenu("Tickets");
		menuBarEmpleadoAdmin.add(menuTickets);

		JMenuItem menuItemListarTickets = new JMenuItem("Listar Tickets");
		menuItemListarTickets.addActionListener(this);
		menuItemListarTickets.setActionCommand(LISTAR_TICKETS);
		menuConsultas.add(menuItemListarTickets);

		JMenuItem menuItemBuscarTicket = new JMenuItem("Buscar Ticket");
		menuItemBuscarTicket.addActionListener(this);
		menuItemBuscarTicket.setActionCommand(BUSCAR_TICKET);
		menuConsultas.add(menuItemBuscarTicket);

		JMenuItem menuItemCrearTicket = new JMenuItem("Crear Ticket");
		menuItemCrearTicket.addActionListener(this);
		menuItemCrearTicket.setActionCommand(CREAR_TICKET);
		menuConsultas.add(menuItemCrearTicket);

		JMenuItem menuItemEditarTicket = new JMenuItem("Editar Ticket");
		menuItemEditarTicket.addActionListener(this);
		menuItemEditarTicket.setActionCommand(EDITAR_TICKET);
		menuConsultas.add(menuItemEditarTicket);

		JMenuItem menuItemBorrarTicket = new JMenuItem("Borrar Ticket");
		menuItemBorrarTicket.addActionListener(this);
		menuItemBorrarTicket.setActionCommand(BORRAR_TICKET);
		menuConsultas.add(menuItemBorrarTicket);

		JMenu menuTarifas = new JMenu("Tarifas");
		menuBarEmpleadoAdmin.add(menuTarifas);

		JMenuItem menuItemListarTarifas = new JMenuItem("Listar Tarifas");
		menuItemListarTarifas.addActionListener(this);
		menuItemListarTarifas.setActionCommand(LISTAR_TARIFAS);
		menuTarifas.add(menuItemListarTarifas);

		JMenuItem menuItemBuscarTarifa = new JMenuItem("Buscar Tarifa");
		menuItemBuscarTarifa.addActionListener(this);
		menuItemBuscarTarifa.setActionCommand(BUSCAR_TARIFA);
		menuTarifas.add(menuItemBuscarTarifa);

		JMenuItem menuItemCrearTarifa = new JMenuItem("Crear Tarifa");
		menuItemCrearTarifa.addActionListener(this);
		menuItemCrearTarifa.setActionCommand(CREAR_TARIFA);
		menuTarifas.add(menuItemCrearTarifa);

		JMenuItem menuItemEditarTarifa = new JMenuItem("Editar Tarifa");
		menuItemEditarTarifa.addActionListener(this);
		menuItemEditarTarifa.setActionCommand(EDITAR_TARIFA);
		menuTarifas.add(menuItemEditarTarifa);

		JMenuItem menuItemBorrarTarifa = new JMenuItem("Borrar Tarifa");
		menuItemBorrarTarifa.addActionListener(this);
		menuItemBorrarTarifa.setActionCommand(BORRAR_TARIFA);
		menuTarifas.add(menuItemBorrarTarifa);

		JMenu menuProveedores = new JMenu("Proveedores");
		menuBarEmpleadoAdmin.add(menuProveedores);

		JMenuItem menuItemListarProveedors = new JMenuItem("Listar Proveedors");
		menuItemListarProveedors.addActionListener(this);
		menuItemListarProveedors.setActionCommand(LISTAR_PROVEEDORES);
		menuProveedores.add(menuItemListarProveedors);

		JMenu menuPeliculas = new JMenu("Peliculas");
		menuBarEmpleadoAdmin.add(menuPeliculas);

		JMenuItem menuItemListarPeliculas = new JMenuItem("Listar Peliculas");
		menuItemListarPeliculas.addActionListener(this);
		menuItemListarPeliculas.setActionCommand(LISTAR_PELICULAS);
		menuPeliculas.add(menuItemListarPeliculas);

		JMenuItem menuItemCrearPelicula = new JMenuItem("Crear Pelicula");
		menuItemCrearPelicula.addActionListener(this);
		menuItemCrearPelicula.setActionCommand(CREAR_PELICULA);
		menuPeliculas.add(menuItemCrearPelicula);

		JMenuItem menuItemEditarPelicula = new JMenuItem("Editar Pelicula");
		menuItemEditarPelicula.addActionListener(this);
		menuItemEditarPelicula.setActionCommand(EDITAR_PELICULA);
		menuPeliculas.add(menuItemEditarPelicula);

		JMenuItem menuItemBorrarPelicula = new JMenuItem("Borrar Pelicula");
		menuItemBorrarPelicula.addActionListener(this);
		menuItemBorrarPelicula.setActionCommand(BORRAR_PELICULA);
		menuPeliculas.add(menuItemBorrarPelicula);

		JMenu menuTiposPelicula = new JMenu("Tipos de peliculas");
		menuBarEmpleadoAdmin.add(menuTiposPelicula);

		JMenu menuPeliculaTipos = new JMenu("Relaciones Peliculas con Tipos");
		menuBarEmpleadoAdmin.add(menuPeliculaTipos);

	}

	/**
	 * funcion que muestra la ventana del menu principal
	 * 
	 */
	public void createAndShowGUI() {
		// Si se cierra el formulario se acaba la aplicacion.
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Display the window.
		this.pack();
		// centrar
		this.setLocationRelativeTo(null);
		// impedir hacer mas grande
		this.setResizable(false);

		this.setVisible(true);
	}

	/**
	 * metodo para escuchar los distintos eventos que ocurren en la ventana
	 * drmMenuPrincipal
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// si hay un panel con algo lo quita de la ventana para reemplazar con lo que
		// vendra
		if (panel != null) {
			desktop.remove(panel);
		}
		switch (e.getActionCommand()) {

		}
	}

	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {
		// TODO Auto-generated method stub
		System.out.println(e);

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}
}
