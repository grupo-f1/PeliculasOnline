package LP;

import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.*;

/**
 * visualizador de la imagen elegida
 * 
 * @author Eric
 *
 */
public class ImageFileView extends FileView {
	ImageIcon jpgIcon = Utilidades.createImageIcon("imagenesLogos/jpgIcon.gif");
	ImageIcon gifIcon = Utilidades.createImageIcon("imagenesLogos/gifIcon.gif");
	ImageIcon tiffIcon = Utilidades.createImageIcon("imagenesLogos/tiffIcon.gif");
	ImageIcon pngIcon = Utilidades.createImageIcon("imagenesLogos/pngIcon.png");

	/**
	 * funcion que devuelve el nombre de la imagen
	 * 
	 * @param f imagen elegida
	 * @return nombre de la imagen
	 */
	public String getName(File f) {
		return null; // let the L&F FileView figure this out
	}

	/**
	 * funcion que devuelve la descripcion de la imagen
	 * 
	 * @param f imagen elegida
	 * @return descripcion de la imagen
	 */
	public String getDescription(File f) {
		return null; // let the L&F FileView figure this out
	}

	/**
	 * funcion que devuelve si la imagen es traversable
	 * 
	 * @param f imagen elegida
	 * @return trabersabilidad de la imagen
	 */
	public Boolean isTraversable(File f) {
		return null; // let the L&F FileView figure this out
	}

	/**
	 * funcion que devuelve el tipo de descripcion de la imagen
	 * 
	 * @param f imagen elegida
	 * @return tipo de descripcion
	 */
	public String getTypeDescription(File f) {
		String extension = Utilidades.getExtension(f);
		String type = null;

		if (extension != null) {
			if (extension.equals(Utilidades.jpeg) || extension.equals(Utilidades.jpg)) {
				type = "JPEG Image";
			} else if (extension.equals(Utilidades.gif)) {
				type = "GIF Image";
			} else if (extension.equals(Utilidades.tiff) || extension.equals(Utilidades.tif)) {
				type = "TIFF Image";
			} else if (extension.equals(Utilidades.png)) {
				type = "PNG Image";
			}
		}
		return type;
	}

	/**
	 * funcion que devuelve el icono de la imagen
	 * 
	 * @param f imagen elegida
	 * @return icono de la imagen
	 */
	public Icon getIcon(File f) {
		String extension = Utilidades.getExtension(f);
		Icon icon = null;

		if (extension != null) {
			if (extension.equals(Utilidades.jpeg) || extension.equals(Utilidades.jpg)) {
				icon = jpgIcon;
			} else if (extension.equals(Utilidades.gif)) {
				icon = gifIcon;
			} else if (extension.equals(Utilidades.tiff) || extension.equals(Utilidades.tif)) {
				icon = tiffIcon;
			} else if (extension.equals(Utilidades.png)) {
				icon = pngIcon;
			}
		}
		return icon;
	}
}