package LP;

import static COMUN.clsConstantes.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import LN.clsGestorLN;
import peliculasOnline.clsMain;

/**
 * Jframe encargado en iniciar el programa donde el usuario podra registrarse o
 * hacer login en la aplicacion
 * 
 * @author Eric
 *
 */
public class frmLogin extends JFrame implements ActionListener, InternalFrameListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1765043199414680654L;
	/**
	 * objeto de el panel de la ventana de login
	 */
	JPanel contentPane;
	/**
	 * campo de texto del nombre de usuario de la ventana de login
	 */
	private JTextField txtUser_name;
	/**
	 * campo de texto de la password de la ventana de login
	 */
	private JPasswordField passPassword;
	/**
	 * objeto de el usuario que se va a conectar
	 */
	private itfProperty usuarioActual;
	/**
	 * objeto del gestor de la logica de negocio
	 */
	private clsGestorLN objGestor;
	/**
	 * escritorio donde se insertaran los internal de registros
	 */
	private JDesktopPane desktop;
	/**
	 * objeto del internal frame del registro de empleado
	 */
	private frmInRegistroEmpleado objRegistroEmpleado;

	/**
	 * constuctor de la ventana de login
	 */
	public frmLogin() {
		objGestor = new clsGestorLN();
		this.CrearPanel();
		this.setLocationRelativeTo(null);

	}

	/**
	 * funcion que crea le panel de la venan de login
	 */
	private void CrearPanel() {

		setBounds(100, 100, 450, 326);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.repaint();

		this.setContentPane(contentPane);
		contentPane.setLayout(null);

		desktop = new JDesktopPane();
		desktop.setBounds(0, 0, 436, 289);
		contentPane.add(desktop);

		JLabel lblUser_name = new JLabel("nombre usuario", SwingConstants.CENTER);
		lblUser_name.setBounds(160, 66, 114, 14);
		desktop.add(lblUser_name);

		JLabel lblLogin = new JLabel("Login", SwingConstants.CENTER);
		lblLogin.setBounds(160, 41, 114, 20);
		desktop.add(lblLogin);

		JLabel lblPassword = new JLabel("Password", SwingConstants.CENTER);
		lblPassword.setBounds(160, 122, 114, 14);
		desktop.add(lblPassword);

		passPassword = new JPasswordField();
		passPassword.setColumns(10);
		passPassword.setBounds(160, 146, 114, 20);
		desktop.add(passPassword);

		txtUser_name = new JTextField();
		txtUser_name.setBounds(160, 91, 114, 20);
		desktop.add(txtUser_name);
		txtUser_name.setColumns(10);

		JButton btnRegistro = new JButton("registrarse");
		btnRegistro.setBounds(160, 212, 114, 23);
		btnRegistro.addActionListener(this);
		btnRegistro.setActionCommand(REGISTRO);
		desktop.add(btnRegistro);

		JButton btnLogin = new JButton("Aceptar");
		btnLogin.setBounds(160, 178, 114, 23);
		btnLogin.addActionListener(this);
		btnLogin.setActionCommand(LOGIN);
		desktop.add(btnLogin);
	}

	/**
	 * funcion que determina que menu mostrar si el de socio o el de algun empleado
	 */
	private void elegirMenu() {
		try {
			boolean tipoUsuario = objGestor.tipoUsuario(usuarioActual);
			if (tipoUsuario) {
				menuEmpleado();
			} else {
				frmMenuSocio objS = new frmMenuSocio(usuarioActual, objGestor);
				objS.createAndShowGUI();
			}
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Menu del empleado, va a dirigir a tres tipos de empleados diferentes
	 */
	private void menuEmpleado() {
		int idTipoEmpleado = (int) usuarioActual.getProperty(EMPLEADO_COL_TIPO_EMPLEADO_FK);
		itfProperty tipoEmpleado = null;
		try {
			tipoEmpleado = objGestor.devolverTipoEmpleado(idTipoEmpleado);
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {

			System.out.println(e.getMessage());
		}
		switch ((String) tipoEmpleado.getProperty(TIPO_EMPLEADO_COL_NOMBRE)) {
		case TIPO_EMPLEADO_ADMIN: {
			frmMenuAdmin objA = new frmMenuAdmin(usuarioActual, objGestor);
			objA.createAndShowGUI();
			break;
		}
		case TIPO_EMPLEADO_GESTOR: {
			frmMenuGestor objG = new frmMenuGestor(usuarioActual, objGestor);
			objG.createAndShowGUI();
			break;
		}
		case TIPO_EMPLEADO_TECNICO: {

			break;
		}
		}

	}

	/**
	 * funcion que se encarga de escuchar los diferentes eventos que ocurren en la
	 * ventana de login
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case LOGIN: {
			String user_name = txtUser_name.getText();
			String password = "";
			char[] passwordChar = passPassword.getPassword();
			for (int i = 0; i < passwordChar.length; i++) {
				password = password + passwordChar[i];
			}
			try {
				usuarioActual = objGestor.devolverUsuario(user_name);
				if (usuarioActual == null) {
					System.out.println("nombre de usuario o password erroneos");
					JOptionPane.showMessageDialog(null, "Nombre de usuario o password erroneos");
					this.dispose();
					clsMain.main(null);
				} else if (!usuarioActual.getProperty(USUARIO_COL_PASSWORD).equals(password)) {
					usuarioActual = null;
					// System.out.println("nombre de usuario o password erroneos");
					JOptionPane.showMessageDialog(null, "Nombre de usuario o password erroneos");
					this.dispose();
					clsMain.main(null);
				}
			} catch (clsErrorSQLConexion | clsErrorSQLConsulta error) {
				System.out.println(error.getMessage());
			}

			System.out.println("saliendo");
			elegirMenu();
			this.dispose();
			break;
		}
		case REGISTRO: {
			int respuesta = JOptionPane.showOptionDialog(this, "Elige tipo Usuario a registrar", // contenido de la
																									// ventana
					"Registrar", // titulo de la ventana
					JOptionPane.YES_NO_CANCEL_OPTION, // para 3 botones si/no/cancel
					JOptionPane.QUESTION_MESSAGE, // tipo de �cono
					null, // null para icono por defecto.
					new Object[] { "Socio", "Empleado", "Cancelar" }, // objeto para las opciones
					// null para YES, NO y CANCEL
					"Socio"); // selecci�n predeterminada
			if (respuesta == 0) {
				// registrar socio
			}
			if (respuesta == 1) {
				iniciarEmpleado();
			}
			break;
		}
		}

	}

	/**
	 * funcion que crea ventana para registrarse como empleado
	 */
	private void iniciarEmpleado() {
		if (objRegistroEmpleado != null) {
			objRegistroEmpleado.dispose();
		}
		objRegistroEmpleado = new frmInRegistroEmpleado(objGestor, REGISTRO + " " + USUARIO_EMPLEADO, false, true,
				false, false);
		desktop.add(objRegistroEmpleado);
		objRegistroEmpleado.addInternalFrameListener(this);
		Dimension desktopSize = desktop.getSize();
		Dimension FrameSize = objRegistroEmpleado.getSize();
		objRegistroEmpleado.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objRegistroEmpleado.show();
	}

	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}
}
