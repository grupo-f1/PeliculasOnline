package LP;

import java.io.*;

import javax.swing.ImageIcon;

/**
 * Esta clase permite hacer uso de los dispositivos de entrada de forma muy
 * sencilla. Posee distintos métodos públicos y estáticos (por lo tanto no se
 * necesita un objeto como tal para acceder a ellos) para la lectura de los
 * tipos de datos básicos en Java. Además, todos los métodos tienen un
 * control de errores mediante petición sucesiva de introducción de dato hasta
 * que estos sean correctos.
 */
public class Utilidades {
	public final static String jpeg = "jpeg";
	public final static String jpg = "jpg";
	public final static String gif = "gif";
	public final static String tiff = "tiff";
	public final static String tif = "tif";
	public final static String png = "png";

	/**
	 * Conseguir la extension de el archivo
	 * 
	 * @param f archivo elegido
	 * @return extension del archivo
	 */
	public static String getExtension(File f) {
		String ext = null;
		String s = f.getName();
		int i = s.lastIndexOf('.');

		if (i > 0 && i < s.length() - 1) {
			ext = s.substring(i + 1).toLowerCase();
		}
		return ext;
	}

	/**
	 * Funcion que devuelve la imagen o null si no es valido
	 * 
	 * @param path ruta del archivo elegido
	 * @return imagen o null del archivo
	 */
	protected static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = Utilidades.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	/**
	 * Permite introducir por teclado en la consola un número entero (32 bits =
	 * -2147483648 2147483648) realizando las comprobaciones pertienentes.
	 * 
	 * @return Devuelve el número entero que se haya introducido por teclado.
	 */
	public static int leerEntero() {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		Integer entero = null;
		boolean error = true;
		do {
			try {
				String cadena = br.readLine();
				entero = new Integer(cadena);
				error = false;
			} catch (NumberFormatException nfe) {
				System.out.println("No tecleo un numero entero-Repetir");
			} catch (Exception e) {
				System.out.println("Error de entrada-Repetir ");
			}
		} while (error);
		return entero.intValue();
	}

	/**
	 * Permite introducir por teclado en la consola un n�mero real realizando las
	 * comprobaciones pertienentes.
	 * 
	 * @return Devuelve el número real que se haya introducido por teclado.
	 */
	public static double leerReal() {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		Double real = null;
		boolean error = true;
		do {
			try {
				String cadena = br.readLine();
				real = new Double(cadena);
				error = false;
			} catch (NumberFormatException nfe) {
				System.out.println("No tecleo un numero real-Repetir ");
			} catch (Exception e) {
				System.out.println("Error de entrada-Repetir ");
			}
		} while (error);
		return real.doubleValue();
	}

	/**
	 * Permite introducir por teclado en la consola un caracter (8 bits = -128 127)
	 * realizando las comprobaciones pertienentes.
	 * 
	 * @return Devuelve el caracter que se haya introducido por teclado.
	 */
	public static char leerCaracter() {
		char caracter = 0;
		boolean error = true;
		do {
			try {
				caracter = (char) System.in.read();
				System.in.skip(System.in.available());
				error = false;
			} catch (Exception e) {
				System.out.println("Error de entrada-Repetir ");
			}
		} while (error);
		return caracter;
	}

	/**
	 * Permite introducir por teclado en la consola una cadena de caracteres
	 * (String) realizando las comprobaciones pertienentes.
	 * 
	 * @return Devuelve la cadena de caracteres que se haya introducido por teclado.
	 */
	public static String leerCadena() {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String cadena = null;
		boolean error = true;
		do {
			try {
				cadena = br.readLine();
				error = false;
			} catch (Exception e) {
				System.out.println("Error de entrada-Repetir ");
			}
		} while (error);
		return cadena;
	}
}
