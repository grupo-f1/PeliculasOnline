package LP;

import static COMUN.clsConstantes.*;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;
import LN.clsGestorLN;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JRadioButton;
import javax.swing.JDesktopPane;

/**
 * ventana interna para la creacion de una nueva pelicula
 * 
 * @author Eric
 *
 */
public class frmInUpdatePelicula extends JInternalFrame implements ActionListener, InternalFrameListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7666912571195053641L;
	/**
	 * objeto gestor de la logica de negocio
	 */
	private clsGestorLN objGestor;
	/**
	 * modelo de la lista de tipos de pelicula
	 */
	private DefaultListModel<String> modelTiposPelicula;
	/**
	 * lista de tipos de pelicula elegidas
	 */
	private JList tipoPeliculasElegidas;
	/**
	 * lista de tipos de peliculas para la combobox de tipos de peliculas
	 */
	private ArrayList<itfProperty> listaPeliculas;
	/**
	 * lista de proveedores para la comboBox de proveedores
	 */
	private ArrayList<itfProperty> listaProveedores;
	/**
	 * comboBox de tipos de pelicula
	 */
	private JComboBox<Object> comboTipoPelicula;
	/**
	 * comboBox de proveedores
	 */
	private JComboBox<Object> comboProveedor;
	/**
	 * cuadro de texto para el titulo de la pelicula
	 */
	private JTextField txtTitulo;
	/**
	 * cuadro de texto para la duracion de la pelicula
	 */
	private JTextField txtDuracion;
	/**
	 * comboBox para la clasificacion de la pelicula
	 */
	private JComboBox<Object> comboClasificacion;
	/**
	 * comboBox para los idiomas de la peliculas
	 */
	private JComboBox<Object> comboIdiomas;
	/**
	 * lista de los idiomas elegidos
	 */
	private JList idiomasLista;
	/**
	 * modelo del comboBox de los idiomas
	 */
	private DefaultListModel<String> modelIdiomas;
	/**
	 * radioButton si no hay subtitulos
	 */
	private JRadioButton rdbtnSubtitulosNo;
	/*
	 * radioButton si hay subtitulos
	 */
	private JRadioButton rdbtnSubtitulosSi;
	/**
	 * comboBox para la resolucion de la pelicula
	 */
	private JComboBox<Object> comboResolucion;
	/**
	 * panel de escritorio donde esta todos los componentes incluidos los
	 * JinternalFrame
	 */
	private JDesktopPane desktopPane;
	/**
	 * area de texto de la descripcion
	 */
	private JTextArea txtAreaDescripcion;
	/**
	 * lista de los tipos de pelicula
	 */
	private ArrayList<itfProperty> listaTiposPelicula;
	/**
	 * objeto de la internalFrame de Elegir Portada
	 */
	private frmArchivoPortada objArchivoPortada;
	/**
	 * archivo de la imgaen de la portada
	 */
	private File portadaArchivo;
	/**
	 * label donde ira el nombre de la portada de la pelicula elegida
	 */
	private JLabel lblPortada;
	/**
	 * comboBox de los empleados
	 */
	private JComboBox<Object> comboEmpleado;
	/**
	 * lista de los empleados
	 */
	private ArrayList<itfProperty> listaEmpleados;
	/**
	 * pelicula a editar
	 */
	private itfProperty peliculaEditar;

	/**
	 * constructor de la internal frame insertar Pelicula
	 * 
	 * @param objGestor       objeto gestor de la logica de negocio
	 * @param pelicula        pelicula a editar
	 * @param titulo          titulo de la internal frame
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmInUpdatePelicula(clsGestorLN objGestor, itfProperty pelicula, String titulo, boolean redimensionable,
			boolean cerrable, boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.objGestor = objGestor;
		this.peliculaEditar = pelicula;
		this.CrearPanel();

	}

	/**
	 * funcion que rellean el panel con los componentes pertinentes
	 */
	private void CrearPanel() {
		setBounds(100, 100, 605, 423);

		desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 595, 394);
		getContentPane().add(desktopPane);

		JLabel lblListaTiposPelicula = new JLabel("lista tipos pelicula", SwingConstants.CENTER);
		lblListaTiposPelicula.setBounds(450, 209, 118, 14);
		desktopPane.add(lblListaTiposPelicula);

		JLabel lblTipoPelicula = new JLabel("Tipo Pelicula", SwingConstants.CENTER);
		lblTipoPelicula.setBounds(450, 155, 108, 14);
		desktopPane.add(lblTipoPelicula);

		JLabel lblListaIdiomas = new JLabel("lista idiomas", SwingConstants.CENTER);
		lblListaIdiomas.setBounds(328, 209, 112, 14);
		desktopPane.add(lblListaIdiomas);

		JLabel lblIdiomas = new JLabel("Idiomas", SwingConstants.CENTER);
		lblIdiomas.setBounds(328, 155, 112, 14);
		desktopPane.add(lblIdiomas);

		JLabel lblProveedor = new JLabel("Proveedor", SwingConstants.CENTER);
		lblProveedor.setBounds(35, 95, 108, 14);
		desktopPane.add(lblProveedor);

		JLabel lblEmpleado = new JLabel("Empleado", SwingConstants.CENTER);
		lblEmpleado.setBounds(35, 50, 108, 14);
		desktopPane.add(lblEmpleado);

		JLabel lblTitulo = new JLabel("Titulo", SwingConstants.CENTER);
		lblTitulo.setBounds(35, 11, 108, 14);
		desktopPane.add(lblTitulo);

		JLabel lblDuracion = new JLabel("Duracion", SwingConstants.CENTER);
		lblDuracion.setBounds(35, 142, 108, 14);
		desktopPane.add(lblDuracion);

		JLabel lblDescripcion = new JLabel("Descripcion", SwingConstants.CENTER);
		lblDescripcion.setBounds(335, 11, 223, 14);
		desktopPane.add(lblDescripcion);

		JLabel lblClasificacion = new JLabel("Clasificacion", SwingConstants.CENTER);
		lblClasificacion.setBounds(35, 191, 108, 14);
		desktopPane.add(lblClasificacion);

		JLabel lblSubtitulos = new JLabel("Subtitulos", SwingConstants.CENTER);
		lblSubtitulos.setBounds(35, 242, 108, 14);
		desktopPane.add(lblSubtitulos);

		JLabel lblResolucion = new JLabel("Resolucion", SwingConstants.CENTER);
		lblResolucion.setBounds(35, 293, 108, 14);
		desktopPane.add(lblResolucion);

		lblPortada = new JLabel();
		lblPortada.setFont(lblPortada.getFont().deriveFont(Font.ITALIC));
		lblPortada.setHorizontalAlignment(JLabel.CENTER);
		lblPortada.setVerticalAlignment(JLabel.CENTER);

		if (peliculaEditar.getProperty(PELICULA_COL_PORTADA) != null) {
			String[] portadaSplit = peliculaEditar.getProperty(PELICULA_COL_PORTADA).toString().split("[.]");
			updateLabel(portadaSplit[0]);
		} else {
			lblPortada.setText("Sin portada");
		}
		lblPortada.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));

		// The preferred size is hard-coded to be the width of the
		// widest image and the height of the tallest image + the border.
		// A real program would compute this.
		lblPortada.setPreferredSize(new Dimension(177, 122 + 10));
		lblPortada.setBounds(169, 108, 144, 199);
		desktopPane.add(lblPortada);

		comboProveedor = new JComboBox<Object>();
		listaProveedores = rellenarComboBoxProveedor();
		for (itfProperty aux : listaProveedores) {
			comboProveedor.addItem((String) aux.getProperty(PROVEEDOR_COL_NOMBRE));
			if (aux.getProperty(PROVEEDOR_COL_ID) == peliculaEditar.getProperty(PELICULA_COL_PROVEEDOR_FK)) {
				comboProveedor.setSelectedItem((String) aux.getProperty(PROVEEDOR_COL_NOMBRE));
			}
		}
		comboProveedor.setBounds(35, 111, 108, 20);
		comboProveedor.addActionListener(this);
		desktopPane.add(comboProveedor);

		comboEmpleado = new JComboBox<Object>();
		listaEmpleados = rellenarComboBoxEmpleado();
		for (itfProperty aux : listaEmpleados) {
			comboEmpleado.addItem((String) aux.getProperty(USUARIO_COL_USER_NAME));
			if (aux.getProperty(EMPLEADO_COL_ID) == peliculaEditar.getProperty(PELICULA_COL_EMPLEADO_FK)) {
				comboEmpleado.setSelectedItem((String) aux.getProperty(USUARIO_COL_USER_NAME));
			}
		}
		comboEmpleado.setBounds(35, 67, 108, 20);
		comboEmpleado.addActionListener(this);
		desktopPane.add(comboEmpleado);

		comboTipoPelicula = new JComboBox<Object>();
		listaPeliculas = rellenarComboBoxTipoPelicula();
		for (itfProperty aux : listaPeliculas) {
			comboTipoPelicula.addItem((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE));
		}
		comboTipoPelicula.setBounds(450, 178, 108, 20);
		comboTipoPelicula.addActionListener(this);
		comboTipoPelicula.setActionCommand(CAMBIO_COMBO_TIPO_PELICULA);
		desktopPane.add(comboTipoPelicula);

		modelTiposPelicula = new DefaultListModel<>();
		JScrollPane scrollPanelListaTiposPelicula = new JScrollPane();
		scrollPanelListaTiposPelicula.setBounds(450, 234, 118, 97);
		desktopPane.add(scrollPanelListaTiposPelicula);

		tipoPeliculasElegidas = new JList<Object>();
		ArrayList<itfProperty> listaPeliculaTipos = new ArrayList<itfProperty>();
		try {
			listaPeliculaTipos = objGestor.devolverPeliculaTipos((int) peliculaEditar.getProperty(PELICULA_COL_ID));
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		for (itfProperty aux : listaPeliculaTipos) {
			for (itfProperty tipo : listaTiposPelicula) {
				if (tipo.getProperty(TIPO_PELICULA_COL_ID) == aux.getProperty(PELICULA_TIPOS_COL_TIPO_PELICULA_FK)) {
					modelTiposPelicula.addElement((String) tipo.getProperty(TIPO_PELICULA_COL_NOMBRE));
				}
			}
		}
		tipoPeliculasElegidas.setModel(modelTiposPelicula);
		scrollPanelListaTiposPelicula.setViewportView(tipoPeliculasElegidas);

		JButton btnBorrarTipoPelicula = new JButton("Borrar");
		btnBorrarTipoPelicula.addActionListener(this);
		btnBorrarTipoPelicula.setActionCommand(BORRAR_TIPO_PELICULA_LISTA);
		btnBorrarTipoPelicula.setBounds(450, 342, 118, 23);
		desktopPane.add(btnBorrarTipoPelicula);

		comboIdiomas = new JComboBox<Object>(IDIOMAS);
		comboIdiomas.setBounds(328, 178, 112, 20);
		comboIdiomas.addActionListener(this);
		comboIdiomas.setActionCommand(CAMBIO_COMBO_IDIOMA);
		desktopPane.add(comboIdiomas);

		modelIdiomas = new DefaultListModel<>();

		JScrollPane scrollPanelIdiomas = new JScrollPane();
		scrollPanelIdiomas.setBounds(328, 234, 112, 97);
		desktopPane.add(scrollPanelIdiomas);

		idiomasLista = new JList<Object>();
		String[] idiomas = peliculaEditar.getProperty(PELICULA_COL_IDIOMAS).toString().split(",");
		for (int i = 0; i < idiomas.length; i++) {
			modelIdiomas.addElement(idiomas[i].trim());
		}
		idiomasLista.setModel(modelIdiomas);
		scrollPanelIdiomas.setViewportView(idiomasLista);

		JButton btnBorrarIdioma = new JButton("Borrar");
		btnBorrarIdioma.addActionListener(this);
		btnBorrarIdioma.setActionCommand(BORRAR_IDIOMA_LISTA);
		btnBorrarIdioma.setBounds(328, 342, 112, 23);
		desktopPane.add(btnBorrarIdioma);

		JButton btnUpdate = new JButton("Editar");
		btnUpdate.addActionListener(this);
		btnUpdate.setActionCommand(EDITAR);
		btnUpdate.setBounds(35, 360, 108, 23);
		desktopPane.add(btnUpdate);

		txtTitulo = new JTextField();
		txtTitulo.setBounds(35, 25, 108, 20);
		txtTitulo.setText((String) peliculaEditar.getProperty(PELICULA_COL_TITULO));
		desktopPane.add(txtTitulo);
		txtTitulo.setColumns(10);

		JScrollPane scrollPanelDescripcion = new JScrollPane();
		scrollPanelDescripcion.setBounds(335, 36, 223, 95);
		desktopPane.add(scrollPanelDescripcion);

		txtAreaDescripcion = new JTextArea();
		txtAreaDescripcion.setText((String) peliculaEditar.getProperty(PELICULA_COL_DESCRIPCION));
		scrollPanelDescripcion.setViewportView(txtAreaDescripcion);

		txtDuracion = new JTextField();
		txtDuracion.setBounds(35, 160, 108, 20);
		txtDuracion.setText(peliculaEditar.getProperty(PELICULA_COL_DURACION).toString());
		desktopPane.add(txtDuracion);
		txtDuracion.setColumns(10);

		comboClasificacion = new JComboBox<Object>(CLASIFICACIONES);
		comboClasificacion.setBounds(35, 209, 108, 20);
		for (int i = 0; i < CLASIFICACIONES.length; i++) {
			if (CLASIFICACIONES[i].equals(peliculaEditar.getProperty(PELICULA_COL_CLASIFICACION))) {
				comboClasificacion.setSelectedItem(CLASIFICACIONES[i]);
			}
		}
		desktopPane.add(comboClasificacion);

		ButtonGroup rBtnGroupSubtitulos = new ButtonGroup();

		rdbtnSubtitulosSi = new JRadioButton("Si", true);
		rdbtnSubtitulosSi.setBounds(35, 263, 52, 23);

		rBtnGroupSubtitulos.add(rdbtnSubtitulosSi);
		desktopPane.add(rdbtnSubtitulosSi);

		rdbtnSubtitulosNo = new JRadioButton("No", false);
		rdbtnSubtitulosNo.setBounds(86, 263, 57, 23);
		rBtnGroupSubtitulos.add(rdbtnSubtitulosNo);
		desktopPane.add(rdbtnSubtitulosNo);
		if ((boolean) peliculaEditar.getProperty(PELICULA_COL_SUBTITULOS)) {
			rdbtnSubtitulosSi.setSelected(true);
		} else {
			rdbtnSubtitulosNo.setSelected(true);
		}
		comboResolucion = new JComboBox<Object>(RESOLUCIONES);

		comboResolucion.setBounds(35, 317, 108, 20);
		desktopPane.setLayout(null);
		for (int i = 0; i < RESOLUCIONES.length; i++) {
			if (RESOLUCIONES[i].equals(peliculaEditar.getProperty(PELICULA_COL_RESOLUCION))) {
				comboResolucion.setSelectedItem(RESOLUCIONES[i]);
			}
		}
		desktopPane.add(comboResolucion);

		JButton btnPortada = new JButton("Portada");
		btnPortada.setBounds(169, 73, 144, 23);
		btnPortada.addActionListener(this);
		btnPortada.setActionCommand(PORTADA);
		desktopPane.add(btnPortada);

	}

	/**
	 * actualiza el nombre de la imagen a mostrar
	 * 
	 * @param name nombre de la imagen a cambiar
	 */
	private void updateLabel(String name) {
		ImageIcon icon = createImageIcon("Portada/" + name + ".gif");
		if (icon != null) {
			lblPortada.setText(null);
			lblPortada.setToolTipText((String) peliculaEditar.getProperty(PELICULA_COL_PORTADA));
			ImageIcon thumbnail = null;
			if (icon.getIconWidth() > 90) {
				thumbnail = new ImageIcon(icon.getImage().getScaledInstance(90, -1, Image.SCALE_DEFAULT));
			} else if (icon.getIconHeight() > 50) {
				thumbnail = new ImageIcon(icon.getImage().getScaledInstance(-1, 50, Image.SCALE_DEFAULT));
			} else { // no need to miniaturize
				thumbnail = icon;
			}
			lblPortada.setIcon(thumbnail);
		} else {
			lblPortada.setText("Portada no encontrada");
		}
	}

	/**
	 * devuelve la imagen o un mensaje de no encontrado
	 * 
	 * @param path ruta a la imagen
	 * @return imagen a mostrar
	 */
	private ImageIcon createImageIcon(String path) {
		File archivoPortada = new File(PORTADA + "\\" + peliculaEditar.getProperty(PELICULA_COL_PORTADA));
		String imgURL = archivoPortada.getPath();
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("No es ha encontrado el archivo: " + path);
			return null;
		}
	}

	/**
	 * coger todos los tipos de peliculas de BBDD para el comboBox de tipos de
	 * peliculas
	 * 
	 * @return lista de los tipos de peliculas
	 */
	private ArrayList<itfProperty> rellenarComboBoxTipoPelicula() {
		listaTiposPelicula = null;
		try {
			listaTiposPelicula = objGestor.listarTipoPelicula();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		return listaTiposPelicula;
	}

	/**
	 * coger todos los proveedores de BBDD para el comboBox de proveedores
	 * 
	 * @return lista de los proveedores
	 */
	private ArrayList<itfProperty> rellenarComboBoxProveedor() {
		ArrayList<itfProperty> listaProveedor = null;
		try {
			listaProveedor = objGestor.listarProveedor();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		return listaProveedor;
	}

	/**
	 * coger todos los empleados de BBDD para el comboBox de empleados
	 * 
	 * @return lista de los proveedores
	 */
	private ArrayList<itfProperty> rellenarComboBoxEmpleado() {
		ArrayList<itfProperty> listaEmpleados = null;
		try {
			listaEmpleados = objGestor.listarUsuario(USUARIO_EMPLEADO);
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		return listaEmpleados;
	}

	/**
	 * escuchador de acciones en la internal frame
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case BORRAR_TIPO_PELICULA_LISTA: {
			String tipoPeliculaList = (String) tipoPeliculasElegidas.getSelectedValue();
			deleteListTipoPeliculas(tipoPeliculaList);
			break;
		}
		case CAMBIO_COMBO_TIPO_PELICULA: {
			String tipoPelicula = (String) comboTipoPelicula.getSelectedItem();
			updateListTipoPeliculas(tipoPelicula);
			break;
		}
		case BORRAR_IDIOMA_LISTA: {
			String idiomaList = (String) idiomasLista.getSelectedValue();
			deleteListIdiomas(idiomaList);
			break;
		}
		case CAMBIO_COMBO_IDIOMA: {
			String idioma = (String) comboIdiomas.getSelectedItem();
			updateIdiomas(idioma);
			break;
		}
		case EDITAR: {
			editarPelicula();
			break;
		}
		case PORTADA: {
			mostrarVentanaPortada();
			break;
		}
		}
	}

	/**
	 * funcion que muestar la ventana de portada
	 */
	private void mostrarVentanaPortada() {
		objArchivoPortada = new frmArchivoPortada(lblPortada, INSERTAR + " " + PORTADA, false, true, false, false);
		desktopPane.add(objArchivoPortada);
		objArchivoPortada.addInternalFrameListener(this);
		objArchivoPortada.pack();
		Dimension desktopSize = desktopPane.getSize();
		Dimension FrameSize = objArchivoPortada.getSize();
		objArchivoPortada.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objArchivoPortada.show();
	}

	/**
	 * funcion que edita una pelicula
	 */
	public void editarPelicula() {
		if (txtTitulo.getText().isEmpty() || txtAreaDescripcion.getText().isEmpty()
				|| idiomasLista.getModel().getSize() == 0 || tipoPeliculasElegidas.getModel().getSize() == 0) {
			JOptionPane.showMessageDialog(this, "campo obligatorio sin rellenar");
		} else {
			int proveedorId = cambiarIdProveedor((String) comboProveedor.getSelectedItem());
			String clasificacion = (String) comboClasificacion.getSelectedItem();
			String resolucion = (String) comboResolucion.getSelectedItem();
			String descripcion = txtAreaDescripcion.getText();
			String titulo = txtTitulo.getText();
			int duracion = Integer.parseInt(txtDuracion.getText());
			String listaIdiomas = "";
			for (int i = 0; i < idiomasLista.getModel().getSize(); i++) {
				if (i == (idiomasLista.getModel().getSize() - 1)) {
					listaIdiomas = listaIdiomas + idiomasLista.getModel().getElementAt(i);
				} else {
					listaIdiomas = listaIdiomas + idiomasLista.getModel().getElementAt(i) + ", ";
				}
			}

			boolean subtitulos = false;
			String portada = null;
			if (lblPortada.getIcon() != null || !lblPortada.getText().equalsIgnoreCase("Sin portada")) {
				if (!lblPortada.getToolTipText().equals(peliculaEditar.getProperty(PELICULA_COL_PORTADA))) {
					String[] portadaDividido = portadaArchivo.getName().toString().split("[.]");
					portada = System.currentTimeMillis() + "." + portadaDividido[(portadaDividido.length - 1)];
				} else {
					portada = (String) peliculaEditar.getProperty(PELICULA_COL_PORTADA);
				}
			}

			if (rdbtnSubtitulosSi.isSelected()) {
				subtitulos = true;
			}
			if (rdbtnSubtitulosNo.isSelected()) {
				subtitulos = false;
			}
			int empleadoId = cambiarIdEmpleado((String) comboEmpleado.getSelectedItem());
			try {
				int filasAfectadas = objGestor.editarPelicula(empleadoId, proveedorId, titulo, descripcion, portada,
						resolucion, listaIdiomas, subtitulos, duracion, clasificacion,
						(int) peliculaEditar.getProperty(PELICULA_COL_ID));
				if (filasAfectadas == 1) {
					JOptionPane.showMessageDialog(this, "pelicula editada");
				} else {
					JOptionPane.showMessageDialog(this, "pelicula no editada");
				}
				objGestor.borrarPeliculaTipos((int) peliculaEditar.getProperty(PELICULA_COL_ID));

				for (int i = 0; i < tipoPeliculasElegidas.getModel().getSize(); i++) {
					int idTipoPelicula = conseguirIdTipoPelicula(
							(String) tipoPeliculasElegidas.getModel().getElementAt(i));
					objGestor.insertarPeliculaTipos(idTipoPelicula, (int) peliculaEditar.getProperty(PELICULA_COL_ID));
				}
			} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConsulta
					| clsErrorSQLConexion e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage());
			}
			File portadaVieja = new File(PORTADA + "\\" + peliculaEditar.getProperty(PELICULA_COL_PORTADA));
			if (portadaVieja.exists()
					&& !lblPortada.getToolTipText().equals(peliculaEditar.getProperty(PELICULA_COL_PORTADA))) {
				portadaVieja.delete();
			}
			// mover imagen a carpeta Portada
			if (portadaArchivo != null) {
				Path origenPath = FileSystems.getDefault().getPath(portadaArchivo.getPath());
				Path destinoPath = FileSystems.getDefault().getPath(PORTADA + "\\" + portada);

				try {
					Files.move(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					System.err.println(e);
				}
			}
			this.dispose();
		}
	}

	/**
	 * funcion que consigue el id del tipo de pelicula
	 * 
	 * @param nombre del tipo de pelicula
	 * @return id del tipo de pelicula
	 */
	private int conseguirIdTipoPelicula(String nombre) {
		int idTipoPelicula = -1;
		for (itfProperty aux : listaTiposPelicula) {
			if (nombre.equalsIgnoreCase((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE))) {
				idTipoPelicula = (int) aux.getProperty(TIPO_PELICULA_COL_ID);
			}
		}
		return idTipoPelicula;
	}

	/**
	 * funcion que cambia el id del proveedor segun que se eliga en el comboBox
	 * 
	 * @param proveedor nombre del proveedor
	 * @return id del proveedor
	 */
	private int cambiarIdProveedor(String proveedor) {
		int id = -1;
		for (itfProperty aux : listaProveedores) {
			if (proveedor.equalsIgnoreCase((String) aux.getProperty(PROVEEDOR_COL_NOMBRE))) {
				id = (int) aux.getProperty(PROVEEDOR_COL_ID);
			}
		}
		return id;
	}

	/**
	 * funcion que cambia el id del empleado segun que se eliga en el comboBox
	 * 
	 * @param empleado nombre del empleado
	 * @return id del proveedor
	 */
	private int cambiarIdEmpleado(String empleado) {
		int id = -1;
		for (itfProperty aux : listaEmpleados) {
			if (empleado.equalsIgnoreCase((String) aux.getProperty(USUARIO_COL_USER_NAME))) {
				id = (int) aux.getProperty(EMPLEADO_COL_ID);
			}
		}
		return id;
	}

	/**
	 * borrar de la lista de tipos de pelicula un tipo de pelicula
	 * 
	 * @param tipoPeliculaList tipo de pelicula a eliminar
	 */
	private void deleteListTipoPeliculas(String tipoPeliculaList) {
		if (tipoPeliculaList != null) {
			modelTiposPelicula.removeElement(tipoPeliculaList);
			tipoPeliculasElegidas.setModel(modelTiposPelicula);

		} else {
			JOptionPane.showMessageDialog(this, "elige un tipo pelicula de la lista para eliminar");
		}
	}

	/**
	 * introducir el tipo pelicula en la lista
	 * 
	 * @param name nombre el tipo de pelicula
	 */
	private void updateListTipoPeliculas(String name) {
		for (itfProperty aux : listaPeliculas) {
			if (name.equalsIgnoreCase((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE))) {
				if (modelTiposPelicula.contains((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE))) {
					JOptionPane.showMessageDialog(this, "tipo pelicula ya elegida");
				} else {
					modelTiposPelicula.addElement((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE));
				}
			}
		}
		tipoPeliculasElegidas.setModel(modelTiposPelicula);
	}

	/**
	 * borrar de la lista de idiomas
	 * 
	 * @param idioma idioma a eliminar
	 */
	private void deleteListIdiomas(String idioma) {
		if (idioma != null) {
			modelIdiomas.removeElement(idioma);
			idiomasLista.setModel(modelIdiomas);

		} else {
			JOptionPane.showMessageDialog(this, "elige un idioma de la lista para eliminar");
		}
	}

	/**
	 * introducir el idioma de la lista de idiomas
	 * 
	 * @param idioma a insertar
	 */
	private void updateIdiomas(String idioma) {
		if (modelIdiomas.contains(idioma)) {
			JOptionPane.showMessageDialog(this, "idioma ya elegido");
		} else {
			modelIdiomas.addElement(idioma);
		}
		idiomasLista.setModel(modelIdiomas);
	}

	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {
		portadaArchivo = objArchivoPortada.cogerFile();

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}
}
