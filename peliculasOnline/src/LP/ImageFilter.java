package LP;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Filtrador de tipos de archivos apra que solo muestre imagenes
 * 
 * @author Eric
 *
 */
public class ImageFilter extends FileFilter {

	/**
	 * funcion que accepta todos los directorios y todos los gif, jpg, tiff, or png
	 * 
	 * @param f archivo
	 * @return si es aceptado el archivo o no
	 */
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}

		String extension = Utilidades.getExtension(f);
		if (extension != null) {
			if (extension.equals(Utilidades.tiff) || extension.equals(Utilidades.tif)
					|| extension.equals(Utilidades.gif) || extension.equals(Utilidades.jpeg)
					|| extension.equals(Utilidades.jpg) || extension.equals(Utilidades.png)) {
				return true;
			} else {
				return false;
			}
		}

		return false;
	}

	/**
	 * funcion que devuelve la descripcion de el filtro
	 */
	public String getDescription() {
		return "Just Images";
	}
}