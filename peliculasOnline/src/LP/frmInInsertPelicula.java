package LP;

import static COMUN.clsConstantes.*;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;
import LN.clsGestorLN;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JRadioButton;
import javax.swing.JDesktopPane;

/**
 * ventana interna para la creacion de una nueva pelicula
 * 
 * @author Eric
 *
 */
public class frmInInsertPelicula extends JInternalFrame implements ActionListener, InternalFrameListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7666912571195053641L;
	/**
	 * objeto gestor de la logica de negocio
	 */
	private clsGestorLN objGestor;
	/**
	 * usuario actual de la pagina
	 */
	private itfProperty usuarioActual;
	/**
	 * modelo de la lista de tipos de pelicula
	 */
	private DefaultListModel<String> modelTiposPelicula;
	/**
	 * lista de tipos de pelicula elegidas
	 */
	private JList tipoPeliculasElegidas;
	/**
	 * lista de tipos de peliculas para la combobox de tipos de peliculas
	 */
	private ArrayList<itfProperty> listaPeliculas;
	/**
	 * lista de proveedores para la comboBox de proveedores
	 */
	private ArrayList<itfProperty> listaProveedores;
	/**
	 * comboBox de tipos de pelicula
	 */
	private JComboBox<Object> comboTipoPelicula;
	/**
	 * comboBox de proveedores
	 */
	private JComboBox<Object> comboProveedor;
	/**
	 * cuadro de texto para el titulo de la pelicula
	 */
	private JTextField txtTitulo;
	/**
	 * cuadro de texto para la duracion de la pelicula
	 */
	private JTextField txtDuracion;
	/**
	 * comboBox para la clasificacion de la pelicula
	 */
	private JComboBox<Object> comboClasificacion;
	/**
	 * comboBox para los idiomas de la peliculas
	 */
	private JComboBox<Object> comboIdiomas;
	/**
	 * lista de los idiomas elegidos
	 */
	private JList idiomasLista;
	/**
	 * modelo del comboBox de los idiomas
	 */
	private DefaultListModel<String> modelIdiomas;
	/**
	 * radioButton si no hay subtitulos
	 */
	private JRadioButton rdbtnSubtitulosNo;
	/*
	 * radioButton si hay subtitulos
	 */
	private JRadioButton rdbtnSubtitulosSi;
	/**
	 * comboBox para la resolucion de la pelicula
	 */
	private JComboBox<Object> comboResolucion;
	/**
	 * panel de escritorio donde esta todos los componentes incluidos los
	 * JinternalFrame
	 */
	private JDesktopPane desktopPane;
	/**
	 * area de texto de la descripcion
	 */
	private JTextArea txtAreaDescripcion;
	/**
	 * lista de los tipos de pelicula
	 */
	private ArrayList<itfProperty> listaTiposPelicula;
	/**
	 * objeto de la internalFrame de Elegir Portada
	 */
	private frmArchivoPortada objArchivoPortada;
	/**
	 * archivo de la imgaen de la portada
	 */
	private File portadaArchivo;
	/**
	 * label donde ira el nombre de la portada de la pelicula elegida
	 */
	private JLabel lblPortada;
	/**
	 * id de la pelicula insertada
	 */
	private int idPelicula = -1;

	/**
	 * constructor de la internal frame insertar Pelicula
	 * 
	 * @param objGestor       objeto gestor de la logica de negocio
	 * @param usuarioActual   usuario actual del programa
	 * @param titulo          titulo de la internal frame
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmInInsertPelicula(clsGestorLN objGestor, itfProperty usuarioActual, String titulo, boolean redimensionable,
			boolean cerrable, boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.objGestor = objGestor;
		this.usuarioActual = usuarioActual;
		this.CrearPanel();

	}

	/**
	 * funcion que rellean el panel con los componentes pertinentes
	 */
	private void CrearPanel() {
		setBounds(100, 100, 605, 423);

		comboProveedor = new JComboBox<Object>();
		listaProveedores = rellenarComboBoxProveedor();
		for (itfProperty aux : listaProveedores) {
			comboProveedor.addItem((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE));
		}
		comboProveedor.setBounds(35, 104, 108, 20);
		comboProveedor.addActionListener(this);
		getContentPane().setLayout(null);

		desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 595, 394);
		getContentPane().add(desktopPane);

		desktopPane.add(comboProveedor);

		comboTipoPelicula = new JComboBox<Object>();
		listaPeliculas = rellenarComboBoxTipoPelicula();
		for (itfProperty aux : listaPeliculas) {
			comboTipoPelicula.addItem((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE));
		}
		comboTipoPelicula.setBounds(450, 178, 108, 20);
		comboTipoPelicula.addActionListener(this);
		comboTipoPelicula.setActionCommand(CAMBIO_COMBO_TIPO_PELICULA);
		desktopPane.setLayout(null);

		desktopPane.add(comboTipoPelicula);
		modelTiposPelicula = new DefaultListModel<>();

		JScrollPane scrollPanelListaTiposPelicula = new JScrollPane();
		scrollPanelListaTiposPelicula.setBounds(450, 234, 118, 97);
		desktopPane.add(scrollPanelListaTiposPelicula);

		tipoPeliculasElegidas = new JList<Object>();
		scrollPanelListaTiposPelicula.setViewportView(tipoPeliculasElegidas);

		JButton btnBorrarTipoPelicula = new JButton("Borrar");
		btnBorrarTipoPelicula.addActionListener(this);
		btnBorrarTipoPelicula.setActionCommand(BORRAR_TIPO_PELICULA_LISTA);
		btnBorrarTipoPelicula.setBounds(450, 342, 118, 23);
		desktopPane.add(btnBorrarTipoPelicula);

		JLabel lblListaTiposPelicula = new JLabel("lista tipos pelicula", SwingConstants.CENTER);
		lblListaTiposPelicula.setBounds(450, 209, 118, 14);
		desktopPane.add(lblListaTiposPelicula);

		JLabel lblTipoPelicula = new JLabel("Tipo Pelicula", SwingConstants.CENTER);
		lblTipoPelicula.setBounds(450, 155, 108, 14);
		desktopPane.add(lblTipoPelicula);

		comboIdiomas = new JComboBox<Object>(IDIOMAS);
		comboIdiomas.setBounds(328, 178, 112, 20);
		comboIdiomas.addActionListener(this);
		comboIdiomas.setActionCommand(CAMBIO_COMBO_IDIOMA);
		desktopPane.setLayout(null);

		desktopPane.add(comboIdiomas);
		modelIdiomas = new DefaultListModel<>();

		JScrollPane scrollPanelIdiomas = new JScrollPane();
		scrollPanelIdiomas.setBounds(328, 234, 112, 97);
		desktopPane.add(scrollPanelIdiomas);

		idiomasLista = new JList<Object>();
		scrollPanelIdiomas.setViewportView(idiomasLista);

		JButton btnBorrarIdioma = new JButton("Borrar");
		btnBorrarIdioma.addActionListener(this);
		btnBorrarIdioma.setActionCommand(BORRAR_IDIOMA_LISTA);
		btnBorrarIdioma.setBounds(328, 342, 112, 23);
		desktopPane.add(btnBorrarIdioma);

		JLabel lblListaIdiomas = new JLabel("lista idiomas", SwingConstants.CENTER);
		lblListaIdiomas.setBounds(328, 209, 112, 14);
		desktopPane.add(lblListaIdiomas);

		JLabel lblIdiomas = new JLabel("Idiomas", SwingConstants.CENTER);
		lblIdiomas.setBounds(328, 155, 112, 14);
		desktopPane.add(lblIdiomas);

		JLabel lblProveedor = new JLabel("Proveedor", SwingConstants.CENTER);
		lblProveedor.setBounds(35, 79, 108, 14);
		desktopPane.add(lblProveedor);

		JButton btnInsert = new JButton("Insertar");
		btnInsert.addActionListener(this);
		btnInsert.setActionCommand(INSERTAR);
		btnInsert.setBounds(35, 360, 108, 23);
		desktopPane.add(btnInsert);

		JLabel lblTitulo = new JLabel("Titulo", SwingConstants.CENTER);
		lblTitulo.setBounds(35, 29, 108, 14);
		desktopPane.add(lblTitulo);

		txtTitulo = new JTextField();
		txtTitulo.setBounds(35, 54, 108, 20);
		desktopPane.add(txtTitulo);
		txtTitulo.setColumns(10);

		JLabel lblDescripcion = new JLabel("Descripcion", SwingConstants.CENTER);
		lblDescripcion.setBounds(335, 11, 223, 14);
		desktopPane.add(lblDescripcion);

		JScrollPane scrollPanelDescripcion = new JScrollPane();
		scrollPanelDescripcion.setBounds(335, 36, 223, 95);
		desktopPane.add(scrollPanelDescripcion);

		txtAreaDescripcion = new JTextArea();
		scrollPanelDescripcion.setViewportView(txtAreaDescripcion);

		JLabel lblDuracion = new JLabel("Duracion", SwingConstants.CENTER);
		lblDuracion.setBounds(35, 135, 108, 14);
		desktopPane.add(lblDuracion);

		txtDuracion = new JTextField();
		txtDuracion.setBounds(35, 160, 108, 20);
		desktopPane.add(txtDuracion);
		txtDuracion.setColumns(10);

		JLabel lblClasificacion = new JLabel("Clasificacion", SwingConstants.CENTER);
		lblClasificacion.setBounds(35, 191, 108, 14);
		desktopPane.add(lblClasificacion);

		comboClasificacion = new JComboBox<Object>(CLASIFICACIONES);

		comboClasificacion.setBounds(35, 209, 108, 20);
		desktopPane.setLayout(null);

		desktopPane.add(comboClasificacion);

		JLabel lblSubtitulos = new JLabel("Subtitulos", SwingConstants.CENTER);
		lblSubtitulos.setBounds(35, 242, 108, 14);
		desktopPane.add(lblSubtitulos);

		ButtonGroup rBtnGroupSubtitulos = new ButtonGroup();

		rdbtnSubtitulosSi = new JRadioButton("Si", true);
		rdbtnSubtitulosSi.setBounds(35, 263, 52, 23);
		rBtnGroupSubtitulos.add(rdbtnSubtitulosSi);
		desktopPane.add(rdbtnSubtitulosSi);

		rdbtnSubtitulosNo = new JRadioButton("No", false);
		rdbtnSubtitulosNo.setBounds(86, 263, 57, 23);
		rBtnGroupSubtitulos.add(rdbtnSubtitulosNo);
		desktopPane.add(rdbtnSubtitulosNo);

		JLabel lblResolucion = new JLabel("Resolucion", SwingConstants.CENTER);
		lblResolucion.setBounds(35, 293, 108, 14);
		desktopPane.add(lblResolucion);

		comboResolucion = new JComboBox<Object>(RESOLUCIONES);

		comboResolucion.setBounds(35, 317, 108, 20);
		desktopPane.setLayout(null);
		desktopPane.add(comboResolucion);

		JButton btnPortada = new JButton("Portada");
		btnPortada.setBounds(169, 75, 156, 23);
		btnPortada.addActionListener(this);
		btnPortada.setActionCommand(PORTADA);
		desktopPane.add(btnPortada);

		lblPortada = new JLabel("");
		lblPortada.setBounds(169, 108, 149, 199);
		desktopPane.add(lblPortada);
	}

	/**
	 * coger todos los tipos de peliculas de BBDD para el comboBox de tipos de
	 * peliculas
	 * 
	 * @return lista de los tipos de peliculas
	 */
	private ArrayList<itfProperty> rellenarComboBoxTipoPelicula() {
		listaTiposPelicula = null;
		try {
			listaTiposPelicula = objGestor.listarTipoPelicula();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		return listaTiposPelicula;
	}

	/**
	 * coger todos los proveedores de BBDD para el comboBox de proveedores
	 * 
	 * @return lista de los proveedores
	 */
	private ArrayList<itfProperty> rellenarComboBoxProveedor() {
		ArrayList<itfProperty> listaProveedor = null;
		try {
			listaProveedor = objGestor.listarProveedor();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		return listaProveedor;
	}

	/**
	 * escuchador de acciones en la internal frame
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case BORRAR_TIPO_PELICULA_LISTA: {
			String tipoPeliculaList = (String) tipoPeliculasElegidas.getSelectedValue();
			deleteListTipoPeliculas(tipoPeliculaList);
			break;
		}
		case CAMBIO_COMBO_TIPO_PELICULA: {
			String tipoPelicula = (String) comboTipoPelicula.getSelectedItem();
			updateListTipoPeliculas(tipoPelicula);
			break;
		}
		case BORRAR_IDIOMA_LISTA: {
			String idiomaList = (String) idiomasLista.getSelectedValue();
			deleteListIdiomas(idiomaList);
			break;
		}
		case CAMBIO_COMBO_IDIOMA: {
			String idioma = (String) comboIdiomas.getSelectedItem();
			updateIdiomas(idioma);
			break;
		}
		case INSERTAR: {
			insertarPelicula();
			break;
		}
		case PORTADA: {
			mostrarVentanaPortada();
			break;
		}
		}
	}

	/**
	 * funcion que muestar la ventana de portada
	 */
	private void mostrarVentanaPortada() {
		objArchivoPortada = new frmArchivoPortada(lblPortada, INSERTAR + " " + PORTADA, false, true, false, false);
		desktopPane.add(objArchivoPortada);
		objArchivoPortada.addInternalFrameListener(this);
		objArchivoPortada.pack();
		Dimension desktopSize = desktopPane.getSize();
		Dimension FrameSize = objArchivoPortada.getSize();
		objArchivoPortada.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objArchivoPortada.show();
	}

	/**
	 * funcion que inserta una pelicula
	 */
	public void insertarPelicula() {
		if (txtTitulo.getText() == null || txtAreaDescripcion.getText() == null
				|| idiomasLista.getModel().getSize() == 0 || tipoPeliculasElegidas.getModel().getSize() == 0) {
			JOptionPane.showMessageDialog(this, "campo obligatorio sin rellenar");
		} else {
			int proveedorId = cambiarIdProveedor((String) comboProveedor.getSelectedItem());
			String clasificacion = (String) comboClasificacion.getSelectedItem();
			String resolucion = (String) comboResolucion.getSelectedItem();
			String descripcion = txtAreaDescripcion.getText();
			String titulo = txtTitulo.getText();
			int duracion = 0;
			if (!txtDuracion.getText().isEmpty()) {
				duracion = Integer.parseInt(txtDuracion.getText());
			} else {
				JOptionPane.showMessageDialog(this, "duracion es un campo obligatorio");
			}
			String listaIdiomas = "";
			for (int i = 0; i < idiomasLista.getModel().getSize(); i++) {
				if (i == (idiomasLista.getModel().getSize() - 1)) {
					listaIdiomas = listaIdiomas + idiomasLista.getModel().getElementAt(i);
				} else {
					listaIdiomas = listaIdiomas + idiomasLista.getModel().getElementAt(i) + ", ";
				}
			}

			boolean subtitulos = false;
			String portada = null;
			if (portadaArchivo != null) {
				String[] portadaDividido = portadaArchivo.getName().toString().split("[.]");
				portada = System.currentTimeMillis() + "." + portadaDividido[(portadaDividido.length - 1)];
			}
			if (rdbtnSubtitulosSi.isSelected()) {
				subtitulos = true;
			}
			if (rdbtnSubtitulosNo.isSelected()) {
				subtitulos = false;
			}
			try {
				idPelicula = objGestor.insertarPelicula((int) usuarioActual.getProperty(EMPLEADO_COL_ID), proveedorId,
						titulo, descripcion, duracion, clasificacion, listaIdiomas, subtitulos, resolucion, portada);
				for (int i = 0; i < tipoPeliculasElegidas.getModel().getSize(); i++) {
					int idTipoPelicula = conseguirIdTipoPelicula(
							(String) tipoPeliculasElegidas.getModel().getElementAt(i));
					objGestor.insertarPeliculaTipos(idTipoPelicula, idPelicula);
				}
			} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConsulta
					| clsErrorSQLConexion e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage());
			}
			// mover imagen a carpeta Portada
			if (portadaArchivo != null) {

				Path origenPath = FileSystems.getDefault().getPath(portadaArchivo.getPath());
				Path destinoPath = FileSystems.getDefault().getPath(PORTADA + "\\" + portada);

				try {
					Files.move(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					System.err.println(e);
				}
			}
			this.dispose();
		}
	}

	/**
	 * funcion que consigue el id del tipo de pelicula
	 * 
	 * @param nombre del tipo de pelicula
	 * @return id del tipo de pelicula
	 */
	private int conseguirIdTipoPelicula(String nombre) {
		int idTipoPelicula = -1;
		for (itfProperty aux : listaTiposPelicula) {
			if (nombre.equalsIgnoreCase((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE))) {
				idTipoPelicula = (int) aux.getProperty(TIPO_PELICULA_COL_ID);
			}
		}
		return idTipoPelicula;
	}

	/**
	 * funcion que cambia el id del proveedor segun que se eliga en el comboBox
	 * 
	 * @param proveedor nombre del proveedor
	 * @return id del proveedor
	 */
	private int cambiarIdProveedor(String proveedor) {
		int id = -1;
		for (itfProperty aux : listaProveedores) {
			if (proveedor.equalsIgnoreCase((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE))) {
				id = (int) aux.getProperty(PROVEEDOR_COL_ID);
			}
		}
		return id;
	}

	/**
	 * borrar de la lista de tipos de pelicula un tipo de pelicula
	 * 
	 * @param tipoPeliculaList tipo de pelicula a eliminar
	 */
	private void deleteListTipoPeliculas(String tipoPeliculaList) {
		if (tipoPeliculaList != null) {
			modelTiposPelicula.removeElement(tipoPeliculaList);
			tipoPeliculasElegidas.setModel(modelTiposPelicula);

		} else {
			JOptionPane.showMessageDialog(this, "elige un tipo pelicula de la lista para eliminar");
		}
	}

	/**
	 * introducir el tipo pelicula en la lista
	 * 
	 * @param name nombre el tipo de pelicula
	 */
	private void updateListTipoPeliculas(String name) {
		for (itfProperty aux : listaPeliculas) {
			if (name.equalsIgnoreCase((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE))) {
				if (modelTiposPelicula.contains((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE))) {
					JOptionPane.showMessageDialog(this, "tipo pelicula ya elegida");
				} else {
					modelTiposPelicula.addElement((String) aux.getProperty(TIPO_PELICULA_COL_NOMBRE));
				}
			}
		}
		tipoPeliculasElegidas.setModel(modelTiposPelicula);
	}

	/**
	 * borrar de la lista de idiomas
	 * 
	 * @param idioma idioma a eliminar
	 */
	private void deleteListIdiomas(String idioma) {
		if (idioma != null) {
			modelIdiomas.removeElement(idioma);
			idiomasLista.setModel(modelIdiomas);

		} else {
			JOptionPane.showMessageDialog(this, "elige un idioma de la lista para eliminar");
		}
	}

	/**
	 * introducir el idioma de la lista de idiomas
	 * 
	 * @param idioma a insertar
	 */
	private void updateIdiomas(String idioma) {
		if (modelIdiomas.contains(idioma)) {
			JOptionPane.showMessageDialog(this, "idioma ya elegido");
		} else {
			modelIdiomas.addElement(idioma);
		}
		idiomasLista.setModel(modelIdiomas);
	}

	/**
	 * funcion que devuleve la pelicula insertada
	 * 
	 * @return pelicula insertada
	 */
	public itfProperty devolverPelicula() {
		itfProperty pelicula = null;
		if (idPelicula != -1) {
			try {
				pelicula = objGestor.devolverPelicula(idPelicula);
			} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
				JOptionPane.showMessageDialog(this, e.getMessage());
			}
		}
		return pelicula;
	}

	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {
		portadaArchivo = objArchivoPortada.cogerFile();

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

}
