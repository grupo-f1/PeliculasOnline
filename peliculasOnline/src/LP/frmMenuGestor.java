package LP;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import COMUN.itfProperty;
import LN.clsGestorLN;

import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;

import static COMUN.clsConstantes.*;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.Font;

/**
 * JFrame del menu Gesotr donde se muestra las acciones que puede hacer un
 * gestor
 * 
 * @author Eric
 *
 */
public class frmMenuGestor extends JFrame implements ActionListener, InternalFrameListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1426329492754787966L;

	/**
	 * objeto de la clase clsGestorLN
	 */
	private clsGestorLN objGestor;
	/**
	 * objeto de la clase clsGestorLN
	 */
	private itfProperty usuarioActual;
	/**
	 * obj de escritorio para poner el jInternalFrame
	 */
	JDesktopPane desktop;
	/**
	 * obj escuchador de la ventana
	 */
	JInternalFrame listenedToWindow;

	/**
	 * item del menu donde se listan las peliculas
	 */
	private JMenuItem menuItemListarPeliculas;
	/**
	 * item del menu donde se listan los proveedores
	 */
	private JMenuItem menuItemListarProveedores;
	/**
	 * objeto de la ventana listar proveedores
	 */
	private frmInListarProveedores objListarProveedores;
	/**
	 * objeto de la ventana listar peliculas
	 */
	private frmInListarPeliculas objListarPeliculas;
	/**
	 * objeto de la ventana de estadisticas
	 */
	private frmInEstadisticas objEstadisticas;
	/**
	 * objeto de la ventana de oscars
	 */
	private frmInListarOscars objListarOscars;

	/**
	 * Constructor del jFrame del menu principal
	 * 
	 * @param usuarioActual usuario conectado
	 * @param objGestor     objeto gestor de la logica de negocio
	 */
	public frmMenuGestor(itfProperty usuarioActual, clsGestorLN objGestor) {
		this.usuarioActual = usuarioActual;
		this.objGestor = objGestor;

		// posicion pantalla
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();// ver tama�o de pantalla
		int heightP = pantalla.height;
		int widthP = pantalla.width;

		JMenuBar menuBarEmpleadoGestor = new JMenuBar();
		menuBarEmpleadoGestor.setBounds(0, 0, widthP, 23);
		menuBarEmpleadoGestor.setPreferredSize(null);
		setJMenuBar(menuBarEmpleadoGestor);

		JMenu menuPelicula = new JMenu("Peliculas");
		menuBarEmpleadoGestor.add(menuPelicula);

		menuItemListarPeliculas = new JMenuItem("Listar Peliculas");
		menuItemListarPeliculas.addActionListener(this);
		menuItemListarPeliculas.setActionCommand(LISTAR_PELICULAS);
		menuPelicula.add(menuItemListarPeliculas);

		JMenu menuProveedor = new JMenu("Proveedores");
		menuBarEmpleadoGestor.add(menuProveedor);

		menuItemListarProveedores = new JMenuItem("Listar Provedores");
		menuItemListarProveedores.addActionListener(this);
		menuItemListarProveedores.setActionCommand(LISTAR_PROVEEDORES);
		menuProveedor.add(menuItemListarProveedores);

		JMenu menuEstadisticas = new JMenu("Estadisticas");
		menuBarEmpleadoGestor.add(menuEstadisticas);

		JMenuItem menuItemEstadisticas = new JMenuItem("Estadisticas");
		menuItemEstadisticas.addActionListener(this);
		menuItemEstadisticas.setActionCommand(ESTADISTICAS);
		menuEstadisticas.add(menuItemEstadisticas);

		desktop = new JDesktopPane() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 8019144478102589327L;
			ImageIcon icon = createImageIcon(FONDO + "\\" + FONDO_GESTOR + ".gif");

			private Image IMG = icon.getImage();

			/**
			 * funcion que pone una imagen de fondo
			 */
			public void paintChildren(Graphics g) {
				g.drawImage(IMG, 0, 0, getWidth(), getHeight(), this);
				super.paintChildren(g);
			}
		};
		desktop.setPreferredSize(new Dimension((widthP), (heightP)));
		setContentPane(desktop);
		desktop.setLayout(null);

		JButton btnOscars = new JButton("Lista Oscars");
		btnOscars.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnOscars.addActionListener(this);
		btnOscars.setActionCommand(OSCARS);
		btnOscars.setBounds(696, 24, 137, 45);
		desktop.add(btnOscars);

	}

	/**
	 * devuelve la imagen o un mensaje de no encontrado
	 * 
	 * @param path ruta a la imagen
	 * @return imagen a mostrar
	 */
	private ImageIcon createImageIcon(String path) {
		File archivoPortada = new File(FONDO + "\\" + FONDO_GESTOR);
		String imgURL = archivoPortada.getPath();
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("No es ha encontrado el archivo: " + path);
			return null;
		}
	}

	/**
	 * funcion que muestra la ventana del menu principal
	 * 
	 */
	public void createAndShowGUI() {
		// Si se cierra el formulario se acaba la aplicacion.
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Display the window.
		this.pack();
		// centrar
		this.setLocationRelativeTo(null);
		// impedir hacer mas grande
		this.setResizable(false);

		this.setVisible(true);
	}

	/**
	 * metodo para escuchar los distintos eventos que ocurren en la ventana del
	 * gestor
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case LISTAR_PELICULAS: {
			listarPeliculas();
			break;
		}
		case LISTAR_PROVEEDORES: {
			listarProveedores();
			break;
		}
		case ESTADISTICAS: {
			estadisticas();
			break;
		}
		case OSCARS: {
			listarOscars();
			break;
		}
		}
	}

	/**
	 * funcion que crea un InternalFrame para listar las pelicula
	 */
	private void listarPeliculas() {
		if (objListarPeliculas != null) {
			objListarPeliculas.dispose();
		}
		objListarPeliculas = new frmInListarPeliculas(objGestor, usuarioActual, desktop.getBounds(),
				LISTA + " " + PELICULA, false, true, false, false);
		desktop.add(objListarPeliculas);
		objListarPeliculas.addInternalFrameListener(this);
		Dimension desktopSize = desktop.getSize();
		Dimension FrameSize = objListarPeliculas.getSize();
		objListarPeliculas.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objListarPeliculas.show();
	}

	/**
	 * funcion que crea un InternalFrame para listar los proveedores
	 * 
	 */
	private void listarProveedores() {
		if (objListarProveedores != null) {
			objListarProveedores.dispose();
		}
		objListarProveedores = new frmInListarProveedores(objGestor, desktop.getBounds(), LISTA + " " + PELICULA, false,
				true, false, false);
		desktop.add(objListarProveedores);
		objListarProveedores.addInternalFrameListener(this);
		Dimension desktopSize = desktop.getSize();
		Dimension FrameSize = objListarProveedores.getSize();
		objListarProveedores.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objListarProveedores.show();
	}

	/**
	 * funcion que crea un InternalFrame para listar los Oscars
	 * 
	 */
	private void listarOscars() {
		if (objListarOscars != null) {
			objListarOscars.dispose();
		}
		objListarOscars = new frmInListarOscars(objGestor, desktop.getBounds(), LISTA + " " + OSCARS, false, true,
				false, false);
		desktop.add(objListarOscars);
		objListarOscars.addInternalFrameListener(this);
		Dimension desktopSize = desktop.getSize();
		Dimension FrameSize = objListarOscars.getSize();
		objListarOscars.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objListarOscars.show();
	}

	/**
	 * funcion que crea un InternalFrame para las estadisticas
	 */
	private void estadisticas() {
		if (objEstadisticas != null) {
			objEstadisticas.dispose();
		}
		objEstadisticas = new frmInEstadisticas(objGestor, desktop.getBounds(), ESTADISTICAS, false, true, false,
				false);
		desktop.add(objEstadisticas);
		objEstadisticas.addInternalFrameListener(this);
		Dimension desktopSize = desktop.getSize();
		Dimension FrameSize = objEstadisticas.getSize();
		objEstadisticas.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objEstadisticas.show();
	}

	/**
	 * funcion que escucha las acciones de esta ventana cuando se abre un
	 * JInternalFrame
	 */
	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		switch (e.getSource().getClass().getName()) {
		case INTERNAL_PROVEEDOR: {
			if (objListarPeliculas != null) {
				objListarPeliculas.dispose();
			}
			if (objEstadisticas != null) {
				objEstadisticas.dispose();
			}
			if (objListarOscars != null) {
				objListarOscars.dispose();
			}
			break;
		}
		case INTERNAL_PELICULA: {
			if (objListarProveedores != null) {
				objListarProveedores.dispose();
			}
			if (objEstadisticas != null) {
				objEstadisticas.dispose();
			}
			if (objListarOscars != null) {
				objListarOscars.dispose();
			}
			break;
		}
		case INTERNAL_ESTADISTICAS: {
			if (objListarProveedores != null) {
				objListarProveedores.dispose();
			}
			if (objListarPeliculas != null) {
				objListarPeliculas.dispose();
			}
			if (objListarOscars != null) {
				objListarOscars.dispose();
			}
			break;
		}
		case INTERNAL_OSCAR: {
			if (objListarProveedores != null) {
				objListarProveedores.dispose();
			}
			if (objListarPeliculas != null) {
				objListarPeliculas.dispose();
			}
			if (objEstadisticas != null) {
				objEstadisticas.dispose();
			}
			break;
		}
		}
	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}
}
