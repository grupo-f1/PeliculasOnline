package LP;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import COMUN.clsDateLabelFormatter;
import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;

import static COMUN.clsConstantes.*;
import LN.clsGestorLN;

/**
 * Ventana interna para cuando hay que hacer una insert de proveedor
 * 
 * @author Eric
 *
 */
public class frmInRegistroEmpleado extends JInternalFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 37022776818509873L;

	/**
	 * panel donde ira todo el contenido de la ventana interna
	 */
	JPanel jpContent;
	/**
	 * objeto gestor de la logica de negocio
	 */
	private clsGestorLN objGestor;
	/**
	 * nombre de usuario introducido
	 */
	private JTextField txtUser_name;
	/**
	 * passowrd del usuario introducido
	 */
	private JPasswordField passPassword;
	/**
	 * nombre introducido
	 */
	private JTextField txtNombre;
	/**
	 * apellido introducido
	 */
	private JTextField txtApellido;
	/**
	 * dni introducido
	 */
	private JTextField txtDni;
	/**
	 * telefono introducido
	 */
	private JTextField txtTelefono;
	/**
	 * email introducido
	 */
	private JTextField txtEmail;
	/**
	 * comboBox de los tipos de empleados
	 */
	private JComboBox<Object> comboTipoEmpleado;
	/**
	 * modelo del datePicker de la fecha de nacimiento
	 */
	private UtilDateModel model;
	/**
	 * lista de los tipos de empleados
	 */
	private ArrayList<itfProperty> listaTiposEmpleados;

	/**
	 * constructor de la internal frame
	 * 
	 * @param objGestor       objeto gestor de la logica de negocio
	 * @param titulo          titulo de la internal frame
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmInRegistroEmpleado(clsGestorLN objGestor, String titulo, boolean redimensionable, boolean cerrable,
			boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.objGestor = objGestor;
		this.CrearPanel();

	}

	/**
	 * funcion que rellean el panel con los componentes pertinentes
	 */
	private void CrearPanel() {
		setBounds(100, 100, 450, 300);
		jpContent = new JPanel();
		jpContent.setBackground(Color.WHITE);
		jpContent.repaint();

		this.setContentPane(jpContent);
		jpContent.setLayout(null);

		JLabel lblTitulo = new JLabel("Registro Empleado", SwingConstants.CENTER);
		lblTitulo.setBounds(152, 11, 110, 14);
		jpContent.add(lblTitulo);

		JLabel lblUser_name = new JLabel("Nombre de usuario", SwingConstants.CENTER);
		lblUser_name.setBounds(10, 42, 122, 14);
		jpContent.add(lblUser_name);

		txtUser_name = new JTextField();
		txtUser_name.setBounds(26, 67, 96, 20);
		jpContent.add(txtUser_name);
		txtUser_name.setColumns(10);

		JLabel lblPassword = new JLabel("Password", SwingConstants.CENTER);
		lblPassword.setBounds(162, 42, 96, 14);
		jpContent.add(lblPassword);

		passPassword = new JPasswordField();
		passPassword.setColumns(10);
		passPassword.setBounds(162, 67, 96, 20);
		jpContent.add(passPassword);

		JLabel lblNombre = new JLabel("Nombre", SwingConstants.CENTER);
		lblNombre.setBounds(26, 98, 96, 14);
		jpContent.add(lblNombre);

		txtNombre = new JTextField();
		txtNombre.setBounds(26, 123, 96, 20);
		jpContent.add(txtNombre);
		txtNombre.setColumns(10);

		JLabel lblApellido = new JLabel("Apellido", SwingConstants.CENTER);
		lblApellido.setBounds(162, 98, 96, 14);
		jpContent.add(lblApellido);

		txtApellido = new JTextField();
		txtApellido.setColumns(10);
		txtApellido.setBounds(162, 123, 96, 20);
		jpContent.add(txtApellido);

		JLabel lblFechaNacimiento = new JLabel("Fecha Nacimiento", SwingConstants.CENTER);
		lblFechaNacimiento.setBounds(265, 98, 165, 14);
		jpContent.add(lblFechaNacimiento);

		model = new UtilDateModel();
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
		JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new clsDateLabelFormatter());
		datePicker.setBounds(295, 119, 122, 24);
		model.setSelected(true);
		datePicker.setVisible(true);
		jpContent.add(datePicker);

		JLabel lblDni = new JLabel("Dni", SwingConstants.CENTER);
		lblDni.setBounds(162, 154, 96, 14);
		jpContent.add(lblDni);

		txtDni = new JTextField();
		txtDni.setColumns(10);
		txtDni.setBounds(162, 179, 96, 20);
		jpContent.add(txtDni);

		JLabel lblTelefono = new JLabel("Telefono", SwingConstants.CENTER);
		lblTelefono.setBounds(26, 154, 96, 14);
		jpContent.add(lblTelefono);

		txtTelefono = new JTextField();
		txtTelefono.setColumns(10);
		txtTelefono.setBounds(26, 179, 96, 20);
		jpContent.add(txtTelefono);

		JLabel lblEmail = new JLabel("Email", SwingConstants.CENTER);
		lblEmail.setBounds(310, 154, 96, 14);
		jpContent.add(lblEmail);

		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(310, 179, 96, 20);
		jpContent.add(txtEmail);

		JLabel lblTipoEmpleado = new JLabel("TipoEmpleado", SwingConstants.CENTER);
		lblTipoEmpleado.setBounds(310, 42, 96, 14);
		jpContent.add(lblTipoEmpleado);

		comboTipoEmpleado = new JComboBox<Object>();
		listaTiposEmpleados = rellenarComboBoxTipoEmpleado();
		for (itfProperty aux : listaTiposEmpleados) {
			comboTipoEmpleado.addItem((String) aux.getProperty(TIPO_EMPLEADO_COL_NOMBRE));
		}
		comboTipoEmpleado.setBounds(295, 67, 111, 20);
		comboTipoEmpleado.addActionListener(this);
		jpContent.add(comboTipoEmpleado);

		JButton btnInsertar = new JButton("Insertar");
		btnInsertar.setBounds(159, 220, 96, 23);
		btnInsertar.addActionListener(this);
		btnInsertar.setActionCommand(INSERTAR);
		jpContent.add(btnInsertar);
	}

	/**
	 * coger todos los tipos de empleado de BBDD para el comboBox de tipos de
	 * empleado
	 * 
	 * @return lista de los tipos de empleados
	 */
	private ArrayList<itfProperty> rellenarComboBoxTipoEmpleado() {
		ArrayList<itfProperty> listaTiposPelicula = null;
		try {
			listaTiposPelicula = objGestor.listarTipoEmpleado();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		return listaTiposPelicula;
	}

	/**
	 * Menu de registro del empleado
	 */
	private void registroEmpleado() {
		// se piden todos los datos del usuario que se necesite

		String usuario = txtUser_name.getText();
		String password = "";
		char[] passwordChar = passPassword.getPassword();
		for (int i = 0; i < passwordChar.length; i++) {
			password = password + passwordChar[i];
		}
		String nombre = txtNombre.getText();
		String apellido = txtApellido.getText();
		Date fechaNacimientoDate = model.getValue();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String fechaNacimiento = sdf.format(fechaNacimientoDate);
		String dni = txtUser_name.getText();
		String telf = txtUser_name.getText();
		String email = txtUser_name.getText();
		String tipoEmpleado = (String) comboTipoEmpleado.getSelectedItem();
		try {
			int tipoEmpleadoId = -1;
			switch (tipoEmpleado) {
			case TIPO_EMPLEADO_ADMIN:
				for (itfProperty aux : listaTiposEmpleados) {
					if (aux.getProperty(TIPO_EMPLEADO_COL_NOMBRE).equals(TIPO_EMPLEADO_ADMIN)) {
						tipoEmpleadoId = (int) aux.getProperty(TIPO_EMPLEADO_COL_ID);
					}
				}
				break;
			case TIPO_EMPLEADO_GESTOR:
				for (itfProperty aux : listaTiposEmpleados) {
					if (aux.getProperty(TIPO_EMPLEADO_COL_NOMBRE).equals(TIPO_EMPLEADO_GESTOR)) {
						tipoEmpleadoId = (int) aux.getProperty(TIPO_EMPLEADO_COL_ID);
					}
				}
				break;
			case TIPO_EMPLEADO_TECNICO:
				for (itfProperty aux : listaTiposEmpleados) {
					if (aux.getProperty(TIPO_EMPLEADO_COL_NOMBRE).equals(TIPO_EMPLEADO_TECNICO)) {
						tipoEmpleadoId = (int) aux.getProperty(TIPO_EMPLEADO_COL_ID);
					}
				}
				break;
			}
			int idEmpleado = -1;
			idEmpleado = objGestor.insertarEmpleado(tipoEmpleadoId, usuario, password, nombre, apellido,
					fechaNacimiento, dni, telf, email);
			if (idEmpleado != 1) {
				JOptionPane.showMessageDialog(this, "Empleado insertado");
			} else {
				JOptionPane.showMessageDialog(this, "Empleado no insertado");
			}
		} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}

	}

	/**
	 * escuchador de eventos
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case INSERTAR: {
			registroEmpleado();
			this.dispose();
			break;
		}
		}
	}
}
