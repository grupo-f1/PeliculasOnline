package LP;

import static COMUN.clsConstantes.*;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import LN.clsGestorLN;

/**
 * ventana internal para listar los oscars y hacer acciones con ellos
 * 
 * @author Eric
 *
 */
public class frmInListarOscars extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2075687260246581181L;
	/**
	 * objeto gestor de la logica de negocio
	 */
	private clsGestorLN objGestor;
	/**
	 * panel de escritorio para las internal frame
	 */
	private JDesktopPane desktop;
	/**
	 * tabla de los oscars
	 */
	private JTable tablaOscars;
	/**
	 * modelo de la tabla de oscars
	 */
	private DefaultTableModel modeloTablaOsc;
	/**
	 * dimensiones de el escritorio padre
	 */
	private Rectangle desktopPadreDim;
	/**
	 * lista de los oscars de la BBDD
	 */
	private ArrayList<itfProperty> listaOscars;

	/**
	 * constructor de la internal frame listar Proveedor
	 * 
	 * @param objGestor       objeto gestor de la logica de negocio
	 * @param desktopPadreDim dimensiones de la ventana padre
	 * @param titulo          titulo de la internal frame
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmInListarOscars(clsGestorLN objGestor, Rectangle desktopPadreDim, String titulo, boolean redimensionable,
			boolean cerrable, boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.objGestor = objGestor;
		this.desktopPadreDim = desktopPadreDim;
		this.CrearPanel();

	}

	/**
	 * Create the frame.
	 */
	private void CrearPanel() {
		setBounds(10, 30, 342, 302);

		desktop = new JDesktopPane();
		desktop.setBounds(10, 30, 502, 302);
		getContentPane().add(desktop);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 336, 302);
		desktop.add(scrollPane);

		tablaOscars = new JTable();
		scrollPane.setViewportView(tablaOscars);

		modeloTablaOsc = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -9168227105848362240L;

			/**
			 * funcion que evita editar columnas en la tabla
			 */
			@Override
			public boolean isCellEditable(int filas, int columnas) {
				if (columnas == 3) {
					return true;
				} else {
					return false;
				}

			}
		};

		modeloTablaOsc.addColumn(OSCAR_COL_PELICULA);
		modeloTablaOsc.addColumn(OSCAR_COL_DESCRIPCION);

		listaOscars = new ArrayList<itfProperty>();
		try {
			listaOscars = objGestor.listarOscars();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {
			JOptionPane.showMessageDialog(this, error.getMessage());
		}
		for (itfProperty aux : listaOscars) {
			modeloTablaOsc.addRow(
					new Object[] { aux.getProperty(OSCAR_COL_PELICULA), aux.getProperty(OSCAR_COL_DESCRIPCION) });
		}
		tablaOscars.setModel(modeloTablaOsc);
		tablaOscars.setRowHeight(30);
		tablaOscars.setAutoCreateRowSorter(true);
		tablaOscars.setPreferredScrollableViewportSize(new Dimension(500, 300));
		tablaOscars.setFillsViewportHeight(true);
		// Se llama al metodo que ajusta las columnas
		redimensionarAnchoColumna(tablaOscars);
		// Se desactiva el Auto Resize de la tabla
		// Es importante que vaya despues de el metodo que ajusta el ancho de la columna
		tablaOscars.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

	}

	/**
	 * Metodo que ajusta el ancho de la columna de una tabla segun su contenido
	 * 
	 * @param tabla tabla a editar
	 */
	private void redimensionarAnchoColumna(JTable tabla) {
		// Se obtiene el modelo de la columna
		TableColumnModel columnModel = tabla.getColumnModel();
		// Se obtiene el total de las columnas
		for (int column = 0; column < tabla.getColumnCount(); column++) {
			// Establecemos un valor minimo para el ancho de la columna
			int width = 100; // Min Width
			// Obtenemos el numero de filas de la tabla
			for (int row = 0; row < tabla.getRowCount(); row++) {
				// Obtenemos el renderizador de la tabla
				TableCellRenderer renderer = tabla.getCellRenderer(row, column);
				// Creamos un objeto para preparar el renderer
				Component comp = tabla.prepareRenderer(renderer, row, column);
				// Establecemos el width segun el valor maximo del ancho de la columna
				width = Math.max(comp.getPreferredSize().width + 1, width);

			}
			// Se establece una condicion para no sobrepasar el valor de 300
			// Esto es Opcional
			if (width > 300) {
				width = 300;
			}
			// Se establece el ancho de la columna
			columnModel.getColumn(column).setPreferredWidth(width);
		}
	}
}
