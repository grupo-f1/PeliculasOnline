package LP;

import static COMUN.clsConstantes.*;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;
import LN.clsGestorLN;

/**
 * ventana internal para listar los proveedores y hacer acciones con ellos
 * 
 * @author Eric
 *
 */
public class frmInListarProveedores extends JInternalFrame implements ActionListener, InternalFrameListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6360330611055663487L;
	/**
	 * objeto gestor de la logica de negocio
	 */
	private clsGestorLN objGestor;
	/**
	 * panel de escritorio para las internal frame
	 */
	private JDesktopPane desktop;
	/**
	 * tabla de los porveedores
	 */
	private JTable tablaProveedores;
	/**
	 * modelo de la tabla de proveedores
	 */
	private DefaultTableModel modeloTablaProv;
	/**
	 * dimensiones de el escritorio padre
	 */
	private Rectangle desktopPadreDim;
	/**
	 * lista de los porveedores de la BBDD
	 */
	private ArrayList<itfProperty> listaProveedores;
	/**
	 * objeto de la internalFrame de insertar proveedor
	 */
	private frmInInsertProveedor objInsertProveedor;
	/**
	 * objeto de la internalFrame de editar proveedor
	 */
	private frmInUpdateProveedor objUpdateProveedor;

	/**
	 * constructor de la internal frame listar Proveedor
	 * 
	 * @param objGestor       objeto gestor de la logica de negocio
	 * @param desktopPadreDim dimensiones de la ventana padre
	 * @param titulo          titulo de la internal frame
	 * @param redimensionable si la ventana es redimensionable
	 * @param cerrable        si la ventan es cerrable
	 * @param maximizable     si la ventana es maximizable
	 * @param minimizable     si la ventana es minimizable
	 */
	public frmInListarProveedores(clsGestorLN objGestor, Rectangle desktopPadreDim, String titulo,
			boolean redimensionable, boolean cerrable, boolean maximizable, boolean minimizable) {
		super(titulo, redimensionable, cerrable, maximizable, minimizable);
		this.objGestor = objGestor;
		this.desktopPadreDim = desktopPadreDim;
		this.CrearPanel();

	}

	/**
	 * Create the frame.
	 */
	private void CrearPanel() {
		setBounds(desktopPadreDim);

		desktop = new JDesktopPane();
		desktop.setBounds(desktopPadreDim);
		getContentPane().add(desktop);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 30, 502, 302);
		desktop.add(scrollPane);

		tablaProveedores = new JTable();
		scrollPane.setViewportView(tablaProveedores);

		modeloTablaProv = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 5302908543210052730L;

			/**
			 * funcion que evita editar columnas en la tabla
			 */
			@Override
			public boolean isCellEditable(int filas, int columnas) {
				if (columnas == 3) {
					return true;
				} else {
					return false;
				}

			}
		};

		modeloTablaProv.addColumn(PROVEEDOR_COL_NOMBRE);
		modeloTablaProv.addColumn(PROVEEDOR_COL_EMAIL);
		modeloTablaProv.addColumn(PROVEEDOR_COL_TELEFONO);

		listaProveedores = new ArrayList<itfProperty>();
		try {
			listaProveedores = objGestor.listarProveedor();
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {
			JOptionPane.showMessageDialog(this, error.getMessage());
		}
		for (itfProperty aux : listaProveedores) {
			modeloTablaProv.addRow(new Object[] { aux.getProperty(PROVEEDOR_COL_NOMBRE),
					aux.getProperty(PROVEEDOR_COL_EMAIL), aux.getProperty(PROVEEDOR_COL_TELEFONO) });
		}
		tablaProveedores.setModel(modeloTablaProv);
		tablaProveedores.setRowHeight(30);
		tablaProveedores.setAutoCreateRowSorter(true);
		tablaProveedores.setPreferredScrollableViewportSize(new Dimension(500, 300));
		tablaProveedores.setFillsViewportHeight(true);
		// Se llama al metodo que ajusta las columnas
		redimensionarAnchoColumna(tablaProveedores);
		// Se desactiva el Auto Resize de la tabla
		// Es importante que vaya despues de el metodo que ajusta el ancho de la columna
		tablaProveedores.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		JButton btnActualizarProveedor = new JButton("Actualizar");
		btnActualizarProveedor.addActionListener(this);
		btnActualizarProveedor.setActionCommand(EDITAR_PROVEEDOR);
		btnActualizarProveedor.setBounds(522, 34, 115, 23);
		desktop.add(btnActualizarProveedor);

		JButton btnInsertarProveedor = new JButton("Insertar");
		btnInsertarProveedor.addActionListener(this);
		btnInsertarProveedor.setActionCommand(CREAR_PROVEEDOR);
		btnInsertarProveedor.setBounds(522, 83, 115, 23);
		desktop.add(btnInsertarProveedor);
		JButton btnEliminarProveedor = new JButton("Eliminar");
		btnEliminarProveedor.addActionListener(this);
		btnEliminarProveedor.setActionCommand(BORRAR_PROVEEDOR);
		btnEliminarProveedor.setBounds(522, 123, 115, 23);
		desktop.add(btnEliminarProveedor);

	}

	/**
	 * Metodo que ajusta el ancho de la columna de una tabla segun su contenido
	 * 
	 * @param tabla tabla a editar
	 */
	private void redimensionarAnchoColumna(JTable tabla) {
		// Se obtiene el modelo de la columna
		TableColumnModel columnModel = tabla.getColumnModel();
		// Se obtiene el total de las columnas
		for (int column = 0; column < tabla.getColumnCount(); column++) {
			// Establecemos un valor minimo para el ancho de la columna
			int width = 100; // Min Width
			// Obtenemos el numero de filas de la tabla
			for (int row = 0; row < tabla.getRowCount(); row++) {
				// Obtenemos el renderizador de la tabla
				TableCellRenderer renderer = tabla.getCellRenderer(row, column);
				// Creamos un objeto para preparar el renderer
				Component comp = tabla.prepareRenderer(renderer, row, column);
				// Establecemos el width segun el valor maximo del ancho de la columna
				width = Math.max(comp.getPreferredSize().width + 1, width);

			}
			// Se establece una condicion para no sobrepasar el valor de 300
			// Esto es Opcional
			if (width > 300) {
				width = 300;
			}
			// Se establece el ancho de la columna
			columnModel.getColumn(column).setPreferredWidth(width);
		}
	}

	/**
	 * funcion que escucha las acciones de esta ventana
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		switch (e.getActionCommand()) {
		case BORRAR_PROVEEDOR: {
			if (tablaProveedores.getSelectedRow() == -1) {
				JOptionPane.showMessageDialog(this, "Elige un proveedor");
			} else {
				String email = (String) modeloTablaProv.getValueAt(tablaProveedores.getSelectedRow(), 1);
				borrarProveedor(email);
				modeloTablaProv.removeRow(tablaProveedores.getSelectedRow());
				tablaProveedores.setModel(modeloTablaProv);
			}
			break;
		}
		case CREAR_PROVEEDOR: {
			crearProveedor();
			break;
		}
		case EDITAR_PROVEEDOR: {
			if (tablaProveedores.getSelectedRow() == -1) {
				JOptionPane.showMessageDialog(this, "Elige un proveedor");
			} else {
				editarProveedor();
			}
			break;
		}
		}
	}

	/**
	 * funcion que crea un InternalFrame para crear un proveedor
	 */
	private void crearProveedor() {
		if (objInsertProveedor != null) {
			objInsertProveedor.dispose();
		}
		objInsertProveedor = new frmInInsertProveedor(objGestor, INSERTAR + " " + PROVEEDOR, false, true, false, false);
		desktop.add(objInsertProveedor);
		objInsertProveedor.addInternalFrameListener(this);
		Dimension desktopSize = desktop.getSize();
		Dimension FrameSize = objInsertProveedor.getSize();
		objInsertProveedor.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objInsertProveedor.show();
	}

	/**
	 * funcion que abre un internalFrame para eitar el proveedor
	 */
	private void editarProveedor() {
		String email = (String) modeloTablaProv.getValueAt(tablaProveedores.getSelectedRow(), 1);

		itfProperty proveedorBuscar = buscarProveedor(email);
		if (objUpdateProveedor != null) {
			objUpdateProveedor.dispose();
		}
		objUpdateProveedor = new frmInUpdateProveedor(objGestor, EDITAR + " " + PROVEEDOR, proveedorBuscar, false, true,
				false, false);
		desktop.add(objUpdateProveedor);
		objUpdateProveedor.addInternalFrameListener(this);
		Dimension desktopSize = desktop.getSize();
		Dimension FrameSize = objUpdateProveedor.getSize();
		objUpdateProveedor.setLocation((desktopSize.width - FrameSize.width) / 2,
				(desktopSize.height - FrameSize.height) / 2);
		objUpdateProveedor.show();
	}

	/**
	 * funcion que busca un proveedor por email
	 * 
	 * @param email email del proveedor
	 * @return itfProperty del proveedor encntrado
	 */
	private itfProperty buscarProveedor(String email) {
		itfProperty proveedorBuscar = null;
		try {
			proveedorBuscar = objGestor.devolverProveedor(email);
		} catch (clsErrorSQLConsulta | clsErrorSQLConexion e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}

		if (proveedorBuscar == null) {
			JOptionPane.showMessageDialog(this, "proveedor no encontrado");

		}
		return proveedorBuscar;
	}

	/**
	 * borrar proveedor
	 * 
	 * @param email email del proveedor
	 */
	private void borrarProveedor(String email) {
		itfProperty proveedorBuscar = buscarProveedor(email);
		int resultado = -1;
		try {
			resultado = objGestor.borrarProveedor((int) proveedorBuscar.getProperty(PROVEEDOR_COL_ID));
		} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion | clsErrorSQLConsulta e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		if (resultado == 1) {
			JOptionPane.showMessageDialog(this, "Proveedor eliminado");
		}
	}

	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		switch (e.getSource().getClass().getName()) {
		case INTERNAL_INSERT_PROVEEDOR: {
			if (objUpdateProveedor != null) {
				objUpdateProveedor.dispose();
			}
			break;
		}
		case INTERNAL_UPDATE_PROVEEDOR: {
			if (objInsertProveedor != null) {
				objInsertProveedor.dispose();
			}
			break;
		}
		}
	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		switch (e.getSource().getClass().getName()) {
		case INTERNAL_INSERT_PROVEEDOR: {
			itfProperty proveedor = objInsertProveedor.devolverProveedor();
			if (proveedor != null) {
				modeloTablaProv.addRow(new Object[] { proveedor.getProperty(PROVEEDOR_COL_NOMBRE),
						proveedor.getProperty(PROVEEDOR_COL_EMAIL), proveedor.getProperty(PROVEEDOR_COL_TELEFONO) });
			}
			tablaProveedores.setModel(modeloTablaProv);
			break;
		}
		case INTERNAL_UPDATE_PROVEEDOR: {
			listaProveedores = new ArrayList<itfProperty>();
			try {
				listaProveedores = objGestor.listarProveedor();
			} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {
				JOptionPane.showMessageDialog(this, error.getMessage());
			}
			int filas = modeloTablaProv.getRowCount();
			for (int i = 0; i < filas; i++) {
				modeloTablaProv.removeRow(0);
			}
			for (itfProperty aux : listaProveedores) {
				try {
					itfProperty proveedor = objGestor.devolverProveedor((int) aux.getProperty(PROVEEDOR_COL_ID));

					modeloTablaProv.addRow(new Object[] { proveedor.getProperty(PROVEEDOR_COL_NOMBRE),
							proveedor.getProperty(PROVEEDOR_COL_EMAIL),
							proveedor.getProperty(PROVEEDOR_COL_TELEFONO) });
				} catch (clsErrorSQLConsulta | clsErrorSQLConexion error) {
					JOptionPane.showMessageDialog(this, error.getMessage());
				}
			}
			tablaProveedores.setModel(modeloTablaProv);
			break;
		}
		}
	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}
}
