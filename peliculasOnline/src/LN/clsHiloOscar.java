package LN;

import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;

/**
 * clase que extiende de thread poara la creacion de hilos para que actuen en
 * segundo plano
 * 
 * @author Eric
 *
 */
public class clsHiloOscar extends Thread {
	/**
	 * objeto gestor encargado de la logica de negocio
	 */
	private clsGestorLN objGestorLN;

	/**
	 * constructor del hilo editado para poder usar el objeto gestor de la logica de
	 * negocio
	 * 
	 * @param objGestorLN objeto gestor de la logica de negocio
	 */
	public clsHiloOscar(clsGestorLN objGestorLN) {
		this.objGestorLN = objGestorLN;
	}

	/**
	 * boolean que pondremos a false cuando queramos parar el hilo
	 */
	private boolean continuar = true;

	/**
	 * metodo para poner el boolean a false.
	 */
	public void detenElHilo() {
		continuar = false;
	}

	/**
	 * metodo del hilo donde cada 5 segundos insertamos un nuevo oscar
	 */
	public void run() {
		int contador = 0;
		while (continuar) {
			String pelicula = "Pelicula Premiada " + contador;
			String descripcion = "Descripcion de la pelicula premiada " + contador;
			try {
				objGestorLN.insertarOscar(pelicula, descripcion);
				System.out.println("insertado" + contador);
			} catch (clsErrorSQLParametros | clsErrorSQLInsertDeleteUpdate | clsErrorSQLConexion e1) {
				e1.printStackTrace();
			}
			contador++;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
