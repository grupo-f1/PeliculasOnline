package LN;

import java.time.LocalDate;

import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto de tipo ticket que esta relacionado con la
 * tabla ticket de la base de datos. Esta relacionado con el {@link clsSocio
 * socio} y con la {@link clsPelicula pelicula}
 * 
 * @author Eric
 *
 */
public class clsTicket implements itfProperty {
	/**
	 * Atributo que indica el identifador del socio
	 */
	private int socio_FK;
	/**
	 * Atributo que indica el identifador de la pelicula
	 */
	private int pelicula_FK;
	/**
	 * Atributo que indica la fecha que se efectuo la compra
	 */
	private LocalDate fecha_compra;

	/**
	 * Constructor del objeto ticket con todos los parametros
	 * 
	 * @param socio_FK     id del socio
	 * @param pelicula_FK  id de la pelicula
	 * @param fecha_compra fecha cuando se efectuo la compra de la pelicula
	 */
	public clsTicket(int socio_FK, int pelicula_FK, LocalDate fecha_compra) {
		super();
		this.socio_FK = socio_FK;
		this.pelicula_FK = pelicula_FK;
		this.fecha_compra = fecha_compra;
	}

	/**
	 * Constructor del objeto ticket vacio
	 */
	public clsTicket() {
		super();
	}

	public int getSocio_FK() {
		return socio_FK;
	}

	public void setSocio_FK(int socio_FK) {
		this.socio_FK = socio_FK;
	}

	public int getPelicula_FK() {
		return pelicula_FK;
	}

	public void setPelicula_FK(int pelicula_FK) {
		this.pelicula_FK = pelicula_FK;
	}

	public LocalDate getFecha_compra() {
		return fecha_compra;
	}

	public void setFecha_compra(LocalDate fecha_compra) {
		this.fecha_compra = fecha_compra;
	}

	@Override
	public String toString() {
		return "El socio: " + socio_FK + ", la pelicula: " + pelicula_FK + " y la fecha de la compra: " + fecha_compra;
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "socio": {
			return this.socio_FK;
		}
		case "pelicula": {
			return this.pelicula_FK;
		}
		case "fecha compra": {
			return this.fecha_compra;
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

}
