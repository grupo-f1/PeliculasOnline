package LN;

import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto donde se guardará la relacion entre pelicula
 * y tipo pelicula que esta relaiconado con la tabla pelicula_tipos de la base
 * de datos datos. Las peliculas estan en {@link clsPelicula pelicula} los tipos
 * de peliculas estan en {@link clsTipoPelicula tipo pelicula}
 * 
 * @author Eric
 *
 */
public class clsPeliculaTipos implements itfProperty {
	/**
	 * atributo que indica el identifiacor de la pelicula
	 */
	private int pelicula_FK;
	/**
	 * atributo que indica el identiicador de el tipo de pelicula
	 */
	private int tipo_pelicula_FK;

	/**
	 * construcotr de pelicula tipos con todos los parametros
	 * 
	 * @param pelicula_FK      id de la peoicla
	 * @param tipo_pelicula_FK id del tipo de pelicula
	 */
	public clsPeliculaTipos(int pelicula_FK, int tipo_pelicula_FK) {
		super();
		this.pelicula_FK = pelicula_FK;
		this.tipo_pelicula_FK = tipo_pelicula_FK;
	}

	/**
	 * constructor de peliculas tipos sin parametros
	 */
	public clsPeliculaTipos() {
		super();
	}

	public int getPelicula_FK() {
		return pelicula_FK;
	}

	public void setPelicula_FK(int pelicula_FK) {
		this.pelicula_FK = pelicula_FK;
	}

	public int getTipo_pelicula_FK() {
		return tipo_pelicula_FK;
	}

	public void setTipo_pelicula_FK(int tipo_pelicula_FK) {
		this.tipo_pelicula_FK = tipo_pelicula_FK;
	}

	@Override
	public String toString() {
		return "Las relaciones de las peliculas con los tipos de pelicualas son: pelicula id: " + pelicula_FK
				+ "y tipo pelicula id: " + tipo_pelicula_FK;
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "pelicula": {
			return this.pelicula_FK;
		}
		case "tipo pelicula": {
			return this.tipo_pelicula_FK;
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

}
