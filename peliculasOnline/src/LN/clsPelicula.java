package LN;

import COMUN.itfProperty;
import COMUN.clsConstantes.*;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto pelicula que esta relacionado con la tabla
 * pelicula de la base de datos. enlace a empleado {@link clsEmpleado empleado}
 * * enlace a proveedor {@link clsProveedor proveedor} * Enlace a ticket
 * {@link clsTicket ticket}
 * 
 * @author Eric y Mikel
 *
 */
public class clsPelicula implements itfProperty, Comparable<clsPelicula> {

	/**
	 * Atributo que indica el id de la pelicula
	 */
	private int idPelicula;
	/**
	 * Atributo que indica el id del empleado que encargo la pelicula
	 */
	private int empleado_FK;
	/**
	 * Atributo que indica el id del proveedor que vendio la pelicula
	 */
	private int proveedor_FK;
	/**
	 * Atributo que indica el la restriccion de edad a la hora de comprar la
	 * pelicula
	 */
	private String clasificacion;
	/**
	 * Atributo que indica la duracion de la pelicula en minutos
	 */
	private int duracion;
	/**
	 * Atributo que indica los idiomas que contiene esta pelicula
	 */
	private String idiomas;
	/**
	 * Atributo que indica si la pelicula tiene o no los subtitulos en los idiomas
	 * que esta continen
	 */
	private boolean subtitulos;
	/**
	 * Atributo que indica la resolucion del video
	 */
	private String resolucion;
	/**
	 * Atributo que indica el titulo de la pelicula
	 */
	private String titulo;
	/**
	 * Atributo que indica la direccion en el servidor de la imagen de la portada
	 */
	private String portada;
	/**
	 * Atributo que indica la descripción de la película
	 */
	private String descripcion;

	/**
	 * Constructor del objeto pelicula con todos los parametros
	 * 
	 * @param idPelicula    id de la pelicula
	 * @param empleado_FK   id del empleado encargado de comprar la pelicula al
	 *                      proveedor
	 * @param proveedor_FK  id del proveedor que ha vendido la pelicula
	 * @param clasificacion edad minima recomendada para ver la pelicula
	 * @param duracion      durecion de la pelicula en minutos
	 * @param idiomas       idiomas que contiene la pelicula
	 * @param subtitulos    si tiene subtiutlos o no la pelicula
	 * @param resolucion    resolucion de video de la pelicula
	 * @param titulo        titulo de la pelicula
	 * @param portada       direccion de la imagen de la portada en el servidor
	 * @param descripcion   resumen de la película
	 */
	public clsPelicula(int idPelicula, int empleado_FK, int proveedor_FK, String clasificacion, int duracion,
			String idiomas, boolean subtitulos, String resolucion, String titulo, String portada, String descripcion) {
		super();
		this.idPelicula = idPelicula;
		this.empleado_FK = empleado_FK;
		this.proveedor_FK = proveedor_FK;
		this.clasificacion = clasificacion;
		this.duracion = duracion;
		this.idiomas = idiomas;
		this.subtitulos = subtitulos;
		this.resolucion = resolucion;
		this.titulo = titulo;
		this.portada = portada;
		this.descripcion = descripcion;

	}

	/**
	 * constructor del objeto pelicula sin parametros
	 */
	public clsPelicula() {
		super();
	}

	public int getIdPelicula() {
		return idPelicula;
	}

	public void setIdPelicula(int idPelicula) {
		this.idPelicula = idPelicula;
	}

	public int getEmpleado_FK() {
		return empleado_FK;
	}

	public void setEmpleado_FK(int empleado_FK) {
		this.empleado_FK = empleado_FK;
	}

	public int getProveedor_FK() {
		return proveedor_FK;
	}

	public void setProveedor_FK(int proveedor_FK) {
		this.proveedor_FK = proveedor_FK;
	}

	public String getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public String getIdiomas() {
		return idiomas;
	}

	public void setIdiomas(String idiomas) {
		this.idiomas = idiomas;
	}

	public boolean isSubtitulos() {
		return subtitulos;
	}

	public void setSubtitulos(boolean subtitulos) {
		this.subtitulos = subtitulos;
	}

	public String getResolucion() {
		return resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getPortada() {
		return portada;
	}

	public void setPortada(String portada) {
		this.portada = portada;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Id: " + idPelicula + ", titulo: " + titulo + ", id del empleado: " + empleado_FK
				+ ", id del proveedor: " + proveedor_FK + ", clasificacion: " + clasificacion + ", duracion: "
				+ duracion + ", idiomas: " + idiomas + ", subtitulos: " + subtitulos + ", resolucion: " + resolucion
				+ ", portada: " + portada + ", descripcion: " + descripcion;
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "id Pelicula": {
			return this.idPelicula;
		}
		case "empleado": {
			return this.empleado_FK;
		}
		case "proveedor": {
			return this.proveedor_FK;
		}
		case "clasificacion": {
			return this.clasificacion;
		}
		case "duracion": {
			return this.duracion;
		}
		case "idiomas": {
			return this.idiomas;
		}
		case "subtitulos": {
			return this.subtitulos;
		}
		case "resolucion": {
			return this.resolucion;
		}
		case "titulo": {
			return this.titulo;
		}
		case "portada": {
			return this.portada;
		}
		case "descripcion": {
			return this.descripcion;
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

	// The natural ordering for a class C is said to be consistent with equals if
	// and only if
	// e1.compareTo(e2) == 0 has the same boolean value as e1.equals(e2) for every
	// e1 and e2 of class C.
	// Note that null is not an instance of any class, and e.compareTo(null) should
	// throw a NullPointerException
	// even though e.equals(null) returns false.
	//
	// a negative integer, zero, or a positive integer as this object is less than,
	// equal to, or greater than the specified object.
	/**
	 * funcion que compara dos titulos de peliculas
	 */
	public int compareTo(clsPelicula arg0) {

		if (arg0 == null)
			return 1;

		// si son iguales, devolvemos un cero. La comprobaci�n de la igualdad, la
		// podemos delegar en equals.
		// De esta forma hacemos que compareTo sea consistente con equals.
		if (this.equals(arg0))
			return 0;

		// Si los objetos no son iguales, debemos comprobar el orden de ambos seg�n el
		// criterio deseado:
		// En este caso el criterio que tomamos es que ordenamos por marca y en caso de
		// ser la misma marca, por tama�o.
		if (!this.titulo.equals(arg0.getTitulo()))
			return this.titulo.compareTo(arg0.getTitulo());

		// Como los titulos eran iguales, ordeno seg�n los tama�os.
		Integer tam = this.proveedor_FK;
		return tam.compareTo(arg0.getProveedor_FK());

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
		result = prime * result + proveedor_FK;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		clsPelicula other = (clsPelicula) obj;
		if (titulo == null) {
			if (other.titulo != null)
				return false;
		} else if (!titulo.equals(other.titulo))
			return false;
		if (proveedor_FK != other.proveedor_FK)
			return false;
		return true;
	}

}
