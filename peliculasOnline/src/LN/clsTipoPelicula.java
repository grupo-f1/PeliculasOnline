package LN;

import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto Tipo pelicula que esta relacionado con la
 * tabla Tipo pelicula de la base de datos. Las peliculas estan en
 * {@link clsPelicula pelicula}
 * 
 * @author Diego y Eric
 *
 */
public class clsTipoPelicula implements itfProperty {
	/**
	 * Atributo que indica el id del tipo de pelicula
	 */
	private int idTipoPelicula;
	/**
	 * Atributo que indica el nombre del tipo de peliucla
	 */
	private String nombre;
	/**
	 * Atributo que indica la descripción del tipo de pelicula
	 */
	private String descripcion;

	/**
	 * Coonstructor del objeto tipo pelicula con todos los parametros
	 * 
	 * @param idTipoPelicula id de tipo de pelicula
	 * @param nombre         nombre del tipo de pelicula
	 * @param descripcion    descripcion del tipo de pelicula
	 */
	public clsTipoPelicula(int idTipoPelicula, String nombre, String descripcion) {
		super();
		this.idTipoPelicula = idTipoPelicula;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	/**
	 * Constructor del objeto tipo pelicula sin parametros
	 */
	public clsTipoPelicula() {
		super();
	}

	public int getIdTipoPelicula() {
		return idTipoPelicula;
	}

	public void setIdTipoPelicula(int idTipoPelicula) {
		this.idTipoPelicula = idTipoPelicula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "nombre: " + nombre + ", su id es" + idTipoPelicula + " y su descripcion es " + descripcion;
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "nombre": {
			return this.nombre;
		}
		case "id Tipo Pelicula": {
			return this.idTipoPelicula;
		}
		case "descripcion": {
			return this.descripcion;
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

}
