package LN;

import java.time.LocalDate;
import COMUN.itfProperty;
import COMUN.clsConstantes.*;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto empleado que esta relacionado con la tabla
 * empleado de la base de datos. Los tipos de empleados son
 * {@link clsTipoEmpleado tipo empleado} enlace a la clase padre
 * {@link clsUsuario usuario}
 * 
 * @author Eric
 *
 */
public class clsEmpleado extends clsUsuario implements itfProperty {
	/**
	 * Atributo que indica el id del empleado
	 */
	private int idEmpleado;
	/**
	 * Atributo que indica el id del tipo de empleado
	 */
	private int tipo_empleado_FK;

	/**
	 * Constructor con del objeto empleado con parametros
	 * 
	 * @param user_name        nombre de usuario en la aplicacion
	 * @param password         contraseña del usuario
	 * @param nombre           nombre del usuario
	 * @param apellidos        apellido del usuario
	 * @param fecha_nacimiento fecha nacimiento del usuario
	 * @param dni              dni del usuario
	 * @param telefono         telefono del usuario
	 * @param email            email del usuario
	 * @param create_time      fecha de creacion del usuario
	 * @param idEmpleado       id del empleado
	 * @param tipo_empleado_FK id del tipo de empleado
	 */
	public clsEmpleado(String user_name, String password, String nombre, String apellidos, LocalDate fecha_nacimiento,
			String dni, String telefono, String email, LocalDate create_time, int idEmpleado, int tipo_empleado_FK) {
		super(user_name, password, nombre, apellidos, fecha_nacimiento, dni, telefono, email, create_time);
		this.idEmpleado = idEmpleado;
		this.tipo_empleado_FK = tipo_empleado_FK;
	}

	/**
	 * Constructor vacio sin parametros
	 */
	public clsEmpleado() {
		super();
	}

	public int getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(int idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public int getTipo_empleado_FK() {
		return tipo_empleado_FK;
	}

	public void setTipo_empleado_FK(int tipo_empleado_FK) {
		this.tipo_empleado_FK = tipo_empleado_FK;
	}

	@Override
	public String toString() {
		return "Los datos del empleado son: id=" + idEmpleado + ", tipo empleado=" + tipo_empleado_FK + " "
				+ super.toString();
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "id Empleado": {
			return this.idEmpleado;
		}
		case "tipo Empleado": {
			return this.tipo_empleado_FK;
		}
		case "user name": {
			return this.getUser_name();
		}
		case "password": {
			return this.getPassword();
		}
		case "nombre": {
			return this.getNombre();
		}
		case "apellidos": {
			return this.getApellidos();
		}
		case "fecha nacimiento": {
			return this.getFecha_nacimiento();
		}
		case "dni": {
			return this.getdni();
		}
		case "telefono": {
			return this.getTelefono();
		}
		case "email": {
			return this.getEmail();
		}
		case "create time": {
			return this.getCreate_time();
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

}
