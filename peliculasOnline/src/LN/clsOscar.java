package LN;

import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto oscar para el hilo
 * 
 * @author Eric
 *
 */
public class clsOscar implements itfProperty {

	/**
	 * Atributo que indica el id del oscar.
	 */
	private int idOscar;

	/**
	 * Atributo que indica la pelicula.
	 *
	 */

	private String pelicula;

	/**
	 * Atributo que indica la descripcion de la pelicula.
	 */
	private String descripcion;

	/**
	 * Constructor del Objeto oscar
	 * 
	 * @param idOscar     id del proveedor
	 * @param pelicula    nombre del proveedor
	 * @param descripcion email de contacto del proveedor
	 */
	public clsOscar(int idOscar, String pelicula, String descripcion) {
		super();
		this.idOscar = idOscar;
		this.pelicula = pelicula;
		this.descripcion = descripcion;
	}

	/**
	 * Constructor del objeto oscar
	 */
	public clsOscar() {
		super();

	}

	public int getIdOscar() {
		return idOscar;
	}

	public void setIdOscar(int idOscar) {
		this.idOscar = idOscar;
	}

	public String getPelicula() {
		return pelicula;
	}

	public void setPelicula(String pelicula) {
		this.pelicula = pelicula;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "pelicula: " + pelicula + ", descripcion es: " + descripcion;
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "id Oscar": {
			return this.idOscar;
		}
		case "pelicula": {
			return this.pelicula;
		}
		case "descripcion": {
			return this.descripcion;
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

}
