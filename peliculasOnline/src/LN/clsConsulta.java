package LN;

import java.time.LocalDate;

import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto consulta que esta relacionado con la tabla
 * consulta de la base de datos. Esta relaciona con empleado y socio. Los
 * empleados estan en {@link clsEmpleado empleado} Los socios estan en
 * {@link clsSocio socio}
 * 
 * @author Eric
 *
 */
public class clsConsulta implements itfProperty {
	/**
	 * Atributo que indica el identifcador del socio
	 */
	private int socio_FK;
	/**
	 * Atributo que indica el identifador del empleado
	 */
	private int empleado_FK;
	/**
	 * Atributo que indica la fecha de creacion de la consulta
	 */
	private LocalDate fecha;
	/**
	 * Atributo que indica la incidencia que ha ocurrido
	 */
	private String incidencia;
	/**
	 * Atributo que indica la resoucion de la incidencia
	 */
	private Boolean resolucion;

	/**
	 * Constructor del objeto consulta con todos los parametros
	 * 
	 * @param socio_FK    id del socio
	 * @param empleado_FK id del empleado
	 * @param fecha       fecha de la incidencia
	 * @param incidencia  titulo de la incidencia
	 * @param resolucion  resolucion de la incidencia
	 */
	public clsConsulta(int socio_FK, int empleado_FK, LocalDate fecha, String incidencia, Boolean resolucion) {
		super();
		this.socio_FK = socio_FK;
		this.empleado_FK = empleado_FK;
		this.fecha = fecha;
		this.incidencia = incidencia;
		this.resolucion = resolucion;
	}

	/**
	 * Constructor del objeto consulta vacio
	 */
	public clsConsulta() {
		super();
	}

	public int getSocio_FK() {
		return socio_FK;
	}

	public void setSocio_FK(int socio_FK) {
		this.socio_FK = socio_FK;
	}

	public int getEmpleado_FK() {
		return empleado_FK;
	}

	public void setEmpleado_FK(int empleado_FK) {
		this.empleado_FK = empleado_FK;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(String incidencia) {
		this.incidencia = incidencia;
	}

	public Boolean getResolucion() {
		return resolucion;
	}

	public void setResolucion(Boolean resolucion) {
		this.resolucion = resolucion;
	}

	@Override
	public String toString() {
		return "socio: " + socio_FK + ", empleado: " + empleado_FK + ", fecha: " + fecha + ", incidencia: " + incidencia
				+ " y la resolucion: " + resolucion;
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "socio": {
			return this.socio_FK;
		}
		case "empleado": {
			return this.empleado_FK;
		}
		case "fecha": {
			return this.fecha;
		}
		case "incidencia": {
			return this.incidencia;
		}
		case "resolucion": {
			return this.resolucion;
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

}
