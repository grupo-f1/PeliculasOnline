package LN;

import java.time.LocalDate;

import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto socio que esta relacionado con la tabla socio
 * de la base de datos. enlace a la clase padre {@link clsUsuario usuario}
 * 
 * @author Eric y Mikel
 *
 */
public class clsSocio extends clsUsuario implements itfProperty {
	/**
	 * Atributo que indica el id del socio
	 */
	private int idSocio;
	/**
	 * Atributo que indica el id de la tarifa escogida
	 */
	private int tarifa_FK;
	/**
	 * Atributo que indica el estado del socio (alta/baja)
	 */
	private boolean estado;
	/**
	 * Atributo que indica la fecha de el pago de la tarifa
	 */
	private LocalDate fecha_tarifa;

	/**
	 * Constructor del objeto socio con todos los parametros
	 * 
	 * @param user_name        nombre de usuario en la aplicacion
	 * @param password         contraseña del usuario
	 * @param nombre           nombre del usuario
	 * @param apellidos        apellido del usuario
	 * @param fecha_nacimiento fecha nacimiento del usuario
	 * @param dni              dni del usuario
	 * @param telefono         telefono del usuario
	 * @param email            email del usuario
	 * @param create_time      fecha de creacion del usuario
	 * @param idSocio          id del socio
	 * @param tarifa_FK        id de la tarifa
	 * @param estado           estado del socio (alta/baja)
	 * @param fecha_tarifa     fecha cuando se efectuo la tarifa
	 */
	public clsSocio(String user_name, String password, String nombre, String apellidos, LocalDate fecha_nacimiento,
			String dni, String telefono, String email, LocalDate create_time, int idSocio, int tarifa_FK,
			boolean estado, LocalDate fecha_tarifa) {
		super(user_name, password, nombre, apellidos, fecha_nacimiento, dni, telefono, email, create_time);
		this.idSocio = idSocio;
		this.tarifa_FK = tarifa_FK;
		this.estado = estado;
		this.fecha_tarifa = fecha_tarifa;
	}

	/**
	 * Constructor del objeto socio sin parametros
	 */
	public clsSocio() {
		super();
	}

	public int getIdSocio() {
		return idSocio;
	}

	public void setIdSocio(int idSocio) {
		this.idSocio = idSocio;
	}

	public int getTarifa_FK() {
		return tarifa_FK;
	}

	public void setTarifa_FK(int tarifa_FK) {
		this.tarifa_FK = tarifa_FK;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public LocalDate getFecha_tarifa() {
		return fecha_tarifa;
	}

	public void setFecha_tarifa(LocalDate fecha_tarifa) {
		this.fecha_tarifa = fecha_tarifa;
	}

	@Override
	public String toString() {
		return "Los datos del socio son: id: " + idSocio + ", id de la tarifa: " + tarifa_FK + ", estado: " + estado
				+ ", fecha de la tarifa: " + fecha_tarifa + " " + super.toString();
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "id Socio": {
			return this.idSocio;
		}
		case "tarifa": {
			return this.tarifa_FK;
		}
		case "estado": {
			return this.estado;
		}
		case "fecha tarifa": {
			return this.fecha_tarifa;
		}
		case "user name": {
			return this.getUser_name();
		}
		case "password": {
			return this.getPassword();
		}
		case "nombre": {
			return this.getNombre();
		}
		case "apellidos": {
			return this.getApellidos();
		}
		case "fecha nacimiento": {
			return this.getFecha_nacimiento();
		}
		case "dni": {
			return this.getdni();
		}
		case "telefono": {
			return this.getTelefono();
		}
		case "email": {
			return this.getEmail();
		}
		case "create time": {
			return this.getCreate_time();
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

}
