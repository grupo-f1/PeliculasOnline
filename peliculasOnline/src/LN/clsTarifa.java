package LN;

import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto tarifa que esta relacionado con la tabla
 * tarifa en la base de datos. La tarifa esta relacionada con los
 * {@link clsSocio socios}
 * 
 * @author Eric
 *
 */
public class clsTarifa implements itfProperty {
	/**
	 * Atributo que indica el identificador de la tarifa
	 */
	private int idTarifa;
	/**
	 * Atributo que indica el tipo de tarifa
	 */
	private String tipo;
	/**
	 * Atributo que indica el precio de esa tarifa
	 */
	private int precio;

	/**
	 * Constructor del objeto tarifa con todos los parametros
	 * 
	 * @param idTarifa id de la tarifa
	 * @param tipo     tipo de tarifa
	 * @param precio   precio de la tarifa
	 */
	public clsTarifa(int idTarifa, String tipo, int precio) {
		super();
		this.idTarifa = idTarifa;
		this.tipo = tipo;
		this.precio = precio;
	}

	/**
	 * Constructor del objeto tarifa vacio
	 */
	public clsTarifa() {
		super();
	}

	public int getIdTarifa() {
		return idTarifa;
	}

	public void setIdTarifa(int idTarifa) {
		this.idTarifa = idTarifa;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Los datos de la tarifa son: el identificador: " + idTarifa + ", el tipo: " + tipo + " y el precio: "
				+ precio;
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "id Tarifa": {
			return this.idTarifa;
		}
		case "tipo": {
			return this.tipo;
		}
		case "precio": {
			return this.precio;
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

}
