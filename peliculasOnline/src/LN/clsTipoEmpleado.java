package LN;

import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto Tipo empleado que esta relacionado con la
 * tabla Tipo empleado de la base de datos. Los empleados estan en
 * {@link clsEmpleado empleado}
 * 
 * @author Diego y Eric
 *
 */
public class clsTipoEmpleado implements itfProperty {
	/**
	 * Atributo que indica el idTipoEmpleado
	 */
	private int idTipoEmpleado;
	/**
	 * Atributo que indica el nombre del Tipo de empleado
	 */
	private String nombre;
	/**
	 * Atributo que indica la descripción delTipo de empleado
	 */
	private String descripcion;

	/**
	 * Coonstructor del objeto Tipo Empleado con todos los parametros
	 * 
	 * @param idTipoEmpleado id del tipo de empleado
	 * @param nombre         nombre del tipo de empleado
	 * @param descripcion    descripcion del tipo de empleado
	 */
	public clsTipoEmpleado(int idTipoEmpleado, String nombre, String descripcion) {
		super();
		this.idTipoEmpleado = idTipoEmpleado;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	/**
	 * Constructor del objeto Tipo Empleado sin parametros
	 */
	public clsTipoEmpleado() {
		super();
	}

	public int getIdTipoEmpleado() {
		return idTipoEmpleado;
	}

	public void setIdTipoEmpleado(int idTipoEmpleado) {
		this.idTipoEmpleado = idTipoEmpleado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "El tipo de empleado es: " + nombre + ", su id es" + idTipoEmpleado + " y su descripción es "
				+ descripcion;
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "nombre": {
			return this.nombre;
		}
		case "id Tipo Empleado": {
			return this.idTipoEmpleado;
		}
		case "descripcion": {
			return this.descripcion;
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

}
