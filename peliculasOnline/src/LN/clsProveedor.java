package LN;

import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que implementa un objeto proveedor que esta relacionado con la tabla
 * proveedor de la base de datos.
 * 
 * @author Diego y Eric
 *
 */
public class clsProveedor implements itfProperty {

	/**
	 * Atributo que indica el id del proveedor.
	 */
	private int idProveedor;

	/**
	 * Atributo que indica el nombre del proveedor.
	 *
	 */

	private String nombre;

	/**
	 * Atributo que indica el email del proveedor.
	 */
	private String email;

	/**
	 * Atributo que indica el telefono de contacto del proveedor.
	 */
	private String telefono;

	/**
	 * Constructor del Objeto proveedor con todos los parámetros.
	 * 
	 * @param idProveedor id del proveedor
	 * @param nombre      nombre del proveedor
	 * @param email       email de contacto del proveedor
	 * @param telefono    telefono de contacto del proveedor
	 */
	public clsProveedor(int idProveedor, String nombre, String email, String telefono) {
		super();
		this.idProveedor = idProveedor;
		this.nombre = nombre;
		this.email = email;
		this.telefono = telefono;
	}

	/**
	 * Constructor del objeto proveedor sin parámetros.
	 */
	public clsProveedor() {
		super();

	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Override
	public String toString() {
		return "nombre: " + nombre + ", email es: " + email + " y telefono es " + telefono;
	}

	@Override
	public Object getProperty(String propiedad) {
		switch (propiedad) {
		case "id Proveedor": {
			return this.idProveedor;
		}
		case "nombre": {
			return this.nombre;
		}
		case "email": {
			return this.email;
		}
		case "telefono": {
			return this.telefono;
		}
		default: {
			throw new clsPropertyNoExistente(propiedad);
		}
		}
	}

}
