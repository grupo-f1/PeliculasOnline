package LN;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import COMUN.clsComparadorProveedor_FK;
import COMUN.itfData;
import COMUN.itfProperty;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;
import LD.clsGestorLD;

import static COMUN.clsConstantesDB.*;
import static COMUN.clsConstantes.*;

/**
 * Clase encargarda de enlazar la logica de presentacion con la logica de
 * negocio mediante funciones.
 * 
 * @author Diego, Eric y Mikel
 *
 */
public class clsGestorLN {
	/**
	 * Atributo con la lista de tarifas
	 */
	private ArrayList<clsTarifa> listaTarifas;
	/**
	 * Atributo con la lista de tipo de empleados
	 */
	private ArrayList<clsTipoEmpleado> listaTipoEmpleados;
	/**
	 * Atributo con la lista de proveedores
	 */
	private ArrayList<clsProveedor> listaProveedores;
	/**
	 * Atributo con la lista de peliculas
	 */
	private ArrayList<clsPelicula> listaPeliculas;
	/**
	 * Atributo con la lista de tipo de peliculas
	 */
	private ArrayList<clsTipoPelicula> listaTipoPeliculas;
	/**
	 * Atributo con la lista de la relacion entre peliculas y tipo de peliculas
	 */
	private ArrayList<clsPeliculaTipos> listaPeliculasTipos;
	/**
	 * Atributo con la lista de consultas
	 */
	private ArrayList<clsConsulta> listaConsultas;
	/**
	 * Atributo con la lista de tickets
	 */
	private ArrayList<clsTicket> listaTickets;
	/**
	 * Atributo con la lista de oscars
	 */
	private ArrayList<clsOscar> listaOscars;
	/**
	 * Atributo con la lista de usuarios
	 */
	private HashMap<String, Object> mUsuarios;
	/**
	 * objeto de la clase del gestor de la logica de datos
	 */
	private clsGestorLD objDatos;

	/**
	 * Constructor del gestor de la logica de negocio, se encarga de mantener
	 * guardados las listas de todas las clases y generar el hilo de las inserciones
	 * de los oscar
	 */
	public clsGestorLN() {

		listaTarifas = new ArrayList<clsTarifa>();
		listaTipoEmpleados = new ArrayList<clsTipoEmpleado>();
		listaProveedores = new ArrayList<clsProveedor>();
		listaPeliculas = new ArrayList<clsPelicula>();
		listaTipoPeliculas = new ArrayList<clsTipoPelicula>();
		listaPeliculasTipos = new ArrayList<clsPeliculaTipos>();
		listaConsultas = new ArrayList<clsConsulta>();
		listaTickets = new ArrayList<clsTicket>();
		listaOscars = new ArrayList<clsOscar>();
		mUsuarios = new HashMap<String, Object>();
		objDatos = new clsGestorLD();
		clsHiloOscar hiloOscar = new clsHiloOscar(this);
		hiloOscar.start();
	}

	// ------------------CREAR LISTAS---------------------//

	/**
	 * Funcion que sirve para rellenar el HashMap de usuarios
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarUsuarios() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		iniciarEmpleados();
		iniciarSocios();
	}

	/**
	 * Funcion que rellena el arraylist con todos los socios de la base de datos
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarSocios() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_SOCIOS, null);
		objDatos.Disconnect();
		for (itfData fila : tabla) {
			clsSocio aux = new clsSocio();
			aux.setApellidos((String) fila.getData(SOCIO_APELLIDOS));
			aux.setdni((String) fila.getData(SOCIO_DNI));
			aux.setEmail((String) fila.getData(SOCIO_EMAIL));
			if ((int) fila.getData(SOCIO_ESTADO) == 1) {
				aux.setEstado(true);
			} else {
				aux.setEstado(false);
			}
			String fecha = fila.getData(SOCIO_FECHA_NACIMIENTO).toString();
			aux.setFecha_nacimiento(transformarFecha(fecha));
			fecha = fila.getData(SOCIO_FECHA_TARIFA).toString();
			aux.setFecha_tarifa(transformarFecha(fecha));
			aux.setNombre((String) fila.getData(SOCIO_NOMBRE));
			aux.setPassword((String) fila.getData(SOCIO_PASSWORD));
			aux.setTarifa_FK((int) fila.getData(SOCIO_TARIFA_FK));
			aux.setTelefono((String) fila.getData(SOCIO_TELEFONO));
			aux.setUser_name((String) fila.getData(SOCIO_USER_NAME));
			aux.setIdSocio((int) fila.getData(SOCIO_ID));

			mUsuarios.put(aux.getUser_name(), aux);
		}
	}

	/**
	 * Funcion que rellena el arraylist con todos los empleados de la base de datos
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarEmpleados() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_EMPLEADOS, null);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			clsEmpleado aux = new clsEmpleado();
			aux.setApellidos((String) fila.getData(EMPLEADO_APELLIDOS));
			aux.setdni((String) fila.getData(EMPLEADO_DNI));
			aux.setEmail((String) fila.getData(EMPLEADO_EMAIL));
			String fecha = fila.getData(EMPLEADO_FECHA_NACIMIENTO).toString();
			aux.setFecha_nacimiento(transformarFecha(fecha));
			aux.setNombre((String) fila.getData(EMPLEADO_NOMBRE));
			aux.setPassword((String) fila.getData(EMPLEADO_PASSWORD));
			aux.setTelefono((String) fila.getData(EMPLEADO_TELEFONO));
			aux.setTipo_empleado_FK((int) fila.getData(EMPLEADO_TIPO_EMPLEADO_FK));
			aux.setUser_name((String) fila.getData(EMPLEADO_USER_NAME));
			aux.setIdEmpleado((int) fila.getData(EMPLEADO_ID));

			mUsuarios.put(aux.getUser_name(), aux);
		}
	}

	/**
	 * funcion que rellena el arraylist de tipo empleados
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarTipoEmpleados() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_TIPO_EMPLEADOS, null);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			clsTipoEmpleado aux = new clsTipoEmpleado();
			aux.setDescripcion((String) fila.getData(TIPO_EMPLEADO_DESCRIPCION));
			aux.setNombre((String) fila.getData(TIPO_EMPLEADO_NOMBRE));
			aux.setIdTipoEmpleado((int) fila.getData(TIPO_EMPLEADO_ID));

			listaTipoEmpleados.add(aux);
		}

	}

	/**
	 * Funcion que rellena el arraylist de peliculas
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarPeliculas() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_PELICULAS, null);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			clsPelicula pelicula = new clsPelicula();

			pelicula.setIdPelicula((int) fila.getData(PELICULA_ID));
			pelicula.setEmpleado_FK((int) fila.getData(PELICULA_EMPLEADO_FK));
			pelicula.setProveedor_FK((int) fila.getData(PELICULA_PROVEEDOR_FK));
			pelicula.setClasificacion((String) fila.getData(PELICULA_CLASIFICACION));
			pelicula.setDuracion(Integer.parseInt(fila.getData(PELICULA_DURACION).toString()));
			pelicula.setIdiomas((String) fila.getData(PELICULA_IDIOMAS));

			pelicula.setSubtitulos(convertirIntBoolean((int) fila.getData(PELICULA_SUBTITULOS)));
			pelicula.setResolucion((String) fila.getData(PELICULA_RESOLUCION));
			pelicula.setTitulo((String) fila.getData(PELICULA_TITULO));
			pelicula.setPortada((String) fila.getData(PELICULA_PROTADA));
			pelicula.setDescripcion((String) fila.getData(PELICULA_DESCRIPCION));

			listaPeliculas.add(pelicula);
		}

	}

	/**
	 * funcion que rellena el arraylist de consultas
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarConsultas() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_CONSULTAS, null);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			clsConsulta aux = new clsConsulta();
			aux.setEmpleado_FK((int) fila.getData(CONSULTA_EMPLEADO_PK));
			String fecha = fila.getData(CONSULTA_FECHA).toString();
			aux.setFecha(transformarFecha(fecha));
			aux.setIncidencia((String) fila.getData(CONSULTA_INCIDENCIA));
			if ((int) fila.getData(CONSULTA_RESOLUCION) == 1) {
				aux.setResolucion(true);
			} else {
				aux.setResolucion(false);
			}
			aux.setSocio_FK((int) fila.getData(CONSULTA_SOCIO_PK));

			listaConsultas.add(aux);
		}
	}

	/**
	 * Funcion que rellena el ArayList de tickets de la base de datos
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarTickets() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_TICKETS, null);
		objDatos.Disconnect();
		for (itfData fila : tabla) {
			LocalDate fechaLocalDate = transformarFecha(fila.getData(TICKET_FECHA_COMPRA).toString());
			clsTicket ticket = new clsTicket((int) fila.getData(TICKET_SOCIO_FK),
					(int) fila.getData(TICKET_PELICULA_FK), fechaLocalDate);
			listaTickets.add(ticket);
		}
	}

	/**
	 * rellena el ArrayList listaTarifas desde la base de datos
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarTarifas() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		// Rellenar el arraylist de clsTarifa desde la base de datos
		ArrayList<itfData> tabla;
		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_TARIFAS, null);
		objDatos.Disconnect();
		for (itfData fila : tabla) {
			clsTarifa tarifa = new clsTarifa();
			tarifa.setIdTarifa((int) fila.getData(TARIFA_ID));
			tarifa.setPrecio((int) fila.getData(TARIFA_PRECIO));
			tarifa.setTipo((String) fila.getData(TARIFA_TIPO));
			listaTarifas.add(tarifa);
		}
	}

	/**
	 * funcion que rellena el ArrayList de proveedores de la base de datos
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarProveedores() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_PROVEEDORES, null);
		objDatos.Disconnect();
		for (itfData fila : tabla) {
			clsProveedor proveedor = new clsProveedor((int) fila.getData(PROVEEDOR_ID),
					(String) fila.getData(PROVEEDOR_NOMBRE), (String) fila.getData(PROVEEDOR_EMAIL),
					(String) fila.getData(PROVEEDOR_TELEFONO));
			listaProveedores.add(proveedor);
		}
	}

	/**
	 * funcion que rellena el ArrayList de tipos de peliculas de la base de datos
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarTipoPeliculas() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_TIPO_PELICULAS, null);
		objDatos.Disconnect();
		for (itfData fila : tabla) {
			clsTipoPelicula tipoPelicula = new clsTipoPelicula((int) fila.getData(TIPO_PELICULA_ID),
					(String) fila.getData(TIPO_PELICULA_NOMBRE), (String) fila.getData(TIPO_PELICULA_DESCRIPCION));
			listaTipoPeliculas.add(tipoPelicula);
		}
	}

	/**
	 * funcion que rellena el ArrayList de la relacion de peliculas y tipos de
	 * peliculas de la base de datos
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarPeliculasTipos() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_PELICULAS_TIPOS, null);
		objDatos.Disconnect();
		for (itfData fila : tabla) {
			clsPeliculaTipos peliculaTipos = new clsPeliculaTipos((int) fila.getData(PELICULA_TIPOS_PELICULA_FK),
					(int) fila.getData(PELICULA_TIPOS_TIPO_PELICULA_FK));
			listaPeliculasTipos.add(peliculaTipos);
		}
	}

	/**
	 * funcion que rellena el ArrayList de los oscars
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private void iniciarOscars() throws clsErrorSQLConsulta, clsErrorSQLConexion {
		ArrayList<itfData> tabla;

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_OSCARS, null);
		objDatos.Disconnect();
		for (itfData fila : tabla) {
			clsOscar oscar = new clsOscar((int) fila.getData(OSCAR_ID), (String) fila.getData(OSCAR_PELICULA),
					(String) fila.getData(OSCAR_DESCRIPCION));
			listaOscars.add(oscar);
		}
	}

	/**
	 * funcion que rellena el Arraylist de socio que tienen ticket comprado
	 * 
	 * @return lista de los socios con ticket
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private ArrayList<itfProperty> iniciarSocioConTicket() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;
		ArrayList<itfProperty> listaSociosConTicket = new ArrayList<itfProperty>();

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_SOCIOS_CON_TICKET, null);
		objDatos.Disconnect();
		for (itfData fila : tabla) {
			clsSocio aux = new clsSocio();
			aux.setApellidos((String) fila.getData(SOCIO_APELLIDOS));
			aux.setdni((String) fila.getData(SOCIO_DNI));
			aux.setEmail((String) fila.getData(SOCIO_EMAIL));
			if ((int) fila.getData(SOCIO_ESTADO) == 1) {
				aux.setEstado(true);
			} else {
				aux.setEstado(false);
			}
			String fecha = fila.getData(SOCIO_FECHA_NACIMIENTO).toString();
			aux.setFecha_nacimiento(transformarFecha(fecha));
			fecha = fila.getData(SOCIO_FECHA_TARIFA).toString();
			aux.setFecha_tarifa(transformarFecha(fecha));
			aux.setNombre((String) fila.getData(SOCIO_NOMBRE));
			aux.setPassword((String) fila.getData(SOCIO_PASSWORD));
			aux.setTarifa_FK((int) fila.getData(SOCIO_TARIFA_FK));
			aux.setTelefono((String) fila.getData(SOCIO_TELEFONO));
			aux.setUser_name((String) fila.getData(SOCIO_USER_NAME));
			aux.setIdSocio((int) fila.getData(SOCIO_ID));

			listaSociosConTicket.add(aux);
		}
		return listaSociosConTicket;
	}

	/**
	 * funcion que rellena el Arraylist de socio que tienen ticket comprado
	 * 
	 * @param socioId id del socio a buscar
	 * @return lista de las peliculas con ticket
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	private ArrayList<itfProperty> iniciarPeliculasConTicketSocio(int socioId)
			throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfData> tabla;
		ArrayList<itfProperty> listarPeliculasConTicketSocio = new ArrayList<itfProperty>();
		ArrayList<Object> parametros = new ArrayList<Object>();

		parametros.add(socioId);

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_PELICULAS_CON_TICKET_DE_SOCIO, parametros);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			clsPelicula pelicula = new clsPelicula();

			pelicula.setIdPelicula((int) fila.getData(PELICULA_ID));
			pelicula.setEmpleado_FK((int) fila.getData(PELICULA_EMPLEADO_FK));
			pelicula.setProveedor_FK((int) fila.getData(PELICULA_PROVEEDOR_FK));
			pelicula.setClasificacion((String) fila.getData(PELICULA_CLASIFICACION));
			pelicula.setDuracion((int) fila.getData(PELICULA_DURACION));
			pelicula.setIdiomas((String) fila.getData(PELICULA_IDIOMAS));

			pelicula.setSubtitulos(convertirIntBoolean((int) fila.getData(PELICULA_SUBTITULOS)));
			pelicula.setResolucion((String) fila.getData(PELICULA_RESOLUCION));
			pelicula.setTitulo((String) fila.getData(PELICULA_TITULO));
			pelicula.setPortada((String) fila.getData(PELICULA_PROTADA));
			pelicula.setDescripcion((String) fila.getData(PELICULA_DESCRIPCION));

			listarPeliculasConTicketSocio.add(pelicula);
		}
		return listarPeliculasConTicketSocio;
	}

	/**
	 * funcion que rellena el Arraylist de tickets de un socio y de una pelicula
	 * 
	 * @param socioId    id del socio a buscar
	 * @param peliculaId id de la pelicula
	 * @return lista de tickets de uns socios y de una pelicula
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> iniciarTicketSocioPelicula(int socioId, int peliculaId)
			throws clsErrorSQLConsulta, clsErrorSQLConexion {
		ArrayList<itfData> tabla;
		ArrayList<itfProperty> listaTicketsSocioPelicula = new ArrayList<itfProperty>();
		ArrayList<Object> parametros = new ArrayList<Object>();

		parametros.add(socioId);
		parametros.add(peliculaId);
		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_TICKET_SOCIO_PELICULA, parametros);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			LocalDate fechaLocalDate = transformarFecha(fila.getData(TICKET_FECHA_COMPRA).toString());
			clsTicket ticket = new clsTicket((int) fila.getData(TICKET_SOCIO_FK),
					(int) fila.getData(TICKET_PELICULA_FK), fechaLocalDate);
			listaTicketsSocioPelicula.add(ticket);
		}
		return listaTicketsSocioPelicula;
	}

	// -------------------LISTAS A MENU---------------------//

	/**
	 * Funcion que manda la lista de los usuario elegidos
	 * 
	 * @param tipo tipo usuario elegido
	 * @return lista de todos los usuarios del tipo elegido
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarUsuario(String tipo) throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfProperty> retorno;
		retorno = new ArrayList<itfProperty>();
		if (mUsuarios.isEmpty()) {
			iniciarUsuarios();
		}
		if (tipo.equalsIgnoreCase(USUARIO_SOCIO)) {
			for (String key : mUsuarios.keySet()) {
				if (mUsuarios.get(key) instanceof clsSocio) {
					retorno.add((itfProperty) mUsuarios.get(key));
				}
			}
		}
		if (tipo.equalsIgnoreCase(USUARIO_EMPLEADO)) {
			for (String key : mUsuarios.keySet()) {
				if (mUsuarios.get(key) instanceof clsEmpleado) {
					retorno.add((itfProperty) mUsuarios.get(key));
				}
			}
		}
		return retorno;
	}

	/**
	 * funcion que manda el Arraylist de socio que tienen ticket comprado a los
	 * menus
	 * 
	 * @return lista de los socios con ticket
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarSociosConTicket() throws clsErrorSQLConsulta, clsErrorSQLConexion {
		ArrayList<itfProperty> listaSociosConTicket = iniciarSocioConTicket();
		return listaSociosConTicket;
	}

	/**
	 * funcion que manda el Arraylist de peliculas que tienen ticket a los menus
	 * 
	 * @param socioId id del socio a buscar
	 * @return lista de las peliculas con ticket
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarPeliculasConTicketSocio(int socioId)
			throws clsErrorSQLConsulta, clsErrorSQLConexion {
		ArrayList<itfProperty> listarPeliculasConTicketSocio = iniciarPeliculasConTicketSocio(socioId);
		return listarPeliculasConTicketSocio;
	}

	/**
	 * funcion que manda el Arraylist de tickets de un socio y de una pelicula a los
	 * menus
	 * 
	 * @param socioId    id del socio a buscar
	 * @param peliculaId id de la pelicula
	 * @return lista de ticekts de uns socios y de una pelicula
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarTicketSocioPelicula(int socioId, int peliculaId)
			throws clsErrorSQLConsulta, clsErrorSQLConexion {
		ArrayList<itfProperty> listarTicketSocioPelicula = iniciarTicketSocioPelicula(socioId, peliculaId);
		return listarTicketSocioPelicula;
	}

	/**
	 * Esta funcion rellena un ArrayList itfproperty de lista tarifas
	 * 
	 * @return todas las tarifas
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarTarifa() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		// coge las tarifas de la base de datos
		ArrayList<itfProperty> tarifas = new ArrayList<itfProperty>();
		if (listaTarifas.isEmpty()) {
			iniciarTarifas();
		}
		for (clsTarifa aux : listaTarifas) {
			tarifas.add(aux);

		}
		return tarifas;
	}

	/**
	 * Funcion que manda la lista de tipo empleados
	 * 
	 * @return lista de todos los tipos de empleados
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarTipoEmpleado() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfProperty> retorno;
		retorno = new ArrayList<itfProperty>();
		if (listaTipoEmpleados.isEmpty()) {
			iniciarTipoEmpleados();
		}
		for (clsTipoEmpleado aux : listaTipoEmpleados) {
			retorno.add(aux);
		}
		return retorno;
	}

	/**
	 * Funcion que manda la lista de proveedores
	 * 
	 * @return lista de todos los proveedores
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarProveedor() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfProperty> retorno;
		retorno = new ArrayList<itfProperty>();
		if (listaProveedores.isEmpty()) {
			iniciarProveedores();
		}
		for (clsProveedor aux : listaProveedores) {
			retorno.add(aux);
		}
		return retorno;
	}

	/**
	 * Funcion que manda la lista de peliculas ordenadas por titulo
	 * 
	 * @return lista de todas las peliculas
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarPeliculaTitulo() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		if (listaPeliculas.isEmpty()) {
			iniciarPeliculas();
		}
		Collections.sort(listaPeliculas); // se llama implicitamente al compareTo de clsPelicula
		ArrayList<itfProperty> retorno;
		retorno = new ArrayList<itfProperty>();
		for (clsPelicula aux : listaPeliculas) {
			retorno.add(aux);
		}
		return retorno;
	}

	/**
	 * Funcion que manda la lista de peliculas ordenadas por proveedor
	 * 
	 * @return lista de todas las peliculas
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarPeliculaProveedor() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		if (listaPeliculas.isEmpty()) {
			iniciarPeliculas();
		}
		Collections.sort(listaPeliculas, new clsComparadorProveedor_FK()); // se llama implicitamente al compareTo de
																			// clsPelicula
		ArrayList<itfProperty> retorno;
		retorno = new ArrayList<itfProperty>();
		for (clsPelicula aux : listaPeliculas) {
			retorno.add(aux);
		}
		return retorno;
	}

	/**
	 * Funcion que manda la lista de tipo peliculas
	 * 
	 * @return lista de todos los tipos de peliculas
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarTipoPelicula() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfProperty> retorno;
		retorno = new ArrayList<itfProperty>();
		if (listaTipoPeliculas.isEmpty()) {
			iniciarTipoPeliculas();
		}
		for (clsTipoPelicula aux : listaTipoPeliculas) {
			retorno.add(aux);
		}
		return retorno;
	}

	/**
	 * Funcion que manda la lista de la relacion entre peliculas y tipo peliculas
	 * 
	 * @return lista de todas las relaciones entre peicluas y tipo de peliculas
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarPeliculaTipos() throws clsErrorSQLConsulta, clsErrorSQLConexion {
		ArrayList<itfProperty> retorno;
		retorno = new ArrayList<itfProperty>();
		if (listaPeliculasTipos.isEmpty()) {
			iniciarPeliculasTipos();
		}
		for (clsPeliculaTipos aux : listaPeliculasTipos) {
			retorno.add(aux);
		}
		return retorno;
	}

	/**
	 * Funcion que manda la lista de consultas
	 * 
	 * @return lista de todas las consultas
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarConsulta() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfProperty> retorno;
		retorno = new ArrayList<itfProperty>();
		if (listaConsultas.isEmpty()) {
			iniciarConsultas();
		}
		for (clsConsulta aux : listaConsultas) {
			retorno.add(aux);
		}
		return retorno;
	}

	/**
	 * Funcion que manda la lista de tickets
	 * 
	 * @return lista de todos los ticket
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarTicket() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfProperty> retorno;
		retorno = new ArrayList<itfProperty>();
		if (listaTickets.isEmpty()) {
			iniciarTickets();
		}
		for (clsTicket aux : listaTickets) {
			retorno.add(aux);
		}
		return retorno;
	}

	/**
	 * Funcion que manda la lista de oscars
	 * 
	 * @return lista de todos los oscars
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> listarOscars() throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfProperty> retorno;
		retorno = new ArrayList<itfProperty>();
		if (listaOscars.isEmpty()) {
			iniciarOscars();
		} else {
			listaOscars.clear();
			iniciarOscars();
		}
		for (clsOscar aux : listaOscars) {
			retorno.add(aux);
		}
		return retorno;
	}

	/**
	 * Funcion que devuelve la relacion de pelicula y tipo pelicula
	 * 
	 * @param idPelicula id de la pelicula
	 * @return arrayList de la relacion entre pelicula y tipo pelicula
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> devolverPeliculaTipos(int idPelicula)
			throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfProperty> tiposPelicula = new ArrayList<itfProperty>();
		if (listaPeliculasTipos.isEmpty()) {
			iniciarPeliculasTipos();
		}
		for (clsPeliculaTipos aux : listaPeliculasTipos) {
			if (aux.getPelicula_FK() == idPelicula) {
				tiposPelicula.add(aux);
			}
		}
		return tiposPelicula;
	}

	/**
	 * funcion que devuelve los tipos de pelicula de una pelicula
	 * 
	 * @param listaTiposEstaPelicula lista de tipos de pelicula a buscar
	 * @return lista de peliculas enncontradas
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public ArrayList<itfProperty> devolverTipoPelicula(ArrayList<itfProperty> listaTiposEstaPelicula)
			throws clsErrorSQLConexion, clsErrorSQLConsulta {
		ArrayList<itfProperty> listaTipos = new ArrayList<itfProperty>();
		if (listaTipoPeliculas.isEmpty()) {
			iniciarTipoPeliculas();
		}
		for (clsTipoPelicula tipo : listaTipoPeliculas) {
			for (itfProperty aux : listaTiposEstaPelicula) {
				if ((int) aux.getProperty(PELICULA_TIPOS_COL_TIPO_PELICULA_FK) == tipo.getIdTipoPelicula()) {
					listaTipos.add(tipo);
				}
			}
		}
		return listaTipos;
	}

	// -------------DEVOLVER UN OBJETO----------------//

	/**
	 * Busqueda a hashMap de usuarios
	 * 
	 * @param usuario el username del usuario
	 * @return itfProperty del objeto empleado o socio
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public itfProperty devolverUsuario(String usuario) throws clsErrorSQLConexion, clsErrorSQLConsulta {
		itfProperty retorno = null;
		if (mUsuarios.isEmpty()) {
			iniciarUsuarios();
		}
		if (mUsuarios.containsKey(usuario)) {
			retorno = (itfProperty) mUsuarios.get(usuario);
		}
		return retorno;
	}

	/**
	 * Funcion que devuelve una consulta desde la base de datos
	 * 
	 * @param empleado empelado de la consulta
	 * @param socio    socio de la consulta
	 * @param fecha    fecha de la consulta
	 * @return itfProperty de la consulta a buscar
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public itfProperty devolverConsulta(int empleado, int socio, String fecha)
			throws clsErrorSQLConexion, clsErrorSQLConsulta {
		LocalDate fechaLocalDate = transformarFecha(fecha);

		itfProperty consultaBuscar = null;
		if (listaConsultas.isEmpty()) {
			iniciarConsultas();
		}
		for (clsConsulta aux : listaConsultas) {
			if (aux.getEmpleado_FK() == empleado && aux.getSocio_FK() == socio
					&& aux.getFecha().equals(fechaLocalDate)) {
				consultaBuscar = aux;
			}
		}
		return consultaBuscar;
	}

	/**
	 * Funcion que devuelve un itfProperty del tipoEmpleado
	 * 
	 * @param idTipoEmpleado id del tipo empleado
	 * @return itProperty de TipoEmpleado
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public itfProperty devolverTipoEmpleado(int idTipoEmpleado) throws clsErrorSQLConexion, clsErrorSQLConsulta {
		itfProperty retorno = null;
		if (listaTipoEmpleados.isEmpty()) {
			iniciarTipoEmpleados();
		}
		for (clsTipoEmpleado aux : listaTipoEmpleados) {
			if (aux.getIdTipoEmpleado() == idTipoEmpleado) {
				retorno = aux;
			}
		}
		return retorno;
	}

	/**
	 * funcion que devuelve un proveedor en base a su email
	 * 
	 * @param email email del proveedor
	 * @return proveedor encontrado
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public itfProperty devolverProveedor(String email) throws clsErrorSQLConexion, clsErrorSQLConsulta {
		itfProperty proveedorItf = null;
		ArrayList<itfData> tabla = new ArrayList<itfData>();

		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(email);

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_PROVEEDOR_EMAIL, parametros);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			clsProveedor proveedor = new clsProveedor((int) fila.getData(PROVEEDOR_ID),
					(String) fila.getData(PROVEEDOR_NOMBRE), (String) fila.getData(PROVEEDOR_EMAIL),
					(String) fila.getData(PROVEEDOR_TELEFONO));
			proveedorItf = proveedor;
		}
		return proveedorItf;
	}

	/**
	 * funcion que devuelve un proveedor en base a su nombre
	 * 
	 * @param nombre nombre del proveedor
	 * @return proveedor encontrado
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public itfProperty devolverProveedorNombre(String nombre) throws clsErrorSQLConexion, clsErrorSQLConsulta {
		itfProperty proveedorItf = null;
		ArrayList<itfData> tabla = new ArrayList<itfData>();

		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(nombre);

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_PROVEEDOR_NOMBRE, parametros);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			clsProveedor proveedor = new clsProveedor((int) fila.getData(PROVEEDOR_ID),
					(String) fila.getData(PROVEEDOR_NOMBRE), (String) fila.getData(PROVEEDOR_EMAIL),
					(String) fila.getData(PROVEEDOR_TELEFONO));
			proveedorItf = proveedor;
		}
		return proveedorItf;
	}

	/**
	 * funcion que devuelve un proveedor en base a su id
	 * 
	 * @param idProveedor del proveedor
	 * @return proveedor encontrado
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public itfProperty devolverProveedor(int idProveedor) throws clsErrorSQLConexion, clsErrorSQLConsulta {
		itfProperty proveedorItf = null;
		ArrayList<itfData> tabla = new ArrayList<itfData>();

		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(idProveedor);

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_PROVEEDOR_ID, parametros);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			clsProveedor proveedor = new clsProveedor((int) fila.getData(PROVEEDOR_ID),
					(String) fila.getData(PROVEEDOR_NOMBRE), (String) fila.getData(PROVEEDOR_EMAIL),
					(String) fila.getData(PROVEEDOR_TELEFONO));
			proveedorItf = proveedor;
		}
		return proveedorItf;
	}

	/**
	 * funcion que devuleve un proveedor de una pelicula en base a el titulo y
	 * nombre del proveedor
	 * 
	 * @param titulo          titulo de la pelicula
	 * @param nombreProveedor nombre del proveedor
	 * @return itfProperty de proveedor encontrado
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public itfProperty devolverProveedorDePelicula(String titulo, String nombreProveedor)
			throws clsErrorSQLConsulta, clsErrorSQLConexion {
		itfProperty proveedorItf = null;
		ArrayList<itfData> tabla = new ArrayList<itfData>();

		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(nombreProveedor);
		parametros.add(titulo);

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_PROVEEDOR_NOMBRE_PELICULA_TITULO, parametros);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			clsProveedor proveedor = new clsProveedor((int) fila.getData(PROVEEDOR_ID),
					(String) fila.getData(PROVEEDOR_NOMBRE), (String) fila.getData(PROVEEDOR_EMAIL),
					(String) fila.getData(PROVEEDOR_TELEFONO));
			proveedorItf = proveedor;
		}
		return proveedorItf;
	}

	/**
	 * Funcion que devuelve una pelicula buscado por titulo y proveedor
	 * 
	 * @param titulo       titulo de la pelicula
	 * @param proveedor_FK id del proveedor de la pelicula
	 * @return itfProperty de la pelicula encontrada
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public itfProperty devolverPeliculaTituloProveedor(String titulo, int proveedor_FK)
			throws clsErrorSQLConexion, clsErrorSQLConsulta {
		itfProperty pelicula = null;
		if (listaPeliculas.isEmpty()) {
			iniciarPeliculas();
		}
		for (clsPelicula aux : listaPeliculas) {
			if (aux.getProveedor_FK() == proveedor_FK && aux.getTitulo().equalsIgnoreCase(titulo)) {
				pelicula = aux;
			}
		}
		return pelicula;
	}

	/**
	 * Funcion que devuelve una pelicula buscado por id
	 * 
	 * @param idPelicula id de la pelicula
	 * @return itfProperty de la pelicula encontrada
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public itfProperty devolverPelicula(int idPelicula) throws clsErrorSQLConexion, clsErrorSQLConsulta {
		itfProperty peliculaBuscar = null;
		ArrayList<itfData> tabla = new ArrayList<itfData>();

		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(idPelicula);

		objDatos.Connect();
		tabla = objDatos.Consulta(SQL_SELECT_PELICULA_ID, parametros);
		objDatos.Disconnect();

		for (itfData fila : tabla) {
			clsPelicula pelicula = new clsPelicula();

			pelicula.setIdPelicula((int) fila.getData(PELICULA_ID));
			pelicula.setEmpleado_FK((int) fila.getData(PELICULA_EMPLEADO_FK));
			pelicula.setProveedor_FK((int) fila.getData(PELICULA_PROVEEDOR_FK));
			pelicula.setClasificacion((String) fila.getData(PELICULA_CLASIFICACION));
			pelicula.setDuracion((int) fila.getData(PELICULA_DURACION));
			pelicula.setIdiomas((String) fila.getData(PELICULA_IDIOMAS));

			pelicula.setSubtitulos(convertirIntBoolean((int) fila.getData(PELICULA_SUBTITULOS)));
			pelicula.setResolucion((String) fila.getData(PELICULA_RESOLUCION));
			pelicula.setTitulo((String) fila.getData(PELICULA_TITULO));
			pelicula.setPortada((String) fila.getData(PELICULA_PROTADA));
			pelicula.setDescripcion((String) fila.getData(PELICULA_DESCRIPCION));

			peliculaBuscar = pelicula;
		}
		return peliculaBuscar;
	}

	// --------------BORRAR OBJETO-----------------//

	/**
	 * Funcion que borra un ticket de la base de datos
	 * 
	 * @param socio    id del socio
	 * @param pelicula id de la pelicula
	 * @param fecha    fehca de compra de la pelicula
	 * @return cantidad de filas afectadas
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public boolean borrarTicket(int socio, int pelicula, String fecha)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLConsulta, clsErrorSQLInsertDeleteUpdate {
		LocalDate fechaLocalDate = transformarFecha(fecha);
		boolean retorno = false;
		ArrayList<Object> parametros;
		parametros = new ArrayList<Object>();
		parametros.add(socio);
		parametros.add(pelicula);
		parametros.add(fechaLocalDate);
		objDatos.Connect();
		int id = objDatos.InsertDeleteUpdate(SQL_DELETE_TICKET_SOCIO_PELICULA_FECHA, parametros);
		objDatos.Disconnect();
		listaTickets.clear();
		iniciarTickets();
		if (id != 0) {
			retorno = true;
		}
		return retorno;
	}

	/**
	 * Funcion que borra una consulta de la base de datos
	 * 
	 * @param empleado id del empleado de la consulta a borrar
	 * @param socio    id del socio de la conuslta a borrar
	 * @param fecha    fecha de la consulta a borrar
	 * @return confirmacion de la consulta
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public boolean borrarConsulta(int empleado, int socio, String fecha)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLConsulta, clsErrorSQLInsertDeleteUpdate {
		LocalDate fechaLocalDate = transformarFecha(fecha);
		boolean retorno;
		retorno = false;
		ArrayList<Object> parametros;

		parametros = new ArrayList<Object>();

		parametros.add(socio);
		parametros.add(empleado);
		parametros.add(fechaLocalDate);
		objDatos.Connect();
		int id = objDatos.InsertDeleteUpdate(SQL_DELETE_CONSULTA_EMPLEADO_SOCIO_FECHA, parametros);
		objDatos.Disconnect();
		listaConsultas.clear();
		iniciarConsultas();

		if (id != 0)
			retorno = true;

		return retorno;

	}

	/**
	 * funcion que borra todos la relacion entre todos los tipos de pelicula y una
	 * pelicula
	 * 
	 * @param idPelicula id de la pelicula a borrar
	 * @return cantidad de filas afectadas
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 */
	public int borrarPeliculaTipos(int idPelicula)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {
		ArrayList<Object> parametros = new ArrayList<Object>();

		parametros.add(idPelicula);
		objDatos.Connect();
		int retorno = objDatos.InsertDeleteUpdate(SQL_DELETE_PELICULA_TIPOS_PELICULA_ID, parametros);
		objDatos.Disconnect();
		listaPeliculasTipos.clear();
		iniciarPeliculasTipos();
		return retorno;
	}

	/**
	 * funcion que borra los ticket con el id de la pelicula
	 * 
	 * @param idPelicula id de la pelicula
	 * @return cantidad de filas afectadas
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public int borrarTicket(int idPelicula)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate {
		ArrayList<Object> parametros = new ArrayList<Object>();

		parametros.add(idPelicula);
		objDatos.Connect();
		int retorno = objDatos.InsertDeleteUpdate(SQL_DELETE_TICKET_PELICULA_ID, parametros);
		objDatos.Disconnect();

		for (clsTicket ticket : listaTickets) {
			if (ticket.getPelicula_FK() == idPelicula) {
				listaTickets.remove(ticket);
			}
		}
		return retorno;
	}

	/**
	 * funcion que borra pelicula con la id de pelicula
	 * 
	 * @param idPelicula id de la pelicula
	 * @return cantidad de filas afectadas
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 */
	public int borrarPelicula(int idPelicula)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {
		ArrayList<Object> parametros = new ArrayList<Object>();

		parametros.add(idPelicula);
		objDatos.Connect();
		int retorno = objDatos.InsertDeleteUpdate(SQL_DELETE_PELICULA, parametros);
		objDatos.Disconnect();
		listaPeliculas.clear();
		iniciarPeliculas();
		return retorno;

	}

	/**
	 * funcion que borra un proveedor de la base de datos
	 * 
	 * @param idProveedor id del proveedor a borrar
	 * @return resultado de la sentencia realizada
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 */
	public int borrarProveedor(int idProveedor)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(idProveedor);
		objDatos.Connect();
		int resultado = objDatos.InsertDeleteUpdate(SQL_DELETE_PROVEEDOR_ID_PROVEEDOR, parametros);
		objDatos.Disconnect();
		listaProveedores.clear();
		iniciarProveedores();
		return resultado;
	}

	/**
	 * Funcion que da de baja a un socio de la base de datos
	 * 
	 * @param idSocio id del socio a borrar
	 * @param estado  estado del socio
	 * @return confirmacion de la eliminacion en bbdd
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public boolean cambiarEstadoSocio(int idSocio, boolean estado)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLConsulta, clsErrorSQLInsertDeleteUpdate {
		boolean retorno = false;
		int estadoInt = 0;
		ArrayList<Object> parametros = new ArrayList<Object>();

		if (estado == true) {
			estadoInt = 0;

		} else {
			estadoInt = 1;
		}
		parametros.add(estadoInt); // en formato int mando el estado VALOR A MODIFICAR
		parametros.add(idSocio); // en formato int mando el id del socio CONDICION
		objDatos.Connect();
		int id = objDatos.InsertDeleteUpdate(SQL_UPDATE_SOCIO_ESTADO, parametros);
		objDatos.Disconnect();

		mUsuarios.clear(); // borro la lista de usuarios
		iniciarUsuarios(); // inicializo la lista de usuarios

		if (id != 0)
			retorno = true;

		return retorno;
	}

	/**
	 * funcion que borra un empleado de la base de datos y en pelicula deja el campo
	 * a null
	 * 
	 * @param idEmpleado id del empleado a borrar
	 * @return cantidad de filas afectadas
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public int borrarEmpleado(int idEmpleado)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(idEmpleado);
		objDatos.Connect();
		int resultado = objDatos.InsertDeleteUpdate(SQL_DELETE_EMPLEADO_ID_EMPLEADO, parametros);
		objDatos.Disconnect();
		mUsuarios.clear();
		iniciarUsuarios();
		return resultado;

	}
	// ------------------INSERTAR-------------------//

	/**
	 * Funcion que crea una consulta nueva en la base de datos
	 * 
	 * @param empleado   id del empleado
	 * @param socio      id del socio
	 * @param fecha      fecha de creacion de la consulta
	 * @param incidencia la incidencia
	 * @param resultado  si se a resuelto o no
	 * @return resultado de la consulta
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public int insertarConsulta(int empleado, int socio, String fecha, String incidencia, int resultado)
			throws clsErrorSQLConexion, clsErrorSQLConsulta, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate {
		ArrayList<Object> parametros;

		LocalDate fechaLocalDate = transformarFecha(fecha);
		boolean resultadoBool;
		if (resultado == 1) {
			resultadoBool = true;
		} else {
			resultadoBool = false;
		}
		clsConsulta consulta = new clsConsulta(socio, empleado, fechaLocalDate, incidencia, resultadoBool);
		if (listaConsultas.isEmpty()) {
			iniciarConsultas();
			listaConsultas.add(consulta);
		}
		parametros = new ArrayList<Object>();
		parametros.add(socio);
		parametros.add(empleado);
		parametros.add(fechaLocalDate);
		parametros.add(incidencia);
		parametros.add(resultado);
		objDatos.Connect();
		int retorno = objDatos.InsertDeleteUpdate(SQL_INSERT_CONSULTA, parametros);
		objDatos.Disconnect();
		return retorno;

	}

	/**
	 * Funcion que crea el ticket de compra del socio sobre una pelicula
	 * 
	 * @param socioID    id del socio que ha comprado la pelicula
	 * @param peliculaID id de la pelicula que ha sido comprada
	 * @return cantidad de filas afectadas en la base de datos
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public int insertarTicket(int socioID, int peliculaID)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate {
		// creo el objeto
		clsTicket ticket = new clsTicket();

		ticket.setSocio_FK(socioID);
		ticket.setSocio_FK(peliculaID);
		LocalDate fechaHoy = LocalDate.now();
		ticket.setFecha_compra(fechaHoy);

		// lo inserto al arraylist de tickets
		listaTickets.add(ticket);

		// preparo mi arraylist de Object para hacer la insercion en las columnas
		// especificas y en orden
		ArrayList<Object> parametros = new ArrayList<Object>();

		parametros.add(socioID);
		parametros.add(peliculaID);
		parametros.add(fechaHoy);

		objDatos.Connect();
		int retorno = objDatos.InsertDeleteUpdate(SQL_INSERT_TICKET, parametros);
		objDatos.Disconnect();
		return retorno;

	}

	/**
	 * funcion que sirve para insertar un socio nuevo en la base de datos
	 * 
	 * @param tarifaId        id de la tarifa
	 * @param usuario         nombre del usuario del socio
	 * @param password        password del socio
	 * @param nombre          nombre del socio
	 * @param apellidos       apellidos del socios
	 * @param fechaNacimiento fehca de nacimiento del socio
	 * @param email           email del socio
	 * @param telefono        telefono del socio
	 * @param dni             dni del socio
	 * @return cantidad de filas afectadas en la consulta
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 */
	public int insertarSocio(int tarifaId, String usuario, String password, String nombre, String apellidos,
			String fechaNacimiento, String email, String telefono, String dni)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {
		LocalDate fechaDeNacimientoDate = transformarFecha(fechaNacimiento);
		LocalDate fechaHoy = LocalDate.now();

		ArrayList<Object> parametros = new ArrayList<Object>();

		parametros.add(tarifaId);
		parametros.add(usuario);
		parametros.add(password);
		parametros.add(nombre);
		parametros.add(apellidos);
		parametros.add(fechaDeNacimientoDate);
		parametros.add(email);
		parametros.add(telefono);
		parametros.add(dni);
		parametros.add(1);
		parametros.add(fechaHoy);

		objDatos.Connect();
		int retorno = objDatos.InsertDeleteUpdate(SQL_INSERT_SOCIO, parametros);
		objDatos.Disconnect();

		mUsuarios.clear(); // borro la lista de usuarios
		iniciarUsuarios(); // inicializo la lista de usuarios
		return retorno;

	}

	/**
	 * funcion que inserta el tipo de pelicula en la pelicula
	 * 
	 * @param tipoPelicula id de tipo de pelicula
	 * @param idPelicula   id de la pelicula
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public void insertarPeliculaTipos(int tipoPelicula, int idPelicula)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLConsulta, clsErrorSQLInsertDeleteUpdate {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(idPelicula);
		parametros.add(tipoPelicula);

		objDatos.Connect();
		objDatos.InsertDeleteUpdate(SQL_INSERT_PELICULA_TIPOS, parametros);
		objDatos.Disconnect();
		listaPeliculasTipos.clear();
		iniciarPeliculasTipos();
	}

	/**
	 * funcion que inserta un proveedor en la base de datos
	 * 
	 * @param nombre   nombre del proveedor
	 * @param email    email del proveedor
	 * @param telefono telefono del proveedor
	 * 
	 * @return resultado de la consulta
	 * 
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de la insert, delete o update
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 */
	public int insertarProveedor(String nombre, String email, String telefono)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {
		ArrayList<Object> parametros = new ArrayList<Object>();

		parametros.add(nombre);
		parametros.add(email);
		parametros.add(telefono);

		objDatos.Connect();
		int resultado = objDatos.InsertDeleteUpdate(SQL_INSERT_PROVEEDOR, parametros);
		objDatos.Disconnect();

		listaProveedores.clear();
		iniciarProveedores();

		return resultado;
	}

	/**
	 * funcion que inserta un empleado en la base de datos
	 * 
	 * @param tipoEmpleado    tipo del empleado del empleado
	 * @param usuario         nombre de usuario del empleado
	 * @param password        password de empleado
	 * @param nombre          nombre del empelado
	 * @param apellido        apellido del empleado
	 * @param fechaNacimiento fecha nacimiento del empleado
	 * @param dni             dni del empleado
	 * @param telf            telefono del empelado
	 * @param email           email del empleado
	 * @return cantidad de filas afectadas en la consulta
	 * @throws clsErrorSQLConexion           Excepcion de la conexion
	 * @throws clsErrorSQLInsertDeleteUpdate Excepcion del Insertdeleteupdate
	 * @throws clsErrorSQLParametros         Excepcion de los parametros
	 * @throws clsErrorSQLConsulta           Excepcion de la consulta
	 */
	public int insertarEmpleado(int tipoEmpleado, String usuario, String password, String nombre, String apellido,
			String fechaNacimiento, String dni, String telf, String email)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {

		LocalDate fechaNacimientoDate = transformarFecha(fechaNacimiento);

		ArrayList<Object> parametros = new ArrayList<Object>();

		parametros.add(tipoEmpleado);
		parametros.add(usuario);
		parametros.add(password);
		parametros.add(nombre);
		parametros.add(apellido);
		parametros.add(fechaNacimientoDate);
		parametros.add(dni);
		parametros.add(telf);
		parametros.add(email);

		objDatos.Connect();

		int retorno = 0;

		retorno = objDatos.InsertDeleteUpdate(SQL_INSERT_EMPLEADO, parametros);

		objDatos.Disconnect();
		mUsuarios.clear();
		iniciarUsuarios();
		return retorno;
	}

	/**
	 * funcion que inserta una pelicula nueva en la base de datos
	 * 
	 * @param empleado_FK   id del empleado
	 * @param proveedor_FK  id del proveedor
	 * @param titulo        titulo de la pelicula
	 * @param descripcion   descripcion de la pelicula
	 * @param duracion      duracion de la pelicula
	 * @param clasificacion clasificacion de la pelicula
	 * @param idiomas       idiomas de la pelicula
	 * @param subtitulos    si tiene subtitulos la pelicula o no
	 * @param resolucion    resolucion de la pelicula
	 * @param portada       direccion de la portada de la pelicula
	 * @return cantidad de filas afectadas
	 * @throws clsErrorSQLConexion           Excepcion de la conexion
	 * @throws clsErrorSQLInsertDeleteUpdate Excepcion del insertdeleteupdate
	 * @throws clsErrorSQLParametros         Excepcion de parametros
	 * @throws clsErrorSQLConsulta           Excepcion de consulta
	 */
	public int insertarPelicula(int empleado_FK, int proveedor_FK, String titulo, String descripcion, int duracion,
			String clasificacion, String idiomas, boolean subtitulos, String resolucion, String portada)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(empleado_FK);
		parametros.add(proveedor_FK);
		parametros.add(clasificacion);
		parametros.add(duracion);
		parametros.add(idiomas);
		parametros.add(subtitulos);
		parametros.add(resolucion);
		parametros.add(titulo);
		parametros.add(portada);
		parametros.add(descripcion);

		objDatos.Connect();

		int retorno = 0;

		retorno = objDatos.InsertDeleteUpdate(SQL_INSERT_PELICULA, parametros);

		objDatos.Disconnect();
		listaPeliculas.clear();

		iniciarPeliculas();

		return retorno;
	}

	/**
	 * Funcion que usara el hilo para crear oscars con el tiempo
	 * 
	 * @param pelicula    titulo de la pelicula (no son los de la BBDD)
	 * @param descripcion descripcion de la pelicual (no son los de la BBDD)
	 * @throws clsErrorSQLConexion           Excepcion de la conexion
	 * @throws clsErrorSQLInsertDeleteUpdate Excepcion del insertdeleteupdate
	 * @throws clsErrorSQLParametros         Excepcion de parametros
	 */
	public void insertarOscar(String pelicula, String descripcion)
			throws clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConexion {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(pelicula);
		parametros.add(descripcion);

		objDatos.Connect();
		objDatos.InsertDeleteUpdate(SQL_INSERT_OSCAR, parametros);
		objDatos.Disconnect();
	}

	// ----------------EDITAR-------------------//

	/**
	 * Funcion que edita una consulta de la base de datos
	 * 
	 * @param empleadoNuevo id del empleado nuevo
	 * @param socioNuevo    id del socio nuevo
	 * @param fechaNuevo    fecha de creacion de la consulta nueva
	 * @param incidencia    la incidencia
	 * @param resultado     si se a resuelto o no
	 * @param empleado      id del empleado de la consulta a editar
	 * @param socio         id del socio de la consulta a editar
	 * @param fecha         fecha de la consulta a editar
	 * @return cantidad de filas afectadas en esta consulta
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public int editarConsulta(int empleadoNuevo, int socioNuevo, String fechaNuevo, String incidencia, int resultado,
			int empleado, int socio, LocalDate fecha)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLConsulta, clsErrorSQLInsertDeleteUpdate {
		ArrayList<Object> parametros;

		LocalDate fechaLocalDate = transformarFecha(fechaNuevo);

		parametros = new ArrayList<Object>();
		parametros.add(socioNuevo);
		parametros.add(empleadoNuevo);
		parametros.add(fechaLocalDate);
		parametros.add(incidencia);
		parametros.add(resultado);
		parametros.add(socio);
		parametros.add(empleado);
		parametros.add(fecha);
		objDatos.Connect();
		int retorno = objDatos.InsertDeleteUpdate(SQL_UPDATE_CONSULTA, parametros);
		objDatos.Disconnect();
		listaConsultas.clear();
		iniciarConsultas();
		return retorno;

	}

	/**
	 * Busca el id de la tarifa que nos trae
	 * 
	 * @param tipoTarifa    nombre de la tarifa
	 * @param usuarioActual los datos del usuario que acaba de iniciar sesión
	 * @return cantidad de filas afectadas en la update
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public int buscarIdTarifa(String tipoTarifa, itfProperty usuarioActual)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLConsulta, clsErrorSQLInsertDeleteUpdate {
		int idTarifa = -1;
		int comprobacion = 0;
		for (clsTarifa cadaTarifa : listaTarifas) {
			if (cadaTarifa.getTipo().equalsIgnoreCase(tipoTarifa)) {
				idTarifa = cadaTarifa.getIdTarifa();
				comprobacion = editarSocioTarifa(idTarifa, usuarioActual);
			}
		}
		return comprobacion;
	}

	/**
	 * Edita un socio cambiando tarifa a la BD
	 * 
	 * @param idTarifa      identificador de la tarifa
	 * @param usuarioActual los datos del usuario que acaba de iniciar sesión
	 * @return cantidad de filas afectadas en la update
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public int editarSocioTarifa(int idTarifa, itfProperty usuarioActual)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLConsulta, clsErrorSQLInsertDeleteUpdate {
		// editar socio de la base de datos
		ArrayList<Object> parametrosSocio = new ArrayList<Object>();
		LocalDate modificacionTarifa = LocalDate.now();

		parametrosSocio.add(idTarifa);
		parametrosSocio.add(usuarioActual.getProperty(USUARIO_COL_USER_NAME));
		parametrosSocio.add(usuarioActual.getProperty(USUARIO_COL_PASSWORD));
		parametrosSocio.add(usuarioActual.getProperty(USUARIO_COL_NOMBRE));
		parametrosSocio.add(usuarioActual.getProperty(USUARIO_COL_APELLIDOS));
		parametrosSocio.add(usuarioActual.getProperty(USUARIO_COL_FECHA_NACIMIENTO));
		parametrosSocio.add(usuarioActual.getProperty(USUARIO_COL_EMAIL));
		parametrosSocio.add(usuarioActual.getProperty(USUARIO_COL_TELEFONO));
		parametrosSocio.add(usuarioActual.getProperty(USUARIO_COL_DNI));
		if ((boolean) usuarioActual.getProperty(SOCIO_COL_ESTADO)) {
			parametrosSocio.add(1);
		} else {
			parametrosSocio.add(0);
		}
		parametrosSocio.add(modificacionTarifa);
		//parametrosSocio.add(usuarioActual.getProperty(SOCIO_COL_ID));
		parametrosSocio.add(usuarioActual.getProperty(SOCIO_COL_ID));
		objDatos.Connect();
		int comprobacion = objDatos.InsertDeleteUpdate(SQL_UPDATE_SOCIO, parametrosSocio);
		objDatos.Disconnect();
		mUsuarios.clear(); // borro la lista de usuarios
		iniciarUsuarios(); // inicializo la lista de usuarios
		return comprobacion;
	}

	/**
	 * Funcion que edita un socio de la base de datos
	 * 
	 * @param nombreUsuario   nombre del usuario en la aplicacion
	 * @param password        password del socio
	 * @param nombre          nombre del socio
	 * @param apellidos       apellidos del socio
	 * @param fechaNacimiento fecha nacimiento del socio
	 * @param email           email del socio
	 * @param telefono        telefono del socio
	 * @param dni             dni del socio
	 * @param idSocio         id del socio
	 * @param tarifaFK        id de la tarifa del socio
	 * @param estado          estado del socio
	 * @param fechaTarifa     fecha de la consulta a editar
	 * @return cantidad de filas afectadas en esta consulta
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public int editarSocio(String nombreUsuario, String password, String nombre, String apellidos,
			String fechaNacimiento, String email, String telefono, String dni, int idSocio, int tarifaFK,
			boolean estado, String fechaTarifa)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLConsulta, clsErrorSQLInsertDeleteUpdate {

		ArrayList<Object> parametros;

		LocalDate fechaNacimientoLocalDate = transformarFecha(fechaNacimiento);
		LocalDate fechaTarifaLocalDate = transformarFecha(fechaTarifa);
		int estadoBoolean = 1;
		if (!estado) {
			estadoBoolean = 0;
		}

		parametros = new ArrayList<Object>();

		parametros.add(tarifaFK);
		parametros.add(nombreUsuario);
		parametros.add(password);
		parametros.add(nombre);
		parametros.add(apellidos);
		parametros.add(fechaNacimientoLocalDate);
		parametros.add(email);
		parametros.add(telefono);
		parametros.add(dni);
		parametros.add(estadoBoolean);
		parametros.add(fechaTarifaLocalDate);
		parametros.add(idSocio);

		objDatos.Connect();
		int retorno = objDatos.InsertDeleteUpdate(SQL_UPDATE_SOCIO, parametros);
		objDatos.Disconnect();
		mUsuarios.clear(); // borro la lista de usuarios
		iniciarUsuarios(); // inicializo la lista de usuarios
		return retorno;

	}

	/**
	 * funcion que edita un empleado
	 * 
	 * @param nombreUsuario   nombre de usuario del empleado
	 * @param password        password del usuario empleado
	 * @param nombre          nombre del empleado
	 * @param apellidos       apellidos del empleado
	 * @param fechaNacimiento fecha nacimiento del empleado
	 * @param dni             dni del empleado
	 * @param telefono        telefono del empleado
	 * @param email           email del empleado
	 * @param idEmpleado      id del empleado
	 * @param tipoEmpleado_FK tipo empleado del empleado
	 * @return cantidad de filas afectadas
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public int editarEmpleado(String nombreUsuario, String password, String nombre, String apellidos,
			String fechaNacimiento, String dni, String telefono, String email, int idEmpleado, int tipoEmpleado_FK)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {
		ArrayList<Object> parametros;

		LocalDate fechaNacimientoLocalDate = transformarFecha(fechaNacimiento);

		parametros = new ArrayList<Object>();

		parametros.add(tipoEmpleado_FK);
		parametros.add(nombreUsuario);
		parametros.add(password);
		parametros.add(nombre);
		parametros.add(apellidos);
		parametros.add(fechaNacimientoLocalDate);
		parametros.add(dni);
		parametros.add(telefono);
		parametros.add(email);
		parametros.add(idEmpleado);
		parametros.add(idEmpleado);

		objDatos.Connect();
		int retorno = objDatos.InsertDeleteUpdate(SQL_UPDATE_EMPLEADO, parametros);
		objDatos.Disconnect();
		mUsuarios.clear(); // borro la lista de usuarios
		iniciarUsuarios(); // inicializo la lista de usuarios
		return retorno;

	}

	/**
	 * funcion que inserta una pelicula nueva en la base de datos
	 * 
	 * @param empleado_FK   id del empleado
	 * @param proveedor_FK  id del proveedor
	 * @param titulo        titulo de la pelicula
	 * @param descripcion   descripcion de la pelicula
	 * @param portada       direccion de la portada de la pelicula
	 * @param resolucion    resolucion de la pelicula
	 * @param idiomas       idiomas de la pelicula
	 * @param subtitulos    si tiene subtitulos la pelicula o no
	 * @param duracion      duracion de la pelicula
	 * @param clasificacion clasificacion de la pelicula
	 * @param idProveedor   id de la pelicula a editar
	 * @return cantidad de filas afectadas
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public int editarPelicula(int empleado_FK, int proveedor_FK, String titulo, String descripcion, String portada,
			String resolucion, String idiomas, boolean subtitulos, int duracion, String clasificacion, int idProveedor)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLConsulta, clsErrorSQLInsertDeleteUpdate {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(empleado_FK);
		parametros.add(proveedor_FK);
		parametros.add(clasificacion);
		parametros.add(duracion);
		parametros.add(idiomas);
		parametros.add(subtitulos);
		parametros.add(resolucion);
		parametros.add(titulo);
		parametros.add(portada);
		parametros.add(descripcion);
		parametros.add(idProveedor);
		parametros.add(idProveedor);

		objDatos.Connect();
		int retorno = objDatos.InsertDeleteUpdate(SQL_UPDATE_PELICULA, parametros);
		objDatos.Disconnect();
		listaPeliculas.clear();
		iniciarPeliculas();
		return retorno;
	}

	/**
	 * funcion que edita los tipos de pelicula que existe en la pelicula
	 * 
	 * @param tiposPelicula          lista de los id de tipos de pelicula a editadas
	 * @param listaTiposEstaPelicula lsita de peliculas tipos que hay que editar
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de insert, delete o update
	 */
	public void editarPeliculaTipos(ArrayList<Integer> tiposPelicula, ArrayList<itfProperty> listaTiposEstaPelicula)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLConsulta, clsErrorSQLInsertDeleteUpdate {
		ArrayList<Object> parametros;

		for (int i = 0; i < listaTiposEstaPelicula.size(); i++) {
			parametros = new ArrayList<Object>();
			parametros.add(listaTiposEstaPelicula.get(i).getProperty(PELICULA_TIPOS_COL_PELICULA_FK));
			parametros.add(tiposPelicula.get(i));
			parametros.add(listaTiposEstaPelicula.get(i).getProperty(PELICULA_TIPOS_COL_PELICULA_FK));
			parametros.add(listaTiposEstaPelicula.get(i).getProperty(PELICULA_TIPOS_COL_TIPO_PELICULA_FK));
			objDatos.Connect();
			objDatos.InsertDeleteUpdate(SQL_UPDATE_PELICULAS_TIPOS, parametros);
			objDatos.Disconnect();
		}
		listaPeliculasTipos.clear();
		iniciarPeliculasTipos();
	}

	/**
	 * funcion que edita los parmetros de un proveedor
	 * 
	 * @param nombre      nombre del proveedor
	 * @param email       email del proveedor
	 * @param telefono    telefono del proveedor
	 * @param idProveedor id del proveedor a editar
	 * @return cantidad de filas afectadas
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLInsertDeleteUpdate excepciond de la insert, delete o
	 *                                       update
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 */
	public int editarProveedor(String nombre, String email, String telefono, int idProveedor)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {
		ArrayList<Object> parametros = new ArrayList<Object>();
		parametros.add(nombre);
		parametros.add(email);
		parametros.add(telefono);
		parametros.add(idProveedor);
		parametros.add(idProveedor);
		objDatos.Connect();
		int resultado = objDatos.InsertDeleteUpdate(SQL_UPDATE_PROVEEDOR, parametros);
		objDatos.Disconnect();

		listaProveedores.clear();
		iniciarProveedores();

		return resultado;
	}

	/**
	 * funcion que edita un ticket de la base de datos
	 * 
	 * @param socio         socio del ticket a editar
	 * @param pelicula      pelicula del ticekt a editar
	 * @param fecha         fecha del ticekt a editar
	 * @param socioNuevo    socio nuevo del ticket
	 * @param peliculaNueva pelicula nueva del ticket
	 * @param fechaNueva    fecha nueva del ticket
	 * @return cantidad de filas afectadas
	 * @throws clsErrorSQLConexion           excepcion de la conexion
	 * @throws clsErrorSQLInsertDeleteUpdate excepciond de la insert, delete o
	 *                                       update
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLConsulta           excepcion de la consulta
	 */
	public int editarTicket(int socio, int pelicula, String fecha, int socioNuevo, int peliculaNueva, String fechaNueva)
			throws clsErrorSQLConexion, clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate, clsErrorSQLConsulta {
		ArrayList<Object> parametros = new ArrayList<Object>();
		LocalDate fechaNuevaDate = transformarFecha(fechaNueva);
		LocalDate fechaDate = transformarFecha(fecha);
		parametros.add(socioNuevo);
		parametros.add(peliculaNueva);
		parametros.add(fechaNuevaDate);
		parametros.add(socio);
		parametros.add(pelicula);
		parametros.add(fechaDate);

		objDatos.Connect();
		int resultado = objDatos.InsertDeleteUpdate(SQL_UPDATE_TICKET, parametros);
		objDatos.Disconnect();

		listaTickets.clear();
		iniciarTickets();

		return resultado;
	}
	// -------------------OTRAS FUNCIONES---------------//

	/**
	 * funcion que busca si el usuario es empleado o socio
	 * 
	 * @param usuario usuario conectado
	 * @return tipo de usuario 1 empleado 0 socio
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */
	public boolean tipoUsuario(itfProperty usuario) throws clsErrorSQLConsulta, clsErrorSQLConexion {
		if (mUsuarios.isEmpty()) {
			iniciarUsuarios();
		}
		Object usuarioObject = mUsuarios.get(usuario.getProperty(USUARIO_COL_USER_NAME));
		if (usuarioObject instanceof clsEmpleado) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Funcion que transforma un string en LocalDate
	 * 
	 * @param fecha String de entrada
	 * @return LocalDate de salida
	 */
	private LocalDate transformarFecha(String fecha) {
		LocalDate fechaLocalDate = java.time.LocalDate.parse(fecha, DateTimeFormatter.ofPattern(PATRON_FECHA));
		return fechaLocalDate;
	}

	/**
	 * Funcion que convierte los tinyint de BBDD a booleanos declarados en las
	 * clases de los objetos.
	 * 
	 * @param numero int entrante
	 * @return boolean boleano de salida
	 */
	private boolean convertirIntBoolean(int numero) {
		if (numero == 1) {
			return true;
		}
		return false;
	}

}
