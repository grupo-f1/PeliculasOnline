package LN;

import java.time.LocalDate;

/**
 * Clase abstracta de usuario con atributos en comun para socio y empleado.
 * enlace a clase hijo {@link clsEmpleado empleado} y enlace a clase hijo
 * {@link clsSocio socio}
 * 
 * @author Eric
 *
 */
public abstract class clsUsuario {

	/**
	 * Atributo que indica el nombre de usuario
	 */
	private String user_name;
	/**
	 * Atributo que indica la contraseña del usuario
	 */
	private String password;
	/**
	 * Atributo que indica nombre del usuario
	 */
	private String nombre;
	/**
	 * Atributo que indica el apellido del usuario
	 */
	private String apellidos;
	/**
	 * Atributo que indica la fecha nacimiento del usuario
	 */
	private LocalDate fecha_nacimiento;
	/**
	 * Atributo que indica el DNI del usuario
	 */
	private String dni;
	/**
	 * Atributo que indica el telefono del usuario
	 */
	private String telefono;
	/**
	 * Atributo que indica el email del usuario
	 */
	private String email;
	/**
	 * Atributo que indica la fecha de creacion del usuario
	 */
	private LocalDate create_time;

	/**
	 * Constructor del Objeto usuario con todos los parámetros.
	 * 
	 * @param user_name        nombre de usuario en la aplicacion
	 * @param password         contrase�a del usuario
	 * @param nombre           nombre del usuario
	 * @param apellidos        apellido del usuario
	 * @param fecha_nacimiento fecha nacimiento del usuario
	 * @param dni              dni del usuario
	 * @param telefono         telefono del usuario
	 * @param email            email del usuario
	 * @param create_time      fecha de creacion del usuario
	 */
	public clsUsuario(String user_name, String password, String nombre, String apellidos, LocalDate fecha_nacimiento,
			String dni, String telefono, String email, LocalDate create_time) {
		super();
		this.user_name = user_name;
		this.password = password;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fecha_nacimiento = fecha_nacimiento;
		this.dni = dni;
		this.telefono = telefono;
		this.email = email;
		this.create_time = create_time;
	}

	/**
	 * Constructor del objeto usuario sin parametros
	 */
	public clsUsuario() {
		super();
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getdni() {
		return dni;
	}

	public void setdni(String dni) {
		this.dni = dni;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getCreate_time() {
		return create_time;
	}

	public void setCreate_time(LocalDate create_time) {
		this.create_time = create_time;
	}

	@Override
	public String toString() {
		return "y los datos de usuario son: nombre de usuario=" + user_name + ", contrase�a=" + password + ", nombre="
				+ nombre + ", apellidos=" + apellidos + ", fecha nacimiento=" + fecha_nacimiento + ", dni=" + dni
				+ ", telefono=" + telefono + ", email=" + email + ", fecha de creacion de cuenta=" + create_time;
	}

}
