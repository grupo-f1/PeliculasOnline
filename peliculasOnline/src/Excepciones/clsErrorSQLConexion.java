package Excepciones;

/**
 * Excepcion cuando la conexion con la base de datos no funciona
 * 
 * @author Eric
 *
 */
public class clsErrorSQLConexion extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1480632965610337200L;
	/**
	 * mensaje de error
	 */
	private Exception e;

	/**
	 * constructor de clsErrorSQLConexion
	 * 
	 * @param e mensaje de error
	 * 
	 */
	public clsErrorSQLConexion(Exception e) {
		this.e = e;
	}

	@Override
	public String getMessage() {
		return "NO CONNECTION " + "\n" + e;
	}

}
