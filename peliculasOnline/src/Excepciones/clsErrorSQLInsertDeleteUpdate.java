package Excepciones;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * excepcion cuando la sentencia insert delete o update estan mal
 * 
 * @author Eric
 *
 */
public class clsErrorSQLInsertDeleteUpdate extends SQLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7070435238809393046L;
	/**
	 * sentencia constante creada
	 */
	private String insert;
	/**
	 * sentencia con los parametros introducidos
	 */
	private PreparedStatement ps;

	/**
	 * constructor de la excepcion clsErrorSQLInsertDeleteUpdate
	 * 
	 * @param insert sentencia constante creada
	 * @param ps     sentencia con los parametros introducidos
	 * 
	 */
	public clsErrorSQLInsertDeleteUpdate(String insert, PreparedStatement ps) {
		this.insert = insert;
		this.ps = ps;
	}

	@Override
	public String getMessage() {
		return "Error al introducir borrar o editar una tabla en la base de datos: " + insert
				+ "\nError al introducir borrar o editar una tabla en la base de datos: " + ps.toString();
	}

}
