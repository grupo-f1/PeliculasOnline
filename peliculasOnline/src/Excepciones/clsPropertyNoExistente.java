package Excepciones;

/**
 * Excepcion implicita que se lanza cunado se solicita una propiedad en
 * itfProperty y no tenga un destino
 * 
 * @author Eric
 *
 */
public class clsPropertyNoExistente extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7720934236289890897L;
	/**
	 * propiedad no incluida en el objeto
	 */
	private String propiedad;

	/**
	 * constructor de la excepcion cuando un itfProperty no encuentra un parametro
	 * 
	 * @param propiedad parametro a encotrar erroneo
	 */
	public clsPropertyNoExistente(String propiedad) {
		this.propiedad = propiedad;
	}

	@Override
	public String getMessage() {
		return "la propiedad " + this.propiedad + " no existe.";
	}
}
