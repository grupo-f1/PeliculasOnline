package Excepciones;

import java.sql.SQLException;

/**
 * Excepcion para el mensaje de sintaxis SQL mal escrita
 * 
 * @author Eric
 *
 */
public class clsErrorSQLConsulta extends SQLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2752743898191664084L;
	/**
	 * sentencia sql
	 */
	private String sql;
	/**
	 * tipo de error
	 */
	private SQLException e;

	/**
	 * constructor de la excepcion cuando una consulta sql salga mal
	 * 
	 * @param sql sentencia sql
	 * @param e   error
	 */
	public clsErrorSQLConsulta(String sql, SQLException e) {
		this.sql = sql;
		this.e = e;
	}

	@Override
	public String getMessage() {
		return "Error en la recuperacion de datos para SQL= " + sql + "\n " + e;
	}

}
