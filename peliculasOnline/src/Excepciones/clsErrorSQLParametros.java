package Excepciones;

import java.sql.SQLException;

/**
 * Excepcion cunado un parametro para la sentencia SQL esta mal
 * 
 * @author Eric
 *
 */
public class clsErrorSQLParametros extends SQLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3481093880418057368L;
	/**
	 * posicion del parametro en la sentencia
	 */
	private int cont;
	/**
	 * error
	 */
	private Exception e;

	/**
	 * constructor de la excepcion cuando los parametros en la sentencia SQL estan
	 * mal
	 * 
	 * @param cont localizacion del parametro en la sentencia
	 * @param e    error
	 */
	public clsErrorSQLParametros(int cont, Exception e) {
		this.cont = cont;
		this.e = e;
	}

	@Override
	public String getMessage() {
		return "Error en la introduccion del parametro: " + cont + " " + e.toString();
	}

}
