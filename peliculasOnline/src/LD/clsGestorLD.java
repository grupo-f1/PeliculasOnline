package LD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import COMUN.itfData;
import Excepciones.clsErrorSQLConexion;
import Excepciones.clsErrorSQLConsulta;
import Excepciones.clsErrorSQLInsertDeleteUpdate;
import Excepciones.clsErrorSQLParametros;

//Import de sentencias SQL para acceder a los datos.
//import static COMUN.clsConstantesDB.SQL_INSERT_ALUMNO;

/**
 * Esta clase es la encargada de acceder a la base de datos y de realizar
 * consultas. Fundamentalmente realiza las operaciones de INSERT, SELECT, UPDATE
 * y DELETE de cualquier tipo de datos.
 * 
 * @author Eric
 *
 */
public class clsGestorLD {

	private static final String URL = "jdbc:mysql://localhost:3306/";
	private static final String SCHEMA = "peliculasonline";
	private static final String PARAMS = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String USER = "root";
	private static final String PASS = "root";

	/**
	 * Objeto para crear la conexi�n a base de datos.
	 */
	Connection conn = null;

	/**
	 * Objeto para crear la consulta a base de datos.
	 */
	PreparedStatement ps = null;

	/**
	 * Objeto para devolver el resultado de la consulta.
	 */
	ResultSet rs = null;

	/**
	 * Constructor sin par�metros de la clase.
	 */
	public clsGestorLD() {

	}

	/**
	 * funcion para la conexion a la base de datos.
	 * 
	 * @throws clsErrorSQLConexion excepcion de la conexion
	 */
	public void Connect() throws clsErrorSQLConexion {
		// Se carga el driver de acceso a datos
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL + SCHEMA + PARAMS, USER, PASS);
			// System.out.println("Connected to the database");
		} catch (ClassNotFoundException | SQLException e) {
			throw new clsErrorSQLConexion(e);
		}
	}

	/**
	 * funcion para desconecar de la base de datos
	 */
	public void Disconnect() {

		try {
			conn.close();
			ps.close(); // cerrar el statement tb cierra el resultset.
		} catch (SQLException e) {

		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				/* no hago nada */}
			try {
				ps.close();
			} catch (Exception e) {
				/* no hago nada */}
		}

	}

//	 /**
//	  * Metodo que lanza una cosulta contra la base de datos.
//	  * @param sql Query a lanzar contra la base de datos
//	  * @param parametros parametros para la consulta. Si no hay paramtros, su valor sera null
//	  * @return ArrayList de HashMap en el que cada posicion es un registro de la tabla y cada columna est� contenida
//	  * 		HashMap. El HashMap contiene como clave la posicion de la columna y como Object el valor de la columna.
//	  */
//	 public ArrayList<HashMap<Integer,Object>> Consulta(String sql, ArrayList<Object>parametros)
//	 {
//			 
//			ArrayList<HashMap<Integer,Object>>resultado;
//			resultado=new ArrayList <HashMap<Integer,Object>>();
//			
//			try 
//			{
//				ps = conn.prepareStatement(sql);
//				
//				if(parametros !=null)
//				{
//					this.CompletarParametrosQuery(parametros);
//				}
//				
//				rs=ps.executeQuery(sql);
//				ResultSetMetaData rsmd = rs.getMetaData();
//				
//				while(rs.next())
//				{
//					HashMap<Integer, Object> fila = new HashMap<Integer, Object>();
//					for(int i=1; i<=rsmd.getColumnCount();i++)
//					{
//						
//						fila.put(i, rs.getObject(i));
//					}
//					resultado.add(fila);
//								
//				}
//				
//				
//			} 
//			catch (SQLException e) 
//			{
//				System.out.println("Error en la recuperaci�n de datos para SQL= " + sql + " " + e);
//			}
//			
//			
//			return resultado; 
//			
//	 }

	/**
	 * Metodo que lanza una cosulta contra la base de datos.
	 * 
	 * @param sql        Query a lanzar contra la base de datos
	 * @param parametros parametros para la consulta. Si no hay paramtros, su valor
	 *                   sera null
	 * @return ArrayList de HashMap en el que cada posicion es un registro de la
	 *         tabla y cada columna est� contenida HashMap. El HashMap contiene
	 *         como clave la posicion de la columna y como Object el valor de la
	 *         columna.
	 *
	 * @throws clsErrorSQLConsulta excepcion de la consulta
	 */

	public ArrayList<itfData> Consulta(String sql, ArrayList<Object> parametros) throws clsErrorSQLConsulta {

		ArrayList<itfData> resultado;
		resultado = new ArrayList<itfData>();

		try {
			ps = conn.prepareStatement(sql);

			if (parametros != null) {
				this.CompletarParametrosQuery(parametros);
			}
			// respuesta de la bbdd
			rs = ps.executeQuery();
			// meta datos del resultado, como la cantidad de columnas
			ResultSetMetaData rsmd = rs.getMetaData();
			// transformar la respuesta para la logica de negocio
			while (rs.next()) {
				clsFila fila = new clsFila();
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {

					fila.ponerColumna(i, rs.getObject(i));
				}
				resultado.add(fila);

			}

		} catch (SQLException e) {
			throw new clsErrorSQLConsulta(sql, e);
		}

		return resultado;

	}

	/*
	 * private ResultSet sendSelect(String sql) {
	 * 
	 * try { ps = conn.prepareStatement(sql); rs = ps.executeQuery(sql); } catch
	 * (SQLException e) {
	 * System.out.println("Error en la recuperacion de datos para SQL= " + sql + " "
	 * + e); }
	 * 
	 * return rs;
	 * 
	 * }
	 */
	/**
	 * Este metodo completa el prepareStament pasandole los parametros de la
	 * consulta. Hace una traduccion del tipo de parametro recibido y lo introduce
	 * en la query. No recibe el parametro PreparedStatement porque usa el objeto
	 * PreparedStatement atributo de la clase.
	 * 
	 * @see #ps
	 * @param parametros Lista de objetos Object que contiene los valores de los
	 *                   parametros a introducir en la query en el orden en el que
	 *                   se deben introducir, segun la query a ejecutar.
	 * @throws clsErrorSQLParametros excepcion de los parametros
	 */
	private void CompletarParametrosQuery(ArrayList<Object> parametros) throws clsErrorSQLParametros {
		int cont = 1;
		for (Object param : parametros) {
			try {
				if (param instanceof String) {
					ps.setString(cont, (String) param);
					cont++;
					// volver a iniciar el bucle
					continue;
				}

				if (param instanceof Integer) {
					ps.setInt(cont, (Integer) param);
					cont++;
					continue;
				}

				if (param instanceof java.time.LocalDate) {
//			    			java.sql.Date fecha=(java.sql.Date)param;
					java.sql.Date fecha = java.sql.Date.valueOf((java.time.LocalDate) param);
					ps.setDate(cont, (java.sql.Date) fecha);
					cont++;
					continue;
				}

				if (param instanceof Float) {
					ps.setFloat(cont, (Float) param);
					cont++;
					continue;
				}

				if (param instanceof Double) {
					ps.setDouble(cont, (Double) param);
					cont++;
					continue;
				}

				if (param instanceof Boolean) {
					ps.setBoolean(cont, (Boolean) param);
					cont++;
					continue;
				}

				if (param instanceof Character) {
					ps.setString(cont, ((Character) param).toString());
					cont++;
					continue;
				}
				if (param == null) {
					ps.setString(cont, null);
					cont++;
					continue;
				}
			} catch (Exception e) {
				throw new clsErrorSQLParametros(cont, e);
			}

		}

	}

	/**
	 * Metodo que ejecuta una query de insercion
	 * 
	 * @param insert     Query a ejecutar
	 * @param parametros parametros de la query en el orden el que se solicitan. Si
	 *                   es null es que la query a ejecutar no contiene parametros.
	 * @return id autonumerico devuelto por la base de datos del registro insertado.
	 * @throws clsErrorSQLParametros         excepcion de los parametros
	 * @throws clsErrorSQLInsertDeleteUpdate excepcion de la insert, delete o update
	 */
	public int InsertDeleteUpdate(String insert, ArrayList<Object> parametros)
			throws clsErrorSQLParametros, clsErrorSQLInsertDeleteUpdate {
		int retorno = 0;
		int regActualizados = 0;

		try {
			ps = conn.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS);

			if (parametros != null)
				this.CompletarParametrosQuery(parametros);

			regActualizados = ps.executeUpdate();
			retorno = regActualizados;
			if (regActualizados == 1) {
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					retorno = rs.getInt(1);
				}
			}

		} catch (SQLException e) {
			retorno = -1;
			throw new clsErrorSQLInsertDeleteUpdate(insert, ps);
		}

		return retorno;

	}
}
