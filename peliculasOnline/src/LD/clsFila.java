package LD;

import java.util.HashMap;

import COMUN.itfData;

/**
 * Objeto que representa a una fila de una tabla de una base de datos.
 * 
 * @author javier.cerro
 *
 */
public class clsFila implements itfData {

	private HashMap<Integer, Object> fila;

	public clsFila() {
		fila = new HashMap<Integer, Object>();
	}

	public void ponerColumna(Integer columna, Object valor) {
		fila.put(columna, valor);
	}

	/**
	 * Recuperar la informacion para la logica de negocio
	 */
	public Object getData(Integer columna) {

		return this.fila.get(columna);
	}

}
