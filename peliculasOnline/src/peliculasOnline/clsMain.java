package peliculasOnline;

import LP.frmLogin;

/**
 * Clase principal donde empezara a funcionar nuestro programa
 * 
 * @author Eric
 *
 */
public class clsMain {
	/**
	 * Clase principal donde se comienza el programa
	 * 
	 * @param args parametro de entrada
	 */
	public static void main(String[] args) {

		frmLogin objLogin = new frmLogin();
		objLogin.setVisible(true);
	}

}
